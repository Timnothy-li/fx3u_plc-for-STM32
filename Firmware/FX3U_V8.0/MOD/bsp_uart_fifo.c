/*
*********************************************************************************************************
*
*	模块名称 : 串口中断+FIFO驱动模块
*	文件名称 : bsp_uart_fifo.c
*	版    本 : V1.0
*	说    明 : 采用串口中断+FIFO模式实现多个串口的同时访问
*	修改记录 :
*		版本号  日期       作者    说明
*		V1.0    正式发布
*		V1.1   FiFo结构增加TxCount成员变量，方便判断缓冲区满; 增加 清FiFo的函数
*		V1.2	增加RS485 MODBUS接口。接收到新字节后，直接执行回调函数。
*
*	Copyright (C), 
*
*********************************************************************************************************
*/

#include "bsp.h"


/* 定义每个串口结构体变量 */
#if UART1_FIFO_EN == 1
	static UART_T g_tUart1;
	static uint8_t g_TxBuf1[UART1_TX_BUF_SIZE];		/* 发送缓冲区 */
	static uint8_t g_RxBuf1[UART1_RX_BUF_SIZE];		/* 接收缓冲区 */
#endif

#if UART2_FIFO_EN == 1
	static UART_T g_tUart2;
	static uint8_t g_TxBuf2[UART2_TX_BUF_SIZE];		/* 发送缓冲区 */
	static uint8_t g_RxBuf2[UART2_RX_BUF_SIZE];		/* 接收缓冲区 */
#endif

#if UART3_FIFO_EN == 1
	static UART_T g_tUart3;
	static uint8_t g_TxBuf3[UART3_TX_BUF_SIZE];		/* 发送缓冲区 */
	static uint8_t g_RxBuf3[UART3_RX_BUF_SIZE];		/* 接收缓冲区 */
#endif

#if UART4_FIFO_EN == 1
	static UART_T g_tUart4;
	static uint8_t g_TxBuf4[UART4_TX_BUF_SIZE];		/* 发送缓冲区 */
	static uint8_t g_RxBuf4[UART4_RX_BUF_SIZE];		/* 接收缓冲区 */
#endif

#if UART5_FIFO_EN == 1
	static UART_T g_tUart5;
	static uint8_t g_TxBuf5[UART5_TX_BUF_SIZE];		/* 发送缓冲区 */
	static uint8_t g_RxBuf5[UART5_RX_BUF_SIZE];		/* 接收缓冲区 */
#endif

static void UartVarInit(void);

static void InitHardUart(void);
static void UartSend(UART_T *pUart, uint8_t *_ucaBuf, uint16_t _usLen);
static uint8_t UartGetChar(UART_T *pUart, uint8_t *_pByte);
static void UartIRQ(UART_T *pUart);
static void ConfigUartNVIC(void);

void RS485_InitTXE(void);

/*
*********************************************************************************************************
*	函 数 名: bsp_InitUart
*	功能说明: 初始化串口硬件，并对全局变量赋初值.
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void bsp_InitUart(void)
{
	UartVarInit();		/* 必须先初始化全局变量,再配置硬件 */

	InitHardUart();		/* 配置串口的硬件参数(波特率等) */

	RS485_InitTXE();	/* 配置RS485芯片的发送使能硬件，配置为推挽输出 */

	ConfigUartNVIC();	/* 配置串口中断 */
}

/*
*********************************************************************************************************
*	函 数 名: ComToUart
*	功能说明: 将COM端口号转换为UART指针
*	形    参: _com: 端口号(COM1 - COM6)
*	返 回 值: uart指针
*********************************************************************************************************
*/
UART_T *ComToUart(uint8_t _com)
{
	if (_com == COM1)
	{
		#if UART1_FIFO_EN == 1
			return &g_tUart1;
		#else
			return 0;
		#endif
	}
	else if (_com == COM2)
	{
		#if UART2_FIFO_EN == 1
			return &g_tUart2;
		#else
			return 0;
		#endif
	}
	else if (_com == COM3)
	{
		#if UART3_FIFO_EN == 1
			return &g_tUart3;
		#else
			return 0;
		#endif
	}
	else if (_com == COM4)
	{
		#if UART4_FIFO_EN == 1
			return &g_tUart4;
		#else
			return 0;
		#endif
	}
	else if (_com == COM5)
	{
		#if UART5_FIFO_EN == 1
			return &g_tUart5;
		#else
			return 0;
		#endif
	}
	else
	{
		/* 不做任何处理 */
		return 0;
	}
}

/*
*********************************************************************************************************
*	函 数 名: comSendBuf
*	功能说明: 向串口发送一组数据。数据放到发送缓冲区后立即返回，由中断服务程序在后台完成发送
*	形    参: _ucPort: 端口号(COM1 - COM6)
*			  _ucaBuf: 待发送的数据缓冲区
*			  _usLen : 数据长度
*	返 回 值: 无
*********************************************************************************************************
*/
void comSendBuf(uint8_t _com, uint8_t *_ucaBuf, uint16_t _usLen)
{
	UART_T *pUart;
	
	pUart = ComToUart(_com);
	if (pUart == 0)
	{
		return;
	}

	if (pUart->SendBefor != 0)
	{
		pUart->SendBefor(_com);		/* 如果是RS485通信，可以在这个函数中将RS485设置为发送模式 */
	}

	UartSend(pUart, _ucaBuf, _usLen);
}

/*
*********************************************************************************************************
*	函 数 名: comSendChar
*	功能说明: 向串口发送1个字节。数据放到发送缓冲区后立即返回，由中断服务程序在后台完成发送
*	形    参: _ucPort: 端口号(COM1 - COM6)
*			  _ucByte: 待发送的数据
*	返 回 值: 无
*********************************************************************************************************
*/
void comSendChar(uint8_t _com, uint8_t _ucByte)
{
	comSendBuf(_com, &_ucByte, 1);
}

/*
*********************************************************************************************************
*	函 数 名: comGetChar
*	功能说明: 从串口缓冲区读取1字节，非阻塞。无论有无数据均立即返回
*	形    参: _ucPort: 端口号(COM1 - COM6)
*			  _pByte: 接收到的数据存放在这个地址
*	返 回 值: 0 表示无数据, 1 表示读取到有效字节
*********************************************************************************************************
*/
uint8_t comGetChar(uint8_t _com, uint8_t *_pByte)
{
	UART_T *pUart;

	pUart = ComToUart(_com);
	if (pUart == 0)
	{
		return 0;
	}

	return UartGetChar(pUart, _pByte);
}

/*
*********************************************************************************************************
*	函 数 名: comClearTxFifo
*	功能说明: 清零串口发送缓冲区
*	形    参: _ucPort: 端口号(COM1 - COM6)
*	返 回 值: 无
*********************************************************************************************************
*/
void comClearTxFifo(uint8_t _com)
{
	UART_T *pUart;

	pUart = ComToUart(_com);
	if (pUart == 0)
	{
		return;
	}

	pUart->txWrite = 0;
	pUart->txRead = 0;
	pUart->txCount = 0;
}

/*
*********************************************************************************************************
*	函 数 名: comClearRxFifo
*	功能说明: 清零串口接收缓冲区
*	形    参: _ucPort: 端口号(COM1 - COM6)
*	返 回 值: 无
*********************************************************************************************************
*/
void comClearRxFifo(uint8_t _com)
{
	UART_T *pUart;

	pUart = ComToUart(_com);
	if (pUart == 0)
	{
		return;
	}

	pUart->rxWrite = 0;
	pUart->rxRead = 0;
	pUart->rxCount = 0;
}

/*
*********************************************************************************************************
*	函 数 名: bsp_SetUart1Baud
*	功能说明: 修改UART1、UART2、USART3...波特率
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
void bsp_SetUartBaud(USART_TypeDef* USARTx,uint32_t _baud)
{
	USART_InitTypeDef USART_InitStructure;

	/* 第2步： 配置串口硬件参数 */
	USART_InitStructure.USART_BaudRate = _baud;	/* 波特率 */
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USARTx, &USART_InitStructure);
	
}


/* 如果是RS485通信，请按如下格式编写函数， 我们仅举了 USART3作为RS485的例子 */

/*
*********************************************************************************************************
*	函 数 名: RS485_InitTXE
*	功能说明: 配置RS485发送使能口线 TXE
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
void RS485_InitTXE(void)
{
//	GPIO_InitTypeDef GPIO_InitStructure;

//	RCC_APB2PeriphClockCmd(HS_MOD1_RCC_RS485_EN, ENABLE);	/* 打开GPIO时钟 */

//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;	/* 推挽输出模式 */
//	GPIO_InitStructure.GPIO_Pin = HS_MOD1_PIN_RS485_EN;
//	GPIO_Init(HS_MOD1_PORT_RS485_EN, &GPIO_InitStructure);
	
//	RCC_APB2PeriphClockCmd(HS_MOD2_RCC_RS485_EN, ENABLE);	/* 打开GPIO时钟 */
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;	/* 推挽输出模式 */
//	GPIO_InitStructure.GPIO_Pin = HS_MOD2_PIN_RS485_EN;
//	GPIO_Init(HS_MOD2_PORT_RS485_EN, &GPIO_InitStructure);
	
}


/*
*********************************************************************************************************
*	函 数 名: RS485_SendBefor
*	功能说明: 发送数据前的准备工作。对于RS485通信，请设置RS485芯片为发送状态，
*			  并修改 UartVarInit()中的函数指针等于本函数名，比如 g_tUart3.SendBefor = RS485_SendBefor
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
void RS485_SendBefor(uint8_t _com)
{
		 // 根据COM获的CH和模式
	 uint8_t ch;
	 for(ch=0;ch<2;ch++)
	 {
	    if(config[ch][0] == _com)
				break;
	 }
	
//	if(ch == 0)              // 第1路
//		HS_MOD1_RS485_TX_EN(); // 切换RS485收发芯片为发送模式
//	else if(ch == 1)         // 第2路
//		HS_MOD2_RS485_TX_EN(); // 切换RS485收发芯片为发送模式
}

/*
*********************************************************************************************************
*	函 数 名: RS485_SendOver
*	功能说明: 发送一串数据结束后的善后处理。对于RS485通信，请设置RS485芯片为接收状态，
*			  并修改 UartVarInit()中的函数指针等于本函数名，比如 g_tUart2.SendOver = RS485_SendOver
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
void RS485_SendOver(uint8_t _com)
{
	 // 根据COM获的CH和模式
	 uint8_t ch;
	 for(ch=0;ch<2;ch++)
	 {
	    if(config[ch][0] == _com)
				break;
	 }
	
//	if(ch == 0)              // 第1路
//		HS_MOD1_RS485_RX_EN(); // 切换RS485收发芯片为接收模式
//	else if(ch == 1)         // 第2路
//		HS_MOD2_RS485_RX_EN(); // 切换RS485收发芯片为接收模式
}


/*
*********************************************************************************************************
*	函 数 名: RS485_SendBuf
*	功能说明: 通过RS485芯片发送一串数据。注意，本函数不等待发送完毕。
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
void RS485_SendBuf(uint8_t _com,uint8_t *_ucaBuf, uint16_t _usLen)
{
	comSendBuf(_com, _ucaBuf, _usLen);
}


/*
*********************************************************************************************************
*	函 数 名: RS485_SendStr
*	功能说明: 向485总线发送一个字符串
*	形    参: _pBuf 数据缓冲区
*			 _ucLen 数据长度
*	返 回 值: 无
*********************************************************************************************************
*/
void RS485_SendStr(uint8_t _com,char *_pBuf)
{
	RS485_SendBuf(_com,(uint8_t *)_pBuf, strlen(_pBuf));
}

/*
*********************************************************************************************************
*	函 数 名: RS485_ReciveNew
*	功能说明: 接收到新的数据
*	形    参: _byte 接收到的新数据
*	返 回 值: 无
*********************************************************************************************************
*/
extern void MODS_ReciveNew(uint8_t _ch,uint16_t _baud,uint8_t _byte); // 从机模式接收处理
extern void MODH_ReciveNew(uint8_t _ch,uint16_t _baud,uint8_t _data); // 主机模式接收处理


void RS485_ReciveNew(uint8_t _com,uint16_t _baud,uint8_t _byte)
{
	 // 根据COM获的CH和模式
	 uint8_t ch;
	 for(ch=0;ch<2;ch++)
	 {
	    if(config[ch][0] == _com)
				break;
	 }
	 
	if(config[ch][1] == 0)      // 从机模式
		MODS_ReciveNew(ch,_baud,_byte);
	else if(config[ch][1] == 1) // 主机模式                 
		MODH_ReciveNew(ch,_baud,_byte);

}

/*
*********************************************************************************************************
*	函 数 名: UartVarInit
*	功能说明: 初始化串口相关的变量
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void UartVarInit(void)
{
#if UART1_FIFO_EN == 1
	g_tUart1.uart = USART1;						// STM32 串口设备
	g_tUart1.com  = COM1;             // 端口号
	g_tUart1.baud = UART1_BAUD;       // 波特率
	
	g_tUart1.pTxBuf = g_TxBuf1;				// 发送缓冲区指针
	g_tUart1.pRxBuf = g_RxBuf1;			  // 接收缓冲区指针 
	g_tUart1.txBufSize = UART1_TX_BUF_SIZE;	// 发送缓冲区大小 
	g_tUart1.usRxBufSize = UART1_RX_BUF_SIZE;	// 接收缓冲区大小
	g_tUart1.txWrite = 0;						  // 发送FIFO写索引
	g_tUart1.txRead = 0;						  // 发送FIFO读索引 
	g_tUart1.rxWrite = 0;						// 接收FIFO写索引
	g_tUart1.rxRead = 0;						// 接收FIFO读索引 
	g_tUart1.rxCount = 0;						// 接收到的新数据个数 
	g_tUart1.txCount = 0;						  // 待发送的数据个数
	g_tUart1.SendBefor = 0;						// 发送数据前的回调函数
	g_tUart1.SendOver = 0;						// 发送完毕后的回调函数 
	g_tUart1.ReciveNew = 0;						// 接收到新数据后的回调函数 
#endif

#if UART2_FIFO_EN == 1
	g_tUart2.uart = USART2;						// STM32 串口设备 
	g_tUart2.com  = COM2;             // 端口号
	g_tUart2.baud = UART2_BAUD;       // 波特率
	
	g_tUart2.pTxBuf = g_TxBuf2;				// 发送缓冲区指针 
	g_tUart2.pRxBuf = g_RxBuf2;				// 接收缓冲区指针
	g_tUart2.txBufSize = UART2_TX_BUF_SIZE;	// 发送缓冲区大小 
	g_tUart2.rxBufSize = UART2_RX_BUF_SIZE;	// 接收缓冲区大小 
	g_tUart2.txWrite = 0;						  // 发送FIFO写索引
	g_tUart2.txRead = 0;						  // 发送FIFO读索引 
	g_tUart2.rxWrite = 0;						// 接收FIFO写索引 
	g_tUart2.rxRead = 0;						// 接收FIFO读索引 
	g_tUart2.rxCount = 0;						// 接收到的新数据个数 
	g_tUart2.txCount = 0;						  // 待发送的数据个数 
//	g_tUart2.SendBefor = 0;						// 发送数据前的回调函数
//	g_tUart2.SendOver = 0;						// 发送完毕后的回调函数 
//	g_tUart2.ReciveNew = 0;						// 接收到新数据后的回调函数 
	
  g_tUart2.SendBefor = RS485_SendBefor;	// 发送数据前的回调函数
	g_tUart2.SendOver = RS485_SendOver;		// 发送完毕后的回调函数
	g_tUart2.ReciveNew = RS485_ReciveNew;	// 接收到新数据后的回调函数
#endif

#if UART3_FIFO_EN == 1
	g_tUart3.uart = USART3;						// STM32 串口设备 
	g_tUart3.com  = COM3;             // 端口号
	g_tUart3.baud = UART3_BAUD;       // 波特率
	
	g_tUart3.pTxBuf = g_TxBuf3;			  // 发送缓冲区指针 
	g_tUart3.pRxBuf = g_RxBuf3;				// 接收缓冲区指针 
	g_tUart3.txBufSize = UART3_TX_BUF_SIZE;	// 发送缓冲区大小 
	g_tUart3.rxBufSize = UART3_RX_BUF_SIZE;	//接收缓冲区大小 
	g_tUart3.txWrite = 0;						  // 发送FIFO写索引 
	g_tUart3.txRead = 0;						  // 发送FIFO读索引 
	g_tUart3.rxWrite = 0;						// 接收FIFO写索引 
	g_tUart3.rxRead = 0;						// 接收FIFO读索引 
	g_tUart3.rxCount = 0;						// 接收到的新数据个数 
	g_tUart3.txCount = 0;						  // 待发送的数据个数 
//	g_tUart3.SendBefor = 0;		        // 发送数据前的回调函数 
//	g_tUart3.SendOver = 0;			      // 发送完毕后的回调函数 
//	g_tUart3.ReciveNew = 0 ;		        // 接收到新数据后的回调函数 
	
	g_tUart3.SendBefor = RS485_SendBefor;	// 发送数据前的回调函数
	g_tUart3.SendOver = RS485_SendOver;		// 发送完毕后的回调函数
	g_tUart3.ReciveNew = RS485_ReciveNew;	// 接收到新数据后的回调函数
#endif

#if UART4_FIFO_EN == 1
	g_tUart4.uart = UART4;						// STM32 串口设备
	g_tUart4.com  = COM4;             // 端口号
	g_tUart4.baud = UART4_BAUD;       // 波特率
	
	g_tUart4.pTxBuf = g_TxBuf4;					// 发送缓冲区指针
	g_tUart4.pRxBuf = g_RxBuf4;					// 接收缓冲区指针
	g_tUart4.txBufSize = UART4_TX_BUF_SIZE;	// 发送缓冲区大小
	g_tUart4.rxBufSize = UART4_RX_BUF_SIZE;	// 接收缓冲区大小 
	g_tUart4.txWrite = 0;						  // 发送FIFO写索引 
	g_tUart4.txRead = 0;						  // 发送FIFO读索引 
	g_tUart4.rxWrite = 0;						// 接收FIFO写索引
	g_tUart4.rxRead = 0;						// 接收FIFO读索引
	g_tUart4.rxCount = 0;						// 接收到的新数据个数
	g_tUart4.txCount = 0;						  // 待发送的数据个数
//	g_tUart4.SendBefor = 0;						// 发送数据前的回调函数
//	g_tUart4.SendOver = 0;						// 发送完毕后的回调函数
//	g_tUart4.ReciveNew = 0;						// 接收到新数据后的回调函数

	g_tUart4.SendBefor = RS485_SendBefor;	// 发送数据前的回调函数
	g_tUart4.SendOver = RS485_SendOver;		// 发送完毕后的回调函数
	g_tUart4.ReciveNew = RS485_ReciveNew;	// 接收到新数据后的回调函数
#endif

#if UART5_FIFO_EN == 1
	g_tUart5.uart = UART5;						// STM32 串口设备 
	g_tUart5.com  = COM5;             // 端口号
	g_tUart5.baud = UART5_BAUD;       // 波特率
	
	g_tUart5.pTxBuf = g_TxBuf5;				// 发送缓冲区指针
	g_tUart5.pRxBuf = g_RxBuf5;				// 接收缓冲区指针 
	g_tUart5.txBufSize = UART5_TX_BUF_SIZE;	// 发送缓冲区大小 
	g_tUart5.rxBufSize = UART5_RX_BUF_SIZE;	// 接收缓冲区大小
	g_tUart5.txWrite = 0;						  // 发送FIFO写索引 
	g_tUart5.txRead = 0;						  // 发送FIFO读索引 
	g_tUart5.rxWrite = 0;						// 接收FIFO写索引 
	g_tUart5.rxRead = 0;						// 接收FIFO读索引 
	g_tUart5.rxCount = 0;						// 接收到的新数据个数 
	g_tUart5.txCount = 0;						  // 待发送的数据个数 
//	g_tUart5.SendBefor = 0;						// 发送数据前的回调函数 
//	g_tUart5.SendOver = 0;						// 发送完毕后的回调函数 
//	g_tUart5.ReciveNew = 0;						// 接收到新数据后的回调函数 
	
	g_tUart5.SendBefor = RS485_SendBefor;	// 发送数据前的回调函数
	g_tUart5.SendOver = RS485_SendOver;		// 发送完毕后的回调函数
	g_tUart5.ReciveNew = RS485_ReciveNew;	// 接收到新数据后的回调函数
#endif


#if UART6_FIFO_EN == 1
	g_tUart6.uart = USART6;						// STM32 串口设备
  g_tUart6.com  = COM6;             // 端口号
	g_tUart6.baud = UART6_BAUD;       // 波特率
	
	g_tUart6.pTxBuf = g_TxBuf6;				// 发送缓冲区指针
	g_tUart6.pRxBuf = g_RxBuf6;				// 接收缓冲区指针
	g_tUart6.txBufSize = UART6_TX_BUF_SIZE;	// 发送缓冲区大小
	g_tUart6.usRxBufSize = UART6_RX_BUF_SIZE;// 接收缓冲区大小
	g_tUart6.txWrite = 0;						  // 发送FIFO写索引 
	g_tUart6.txRead = 0;						  // 发送FIFO读索引
	g_tUart6.rxWrite = 0;						// 接收FIFO写索引 
	g_tUart6.rxRead = 0;						// 接收FIFO读索引 
	g_tUart6.rxCount = 0;						// 接收到的新数据个数 
	g_tUart6.txCount = 0;						  // 待发送的数据个数 
	g_tUart6.SendBefor = 0;						// 发送数据前的回调函数 
	g_tUart6.SendOver = 0;						// 发送完毕后的回调函数 
	g_tUart6.ReciveNew = 0;						// 接收到新数据后的回调函数 
#endif
}

/*
*********************************************************************************************************
*	函 数 名: InitHardUart
*	功能说明: 配置串口的硬件参数（波特率，数据位，停止位，起始位，校验位，中断使能）适合于STM32-F4开发板
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void InitHardUart(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

#if UART1_FIFO_EN == 1		/* 串口1 TX = PA9   RX = PA10 或 TX = PB6   RX = PB7*/

	/* 第1步：打开GPIO和USART部件的时钟 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	/* 第2步：将USART Tx的GPIO配置为推挽复用模式 */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* 第3步：将USART Rx的GPIO配置为浮空输入模式
		由于CPU复位后，GPIO缺省都是浮空输入模式，因此下面这个步骤不是必须的
		但是，我还是建议加上便于阅读，并且防止其它地方修改了这个口线的设置参数
	*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	/* 第4步： 配置串口硬件参数 */
	USART_InitStructure.USART_BaudRate = UART1_BAUD;	/* 波特率 */
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);

	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);	/* 使能接收中断 */
	/*
		USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
		注意: 不要在此处打开发送中断
		发送中断使能在SendUart()函数打开
	*/
	USART_Cmd(USART1, ENABLE);		/* 使能串口 */

	/* CPU的小缺陷：串口配置好，如果直接Send，则第1个字节发送不出去
		如下语句解决第1个字节无法正确发送出去的问题 */
	USART_ClearFlag(USART1, USART_FLAG_TC);     /* 清发送完成标志，Transmission Complete flag */
#endif

#if UART2_FIFO_EN == 1		/* 串口2 TX = PA2， RX = PA3  */
	/* 第1步：打开GPIO和USART部件的时钟 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	/* 第2步：将USART Tx的GPIO配置为推挽复用模式 */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* 第3步：将USART Rx的GPIO配置为浮空输入模式
		由于CPU复位后，GPIO缺省都是浮空输入模式，因此下面这个步骤不是必须的
		但是，我还是建议加上便于阅读，并且防止其它地方修改了这个口线的设置参数
	*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);


	/* 第4步： 配置串口硬件参数 */
	USART_InitStructure.USART_BaudRate = UART2_BAUD;	/* 波特率 */
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;		/* 仅选择接收模式 */
	USART_Init(USART2, &USART_InitStructure);

	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);	/* 使能接收中断 */
	/*
		USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
		注意: 不要在此处打开发送中断
		发送中断使能在SendUart()函数打开
	*/
	USART_Cmd(USART2, ENABLE);		/* 使能串口 */

	/* CPU的小缺陷：串口配置好，如果直接Send，则第1个字节发送不出去
		如下语句解决第1个字节无法正确发送出去的问题 */
	USART_ClearFlag(USART2, USART_FLAG_TC);     /* 清发送完成标志，Transmission Complete flag */
#endif

#if UART3_FIFO_EN == 1			/* 串口3 TX = PB10   RX = PB11 */

	/* 配置 PB2为推挽输出，用于切换 RS485芯片的收发状态 */
	/* 第1步： 开启GPIO和UART时钟 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);

	/* 第2步：将USART Tx的GPIO配置为推挽复用模式 */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	/* 第3步：将USART Rx的GPIO配置为浮空输入模式
		由于CPU复位后，GPIO缺省都是浮空输入模式，因此下面这个步骤不是必须的
		但是，我还是建议加上便于阅读，并且防止其它地方修改了这个口线的设置参数
	*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	/*  第3步已经做了，因此这步可以不做
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	*/

	/* 第4步： 配置串口硬件参数 */
	USART_InitStructure.USART_BaudRate = UART3_BAUD;	/* 波特率 */
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART3, &USART_InitStructure);

	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);	/* 使能接收中断 */
	/*
		USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
		注意: 不要在此处打开发送中断
		发送中断使能在SendUart()函数打开
	*/
	USART_Cmd(USART3, ENABLE);		/* 使能串口 */

	/* CPU的小缺陷：串口配置好，如果直接Send，则第1个字节发送不出去
		如下语句解决第1个字节无法正确发送出去的问题 */
	USART_ClearFlag(USART3, USART_FLAG_TC);     /* 清发送完成标志，Transmission Complete flag */
#endif

#if UART4_FIFO_EN == 1			/* 串口4 TX = PC10   RX = PC11 */
	/* 第1步： 开启GPIO和UART时钟 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC | RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);

	/* 第2步：将USART Tx的GPIO配置为推挽复用模式 */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	/* 第3步：将USART Rx的GPIO配置为浮空输入模式
		由于CPU复位后，GPIO缺省都是浮空输入模式，因此下面这个步骤不是必须的
		但是，我还是建议加上便于阅读，并且防止其它地方修改了这个口线的设置参数
	*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	/* 第4步： 配置串口硬件参数 */
	USART_InitStructure.USART_BaudRate = UART4_BAUD;	/* 波特率 */
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(UART4, &USART_InitStructure);

	USART_ITConfig(UART4, USART_IT_RXNE, ENABLE);	/* 使能接收中断 */
	/*
		USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
		注意: 不要在此处打开发送中断
		发送中断使能在SendUart()函数打开
	*/
	USART_Cmd(UART4, ENABLE);		/* 使能串口 */

	/* CPU的小缺陷：串口配置好，如果直接Send，则第1个字节发送不出去
		如下语句解决第1个字节无法正确发送出去的问题 */
	USART_ClearFlag(UART4, USART_FLAG_TC);     /* 清发送完成标志，Transmission Complete flag */
#endif

#if UART5_FIFO_EN == 1			/* 串口5 TX = PC12   RX = PD2 */
	/* 第1步： 开启GPIO和UART时钟 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD | RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5, ENABLE);

	/* 第2步：将USART Tx的GPIO配置为推挽复用模式 */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	/* 第3步：将USART Rx的GPIO配置为浮空输入模式
		由于CPU复位后，GPIO缺省都是浮空输入模式，因此下面这个步骤不是必须的
		但是，我还是建议加上便于阅读，并且防止其它地方修改了这个口线的设置参数
	*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOD, &GPIO_InitStructure);


	/* 第4步： 配置串口硬件参数 */
	USART_InitStructure.USART_BaudRate = UART5_BAUD;	/* 波特率 */
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(UART5, &USART_InitStructure);

	USART_ITConfig(UART5, USART_IT_RXNE, ENABLE);	/* 使能接收中断 */
	/*
		USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
		注意: 不要在此处打开发送中断
		发送中断使能在SendUart()函数打开
	*/
	USART_Cmd(UART5, ENABLE);		/* 使能串口 */

	/* CPU的小缺陷：串口配置好，如果直接Send，则第1个字节发送不出去
		如下语句解决第1个字节无法正确发送出去的问题 */
	USART_ClearFlag(UART5, USART_FLAG_TC);     /* 清发送完成标志，Transmission Complete flag */
#endif
}

/*
*********************************************************************************************************
*	函 数 名: ConfigUartNVIC
*	功能说明: 配置串口硬件中断.
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
static void ConfigUartNVIC(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Configure the NVIC Preemption Priority Bits */
	/*	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);  --- 在 bsp.c 中 bsp_Init() 中配置中断优先级组 */

#if UART1_FIFO_EN == 1
	/* 使能串口1中断 */
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif

#if UART2_FIFO_EN == 1
	/* 使能串口2中断 */
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif

#if UART3_FIFO_EN == 1
	/* 使能串口3中断t */
	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif

#if UART4_FIFO_EN == 1
	/* 使能串口4中断t */
	NVIC_InitStructure.NVIC_IRQChannel = UART4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif

#if UART5_FIFO_EN == 1
	/* 使能串口5中断t */
	NVIC_InitStructure.NVIC_IRQChannel = UART5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif

#if UART6_FIFO_EN == 1
	/* 使能串口6中断t */
	NVIC_InitStructure.NVIC_IRQChannel = USART6_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif
}

/*
*********************************************************************************************************
*	函 数 名: UartSend
*	功能说明: 填写数据到UART发送缓冲区,并启动发送中断。中断处理函数发送完毕后，自动关闭发送中断
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
static void UartSend(UART_T *_pUart, uint8_t *_buf, uint16_t _len)
{
	uint16_t i;

	for (i = 0; i < _len; i++)
	{
		/* 如果发送缓冲区已经满了，则等待缓冲区空 */
	#if 0
		/*
			在调试GPRS例程时，下面的代码出现死机，while 死循环
			原因： 发送第1个字节时 _pUart->txWrite = 1；_pUart->txRead = 0;
			将导致while(1) 无法退出
		*/
		while (1)
		{
			uint16_t usRead;

			DISABLE_INT();
			usRead = _pUart->txRead;
			ENABLE_INT();

			if (++usRead >= _pUart->txBufSize)
			{
				usRead = 0;
			}

			if (usRead != _pUart->txWrite)
			{
				break;
			}
		}
	#else
		/* 当 _pUart->txBufSize == 1 时, 下面的函数会死掉(待完善) */
		while (1)
		{
			__IO uint16_t usCount;

			DISABLE_INT();
			usCount = _pUart->txCount;
			ENABLE_INT();

			if (usCount < _pUart->txBufSize)
			{
				break;
			}
		}
	#endif

		/* 将新数据填入发送缓冲区 */
		_pUart->pTxBuf[_pUart->txWrite] = _buf[i];

		DISABLE_INT();
		if (++_pUart->txWrite >= _pUart->txBufSize)
		{
			_pUart->txWrite = 0;
		}
		_pUart->txCount++;
		ENABLE_INT();
	}

	USART_ITConfig(_pUart->uart, USART_IT_TXE, ENABLE);
}


/*
*********************************************************************************************************
*	函 数 名: UartGetChar
*	功能说明: 从串口接收缓冲区读取1字节数据 （用于主程序调用）
*	形    参: pUart : 串口设备
*			  _pByte : 存放读取数据的指针
*	返 回 值: 0 表示无数据  1表示读取到数据
*********************************************************************************************************
*/
static uint8_t UartGetChar(UART_T *pUart, uint8_t *_pByte)
{
	uint16_t usCount;

	/* rxWrite 变量在中断函数中被改写，主程序读取该变量时，必须进行临界区保护 */
	DISABLE_INT();
	usCount = pUart->rxCount;
	ENABLE_INT();

	/* 如果读和写索引相同，则返回0 */
	//if (pUart->rxRead == rxWrite)
	if (usCount == 0)	/* 已经没有数据 */
	{
		return 0;
	}
	else
	{
		*_pByte = pUart->pRxBuf[pUart->rxRead];		/* 从串口接收FIFO取1个数据 */

		/* 改写FIFO读索引 */
		DISABLE_INT();
		if (++pUart->rxRead >= pUart->rxBufSize)
		{
			pUart->rxRead = 0;
		}
		pUart->rxCount--;
		ENABLE_INT();
		return 1;
	}
}

/*
*********************************************************************************************************
*	函 数 名: UartIRQ
*	功能说明: 供中断服务程序调用，通用串口中断处理函数
*	形    参: pUart : 串口设备
*	返 回 值: 无
*********************************************************************************************************
*/
//static void UartIRQ(UART_T *_pUart)
//{
//	/* 处理接收中断  */
//	if (USART_GetITStatus(_pUart->uart, USART_IT_RXNE) != RESET)
//	{
//		/* 从串口接收数据寄存器读取数据存放到接收FIFO */
//		uint8_t data;
//		
//		USART_ClearITPendingBit(_pUart->uart, USART_IT_RXNE);    // 传人记，20170211新增，清除中断标志位 

//		data = USART_ReceiveData(_pUart->uart);
//		_pUart->pRxBuf[_pUart->rxWrite] = data;
//		if (++_pUart->rxWrite >= _pUart->rxBufSize)
//		{
//			_pUart->rxWrite = 0;
//		}
//		if (_pUart->rxCount < _pUart->rxBufSize)
//		{
//			_pUart->rxCount++;
//		}

//		/* 回调函数,通知应用程序收到新数据,一般是发送1个消息或者设置一个标记 */
//		//if (pUart->rxWrite == pUart->rxRead)
//		//if (pUart->rxCount == 1)
//		{			
//			if (_pUart->ReciveNew)  // 确定已使用
//			{				
//				_pUart->ReciveNew(_pUart->com,_pUart->baud,data);
//			}
//		}
//	}

//	/* 处理发送缓冲区空中断 */
//	if (USART_GetITStatus(_pUart->uart, USART_IT_TXE) != RESET)
//	{
//		//if (pUart->txRead == pUart->txWrite)
//		if (_pUart->txCount == 0)
//		{
//			/* 发送缓冲区的数据已取完时， 禁止发送缓冲区空中断 （注意：此时最后1个数据还未真正发送完毕）*/
//			USART_ITConfig(_pUart->uart, USART_IT_TXE, DISABLE);

//			/* 使能数据发送完毕中断 */
//			USART_ITConfig(_pUart->uart, USART_IT_TC, ENABLE);
//		}
//		else
//		{
//			/* 从发送FIFO取1个字节写入串口发送数据寄存器 */
//			USART_SendData(_pUart->uart, _pUart->pTxBuf[_pUart->txRead]);
//			if (++_pUart->txRead >= _pUart->txBufSize)
//			{
//				_pUart->txRead = 0;
//			}
//			_pUart->txCount--;
//		}

//	}
//	/* 数据bit位全部发送完毕的中断 */
//	else if (USART_GetITStatus(_pUart->uart, USART_IT_TC) != RESET)
//	{
//		//if (pUart->txRead == pUart->txWrite)
//		if (_pUart->txCount == 0)
//		{
//			/* 如果发送FIFO的数据全部发送完毕，禁止数据发送完毕中断 */
//			USART_ITConfig(_pUart->uart, USART_IT_TC, DISABLE);

//			/* 回调函数, 一般用来处理RS485通信，将RS485芯片设置为接收模式，避免抢占总线 */
//			if (_pUart->SendOver)
//			{
//				_pUart->SendOver(_pUart->com);
//			}
//		}
//		else
//		{
//			/* 正常情况下，不会进入此分支 */

//			/* 如果发送FIFO的数据还未完毕，则从发送FIFO取1个数据写入发送数据寄存器 */
//			USART_SendData(_pUart->uart, _pUart->pTxBuf[_pUart->txRead]);
//			if (++_pUart->txRead >= _pUart->txBufSize)
//			{
//				_pUart->txRead = 0;
//			}
//			_pUart->txCount--;
//		}
//	}
//	
//	if(USART_GetITStatus(_pUart->uart, USART_FLAG_ORE) != RESET) // 传人记，20170211新增
//	{
//	   USART_ClearFlag(_pUart->uart,USART_FLAG_ORE);//读SR
//		 USART_ReceiveData(_pUart->uart);//读DR	
//	}
//	
//}

// 0323修复
static void UartIRQ(UART_T *_pUart)
{
		/* 处理接收中断  */
		if (USART_GetITStatus(_pUart->uart, USART_IT_RXNE) != RESET)
		{
			/* 从串口接收数据寄存器读取数据存放到接收FIFO */
			uint8_t data;

			_pUart->SendOver(_pUart->com); //设定485管脚接收模式
			USART_ClearITPendingBit(_pUart->uart, USART_IT_RXNE);  // 新增，清除中断标志位 

			data = USART_ReceiveData(_pUart->uart);//接收串口数据
			
			_pUart->pRxBuf[_pUart->rxWrite] = data;//数据存放到BUFF
			if (++_pUart->rxWrite >= _pUart->rxBufSize)//BUFF序号比较，是否到达最大值
			{
					_pUart->rxWrite = 0;//序号清零
			}
			if (_pUart->rxCount < _pUart->rxBufSize)//接收次数计数
			{
					_pUart->rxCount++;
			}

			/* 回调函数,通知应用程序收到新数据,一般是发送1个消息或者设置一个标记 */
			//if (_pUart->rxWrite == _pUart->rxRead)
			//if (_pUart->rxCount == 1)
			//{			
				if (_pUart->ReciveNew)  // 确定已使用
				{	
					_pUart->ReciveNew(_pUart->com,_pUart->baud,data);//485主从接收函数
				}
			//}
		}
		/* 处理发送缓冲区空中断 */
		if (USART_GetITStatus(_pUart->uart, USART_IT_TXE) != RESET)
		{
			USART_ClearITPendingBit(_pUart->uart, USART_IT_TXE);

			if (_pUart->txCount == 0)
			{
				USART_ITConfig(_pUart->uart, USART_IT_TXE, DISABLE); // 发送缓冲区的数据已取完时， 禁止空中断 （此时最后1个数据还未真正发送完毕)
				USART_ITConfig(_pUart->uart, USART_IT_TC, ENABLE); // 关闭数据发送完毕中断
			}
			else
			{ 
				USART_SendData(_pUart->uart, _pUart->pTxBuf[_pUart->txRead]);/* 从发送FIFO取1个字节写入串口发送数据寄存器 */
				if (++_pUart->txRead >= _pUart->txBufSize)
				{
					_pUart->txRead = 0;
				}
				_pUart->txCount--;//发送缓存计数，为0时，一组数据发送完毕
				USART_ITConfig(_pUart->uart, USART_IT_TXE, DISABLE); // 发送缓冲区的数据已取完时， 禁止发送空中断 （此时最后1个数据还未真正发送完毕）
				USART_ITConfig(_pUart->uart, USART_IT_TC, ENABLE); // 关闭数据发送完毕中断
			}
		}
		
		
		/* 数据bit位全部发送完毕的中断 */
		else if (USART_GetITStatus(_pUart->uart, USART_IT_TC) != RESET)
		{
				USART_ClearITPendingBit(_pUart->uart, USART_IT_TC);
				if (_pUart->txCount!= 0)//如果还有数据未发送，继续发送
				{//在这里增加等待功能，等待数据发送结束 非常关键，否则可能冲掉发送缓冲区的数据，调试两周才发现
						USART_ITConfig(_pUart->uart, USART_IT_TC, DISABLE);//关闭发送结束中断，在发送空中断中开启
						USART_ITConfig(_pUart->uart, USART_IT_TXE, ENABLE);	//开发送空中断，继续发送
				}

				else//在这里结束发送数据
				{
						/* 如果发送FIFO的数据全部发送完毕，禁止数据发送完毕中断 */
						USART_ITConfig(_pUart->uart, USART_IT_TC, DISABLE);
						USART_ITConfig(_pUart->uart, USART_IT_TXE, DISABLE);
						/* 回调函数, 一般用来处理RS485通信，将RS485芯片设置为接收模式，避免抢占总线 */
						if (_pUart->SendOver)
						{
							_pUart->SendOver(_pUart->com);
						}
				}
		}
		
		if(USART_GetITStatus(_pUart->uart, USART_FLAG_ORE) != RESET) // 解决开接收中断后 自动打开ORE中断的BUG，检测并关闭ORE，否则可能死机
		{
			 USART_ClearFlag(_pUart->uart,USART_FLAG_ORE);//读SR
			 USART_ReceiveData(_pUart->uart);//读DR	
		}
}

/*
*********************************************************************************************************
*	函 数 名: USART1_IRQHandler  USART2_IRQHandler USART3_IRQHandler UART4_IRQHandler UART5_IRQHandler
*	功能说明: USART中断服务程序
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
#if UART1_FIFO_EN == 1
void USART1_IRQHandler(void)
{
	UartIRQ(&g_tUart1);
}
#endif

#if UART2_FIFO_EN == 1
void USART2_IRQHandler(void)
{
	UartIRQ(&g_tUart2);
}
#endif

#if UART3_FIFO_EN == 1
void USART3_IRQHandler(void)
{
	UartIRQ(&g_tUart3);
}
#endif

#if UART4_FIFO_EN == 1
void UART4_IRQHandler(void)
{
	UartIRQ(&g_tUart4);
}
#endif

#if UART5_FIFO_EN == 1
void UART5_IRQHandler(void)
{
	UartIRQ(&g_tUart5);
}
#endif

#if UART6_FIFO_EN == 1
void USART6_IRQHandler(void)
{
	UartIRQ(&g_tUart6);
}
#endif

///*
//*********************************************************************************************************
//*	函 数 名: fputc
//*	功能说明: 重定义putc函数，这样可以使用printf函数从串口1打印输出
//*	形    参: 无
//*	返 回 值: 无
//*********************************************************************************************************
//*/
//int fputc(int data, FILE *f)
//{
//#if 1	/* 将需要printf的字符通过串口中断FIFO发送出去，printf函数会立即返回 */
//	comSendChar(COM1, data);

//	return data;
//#else	/* 采用阻塞方式发送每个字符,等待数据发送完毕 */
//	/* 写一个字节到USART1 */
//	USART_SendData(USART1, (uint8_t) data);

//	/* 等待发送结束 */
//	while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET)
//	{}

//	return ch;
//#endif
//}

///*
//*********************************************************************************************************
//*	函 数 名: fgetc
//*	功能说明: 重定义getc函数，这样可以使用getchar函数从串口1输入数据
//*	形    参: 无
//*	返 回 值: 无
//*********************************************************************************************************
//*/
//int fgetc(FILE *f)
//{

//#if 1	/* 从串口接收FIFO中取1个数据, 只有取到数据才返回 */
//	uint8_t ucData;

//	while(comGetChar(COM1, &ucData) == 0);

//	return ucData;
//#else
//	/* 等待串口1输入数据 */
//	while (USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == RESET);

//	return (int)USART_ReceiveData(USART1);
//#endif
//}

/// 重定向c库函数printf到USART2
int fputc(int ch, FILE *f)
{
		/* 发送一个字节数据到USART1 */
		USART_SendData(USART2, (uint8_t) ch);
		
		/* 等待发送完毕 */
		while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);		
	
		return (ch);
}

/// 重定向c库函数scanf到USART1
int fgetc(FILE *f)
{
		/* 等待串口1输入数据 */
		while (USART_GetFlagStatus(USART2, USART_FLAG_RXNE) == RESET);

		return (int)USART_ReceiveData(USART2);
}


/***************************** 传人记 (END OF FILE) *********************************/
