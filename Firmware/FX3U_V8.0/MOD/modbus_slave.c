/*
*********************************************************************************************************
*
*	模块名称 : MODS通信模块. 从站模式
*	文件名称 : modbus_slave.c
*	版    本 : V1.4
*	说    明 : 头文件
*
*	Copyright (C),
*
* 
*********************************************************************************************************
*/
#include "bsp.h"
#include "modbus_slave.h"
#include "main.h"

#include "PLC_Dialogue.h"
#include "PLC_IO.h"
#include "plc_conf.h"


MODS_T g_tModS[2];  // 定义2路modbus,供从机模式使用

// 10新增
const uint8_t config[2][2] ={{COM4,HS_MOD1_MODE},{COM4,HS_MOD2_MODE}};

// 26新增，线圈或者输入继电器缓冲
uint8_t     gModbusBuf[200]; 
uint8_t     gModbusBitBuf[200]; 

static void MODS_SendWithCRC(uint8_t _ch,uint8_t *_pBuf, uint8_t _ucLen);
static void MODS_SendAckOk(uint8_t _ch);
static void MODS_SendAckErr(uint8_t _ch,uint8_t _ucErrCode);

static void MODS_AnalyzeApp(uint8_t _ch);

static void MODS_RxTimeOut(uint8_t _ch);

static void MODS_01H(uint8_t _ch);
static void MODS_02H(uint8_t _ch);
static void MODS_03H(uint8_t _ch);
static void MODS_04H(uint8_t _ch);
static void MODS_05H(uint8_t _ch);
static void MODS_06H(uint8_t _ch);
static void MODS_0FH(uint8_t _ch);
static void MODS_10H(uint8_t _ch);


/*
*********************************************************************************************************
*	函 数 名: MODS_Poll
*	功能说明: 解析数据包. 在主程序中轮流调用。
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
void MODS_Poll(uint8_t _ch)
{
	uint16_t addr;
	uint16_t crc1;
	
	/* 超过3.5个字符时间后执行MODH_RxTimeOut()函数。全局变量 g_rtu_timeout = 1; 通知主程序开始解码 */
	if (g_tModS[_ch].c3_5Timeout == 0)	
	{
		return; /* 没有超时，继续接收。不要清零 g_tModS[_ch].RxCount */
	}
	
	g_tModS[_ch].c3_5Timeout = 0;	// 清标志

	if (g_tModS[_ch].RxCount < 4) /* 接收到的数据小于4个字节就认为错误 */
	{
		goto err_ret;
	}

	/* 计算CRC校验和 */
	crc1 = CRC16_Modbus(g_tModS[_ch].RxBuf, g_tModS[_ch].RxCount);
	if (crc1 != 0)
	{
		goto err_ret;
	}

	/* 站地址 (1字节） */
	addr = g_tModS[_ch].RxBuf[0]; /* 第1字节 站号 */
	if (addr != HS_MOD_LOCAL_ADDR)	/* 判断主机发送的命令地址是否符合 */
	{
		goto err_ret;
	}

   gCommLedFlashFlg =2; // 闪烁
	 
	/* 分析应用层协议 */
	MODS_AnalyzeApp(_ch);						
	
err_ret:
//#if 1										        /* 此部分为了串口打印结果,实际运用中可不要 */
//	g_tPrint.Rxlen = g_tModS[_ch].RxCount;
//	memcpy(g_tPrint.RxBuf, g_tModS[_ch].RxBuf, g_tModS[_ch].RxCount);
//#endif
	
	g_tModS[_ch].RxCount = 0;					/* 必须清零计数器，方便下次帧同步 */
}

/*
*********************************************************************************************************
*	函 数 名: MODS_ReciveNew
*	功能说明: 串口接收中断服务程序会调用本函数。当收到一个字节时，执行一次本函数。
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/

void MODS_ReciveNew(uint8_t _ch,uint16_t baud,uint8_t _byte)
{
	/*
		3.5个字符的时间间隔，只是用在RTU模式下面，因为RTU模式没有开始符和结束符，
		两个数据包之间只能靠时间间隔来区分，Modbus定义在不同的波特率下，间隔时间是不一样的，
		所以就是3.5个字符的时间，波特率高，这个时间间隔就小，波特率低，这个时间间隔相应就大

		4800  = 7.297ms
		9600  = 3.646ms
		19200  = 1.771ms
		38400  = 0.885ms
	*/
	uint32_t timeout;

	g_tModS[_ch].c3_5Timeout = 0;
	
	timeout = 35000000 / baud;			/* 计算超时时间，单位us 35000000*/
	
	/* 硬件定时中断，定时精度us 硬件定时器1用于ADC, 定时器2用于Modbus */
	bsp_StartHardTimer(_ch +1, timeout, (void (*)(uint8_t))MODS_RxTimeOut); // TIM5 CC1或者CC2。CC3和CC4暂未使用

	if (g_tModS[_ch].RxCount < MOD_RX_BUF_SIZE)
	{
		g_tModS[_ch].RxBuf[g_tModS[_ch].RxCount++] = _byte;
	}
}


/*
*********************************************************************************************************
*	函 数 名: MODS_RxTimeOut
*	功能说明: 超过3.5个字符时间后执行本函数。 设置全局变量 g_mods_timeout = 1; 通知主程序开始解码。
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODS_RxTimeOut(uint8_t _ch)
{
	g_tModS[_ch].c3_5Timeout = 1;
}

/*
*********************************************************************************************************
*	函 数 名: MODS_SendWithCRC
*	功能说明: 发送一串数据, 自动追加2字节CRC
*	形    参: _pBuf 数据；
*			  _ucLen 数据长度（不带CRC）
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODS_SendWithCRC(uint8_t _ch,uint8_t *_pBuf, uint8_t _ucLen)
{
	uint16_t crc;
	uint8_t buf[MOD_TX_BUF_SIZE];

	memcpy(buf, _pBuf, _ucLen);
	crc = CRC16_Modbus(_pBuf, _ucLen);
	buf[_ucLen++] = crc >> 8;
	buf[_ucLen++] = crc;

	RS485_SendBuf(config[_ch][0],buf, _ucLen);
}

/*
*********************************************************************************************************
*	函 数 名: MODS_SendAckErr
*	功能说明: 发送错误应答
*	形    参: _ucErrCode : 错误代码
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODS_SendAckErr(uint8_t _ch,uint8_t _ucErrCode)
{
	uint8_t txbuf[3];

	txbuf[0] = g_tModS[_ch].RxBuf[0];					/* 485地址 */
	txbuf[1] = g_tModS[_ch].RxBuf[1] | 0x80;				/* 异常的功能码 */
	txbuf[2] = _ucErrCode;							/* 错误代码(01,02,03,04) */

	MODS_SendWithCRC(_ch,txbuf, 3);
}

/*
*********************************************************************************************************
*	函 数 名: MODS_SendAckOk
*	功能说明: 发送正确的应答.
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODS_SendAckOk(uint8_t _ch)
{
	uint8_t txbuf[6];
	uint8_t i;

	for (i = 0; i < 6; i++)
	{
		txbuf[i] = g_tModS[_ch].RxBuf[i];
	}
	MODS_SendWithCRC(_ch,txbuf, 6);
}

/*
*********************************************************************************************************
*	函 数 名: MODS_AnalyzeApp
*	功能说明: 分析应用层协议
*	形    参: 无
*	返 回 值: 无

1、线圈状态（读取（01）/写入用（05单写、15多写），寄存器类型为0区）
M、M8000、S、T、C、Y

2、离散输入状态（读取专用（02），寄存器类型为1区）
X

3、输入寄存器（读取专用（04），寄存器类型为3区）
无

4、保持寄存器（读取（03）/写入用（06单写、16多写），寄存器类型为4区）
D、D8000、T、C、C200
*********************************************************************************************************
*/
static void MODS_AnalyzeApp(uint8_t _ch)
{
	switch (g_tModS[_ch].RxBuf[1])				/* 第2个字节 功能码 */
	{
		case 0x01:							/* 读取线圈状态*/
			MODS_01H(_ch);
			break;

		case 0x02:							/* 读取输入状态*/
			MODS_02H(_ch);
			break;
		
		case 0x03:							/* 读取保持寄存器*/
			MODS_03H(_ch);
			break;
		
		case 0x04:							/* 读取输入寄存器*/
			MODS_04H(_ch);
			break;
		
		case 0x05:							/* 强制单线圈*/
			MODS_05H(_ch);
			break;
		
		case 0x06:							/* 写单个保存寄存器*/
			MODS_06H(_ch);	
			break;
			
		// 传人记，20161125新增 //???
		case 0x0F:							/* 写多个线圈*/
			MODS_0FH(_ch);	
			break;
		case 0x10:							/* 写多个保存寄存器*/
			MODS_10H(_ch);
			break;
		
		default:
			g_tModS[_ch].RspCode = RSP_ERR_CMD;
			MODS_SendAckErr(_ch,g_tModS[_ch].RspCode);	/* 告诉主机命令错误 */
			break;
	}
}

/*
*********************************************************************************************************
*	函 数 名: GetBitOffset
*	功能说明: 获取继电器地址偏移值
*	形    参: 
*	返 回 值: 
*********************************************************************************************************
*/
int16_t GetBitOffset(uint16_t addr,uint16_t num)
{
		 uint16_t offset=0;
  
		 if (addr + num <= COIL_S_END_ADDR)                                	       // S  
		 {  
			  offset = addr;
		 }			
		 else if((addr >= COIL_Y_START_ADDR) && (addr + num <= COIL_Y_END_ADDR))   // Y
		 {
		    offset = addr - COIL_Y_START_ADDR;
		 }
		 else if((addr >= COIL_T_START_ADDR) && (addr + num <= COIL_T_END_ADDR))   // T
		 {
		    offset = addr - COIL_T_START_ADDR;
		 }
		 else if((addr >= COIL_M_START_ADDR) && (addr + num <= COIL_M_END_ADDR))   // M
		 {
		    offset = addr - COIL_M_START_ADDR;
		 }
		 else if((addr >= COIL_C_START_ADDR) && (addr + num <= COIL_C_END_ADDR))  // C
		 {
		    offset = addr - COIL_C_START_ADDR;
		 }
		 else if((addr >= COIL_M8000_START_ADDR) && (addr + num <= COIL_M8000_END_ADDR))   // M8000
		 {
				offset = addr -  COIL_M8000_START_ADDR;		
		 }
		 else if((addr >= COIL_M1538_START_ADDR) && (addr + num <= COIL_M1538_END_ADDR))   // M1536     
		 {	
				offset = addr - COIL_M1538_START_ADDR;				
		 }
     //输入寄存器，X
     else if((addr >= INPUT_X_START_ADDR) && (addr + num <= INPUT_X_END_ADDR))         // X
     {
       offset = addr - INPUT_X_START_ADDR;				
     }
		 else
		 {
				//g_tModS[_ch].RspCode = RSP_ERR_REG_ADDR;	  // 寄存器地址错误 
		 }
	
	return offset;
}

/*
*********************************************************************************************************
*	函 数 名: GetWordOffset
*	功能说明: 获取寄存器地址偏移值
*	形    参: 
*	返 回 值: 
*********************************************************************************************************
*/
int16_t GetWordOffset(uint16_t addr,uint16_t num)
{
		uint16_t offset=0;
		if ((addr >= REG_C_START_ADDR) && (addr + num <= REG_C_END_ADDR))             // C
		{	
			offset = addr - REG_C_START_ADDR;
		}
		else if((addr >= REG_C200_START_ADDR) && (addr + num <= REG_C200_END_ADDR))   // C200
		{	
			offset = addr - REG_C200_START_ADDR;
		}
		else if((addr >= REG_D8000_START_ADDR) && (addr + num <= REG_D8000_END_ADDR)) // D8000
		{	
			offset = addr - REG_D8000_START_ADDR;
		}
		else if((addr >= REG_T_START_ADDR) && (addr + num <= REG_T_END_ADDR))         // T
		{	
			offset = addr - REG_T_START_ADDR;
		}
		else if((addr >= REG_D_START_ADDR) && (addr + num <= REG_D_END_ADDR))         // D
		{	
			offset = addr - REG_D_START_ADDR;
		}
		else
		{
				//g_tModS[_ch].RspCode = RSP_ERR_REG_ADDR;	  // 寄存器地址错误  
		}
	  return offset;
}



// 新增
//void xMBUtilSetBits( uint8_t * ucByteBuf, uint16_t usBitOffset, uint8_t ucNBits, uint8_t ucValue )
//{
//    uint16_t          usWordBuf;
//    uint16_t          usMask;
//    uint16_t          usByteOffset;
//    uint16_t          usNPreBits;
//    uint16_t          usValue = ucValue;

//    assert_param( ucNBits <= 8 );
//    assert_param( ( size_t )BITS_U08 == sizeof( v ) * 8 );

//    /* Calculate byte offset for first byte containing the bit values starting
//     * at usBitOffset. */
//    usByteOffset = ( uint16_t )( ( usBitOffset ) / 8 );

//    /* How many bits precede our bits to set. */
//    usNPreBits = ( uint16_t )( usBitOffset - usByteOffset * 8 );

//    /* Move bit field into position over bits to set */
//    usValue <<= usNPreBits;

//    /* Prepare a mask for setting the new bits. */
//    usMask = ( uint16_t )( ( 1 << ( uint16_t ) ucNBits ) - 1 );
//    usMask <<= usBitOffset - usByteOffset * 8;

//    /* copy bits into temporary storage. */
//    usWordBuf = ucByteBuf[usByteOffset];
//    usWordBuf |= ucByteBuf[usByteOffset + 1] << 8;

//    /* Zero out bit field bits and then or value bits into them. */
//    usWordBuf = ( uint16_t )( ( usWordBuf & ( ~usMask ) ) | usValue );

//    /* move bits back into storage */
//    ucByteBuf[usByteOffset] = ( uint8_t )( usWordBuf & 0xFF );
//    ucByteBuf[usByteOffset + 1] = ( uint8_t )( usWordBuf >> 8 );
//}

void xMBUtilSetBits( uint8_t * ucByteBuf, uint16_t usBitOffset, uint8_t ucNBits, uint8_t ucValue )
{
    uint16_t          usWordBuf;
    uint16_t          usMask;
    uint16_t          usByteOffset;
    uint16_t          usNPreBits;
    uint16_t          usValue = ucValue;

    assert_param( ucNBits <= 8 );
    assert_param( ( size_t )BITS_U08 == sizeof( v ) * 8 );

    /* Calculate byte offset for first byte containing the bit values starting
     * at usBitOffset. */
    usByteOffset = ( uint16_t )( ( usBitOffset ) / 8 );

    /* How many bits precede our bits to set. */
    usNPreBits = ( uint16_t )( usBitOffset - usByteOffset * 8 );

    /* Move bit field into position over bits to set */
    usValue <<= usNPreBits;

    /* Prepare a mask for setting the new bits. */
    usMask = ( uint16_t )( ( 1 << ( uint16_t ) ucNBits ) - 1 );
    usMask <<= usBitOffset - usByteOffset * 8;

    /* copy bits into temporary storage. */
//    usWordBuf = ucByteBuf[usByteOffset];
//    usWordBuf |= ucByteBuf[usByteOffset + 1] << 8;

    usWordBuf = ucByteBuf[0];
    usWordBuf |= ucByteBuf[1] << 8;

    /* Zero out bit field bits and then or value bits into them. */
    usWordBuf = ( uint16_t )( ( usWordBuf & ( ~usMask ) ) | usValue );

    /* move bits back into storage */
//    ucByteBuf[usByteOffset] = ( uint8_t )( usWordBuf & 0xFF );
//    ucByteBuf[usByteOffset + 1] = ( uint8_t )( usWordBuf >> 8 );

    ucByteBuf[0] = ( uint8_t )( usWordBuf & 0xFF );
    ucByteBuf[1] = ( uint8_t )( usWordBuf >> 8 );

}




// 新增
//uint8_t xMBUtilGetBits( uint8_t * ucByteBuf, uint16_t usBitOffset, uint8_t ucNBits )
//{
//    uint16_t          usWordBuf;
//    uint16_t          usMask;
//    uint16_t          usByteOffset;
//    uint16_t          usNPreBits;

//    /* Calculate byte offset for first byte containing the bit values starting
//     * at usBitOffset. */
//    usByteOffset = ( uint16_t )( ( usBitOffset ) / 8 );

//    /* How many bits precede our bits to set. */
//    usNPreBits = ( uint16_t )( usBitOffset - usByteOffset * 8 );

//    /* Prepare a mask for setting the new bits. */
//    usMask = ( uint16_t )( ( 1 << ( uint16_t ) ucNBits ) - 1 );

//    /* copy bits into temporary storage. */
//    usWordBuf = ucByteBuf[usByteOffset];
//    usWordBuf |= ucByteBuf[usByteOffset + 1] << 8;

//    /* throw away unneeded bits. */
//    usWordBuf >>= usNPreBits;

//    /* mask away bits above the requested bitfield. */
//    usWordBuf &= usMask;

//    return ( uint8_t ) usWordBuf;
//}

uint8_t xMBUtilGetBits( uint8_t * ucByteBuf, uint16_t usBitOffset, uint8_t ucNBits )
{
    uint16_t          usWordBuf;
    uint16_t          usMask;
    uint16_t          usByteOffset;
    uint16_t          usNPreBits;

    /* Calculate byte offset for first byte containing the bit values starting
     * at usBitOffset. */
    usByteOffset = ( uint16_t )( ( usBitOffset ) / 8 );

    /* How many bits precede our bits to set. */
    usNPreBits = ( uint16_t )( usBitOffset - usByteOffset * 8 );

    /* Prepare a mask for setting the new bits. */
    usMask = ( uint16_t )( ( 1 << ( uint16_t ) ucNBits ) - 1 );

    /* copy bits into temporary storage. */
//    usWordBuf = ucByteBuf[usByteOffset];
//    usWordBuf |= ucByteBuf[usByteOffset + 1] << 8;
	
	  usWordBuf = ucByteBuf[0];
    usWordBuf |= ucByteBuf[1] << 8;

    /* throw away unneeded bits. */
    usWordBuf >>= usNPreBits;

    /* mask away bits above the requested bitfield. */
    usWordBuf &= usMask;

    return ( uint8_t ) usWordBuf;
}

/*
*********************************************************************************************************
*	函 数 名: MODS_01H
*	功能说明: 读取线圈状态
*	形    参: 无
*	返 回 值: 无

*	注：
*	
* S、Y、T、M、C、M8000、M1536
*********************************************************************************************************
*/
static void MODS_01H(uint8_t _ch)
{
	/* 举例：
		主机发送:
		[0]	11 从机地址
		[1]	01 功能码
		[2]	00 寄存器起始地址高字节
		[3]	13 寄存器起始地址低字节
		[4]	00 寄存器数量高字节
		[5]	25 寄存器数量低字节
		[6]	0E CRC校验高字节
		[7]	84 CRC校验低字节

		从机应答: 	1代表ON，0代表OFF。若返回的线圈数不为8的倍数，则在最后数据字节未尾使用0代替. BIT0对应第1个
			11 从机地址
			01 功能码
			05 返回字节数
			CD 数据1(线圈0013H-线圈001AH)
			6B 数据2(线圈001BH-线圈0022H)
			B2 数据3(线圈0023H-线圈002AH)
			0E 数据4(线圈0032H-线圈002BH)
			1B 数据5(线圈0037H-线圈0033H)
			45 CRC校验高字节
			E6 CRC校验低字节	
	*/
			
	uint16_t addr;
	uint16_t byteAddr;
	
	int16_t num ;
	uint16_t i;
	uint16_t bytes;
	
  uint8_t *pStatusBuf = gModbusBitBuf;
	
	uint16_t bitOffset = 0;
	int16_t  realBitOffset =0;

	g_tModS[_ch].RspCode = RSP_OK;

	/* 没有外部继电器，直接应答错误 */
	if (g_tModS[_ch].RxCount != 8)
	{
		g_tModS[_ch].RspCode = RSP_ERR_VALUE;			 // 数据值域错误
		goto err_ret;
	}
	addr = BEBufToUint16(&g_tModS[_ch].RxBuf[2]); //【2高】【3低】寄存器号即寄存器起始地址 
	num = BEBufToUint16(&g_tModS[_ch].RxBuf[4]);  //【4高】【5低】寄存器个数即寄存器数量 
	bytes = (num + 7) / 8;	
	
	bitOffset = GetBitOffset(addr,num);	
	/* 修复，不为8的倍数时处理 */
//	realBitOffset = (bitOffset - (bytes << 3));
//	if(realBitOffset < 0) 
		realBitOffset = bitOffset;  	
	
	if(g_tModS[_ch].RspCode == RSP_ERR_REG_ADDR)	 // 寄存器地址错误 
	{	
		goto err_ret;
	}	
	byteAddr = addr /8;		 	                 // 得到字节地址 
	
	for(i = 0;i < bytes; i++)
			gModbusBuf[i] = PLC_RW_RAM_8BIT(byteAddr + i);

	
	while( num > 0 )
	{
			*pStatusBuf++ = xMBUtilGetBits( (uint8_t *)gModbusBuf, realBitOffset, 
																			( uint8_t )(num > 8 ? 8 : num));
			num -= 8;
			bitOffset += 8;
	}		
	
err_ret:		
	if (g_tModS[_ch].RspCode == RSP_OK)						
	{
			g_tModS[_ch].TxCount = 0;
			g_tModS[_ch].TxBuf[g_tModS[_ch].TxCount++] = g_tModS[_ch].RxBuf[0]; // 本机通讯地址    
			g_tModS[_ch].TxBuf[g_tModS[_ch].TxCount++] = g_tModS[_ch].RxBuf[1]; // 功能代码    
			g_tModS[_ch].TxBuf[g_tModS[_ch].TxCount++] = bytes;	         // 返回字节数
			
			/* 线圈转态 */
			for (i = 0; i < bytes; i++)                     
			{
				g_tModS[_ch].TxBuf[g_tModS[_ch].TxCount++] = gModbusBitBuf[i];
			}
			MODS_SendWithCRC(_ch,g_tModS[_ch].TxBuf, g_tModS[_ch].TxCount);    // CRC校验
	}
  else
	{
		MODS_SendAckErr(_ch,g_tModS[_ch].RspCode);				                   // 告诉主机命令错误
	}
}

/*
*********************************************************************************************************
*	函 数 名: MODS_02H
*	功能说明: 读取输入状态，X
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODS_02H(uint8_t _ch)
{
	/*
		主机发送:
			11 从机地址
			02 功能码
			00 寄存器地址高字节
			C4 寄存器地址低字节
			00 寄存器数量高字节
			16 寄存器数量低字节
			BA CRC校验高字节
			A9 CRC校验低字节

		从机应答:  响应各离散输入寄存器状态，分别对应数据区中的每位值，1 代表ON；0 代表OFF。
		           第一个数据字节的LSB(最低字节)为查询的寻址地址，其他输入口按顺序在该字节中由低字节
		           向高字节排列，直到填充满8位。下一个字节中的8个输入位也是从低字节到高字节排列。
		           若返回的输入位数不是8的倍数，则在最后的数据字节中的剩余位至该字节的最高位使用0填充。
			11 从机地址
			02 功能码
			03 返回字节数
			AC 数据1(00C4H-00CBH)
			DB 数据2(00CCH-00D3H)
			35 数据3(00D4H-00D9H)
			20 CRC校验高字节
			18 CRC校验低字节
	*/

		uint16_t addr;
		uint16_t byteAddr;
		
		int16_t num;
		uint16_t i;
		uint16_t bytes;
		
	  uint8_t *pStatusBuf = gModbusBitBuf;	
	  uint16_t bitOffset = 0;
		int16_t  realBitOffset =0;
		

		g_tModS[_ch].RspCode = RSP_OK;

		if (g_tModS[_ch].RxCount != 8)
		{
			g_tModS[_ch].RspCode = RSP_ERR_VALUE;				           // 数据值域错误
			goto err_ret;
		}
		addr = BEBufToUint16(&g_tModS[_ch].RxBuf[2]); 	           //【2高】【3低】寄存器号即寄存器起始地址 
		num = BEBufToUint16(&g_tModS[_ch].RxBuf[4]);		           //【4高】【5低】寄存器个数即寄存器数量 
		bytes = (num + 7) / 8;
		
		
		bitOffset = GetBitOffset(addr,num);
		/* 531修复，不为8的倍数时处理 */
//		realBitOffset = (bitOffset - (bytes << 3));
//		if(realBitOffset < 0) 
			realBitOffset = bitOffset;  	
		
		if(g_tModS[_ch].RspCode == RSP_ERR_REG_ADDR)	 // 寄存器地址错误 
		{	
			goto err_ret;
		}
    //////////////////////////////////////////////////////////////
		byteAddr = addr /8;	// 得到字节地址 

		for(i = 0;i < bytes; i++)
				gModbusBuf[i] = PLC_RW_RAM_8BIT(byteAddr + i);
		
		while( num > 0 )
		{
				*pStatusBuf++ = xMBUtilGetBits( (uint8_t *)gModbusBuf, realBitOffset, 
																				( uint8_t )(num > 8 ? 8 : num));
				num -= 8;
				bitOffset += 8;
		}	
		
err_ret:
		if (g_tModS[_ch].RspCode == RSP_OK) // 正确应答
		{
			
			g_tModS[_ch].TxCount = 0;
			g_tModS[_ch].TxBuf[g_tModS[_ch].TxCount++] = g_tModS[_ch].RxBuf[0];
			g_tModS[_ch].TxBuf[g_tModS[_ch].TxCount++] = g_tModS[_ch].RxBuf[1];
			g_tModS[_ch].TxBuf[g_tModS[_ch].TxCount++] = bytes;// 返回字节数

			for (i = 0; i < bytes; i++)
			{
				g_tModS[_ch].TxBuf[g_tModS[_ch].TxCount++] =  gModbusBitBuf[i];
			}
			MODS_SendWithCRC(_ch,g_tModS[_ch].TxBuf, g_tModS[_ch].TxCount);		
		}
		else
		{
				MODS_SendAckErr(_ch,g_tModS[_ch].RspCode); // 告诉主机命令错误
		}
}

/*
*********************************************************************************************************
*	函 数 名: MODS_03H
*	功能说明: 读取保持寄存器 在一个或多个保持寄存器中取得当前的二进制值
*	形    参: 无
*	返 回 值: 无

*	注：
*	1新增
*	D、D8000、T、C、C200
*********************************************************************************************************
*/
static void MODS_03H(uint8_t _ch)
{
	/*
		主机发送:
			11 从机地址
			03 功能码
			00 寄存器地址高字节
			6B 寄存器地址低字节
			00 寄存器数量高字节
			03 寄存器数量低字节
			76 CRC高字节
			87 CRC低字节

		从机应答: 	保持寄存器的长度为2个字节。对于单个保持寄存器而言，寄存器高字节数据先被传输，
					低字节数据后被传输。保持寄存器之间，低地址寄存器先被传输，高地址寄存器后被传输。
			11 从机地址
			03 功能码
			06 字节数
			00 数据1高字节(006BH)
			6B 数据1低字节(006BH)
			00 数据2高字节(006CH)
			13 数据2 低字节(006CH)
			00 数据3高字节(006DH)
			00 数据3低字节(006DH)
			38 CRC高字节
			B9 CRC低字节
	*/
	uint16_t reg;
	uint16_t num;
	uint16_t i;

	g_tModS[_ch].RspCode = RSP_OK;

	if (g_tModS[_ch].RxCount != 8)								             // 03H命令必须是8个字节 */
	{
		g_tModS[_ch].RspCode = RSP_ERR_VALUE;					         // 数据值域错误
		goto err_ret;
	}
	
  reg = BEBufToUint16(&g_tModS[_ch].RxBuf[2]); 	           //【2高】【3低】寄存器号即寄存器起始地址 
	num = BEBufToUint16(&g_tModS[_ch].RxBuf[4]);		           //【4高】【5低】寄存器个数即寄存器数量 
	
	/* 地址范围判断 */
	 if ( ((reg >= REG_C_START_ADDR) && (reg + num <= REG_C_END_ADDR))          ||  // C
	       ((reg >= REG_C200_START_ADDR) && (reg + num <= REG_C200_END_ADDR))   ||  // C200
	       ((reg >= REG_D8000_START_ADDR) && (reg + num <= REG_D8000_END_ADDR)) ||  // D8000
	       ((reg >= REG_T_START_ADDR) && (reg + num <= REG_T_END_ADDR))         ||  // T
	       ((reg >= REG_D_START_ADDR) && (reg + num <= REG_D_END_ADDR)) )           // D
	 {	 
      /* 装载数据 */	 
			g_tModS[_ch].TxCount = 0;
			g_tModS[_ch].TxBuf[g_tModS[_ch].TxCount++] = g_tModS[_ch].RxBuf[0];
			g_tModS[_ch].TxBuf[g_tModS[_ch].TxCount++] = g_tModS[_ch].RxBuf[1];
			g_tModS[_ch].TxBuf[g_tModS[_ch].TxCount++] = num * 2;	        // 返回字节数

			for (i = 0; i < num; i++)
			{
				g_tModS[_ch].TxBuf[g_tModS[_ch].TxCount++] = plc_16BitBuf[reg + i] / 256;
				g_tModS[_ch].TxBuf[g_tModS[_ch].TxCount++] = plc_16BitBuf[reg + i] % 256;
			}
			MODS_SendWithCRC(_ch,g_tModS[_ch].TxBuf, g_tModS[_ch].TxCount);	 // 发送正确应答
	 }
	 else
	 {
			g_tModS[_ch].RspCode = RSP_ERR_REG_ADDR;	               // 寄存器地址错误 
	 }	
	
err_ret:
	if (g_tModS[_ch].RspCode != RSP_OK)							          // 正确应答
	{
		MODS_SendAckErr(_ch,g_tModS[_ch].RspCode);					          // 发送错误应答
	}
}

/*
*********************************************************************************************************
*	函 数 名: MODS_04H
*	功能说明: 读取输入寄存器（对应A01/A02） SMA
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODS_04H(uint8_t _com)
{
	/*
		主机发送:
			11 从机地址
			04 功能码
			00 寄存器起始地址高字节
			08 寄存器起始地址低字节
			00 寄存器个数高字节
			02 寄存器个数低字节
			F2 CRC高字节
			99 CRC低字节

		从机应答:  输入寄存器长度为2个字节。对于单个输入寄存器而言，寄存器高字节数据先被传输，
				低字节数据后被传输。输入寄存器之间，低地址寄存器先被传输，高地址寄存器后被传输。
			11 从机地址
			04 功能码
			04 字节数
			00 数据1高字节(0008H)
			0A 数据1低字节(0008H)
			00 数据2高字节(0009H)
			0B 数据2低字节(0009H)
			8B CRC高字节
			80 CRC低字节
	*/
//	uint16_t reg;
//	uint16_t num;
//	uint16_t i;
//	uint16_t status[10];

//	memset(status, 0, 10);

//	g_tModS[_ch].RspCode = RSP_OK;

//	if (g_tModS[_ch].RxCount != 8)
//	{
//		g_tModS[_ch].RspCode = RSP_ERR_VALUE;	/* 数据值域错误 */
//		goto err_ret;
//	}

//	reg = BEBufToUint16(&g_tModS[_ch].RxBuf[2]); 	/* 寄存器号 */
//	num = BEBufToUint16(&g_tModS[_ch].RxBuf[4]); 	/* 寄存器个数 */
//	
//	
//	/* 测试 */
//	if ((reg >= 1) && (num > 0) && (reg + num <= 10))
//	{	
//			for (i = 0; i < num; i++)
//			{
//				status[i] = 0xA5;
//			}
//	}
//	else
//	{
//		g_tModS[_ch].RspCode = RSP_ERR_REG_ADDR;		/* 寄存器地址错误 */
//	}

//err_ret:
//	if (g_tModS[_ch].RspCode == RSP_OK)		/* 正确应答 */
//	{
//		g_tModS[_ch].TxCount = 0;
//		g_tModS[_ch].TxBuf[g_tModS[_ch].TxCount++] = g_tModS[_ch].RxBuf[0];
//		g_tModS[_ch].TxBuf[g_tModS[_ch].TxCount++] = g_tModS[_ch].RxBuf[1];
//		g_tModS[_ch].TxBuf[g_tModS[_ch].TxCount++] = num * 2;			/* 返回字节数 */

//		for (i = 0; i < num; i++)
//		{
//			g_tModS[_ch].TxBuf[g_tModS[_ch].TxCount++] = status[i] >> 8;
//			g_tModS[_ch].TxBuf[g_tModS[_ch].TxCount++] = status[i] & 0xFF;
//		}
//		MODS_SendWithCRC(g_tModS[_ch].TxBuf, g_tModS[_ch].TxCount);
//	}
//	else
//	{
//		MODS_SendAckErr(_com,g_tModS[_ch].RspCode);	/* 告诉主机命令错误 */
//	}
}

/*
*********************************************************************************************************
*	函 数 名: MODS_05H
*	功能说明: 强制单线圈
*	形    参: 无
*	返 回 值: 无

*	注：
*	新增
*	M、M8000、S、T、C、Y
*********************************************************************************************************
*/
static void MODS_05H(uint8_t _ch)
{
	/*
		主机发送:
			11 从机地址
			05 功能码
			00 寄存器地址高字节
			AC 寄存器地址低字节
			FF 数据1高字节
			00 数据2低字节
			4E CRC校验高字节
			8B CRC校验低字节

		从机应答:
			11 从机地址
			05 功能码
			00 寄存器地址高字节
			AC 寄存器地址低字节
			FF 寄存器1高字节
			00 寄存器1低字节
			4E CRC校验高字节
			8B CRC校验低字节
	*/
		uint16_t addr;
		uint16_t byteAddr;
	  uint16_t bitOffset = 0;
		uint16_t value;

		g_tModS[_ch].RspCode = RSP_OK;
		
		if (g_tModS[_ch].RxCount != 8)
		{
			g_tModS[_ch].RspCode = RSP_ERR_VALUE;		/* 数据值域错误 */
			goto err_ret;
		}

		addr = BEBufToUint16(&g_tModS[_ch].RxBuf[2]); 	/* 寄存器号 */
		value = BEBufToUint16(&g_tModS[_ch].RxBuf[4]);	/* 数据 */

		bitOffset = GetBitOffset(addr,0);
			
		if(g_tModS[_ch].RspCode == RSP_ERR_REG_ADDR) // 寄存器地址错误 
		{	
			goto err_ret;
		}
    //////////////////////////////////////////////////////////////
		byteAddr = addr /8;	// 得到字节地址 
		
		// 写入内存
		if(0 < value) // 数据
		  PLC_RW_RAM_8BIT(byteAddr) |= (1 << (bitOffset % 8));
		else
			PLC_RW_RAM_8BIT(byteAddr) &= ~(1 << (bitOffset % 8));
		
err_ret:
	if (g_tModS[_ch].RspCode == RSP_OK)				/* 正确应答 */
	{
		MODS_SendAckOk(_ch);
	}
	else
	{
		MODS_SendAckErr(_ch,g_tModS[_ch].RspCode);		/* 告诉主机命令错误 */
	}
}

/*
*********************************************************************************************************
*	函 数 名: MODS_06H
*	功能说明: 写单个寄存器
*	形    参: 无
*	返 回 值: 无

*	注：
*	
*	D、D8000、T、C、C200
*********************************************************************************************************
*/
static void MODS_06H(uint8_t _ch)
{
	/*
		写保持寄存器。注意06指令只能操作单个保持寄存器，16指令可以设置单个或多个保持寄存器
		主机发送:
			11 从机地址
			06 功能码
			00 寄存器地址高字节
			01 寄存器地址低字节
			00 数据1高字节
			01 数据1低字节
			9A CRC校验高字节
			9B CRC校验低字节

		从机响应:
			11 从机地址
			06 功能码
			00 寄存器地址高字节
			01 寄存器地址低字节
			00 数据1高字节
			01 数据1低字节
			1B CRC校验高字节
			5A	CRC校验低字节
*/

	uint16_t reg;
	uint16_t value;

	g_tModS[_ch].RspCode = RSP_OK;

	if (g_tModS[_ch].RxCount != 8)
	{
		g_tModS[_ch].RspCode = RSP_ERR_VALUE; // 数据值域错误
		goto err_ret;
	}

	reg = BEBufToUint16(&g_tModS[_ch].RxBuf[2]); //【2高】【3低】寄存器号即寄存器起始地址 
	value = BEBufToUint16(&g_tModS[_ch].RxBuf[4]);	//【4高】【5低】寄存器值

		/* 地址范围判断 */
	 if ( ((reg >= REG_C_START_ADDR) && (reg  <= REG_C_END_ADDR)) ||  // C
	       ((reg >= REG_C200_START_ADDR) && (reg <= REG_C200_END_ADDR)) ||  // C200
	       ((reg >= REG_D8000_START_ADDR) && (reg <= REG_D8000_END_ADDR)) ||  // D8000
	       ((reg >= REG_T_START_ADDR) && (reg <= REG_T_END_ADDR)) ||  // T
	       ((reg >= REG_D_START_ADDR) && (reg <= REG_D_END_ADDR)) )  // D
	 {	 
		 		// 需要修改
				plc_16BitBuf[reg] = value;
	 }
	 else
	 {
			g_tModS[_ch].RspCode = RSP_ERR_REG_ADDR;	// 寄存器地址错误 
	 }
	 
err_ret:
	if (g_tModS[_ch].RspCode == RSP_OK) // 正确应答
	{
		MODS_SendAckOk(_ch);
	}
	else
	{
		MODS_SendAckErr(_ch,g_tModS[_ch].RspCode); /* 告诉主机命令错误 */
	}
}

/*
*********************************************************************************************************
*	函 数 名: MODS_0FH
*	功能说明: 连续写多个线圈
*	形    参: 无
*	返 回 值: 无

*	注：
*	
*	M、M8000、S、T、C、Y
*********************************************************************************************************
*/
static void MODS_0FH(uint8_t _ch)
{
	uint16_t addr;
	uint16_t byteAddr;
	int16_t num;        // 注意：有符号
	uint8_t bytes;
	uint8_t i;
	uint8_t *pStatusBuf = gModbusBitBuf;	
	uint16_t bitOffset = 0;
	int16_t realBitOffset =0;

	g_tModS[_ch].RspCode = RSP_OK;

	if (g_tModS[_ch].RxCount < 10)
	{
		g_tModS[_ch].RspCode = RSP_ERR_VALUE;			/* 数据值域错误 */
		goto err_ret;
	}

	addr = BEBufToUint16(&g_tModS[_ch].RxBuf[2]); 	/* 寄存器号 */
	num = BEBufToUint16(&g_tModS[_ch].RxBuf[4]);		/* 寄存器个数 */
	bytes = g_tModS[_ch].RxBuf[6];					/* 后面的数据体字节数 */

//	if (bytes != 2 * num)
//	{
//		;
//	}
	
	bitOffset = GetBitOffset(addr,num);
	
	/* 1修复，不为8的倍数时处理 */
//	realBitOffset = (bitOffset - (bytes << 3));
//	if(realBitOffset < 0) 
		realBitOffset = bitOffset;  
	
	if(g_tModS[_ch].RspCode == RSP_ERR_REG_ADDR) // 寄存器地址错误 
		goto err_ret;

	// 从接收缓存中得到寄存器值    
	byteAddr = addr /8;	// 得到字节地址
	
	for (i = 0; i < bytes; i++)
			gModbusBuf[i] = g_tModS[_ch].RxBuf[7 + i]; // 寄存器值       

  // 对应位
	i =0; // 清零
	while( num > 0 )
	{
			xMBUtilSetBits( pStatusBuf, realBitOffset,( uint8_t )(num > 8 ? 8 : num), gModbusBuf[i++]);
			num -= 8;
			bitOffset += 8;
	}		

	// 写入内存
	for (i = 0; i < bytes; i++)
		PLC_RW_RAM_8BIT(byteAddr) = gModbusBitBuf[i];
	 
err_ret:
	if (g_tModS[_ch].RspCode == RSP_OK)					/* 正确应答 */
	{
		MODS_SendAckOk(_ch);
	}
	else
	{
		MODS_SendAckErr(_ch,g_tModS[_ch].RspCode);			/* 告诉主机命令错误 */
	}
}


/*
*********************************************************************************************************
*	函 数 名: MODS_10H
*	功能说明: 连续写多个寄存器.  进用于改写时钟
*	形    参: 无
*	返 回 值: 无

*	注：
*	1新增
*	D、D8000、T、C、C200
*********************************************************************************************************
*/
static void MODS_10H(uint8_t _ch)
{
	/*
		主机发送:
			11 从机地址
			10 功能码
			00 寄存器起始地址高字节
			01 寄存器起始地址低字节
			00 寄存器数量高字节
			02 寄存器数量低字节
			04 字节数
			00 数据1高字节
			0A 数据1低字节
			01 数据2高字节
			02 数据2低字节
			C6 CRC校验高字节
			F0 CRC校验低字节

		从机响应:
			11 从机地址
			06 功能码
			00 寄存器地址高字节
			01 寄存器地址低字节
			00 数据1高字节
			01 数据1低字节
			1B CRC校验高字节
			5A	CRC校验低字节
	*/
	uint16_t addr;
	uint16_t num;
//  uint8_t bytes;
	uint8_t i;
	uint16_t value;
	
	g_tModS[_ch].RspCode = RSP_OK;

	if (g_tModS[_ch].RxCount < 11)
	{
		g_tModS[_ch].RspCode = RSP_ERR_VALUE;			/* 数据值域错误 */
		goto err_ret;
	}

	addr = BEBufToUint16(&g_tModS[_ch].RxBuf[2]); 	/* 寄存器号 */
	num = BEBufToUint16(&g_tModS[_ch].RxBuf[4]);		/* 寄存器个数 */
//	bytes = g_tModS[_ch].RxBuf[6];					/* 后面的数据体字节数 */

//	if (bytes != 2 * num)
//	{
//		;
//	}
	
	/* 地址范围判断 */
	if ( ((addr >= REG_C_START_ADDR) && (addr + num <= REG_C_END_ADDR))         ||  // C
			 ((addr >= REG_C200_START_ADDR) && (addr + num <= REG_C200_END_ADDR))   ||  // C200
			 ((addr >= REG_D8000_START_ADDR) && (addr + num <= REG_D8000_END_ADDR)) ||  // D8000
			 ((addr >= REG_T_START_ADDR) && (addr + num <= REG_T_END_ADDR))         ||  // T
			 ((addr >= REG_D_START_ADDR) && (addr + num <= REG_D_END_ADDR)) )           // D
	{	 
		for (i = 0; i < num; i++)
		{
				value = BEBufToUint16(&g_tModS[_ch].RxBuf[7 + 2 * i]);	// 寄存器值       
				plc_16BitBuf[addr + i] = value; // 写入内存
		}
	}
	else
	{
		g_tModS[_ch].RspCode = RSP_ERR_REG_ADDR;	// 寄存器地址错误 
	}
	

err_ret:
	if (g_tModS[_ch].RspCode == RSP_OK)					/* 正确应答 */
	{
		MODS_SendAckOk(_ch);
	}
	else
	{
		MODS_SendAckErr(_ch,g_tModS[_ch].RspCode);			/* 告诉主机命令错误 */
	}
}

/***************************** (END OF FILE) *********************************/
