/*
*********************************************************************************************************
*
*	模块名称 : MODEBUS 通信模块 (从站）
*	文件名称 : modbus_slave.h
*	版    本 : V1.4
*	说    明 : 头文件
*
*	Copyright (C), 2015-2016, 传人记
*
*********************************************************************************************************
*/

#ifndef __MODBUY_SLAVE_H
#define __MODBUY_SLAVE_H

typedef struct
{
	uint8_t RxBuf[MOD_RX_BUF_SIZE];
	uint16_t RxCount;
	uint8_t RxStatus;
	uint8_t RxNewFlag;

	uint8_t RspCode;
	uint16_t  c3_5Timeout; // modbus3.5个字符的时间，超时
	
	uint8_t TxBuf[MOD_TX_BUF_SIZE];
	uint16_t TxCount;
}MODS_T;
extern MODS_T g_tModS[];

extern uint8_t     gModbusBuf[]; 
extern uint8_t     gModbusBitBuf[]; 


/* 函数声明 */
void MODS_Poll(uint8_t _ch);
extern int16_t GetBitOffset(uint16_t addr,uint16_t num); // 获取继电器地址偏移
extern int16_t GetWordOffset(uint16_t addr,uint16_t num);// 获燃拇嫫鳓地址偏移
extern uint8_t xMBUtilGetBits( uint8_t * ucByteBuf, uint16_t usBitOffset, uint8_t ucNBits );
extern void xMBUtilSetBits( uint8_t * ucByteBuf, uint16_t usBitOffset, uint8_t ucNBits, uint8_t ucValue );

#endif

/***************************** 传人记 (END OF FILE) *********************************/
