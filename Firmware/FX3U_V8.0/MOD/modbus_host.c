/*
*********************************************************************************************************
*
*	模块名称 : MODSBUS通信程序 （主机）
*	文件名称 : modbus_host.c
*	版    本 : V1.4
*	说    明 : 无线通信程序。通信协议基于MODBUS

*
*********************************************************************************************************
*/
#include "bsp.h"
#include "main.h"
#include "modbus_host.h"

#include "modbus_slave.h"
#include "PLC_Dialogue.h"
#include "PLC_IO.h"
#include "plc_conf.h"


MODH_T g_tModH[2]; // 定义2路modbus,供主机模式使用

//////////////////////////////////////////////////////////////
void MODH_RxTimeOut(uint8_t _ch);
static void MODH_AnalyzeApp(uint8_t _ch);

// Modbus接收
static void MODH_Read_01H(uint8_t _ch);
static void MODH_Read_02H(uint8_t _ch);
static void MODH_Read_03H(uint8_t _ch);
static void MODH_Read_04H(uint8_t _ch);
static void MODH_Read_05To10H(uint8_t _ch);


// Modbus发送
void MODH_Send_01To04H(uint8_t _ch,uint8_t _addr, uint8_t _funCode, uint16_t _reg, uint16_t _num);
void MODH_Send_05H(uint8_t _ch,uint8_t _addr, uint16_t _reg, uint16_t _place);
void MODH_Send_06H(uint8_t _ch,uint8_t _addr, uint16_t _reg, uint16_t _place);
void MODH_Send_0FH(uint8_t _ch,uint8_t _addr, uint16_t _reg, uint8_t _num, uint16_t _place);/* 写多个线圈 */
void MODH_Send_10H(uint8_t _ch,uint8_t _addr, uint16_t _reg, uint8_t _num, uint16_t _place);


/*
*********************************************************************************************************
*	函 数 名: MODH_SendPacket
*	功能说明: 发送数据包 COM1口
*	形    参: _buf : 数据缓冲区
*			  _len : 数据长度
*	返 回 值: 无
*********************************************************************************************************
*/
void MODH_SendPacket(uint8_t _ch,uint8_t *_buf, uint16_t _len)
{
	RS485_SendBuf(config[_ch][0],_buf, _len);
}

/*
*********************************************************************************************************
*	函 数 名: MODH_SendAckWithCRC
*	功能说明: 发送应答,自动加CRC.  
*	形    参: 无。发送数据在 g_tModH.TxBuf[], [g_tModH.TxCount
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODH_SendAckWithCRC(uint8_t _ch)
{
	uint16_t crc;
	
	crc = CRC16_Modbus(g_tModH[_ch].TxBuf, g_tModH[_ch].TxCount);
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = crc >> 8;
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = crc;	
	
	MODH_SendPacket(_ch,g_tModH[_ch].TxBuf, g_tModH[_ch].TxCount);
}

/*
*********************************************************************************************************
*	函 数 名: MODH_AnalyzeApp
*	功能说明: 分析应用层协议。处理应答。通俗理解为从机回复的数据处理
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODH_AnalyzeApp(uint8_t _ch)
{	
	switch (g_tModH[_ch].RxBuf[1])			/* 第2个字节 功能码 */
	{
		case 0x01:	// 读取线圈状态
		{
			MODH_Read_01H(_ch);
			break;
    }
		case 0x02:	// 读取输入状态
		{
			MODH_Read_02H(_ch);
			break;
		}
		case 0x03:	// 读取保持寄存器 在一个或多个保持寄存器
		{
			MODH_Read_03H(_ch);
			break;
    }
		case 0x04:	// 读取输入寄存器
		{
			MODH_Read_04H(_ch);
			break;
    }
		case 0x05:	// 强制单线圈
		case 0x06:	// 写单个寄存器
		case 0x10:	// 写多个寄存器
		{
			MODH_Read_05To10H(_ch);
			break;
		}
		default: // 不识别的指令
		{
			
			break;
		}
	}
}

 
/*
*********************************************************************************************************
*	函 数 名: ModHostMode_SendDeal
*	功能说明: Modbus主机模式发送处理
*	形    参: _slaveAddr : 从站地址
            _funCode : 功能码
*			      _reg : 寄存器编号
*			      _num : 寄存器个数
            _place:接收数据要存放位置或者要发送的数据位置 

*	返 回 值: 无

*	ADPRW S. S1. S2. S3. S4./D. ； S. 子站号，S1. 功能码，S2. S3. 发送数据地址和点数 ,S4/D 接收数据要存放位置或者要发送的数据位置 

*********************************************************************************************************
*/
void ModHostMode_SendDeal(uint8_t _ch,uint8_t _slaveAddr,uint8_t _funCode, uint16_t _reg, uint16_t _num,uint16_t _place)
{

     // 1、功能码、寄存器或继电器地址判断

     // 2、发送处理
     switch (_funCode)
		 {
						case 1: // 读线圈
						{
								if((_num <= 2000) && _num)  // 最大数量参照FX3U的ADPRW指令
                   MODH_Send_01To04H (_ch,HS_MOD_SAVLE_ADDR,_funCode, _reg, _num);		 // 发送命令
								else
										;//eErrStatus = MB_MRE_REG_NUM;												
								break;							
						}
						case 2: // 读多个离散输入
						{
								if((_num <= 2000) && _num)
										MODH_Send_01To04H (_ch,HS_MOD_SAVLE_ADDR,_funCode, _reg, _num);
								else
										;//eErrStatus = MB_MRE_REG_NUM;
								break;							
						}
						case 3: // 读保持寄存器
						{
								if((_num <= 125) && _num)
										MODH_Send_01To04H (_ch,HS_MOD_SAVLE_ADDR,_funCode, _reg, _num);
								else
										;//eErrStatus = MB_MRE_REG_NUM;
								break;							
						}
						case 4: // 读输入寄存器
						{
								if((_num <= 125) && _num)
										MODH_Send_01To04H (_ch,HS_MOD_SAVLE_ADDR,_funCode, _reg, _num);
								else
										;//eErrStatus = MB_MRE_REG_NUM;
								break;							
						}
						case 5: // 写单个线圈
						{
                // 根据_place值找到软元件 
                MODH_Send_05H (_ch,HS_MOD_SAVLE_ADDR, _reg, _place); // 数据必须为0xFF00或0x0000
								break;							
						}
						case 6: // 写单个保持寄存器
						{                
                MODH_Send_06H (_ch,HS_MOD_SAVLE_ADDR, _reg, _place); 
								break;							
						}
						case 15: // 写多个线圈
						{
								if((_num <= 1968) && _num)
										MODH_Send_0FH(_ch,HS_MOD_SAVLE_ADDR, _reg, _num, _place);  
                else
										;//eErrStatus = MB_MRE_REG_NUM;
								break;							
						}
						case 16: // 写多个保持寄存器
						{
								if((_num <= 123) && _num) 
								{   
                    MODH_Send_10H(_ch,HS_MOD_SAVLE_ADDR, _reg, _num, _place);
								}
                else
                {
										;//eErrStatus = MB_MRE_REG_NUM;
                }
								break;							
						}
						default : // 其它命令暂不支持,功能码不支持出错
						{
								D8125 = 5218;
								break;							
						}
			}
		  /* 支持的指令 */
			//if(D8122 == 5218)
			{
				g_tModH[_ch].fAck =0; // 清接收标志 
				g_tModH[_ch].txStatus =1; // 标记存在发送操作
				g_tModH[_ch].responseTimeoutCnt =1; // 启动超时处理
			}	
}


/*
*********************************************************************************************************
*	函 数 名: MODH_Send01H
*	功能说明: 发送01H指令，查询1个或多个保持寄存器
           发送02H指令，读离散输入寄存器
           发送03H指令，查询1个或多个保持寄存器
           发送04H指令，读输入寄存器

*	形    参: _addr : 从站地址
*	          _funCode:功能码
*			      _reg : 寄存器编号
*			      _num : 寄存器个数
*	返 回 值: 无
*********************************************************************************************************
*/
void MODH_Send_01To04H(uint8_t _ch,uint8_t _addr, uint8_t _funCode, uint16_t _reg, uint16_t _num)
{
	g_tModH[_ch].TxCount = 0;
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _addr;	// 从站地址
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _funCode;	// 功能码
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _reg >> 8;	// 寄存器编号 高字节 
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _reg; // 寄存器编号 低字节
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _num >> 8;	// 寄存器个数 高字节
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _num; // 寄存器个数 低字节 
	
	MODH_SendAckWithCRC(_ch);		/* 发送数据，自动加CRC */
}

/*
*********************************************************************************************************
*	函 数 名: MODH_Send_05H
*	功能说明: 发送05H指令，写强置单线圈
*	形    参: _addr : 从站地址
*			  _reg : 寄存器编号
*			  _value : 寄存器值,2字节
*	返 回 值: 无
*********************************************************************************************************
*/
void MODH_Send_05H(uint8_t _ch,uint8_t _addr, uint16_t _reg, uint16_t _place)
{
	g_tModH[_ch].TxCount = 0;
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _addr;			/* 从站地址 */
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = 0x05;			/* 功能码 */	
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _reg >> 8;		/* 寄存器编号 高字节 */
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _reg;			/* 寄存器编号 低字节 */
	
//	g_tModH[_ch].TxBuf[g_tModH.TxCount++] = _value >> 8;		/* 寄存器值 高字节 */
//	g_tModH[_ch].TxBuf[g_tModH.TxCount++] = _value;			/* 寄存器值 低字节 */
	
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = (plc_16BitBuf[_place] >> 8) & 0x00FF; // 取高8位的数据
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = plc_16BitBuf[_place] & 0x00FF; // 取低8位的数据	
	
	
	MODH_SendAckWithCRC(_ch);		/* 发送数据，自动加CRC */
}

/*
*********************************************************************************************************
*	函 数 名: MODH_Send_06H
*	功能说明: 发送06H指令，写1个保持寄存器
*	形    参: _addr : 从站地址
*			  _reg : 寄存器编号
*			  _value : 寄存器值,2字节
*	返 回 值: 无
*********************************************************************************************************
*/
void MODH_Send_06H(uint8_t _ch,uint8_t _addr, uint16_t _reg, uint16_t _place)
{
	g_tModH[_ch].TxCount = 0;
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _addr;			/* 从站地址 */
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = 0x06;			/* 功能码 */	
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _reg >> 8;		/* 寄存器编号 高字节 */
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _reg;			/* 寄存器编号 低字节 */
	
//	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _value >> 8;		// 寄存器值 高字节 
//	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _value;			// 寄存器值 低字节 	
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = (plc_16BitBuf[_place] >> 8) & 0x00FF; // 取高8位的数据
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = plc_16BitBuf[_place] & 0x00FF; // 取低8位的数据	
	
	MODH_SendAckWithCRC(_ch);		/* 发送数据，自动加CRC */
}

/*
*********************************************************************************************************
*	函 数 名: MODH_Send_0FH
*	功能说明: 发送0FH指令，连续写多个线圈
*	形    参:  _addr : 从站地址
*			  	   _reg : 寄存器编号
*			       _num : 寄存器个数n (每个寄存器2个字节) 值域
*			  _    _place : 线圈的数据位置
*	返 回 值: 无
*********************************************************************************************************
*/
void MODH_Send_0FH(uint8_t _ch,uint8_t _addr, uint16_t _reg, uint8_t _num, uint16_t _place)
{
	uint16_t i;
	int16_t bitOffset;
	int16_t tmpNum = _num;
	
	uint16_t bytes =(_num + 7)/ 8;    // 计算数据字节数
	uint8_t *pStatusBuf = gModbusBitBuf;
	
	g_tModH[_ch].TxCount = 0;
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _addr;		/* 从站地址 */
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = 0x0F;		// 功能码
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _reg >> 8;	/* 寄存器编号 高字节 */
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _reg;		/* 寄存器编号 低字节 */
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _num >> 8;	/* 寄存器个数 高字节 */
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _num;		/* 寄存器个数 低字节 */
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] =  bytes;	/* 数据字节数 */
	
	/* 发送的数据处理 */
	bitOffset = GetBitOffset(_place,_num); // 获取偏移地址, 等于减去某软元件起始地址  
	for(i = 0;i < bytes; i++)
			gModbusBuf[i] = PLC_RW_RAM_8BIT(_place /8 + i); // 读取内存数据，_place /8 得到字节地址 
	/* 对应上相应位 */
	while( tmpNum > 0 )
	{
			*pStatusBuf++ = xMBUtilGetBits( (uint8_t *)gModbusBuf, bitOffset, 
																			( uint8_t )(tmpNum > 8 ? 8 : tmpNum));
			tmpNum -= 8;
			bitOffset += 8;
	}
	/* 把数据赋值给发送区 */
	for (i = 0; i < bytes; i++)
	{
		if (g_tModH[_ch].TxCount > MOD_RX_BUF_SIZE - 3)
		{
			return;		// 数据超过缓冲区超度，直接丢弃不发送 
		}
		g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = gModbusBitBuf[i];
	}
	
	MODH_SendAckWithCRC(_ch);	//发送数据，自动加CRC
}


/*
*********************************************************************************************************
*	函 数 名: MODH_Send_10H
*	功能说明: 发送10H指令，连续写多个保持寄存器. 最多一次支持23个寄存器。
*	形    参: _addr : 从站地址
*			  _reg : 寄存器编号
*			  _num : 寄存器个数n (每个寄存器2个字节) 值域
*			  _buf : n个寄存器的数据。长度 = 2 * n
*	返 回 值: 无
*********************************************************************************************************
*/
void MODH_Send_10H(uint8_t _ch,uint8_t _addr, uint16_t _reg, uint8_t _num, uint16_t _place)
{
	uint16_t i;

	g_tModH[_ch].TxCount = 0;
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _addr;		/* 从站地址 */
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = 0x10;		/* 从站地址 */	
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _reg >> 8;	/* 寄存器编号 高字节 */
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _reg;		/* 寄存器编号 低字节 */
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _num >> 8;	/* 寄存器个数 高字节 */
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = _num;		/* 寄存器个数 低字节 */
	g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = 2 * _num;	/* 数据字节数 */
	
	/* 发送的数据处理 */
	for (i = 0; i < _num; i++)
	{
		if (g_tModH[_ch].TxCount > MOD_RX_BUF_SIZE - 3)
		{
			return;		/* 数据超过缓冲区超度，直接丢弃不发送 */
		}
		g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = plc_16BitBuf[_place + i] / 256; // 取高8位的数据
		g_tModH[_ch].TxBuf[g_tModH[_ch].TxCount++] = plc_16BitBuf[_place + i] % 256; // 取低8位的数据	
	}	
	MODH_SendAckWithCRC(_ch);	/* 发送数据，自动加CRC */
}

/*
*********************************************************************************************************
*	函 数 名: MODH_ReciveNew
*	功能说明: 串口接收中断服务程序会调用本函数。当收到一个字节时，执行一次本函数。
*	形    参: 
*	返 回 值: 1 表示有数据
*********************************************************************************************************
*/
void MODH_ReciveNew(uint8_t _ch,uint16_t _baud,uint8_t _data)
{
	/*
		3.5个字符的时间间隔，只是用在RTU模式下面，因为RTU模式没有开始符和结束符，
		两个数据包之间只能靠时间间隔来区分，Modbus定义在不同的波特率下，间隔时间是不一样的，
		所以就是3.5个字符的时间，波特率高，这个时间间隔就小，波特率低，这个时间间隔相应就大

		4800  = 7.297ms
		9600  = 3.646ms
		19200  = 1.771ms
		38400  = 0.885ms
	*/
	uint32_t timeout;

	g_tModH[_ch].c3_5Timeout = 0;
	
	timeout = 35000000 / _baud;		/* 计算超时时间，单位us 35000000*/
	
	/* 硬件定时中断，定时精度us 硬件定时器5用于MODBUS从机或主机*/
	bsp_StartHardTimer(_ch + 1, timeout, (void(*)(uint8_t))MODH_RxTimeOut);  // TIM5 CC1或者CC2。CC3和CC4暂未使用

	if(g_tModH[_ch].RxCount < MOD_RX_BUF_SIZE)
	{
		g_tModH[_ch].RxBuf[g_tModH[_ch].RxCount++] = _data;
	}
}

/*
*********************************************************************************************************
*	函 数 名: MODH_RxTimeOut
*	功能说明: 超过3.5个字符时间后执行本函数。 设置全局变量 g_rtu_timeout = 1; 通知主程序开始解码。
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
void MODH_RxTimeOut(uint8_t _ch)
{
	g_tModH[_ch].c3_5Timeout = 1;
}

/*
*********************************************************************************************************
*	函 数 名: MODH_Poll
*	功能说明: 接收控制器指令. 1ms 响应时间。
*	形    参: 无
*	返 回 值: 0 表示无数据 1表示收到正确命令
*********************************************************************************************************
*/
void MODH_Poll(uint8_t _ch)
{	
	uint16_t crc1;
	
	// 传人记，20161130新增，等待应答超时
	if(TIMEOUT <= g_tModH[_ch].responseTimeoutCnt)
	{
		 g_tModH[_ch].fAck |= 0x02; // bit1置1标记应答超时
	   goto err_ret;  // MODBUS通信发生出错 M8125 =1	
	}
	
	if (g_tModH[_ch].c3_5Timeout == 0)	/* 超过3.5个字符时间后执行MODH_RxTimeOut()函数。全局变量 g_rtu_timeout = 1 */
	{
		/* 没有超时，继续接收。不要清零 g_tModH.RxCount */
		return ;
	}

	/* 收到命令 */
	g_tModH[_ch].c3_5Timeout = 0;

	if (g_tModH[_ch].RxCount < 4)
	{
		g_tModH[_ch].fAck |= 0x04; // bit2置1标记应答数据错误
		goto err_ret;  // MODBUS通信发生出错 M8125 =1
	}

	/* 计算CRC校验和 */
	crc1 = CRC16_Modbus(g_tModH[_ch].RxBuf, g_tModH[_ch].RxCount);
	if (crc1 != 0)
	{
		g_tModH[_ch].fAck |= 0x08; // bit3置1标记应答数据crc校验错误
		goto err_ret;  // MODBUS通信发生出错 M8125 =1
	}
	
	 gCommLedFlashFlg =2; // 闪烁
	
	/* 分析应用层协议 */
	MODH_AnalyzeApp(_ch);

err_ret:
	g_tModH[_ch].RxCount = 0;	/* 必须清零计数器，方便下次帧同步 */
}


/*
*********************************************************************************************************
*	函 数 名: MODH_Read_01H
*	功能说明: 分析01H指令的应答数据
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODH_Read_01H(uint8_t _ch)
{
	uint16_t i;
	int16_t tmpNum;
	uint8_t bytes;
	uint16_t bitOffset;
	uint8_t *pStatusBuf =gModbusBitBuf;
	
	if (g_tModH[_ch].RxCount > 0)
	{
		bytes = g_tModH[_ch].RxBuf[2];	/* 数据长度 字节数 */			

//    if(bytes != (g_tModH[_ch].num + 7)/8) // 判断 得到的数据长度与发送的数据长度是否相等
//		{	
//		}

		/* 把子站返回的数据写入内存中 */
	  bitOffset = GetBitOffset(g_tModH[_ch].place,g_tModH[_ch].num);

		for (i = 0; i < bytes; i++)
			gModbusBuf[i] = g_tModH[_ch].RxBuf[3+ i]; // 寄存器值       
		// 对应位
		tmpNum = g_tModH[_ch].num; // 避免原始数据参与运算
		while( tmpNum > 0 )
		{
				xMBUtilSetBits( pStatusBuf, bitOffset,( uint8_t )(tmpNum > 8 ? 8 : tmpNum), gModbusBuf[i++]);
				tmpNum -= 8;
				bitOffset += 8;
		}	
		// 写入内存
		for (i = 0; i < bytes; i++)
			PLC_RW_RAM_8BIT(g_tModH[_ch].place /8) = gModbusBitBuf[0];
		
		
		g_tModH[_ch].fAck |= 0x01; // bit0置1标记执行成功 
	}
}

/*
*********************************************************************************************************
*	函 数 名: MODH_Read_02H
*	功能说明: 分析02H指令的应答数据
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODH_Read_02H(uint8_t _ch)
{
	uint16_t i;
	int16_t tmpNum;
	uint8_t bytes;
	uint16_t bitOffset;
	uint8_t *pStatusBuf =gModbusBitBuf;
	
	if (g_tModH[_ch].RxCount > 0)
	{
		bytes = g_tModH[_ch].RxBuf[2];	/* 数据长度 字节数 */				
		
//    if(bytes != (g_tModH[_ch].num + 7)/8) // 判断 得到的数据长度与发送的数据长度是否相等
//		{	
//		}

		/* 把子站返回的数据写入内存中 */
	  bitOffset = GetBitOffset(g_tModH[_ch].place,g_tModH[_ch].num);

		for (i = 0; i < bytes; i++)
			gModbusBuf[i] = g_tModH[_ch].RxBuf[3+ i]; // 寄存器值       
		// 对应位
		tmpNum = g_tModH[_ch].num; // 避免原始数据参与运算
		while( tmpNum > 0 )
		{
				xMBUtilSetBits( pStatusBuf, bitOffset,( uint8_t )(tmpNum > 8 ? 8 : tmpNum), gModbusBuf[i++]);
				tmpNum -= 8;
				bitOffset += 8;
		}	
		// 写入内存
		for (i = 0; i < bytes; i++)
			PLC_RW_RAM_8BIT(g_tModH[_ch].place /8) = gModbusBitBuf[0];
		
		g_tModH[_ch].fAck |= 0x01; // bit0置1标记执行成功
	}
}

/*
*********************************************************************************************************
*	函 数 名: MODH_Read_03H
*	功能说明: 分析03H指令的应答数据
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
void MODH_Read_03H(uint8_t _ch)
{
	uint16_t i;
	uint8_t bytes;
  uint16_t value;
	
	if (g_tModH[_ch].RxCount > 0)
	{
		bytes = g_tModH[_ch].RxBuf[2];	/* 数据长度 字节数 */			
		
		if(bytes != g_tModH[_ch].num *2 ) // 判断 得到的数据长度与发送的数据长度是否相等
		{	
		}
		
		/* 把子站返回的数据写入内存中 */
		//g_tModH[_ch].RxBuf[3];			
		for (i = 0; i < g_tModH[_ch].num; i++)
		{
				value = BEBufToUint16(&g_tModH[_ch].RxBuf[3 + 2 * i]);	// 寄存器值       
				plc_16BitBuf[g_tModH[_ch].place + i] = value; // 写入内存
		}
		
		g_tModH[_ch].fAck |= 0x01; // bit0置1标记执行成功
		
	}
}

/*
*********************************************************************************************************
*	函 数 名: MODH_Read_04H
*	功能说明: 分析04H指令的应答数据 , 功能待完善
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODH_Read_04H(uint8_t _com)
{
//	uint8_t bytes;
//	if (g_tModH[_ch].RxCount > 0)
//	{
//		bytes = g_tModH[_ch].RxBuf[2];	/* 数据长度 字节数 */		

//    // g_tModH[_ch].RxBuf[3]	数据起
//		g_tModH[_ch].fAck |= 0x01; // bit0置1标记执行成功
//	}
}

/*
*********************************************************************************************************
*	函 数 名: MODH_Read_05H
*	功能说明: 分析05H、06H、0FH、10H指令的应答数据
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void MODH_Read_05To10H(uint8_t _ch)
{
	if (g_tModH[_ch].RxCount > 0)
	{
		if (g_tModH[_ch].RxBuf[0] == HS_MOD_SAVLE_ADDR)		
		{
			g_tModH[_ch].fAck |= 0x01; // bit0置1标记执行成功		/* 接收到应答 */
		}
	};
}


