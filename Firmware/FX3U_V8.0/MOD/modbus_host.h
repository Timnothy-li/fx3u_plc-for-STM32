/*
*********************************************************************************************************
*
*	模块名称 : MODEBUS 通信模块 (主机程序）
*	文件名称 : modbus_host.h
*	版    本 : V1.4
*	说    明 : 头文件
*
*	Copyright (C), 2015-2016, 传人记 shop148504253.taobao.com
*
*********************************************************************************************************
*/
#ifndef __MOSBUS_HOST_H
#define __MOSBUS_HOST_H

// 传人记，20161129新增，modbus主机模式时，记录发送时使用的子站号、功能码、发送地址、操作的数量及发送或者接收的数据存放位置
typedef struct
{
	uint8_t RxBuf[MOD_RX_BUF_SIZE];
	uint16_t RxCount;
	uint8_t RxStatus;
	uint8_t RxNewFlag;

	uint8_t RspCode;

	uint8_t TxBuf[MOD_TX_BUF_SIZE];
	uint16_t TxCount;
	uint8_t txStatus; // 是否存在发送操作，1表示发送，0表示未发送
	
	uint16_t  c3_5Timeout; // modbus3.5个字符的时间，超时
	uint16_t responseTimeoutCnt; // modbus,等待应答超时
	/* 应答命令标志 bit0:0表示执行失败，1表示执行成功; 
	               bit1: 1表示应答超时，0表示正常;
								 bit2: 1表示应答数据长度错误，0表示正常;
								 bit3: 1表示应答数据crc错误， 0表示正常
	*/
	uint8_t fAck;		
	
	uint8_t retryCnt; // 记录重试次数

  uint16_t slaveAddr;
	uint8_t  funCode;
	uint16_t addr;	
	uint16_t num;
	uint16_t place;
}MODH_T;

extern MODH_T g_tModH[];


/* 函数声明 */
extern void MODH_Poll(uint8_t _ch);
extern void ModHostMode_SendDeal(uint8_t _ch,uint8_t _slaveAddr,uint8_t _funCode, uint16_t _reg, uint16_t _num,uint16_t _place);


#endif

/***************************** 传人记 shop148504253.taobao.com (END OF FILE) *********************************/
