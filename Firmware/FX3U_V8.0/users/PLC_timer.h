/************************************************************
库名称:    PLC_timer
功能描述： PLC宏处理
作　者:    传人记
日　期:    2017年4月26日
版  本:    1.0.1
************************************************************/
#ifndef __PLC_TIMER_H__
#define __PLC_TIMER_H__

#include "stm32f10x.h"

extern void T_1MS(void);
extern void T_10MS(void);
extern void T_100MS(void);
extern void T_H100MS(void);


#endif /* #ifndef __PLC_TIMER_H__ */






