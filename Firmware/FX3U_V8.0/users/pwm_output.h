#ifndef __PWM_OUTPUT_H
#define	__PWM_OUTPUT_H

#include "stm32f10x.h"
#include "stm32f10x_tim.h"

#include "plc_conf.h"
#include "PLC_Dialogue.h"

#if PWM_OUTPUT_FUNC

typedef enum _Pluse_Mode
{
	ePWM_MODE,
	ePTY_MODE,
	ePTR_MODE,
}Pluse_Mode;

typedef enum _Plsr_Mode
{
	eTRIOD_MODE,
	eTRI_MODE,
}Plsr_Mode;

typedef enum _Pluse_Stage
{
	eACC_STAGE,
	ePLAIN_STAGE,
	eDEC_STAGE,
}Pluse_Stage;


typedef struct _pulse_Pro{
	 u32 used;
   u16 Gpio_pin;   //输出IO口
   u16 Gpio_pin2;   //FOR DIRV
   Pluse_Mode mode;       //PWM 还是  PTO 
   u32 CurPto;     //当前已发脉冲个数  //  只对  PTO 有效
	 u32 TargetPto;  //目标脉冲个数      //  只对  PWM 有效
	
	 u32 Place;      //启动位置  这个是关键
	 u32 PWM_Width;  //only for pwm 脉冲宽度
	 u32 PWM_Cycle;  //only for pwm  脉冲最大频率
	 u32 Last_PWM_Cycle;
	
	 u32 Ptr_Raise_time;  //上升时间
	 u32 Interploter;  //  根据 脉冲频率算出来的 插补周期 
	 u32 Stage;       //处于哪个阶段  是  加速段  匀速段  减速段   三角加减速 没有 匀速阶段
	 u32 StageCurPto; //每个阶段的当前脉冲数据
	 u32 StageTartag; //一个阶段目标脉冲数目
	 u32 StageCurFre; //当前阶段的脉冲频率
	 u32 IncrementPl;//acc/dec 每次增加的脉冲个数
	 u32 IncrementTimes;//acc/dec	  加速和减速时间  
   u32 PLSRMODE;//  是梯形加减速   还是三角加减速
	 u32 PLSR_MaxFreq;// PLSR脉冲输出最大频率
	 u32 IncrementCurTimes;//当前已经脉冲频率改变的次数 IncrementTimes一起用判断处于什么阶段
	 u32 PlainCurFreq;     //匀速段的频率  和 脉冲个数计算
	 u32 PlainPulseCount;
	 u32 AccPulseCount;
	 u32 DecPulseCount;	 
}pulse_Pro;
 
extern pulse_Pro gPulse_Pro[2];//全局处理结构体
void PWM_Y0_Restore_Config(void);//恢复IO口的设置
void PWM_Y1_Restore_Config(void);
void PWM_Plc_Pro(pulse_Pro *StPwm,FunctionalState NewState);
//添加全局结构体初始化
void PLSY_Plc_Pro(pulse_Pro *StPwm,FunctionalState NewState);
void PLSR_Plc_Pro(pulse_Pro *StPty,FunctionalState NewState);
void Encoder_A0_Configuration(void);

void PTO_Plc_Plan_Pro(pulse_Pro *PtoPty);//PLSR_MaxFreq
void PulseOutputInit(void);

#endif

#endif /* __PWM_OUTPUT_H */


