/************* IOI2C.c file**************
功能：
提供I2C接口操作API 。
使用IO模拟方式
硬件连接线是 ：
PB6     -->   SCL
PB7     -->   SDA
*****************************************/
#include <stm32f10x.h>
#include "usart.h"		 
#include "PLC_IO.h"

#define IIC_SCL   BIT_ADDR(0x010C0C,6)    //IIC_SCL输出 

#define IIC_SDA    BIT_ADDR(0x010C0C,7)   //IIC_SDA输出 
#define READ_SDA   BIT_ADDR(0x010C08,7)   //IIC_SDA输入

//IO方向设置
#define SDA_IN()  {GPIOB->CRL&=0X0FFFFFFF;GPIOB->CRL|=0x80000000;}
#define SDA_OUT() {GPIOB->CRL&=0X0FFFFFFF;GPIOB->CRL|=0x30000000;}

void delay_us(unsigned char x) { while((x)--){unsigned int i=10;while((i)--);}}    //微秒延时程序 
/**************************实现函数********************************************
*函数原型:		void IIC_Init(void)
*功　　能:		初始化I2C对应的接口引脚。
*******************************************************************************/
void IIC_Init(void)
{			
	GPIO_InitTypeDef GPIO_InitStructure;			      //定义引脚结构体对象
 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);//使能APB2线上GPIOB口的时钟		     
 	//配置PB6 PB7 为开漏输出  刷新频率为10Mhz
 	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;	
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;       
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  //应用配置到GPIOB 
  GPIO_Init(GPIOB, &GPIO_InitStructure);
}

/**************************实现函数********************************************
*函数原型:		void IIC_Start(void)
*功　　能:		产生IIC起始信号
*******************************************************************************/
void IIC_Start(void)
{
	SDA_OUT();     //sda线输出
	IIC_SDA=1;	  	  
	IIC_SCL=1;
	delay_us(5);
 	IIC_SDA=0;//START:when CLK is high,DATA change form high to low 
	delay_us(5);
	IIC_SCL=0;//钳住I2C总线，准备发送或接收数据 
}

/**************************实现函数********************************************
*函数原型:		void IIC_Stop(void)
*功　　能:	    //产生IIC停止信号
*******************************************************************************/	  
void IIC_Stop(void)
{
	SDA_OUT();//sda线输出
	IIC_SCL=0;
	IIC_SDA=0;//STOP:when CLK is high DATA change form low to high
 	delay_us(5);
	IIC_SCL=1; 
	IIC_SDA=1;//发送I2C总线结束信号
	delay_us(5);							   	
}

/**************************实现函数********************************************
*函数原型:		u8 IIC_Wait_Ack(void)
*功　　能:	  等待应答信号到来 
*******************************************************************************/
void IIC_Wait_Ack(void)
{
	u8 ucErrTime=0;
	SDA_IN();      //SDA设置为输入  
	IIC_SDA=1;delay_us(6);	   
	IIC_SCL=1;delay_us(6);	 
	while(READ_SDA)
	{
		if(++ucErrTime>250)IIC_Stop();
	  delay_us(1);
	}
	IIC_SCL=0;//时钟输出0 	   
} 

/**************************实现函数********************************************
*函数原型:		void IIC_Ack(void)
*功　　能:	    产生ACK应答
*******************************************************************************/
void IIC_Ack(void)
{
	IIC_SCL=0;
	SDA_OUT();
	IIC_SDA=0;
	delay_us(5);
	IIC_SCL=1;
	delay_us(5);
	IIC_SCL=0;
}
	
/**************************实现函数********************************************
*函数原型:		void IIC_NAck(void)
*功　　能:	  产生NACK应答
*******************************************************************************/	    
void IIC_NAck(void)
{
	IIC_SCL=0;
	SDA_OUT();
	IIC_SDA=1;
	delay_us(5);
	IIC_SCL=1;
	delay_us(5);
	IIC_SCL=0;
}					 				     

/**************************实现函数********************************************
*函数原型:		void IIC_Send_Byte(u8 txd)
*功　　能:	    IIC发送一个字节
*******************************************************************************/		  
void IIC_Send_Byte(u8 txd)
{                        
    u8 t;   
	SDA_OUT(); 	    
    IIC_SCL=0;//拉低时钟开始数据传输
    for(t=0;t<8;t++)
    {              
        IIC_SDA=(txd&0x80)>>7;
        txd<<=1; 	  
		    delay_us(5);   
		    IIC_SCL=1;
		    delay_us(5); 
		   IIC_SCL=0;	
    }	 
} 	 
   
/**************************实现函数********************************************
*函数原型:		u8 IIC_Read_Byte(unsigned char ack)
*功　　能:	    //读1个字节，ack=1时，发送ACK，ack=0，发送nACK 
*******************************************************************************/  
u8 IIC_Read_Byte(unsigned char ack)
{
	 unsigned char i,receive=0;
	 SDA_IN();//SDA设置为输入
   for(i=0;i<8;i++ )
	 {
        IIC_SCL=0; 
        delay_us(5);
		    IIC_SCL=1;
        receive<<=1;
        if(READ_SDA)receive++;   
		    delay_us(5); 
    }
   /*******发送ACK****发送nACK***/  
   (ack) ? IIC_Ack() : IIC_NAck();
    return receive;
}
/**************************实现函数********************************************
*函数原型:	u8 KS103_ReadOneByte(u8 address, u8 reg)
*功　　能:	读取寄存器2、3数据，获得距离值
*******************************************************************************/  
u8 KS103_ReadOneByte(u8 address, u8 reg)
{
    u8 temp=1;
    IIC_Start();
    IIC_Send_Byte(address);     //发送低地址
    IIC_Wait_Ack();	  
    IIC_Send_Byte(reg);         //发送低地址
    IIC_Wait_Ack();
    IIC_Start();
    IIC_Send_Byte(address + 1); //进入接收模式
    IIC_Wait_Ack();
    delay_us(50);               //增加此代码通信成功！！！
    temp=IIC_Read_Byte(0);      //读寄存器 3
    IIC_Stop();                 //产生一个停止条件
    return temp;
}
/**************************实现函数********************************************
*函数原型:		void KS103_WriteOneByte(u8 address,u8 reg,u8 command)
*功　　能:	    发送一个字节的指令到指定地址 
*******************************************************************************/  
void KS103_WriteOneByte(u8 address,u8 reg,u8 command)
{
    IIC_Start();
    IIC_Send_Byte(address);//发送写命令
    IIC_Wait_Ack();
    IIC_Send_Byte(reg);    //发送高地址
    IIC_Wait_Ack();
    IIC_Send_Byte(command);//发送低地址
    IIC_Wait_Ack();
    IIC_Stop();            //产生一个停止条件
}

//------------------End of File----------------------------
