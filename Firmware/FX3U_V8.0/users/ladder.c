/********************************************************/
// CPU需要：STM32F103--RAM内存不小于64K	Flash内存不小于128K
// 本代码已在STM32F103RDT6、VET6测试通过

/********************************************************/

#include "stm32f10x.h"
#include <stdio.h>
#include <absacc.h> 
#include "PLC_IO.h"
#include "PLC_Dialogue.h"
#include "math.h"          //数学函数库 
#include <string.h>

#include "Gray.h"          //格雷码转换库
#include "plc_conf.h"

#include "modbus_host.h"
#include "bsp_uart_fifo.h"

#include "encode.h"
#include "pwm_output.h"
#include "bsp_timer.h"

const unsigned char PLC_BIT_OR[]={0X01,0X02,0X04,0X08,0X10,0X20,0X40,0X80}; // bit0~bit7
const unsigned char PLC_BIT_AND[]={0XFE,0XFD,0XFB,0XF7,0XEF,0XDF,0XBF,0X7F};



// 423新增

const uint8_t  HEX_ASC []={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
const uint8_t  ASC_HEX0[]={0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
						               0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
						               0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
						               0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
						               0x00,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f};
// 译码、编码
const u16 DE[]={0x0000,0x0001,0x0003,0x0007,0x000F,0X001F,0X003F,0X007F,0X00FF,0X01FF,0X03FF,0X07FF,0X0FFF,0X1FFF,0X3FFF,0X7FFF,0XFFFF};	
const uint8_t  SEGD_LED[]={0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x27,0x7f,0x6f,0x77,0x7c,0x39,0x5e,0x79,0x71};
							 
extern void RST_T_D_C_M_data(void);	 //ADD 20151214
extern unsigned char Y0P,Y1P;                //
extern unsigned short Plus_CMP0,Plus_CMP1;   //脉冲标志位
extern u8  X_DIY;	                           //滤波时间
extern u16 plc_scan_time;                     //扫描时间
extern void RTC_Set(u16 syear,u8 smon,u8 sday,u8 hour,u8 min,u8 sec);//时间修改程序
extern void PLC_IO_Refresh(void);            //IO刷新输出程序

extern void timer_enable(u16 timer_number);	 // 此处在定时器2里面，用于开启定时器处理工
extern void timer_disble(u16 timer_number);	 // 此处在定时器2里面     
static u8 PLC_ACC_BIT,PLC_MPS_BIT;           // 程序执行专用(运算栈及分线栈)
static const u16 *PLC_Addr;                  // PLC程序指针
static const u16 *PLC_Err;                   // PLC出错步
static u8 T_number,C_number;                 // T&C地址缓存寄存器
static u16 T_value;                          // T比较缓存寄存器
static u16 C_value;                          // C比较缓存寄存器
static u32 mov_d_addr;                       // K?M&Y&S&X指令缓存
static const u16 *PLC_P_Addr[129];	         // 方便调子程序取指针
static const u16 *p_save[129];               // 调子程序时保存上一个执行点位

u8  Flag_bit=0xff,Transfer_bit,Transfer_bit1;// 其实就是k的标志位  减小函数的量，减轻CPU负担     

u16 process[64];                             // 调子程序时保存上一个子程序值
u32 trade;                                   // 作用于加减法，减小函数的量，减轻CPU负担     
u16 Transfer=0;                              // 作用多地传递和成批传递 减小函数的量，减轻CPU负担  
u8 edit_prog;                                // 从新编程缓存寄存器
extern u8 Write_Pro_flag;

float_union FLOAT;
s32_union   u32data,u32data1;
u64_union   u64data,u64data2;

/***************************************************FOR**************************************************/
struct 
{
	const u16 *Addr[7];  //FOR 地址记录
	u16 cycle[7];        //当前循环的次数
	u16 count[7];        //目标循环的次数
	u8  point;           //for指向的点数	
} FOR_CMD;

/***************************************************STL**************************************************/
static u16 PLC_STL_Addr;	    // STL指令地址号
static u8  PLC_STL_Status;    // STL指令当前状态 0整套程序没有STL状态，程序1为STL有状态，2为STL停止状态 
static u8  PLC_STL_CMD;		    // STL标志
static u8  PLC_STL_Count;     // 计数线圈数量
static u16 PLC_STL_Coil[256]; // 线圈缓存寄存器
/********************************************************************************************************/

//317编写
struct MC_MCR
{     
//	 uint16_t MC_Addr;    // MC指令地址号
	 uint16_t PLC_MC_BIT; // MC指令当前状态 0整套程序没有MC状态，程序1为MC有状态，2为MC停止状态 
	 uint8_t  MC_Flg;     // MC标志
	 int8_t  MC_SFR;	    // MC累加栈
} MC;


static u8 PLC_PL_BIT_TEST(u16 x){return((step_address[(x)/8] & PLC_BIT_OR[(x)%8])) ? (1) : (0);}
static u8 PLC_LD_BIT(u16 x){return((PLC_RAM8(RAM_ADDR+((x)/8)) & PLC_BIT_OR[(x)%8])) ? (1) : (0) ;}

static u8 PLC_LDP_TEST(void)	                        //查看是不上升沿
{ 
	if(PLC_PL_BIT_TEST(PLC_Addr-PLC_LAD_START_ADDR)==off)//上升沿判断
	{ 
		   if(PLC_ACC_BIT&0X01)			                      //当前值判断
	     {
				 PLC_PL_BIT_ON(PLC_Addr-PLC_LAD_START_ADDR);   //
         return 1;
	     }
			 else return 0;
	 }
	 else
	 {
			if(!(PLC_ACC_BIT&0x01))						             //当前值判断
		  PLC_PL_BIT_OFF(PLC_Addr-PLC_LAD_START_ADDR);    //
			return 0;
	 } 
}

/*********************************************
  函数功能：PLC代码错误处理程序
  err_id=01:指令出错(未识别指令)
  err_id=02:指令出错(暂不支持指令)
  err_id=10:数据出错(无法识别数据类型)
  err_id=11:数据出错(数据读取地址超出)
  err_id=12:数据出错(变址Z地址未知)
  err_id=13:数据出错(变址Z地址超出)
  err_id=20:CJ指令地址出错
  D8061,M8061=PC硬件错误
  D8063,M8063=链接,通信错误
  D8064,M8064=参数错误
  D8065,M8065=语法错误
  D8066,M8066=回路错误
  D8067,M8067=运算错误
  D8068,M8068=运算错误锁存
***********************************************/
void PLC_PROG_ERROR(u16 err,u16 err_id)
{
// PLC_BIT_ON(err);                              //出错标志   屏蔽
 D8012=0;	                                     //扫描时间
 if (D8068==0)D8067=err_id;                      //语法错误
 if (D8068==0)D8068=(PLC_Err-(u16*)(0x800605D)); //保存出错PC步
 D8069=D8068;
}

static void LD(u16 start_addr) // 起始地址，加元件编号值
{ 
	 // 为STL状态区，全局步进
	 if( (PLC_STL_Status == 1) || (MC.MC_Flg == 1)) 
	 {  
		 if(PLC_STL_Status == 1)
		 {
				PLC_ACC_BIT<<=1;
				if(PLC_BIT_TEST(start_addr)&& PLC_BIT_TEST(PLC_STL_Addr))     
						PLC_ACC_BIT |=0x01;
		 }
#if DEBUG
#warning "7月20日新增，MC和MCR指令";
#endif
		 if (MC.MC_Flg == 1) // MC,MCR指令
		 {
//					if(PLC_BIT_TEST(start_addr)&& PLC_BIT_TEST(MC.MC_Addr)) 
				 if(PLC_BIT_TEST(start_addr)&& (MC.PLC_MC_BIT & 0x01))     						
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x01);
				 else				 
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x00);
		 }
	 }	
   else
   {  
			PLC_ACC_BIT<<=1;
			if(PLC_BIT_TEST(start_addr)) 
					PLC_ACC_BIT |=0x01;
	 }		 		
}


static void LDI(u16 start_addr)
{ 
	if( (PLC_STL_Status == 1) || (MC.MC_Flg == 1)) 
	{
			if(PLC_STL_Status == 1)                          //为STL状态区  全局步进
			{   
				PLC_ACC_BIT<<=1;
				if((!(PLC_BIT_TEST(start_addr)))&&(PLC_BIT_TEST(PLC_STL_Addr)))
				PLC_ACC_BIT |=0x01;
			}
#if DEBUG
#warning "�2019年03月20日新增，MC和MCR指令";
#endif
		 if (MC.MC_Flg == 1) // MC,MCR指令
		 {
				if(PLC_BIT_TEST(start_addr)&& (MC.PLC_MC_BIT & 0x01))     		  
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x00);
				 else				 
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x01);
		 }
	}
	else	
	{		
		PLC_ACC_BIT<<=1;
		if(PLC_BIT_TEST(start_addr));
		else		
			PLC_ACC_BIT |=0x01;
	}
}

void AND(u16 start_addr)
{ 
	if((PLC_BIT_TEST(start_addr))&&(PLC_ACC_BIT&0X01)) 
	PLC_ACC_BIT|=0X01;
	else
	PLC_ACC_BIT&=0XFE; 
}

static void ANI(u16 start_addr)
{ 
	if((!(PLC_BIT_TEST(start_addr)))&&(PLC_ACC_BIT&0X01)) 
	PLC_ACC_BIT|=0X01;
	else
	PLC_ACC_BIT&=0XFE; 
}

static void OR(u16 start_addr)
{ 
	if((PLC_BIT_TEST(start_addr))||(PLC_ACC_BIT&0X01))
	PLC_ACC_BIT|=0X01;
	else
	PLC_ACC_BIT&=0XFE; 
}

static void ORI(u16 start_addr)
{ 
	if((!(PLC_BIT_TEST(start_addr)))||(PLC_ACC_BIT&0X01)) 
	PLC_ACC_BIT|=0X01;
	else
	PLC_ACC_BIT&=0XFE; 
}

void OUT(u16 start_addr)
{	
   if (PLC_STL_CMD == 1)                 //判断是不是进入步进模式
	 {
      if (PLC_STL_Status == 1)           //是不是打开STL模式
			{
				 if(start_addr < 0X000A)//判断开始步进 S000-S009
			   {
             if((PLC_ACC_BIT&0x01)==0x01)
             {	
							   PLC_BIT_OFF(PLC_STL_Addr); //OFF			      
							   PLC_BIT_ON(start_addr);    //ON						 
				     }
			   }
				 else 
				 {
					   if(PLC_BIT_TEST(PLC_STL_Addr))
					   {						 
				        if((PLC_ACC_BIT&0x01)==0x01)             
								{ 
									PLC_BIT_ON(start_addr); //ON
								  PLC_STL_Coil[PLC_STL_Count++]=start_addr;//记录步进中ON线圈地址 位下个步进成立清除用
								}       
				        else 
 								  PLC_BIT_OFF(start_addr); //OFF 
						 }

				 }
     }
     else
		 {
				 if(start_addr < 0X000A)//判断开始步进 S000-S009
			   {
				     if(PLC_ACC_BIT & 0x01)
				     {
				       PLC_BIT_ON(start_addr); //ON
				     }
				 }
				 else
				 {
				    if(PLC_ACC_BIT&0x01)
				       PLC_BIT_ON(start_addr); //ON 
						else 
						   PLC_BIT_OFF(start_addr); //OFF 
				 }
     }
	 }
	 else
	 {
       if(PLC_ACC_BIT&0X01)
					PLC_BIT_ON(start_addr); //ON
	     else
					PLC_BIT_OFF(start_addr); //OFF 
   }
}


static void BIT_SET(u16 start_addr)//位设置
{ 
	u8 temp;
	if(PLC_ACC_BIT&0x01)
	{
		if (PLC_STL_Status == 1) // 为STL状态区
		{
			for(temp=0;temp<=PLC_STL_Count;temp++)
				PLC_BIT_OFF(PLC_STL_Coil[temp]); //清除上次ON线圈状态
			
			PLC_BIT_OFF(PLC_STL_Addr); // OFF
			PLC_BIT_ON(start_addr); // ON
			PLC_STL_Count=0; // 清除上次记录ON线圈数量
		}
		else
    {			
			PLC_BIT_ON(start_addr); // 0N
		}
	}
}


static void RST(u16 start_addr)//复位位
{ 
  if((PLC_ACC_BIT&0X01)==0X01)
  PLC_BIT_OFF(start_addr);     //OFF 
}

static void RET(void)
{  
  PLC_STL_Status =0;               //退出步进模式 让程序进入梯形图
}

void STL(u16 start_addr)	      //步进 模式
{
	PLC_STL_CMD = 1;            //全局程序启用步进标志
	PLC_STL_Status = 1;         //启动步进模式
	PLC_STL_Addr = start_addr;  //记录步进地址
	PLC_ACC_BIT<<=1;
	if(PLC_BIT_TEST(PLC_STL_Addr))     
			PLC_ACC_BIT |=0x01;
}

// 传人记，20160926优化
static void other_function(u8 process_addr)
{
	 switch(process_addr)
   { 
		case 0xF8: //块串联 ANB 
    {			
			PLC_ACC_BIT = (PLC_ACC_BIT >> 1)   & ((PLC_ACC_BIT & 0x01)|0xFE);           
			break;  
		}
		case 0xF9: //块并联 ORB
    {			
			PLC_ACC_BIT = (PLC_ACC_BIT >> 1)   | (PLC_ACC_BIT & 0x01);                  
			break;  
	  }
		case 0xFA: //进栈   MPS
    {			
			PLC_MPS_BIT = (PLC_MPS_BIT << 1)   | (PLC_ACC_BIT & 0x01);                  
			break;  
		}
		case 0xFB: //读栈   MRD
    {			
			PLC_ACC_BIT = (PLC_ACC_BIT & 0xfe) | (PLC_MPS_BIT & 0x01);                  
			break;	
		}
		case 0xFC: //出栈   MPP
    {			
			PLC_ACC_BIT = (PLC_ACC_BIT & 0xfe) | (PLC_MPS_BIT & 0x01),PLC_MPS_BIT >>= 1;
			break;  
		}
		case 0xFD: // 取反   INV  
		{
			PLC_ACC_BIT = (PLC_ACC_BIT & 0xfe) | (~PLC_ACC_BIT & 0x01);                 
			break;
		}
		case 0xFF: //取反   POP  
    {			
			break;  
		}
		default:
		{
			PLC_PROG_ERROR(M8064,02);                                                        
		  break;
		}
   }
}


static void LPS(void)              //M1536~M3071位LPS指令函数
{ 
	 if(PLC_ACC_BIT&0x01)
	 {
		  if(PLC_PL_BIT_TEST(PLC_Addr-PLC_LAD_START_ADDR)==0)
		  {
			   PLC_PL_BIT_ON(PLC_Addr-PLC_LAD_START_ADDR);
			   PLC_BIT_ON((0x2fff&*PLC_Addr));	 
		  }
			else{PLC_BIT_OFF((0x2fff&*PLC_Addr));}
	 }
	 else{PLC_PL_BIT_OFF(PLC_Addr-PLC_LAD_START_ADDR);}
	 PLC_Addr++;
}

static void LPF(void)              //M1536~M3071位LPS指令函数
{ 
   if(PLC_ACC_BIT&0x01)
	 {
		if(PLC_PL_BIT_TEST(PLC_Addr-PLC_LAD_START_ADDR)==0)
		{PLC_PL_BIT_ON(PLC_Addr-PLC_LAD_START_ADDR);}
	 }
	 else
	 {
		   if(PLC_PL_BIT_TEST(PLC_Addr-PLC_LAD_START_ADDR)) 
		   {
			   PLC_PL_BIT_OFF(PLC_Addr-PLC_LAD_START_ADDR);	 
		       PLC_BIT_ON((0x2fff&*PLC_Addr));
		   }	 
		   else{PLC_BIT_OFF((0x2fff&*PLC_Addr));} 
	 }
	 PLC_Addr++;
}

// 8注：有可能有问题？？？
static void RESET_T(u8 process_addr) //定时器复位
{  
	if(PLC_ACC_BIT&0x01)//当前值是否有效
	{
		PLC_BIT_OFF(0x600 + process_addr);//溢出线圈
		
		PLC_BIT_OFF(0x2600 + process_addr);//使能线圈 0x1600？
		PLC_BIT_ON(0x2300 + process_addr); //复位线圈 0x3800？
		
		plc_16BitBuf[0x0800 + process_addr]=0;//实际计数即当前值
	}
	else
	{
	  PLC_BIT_OFF(0x2300 + process_addr);//复位线圈 0x3800？
	}
}

// 0423
static void RESET_C(u8 process_addr) //定时器复位
{ 
	static u16 *p_data;
	if((PLC_ACC_BIT&0x01)==0x01) //当前值是否有效
	{ 
		if((process_addr >= 200) && (process_addr <= 255))
		{
			/* 3新增，编码器 */
#if ENCODE_FUNC ==1			
			if(C_number == 236)
			{
					if(S_Cap_Pulse_Pro[0].used==1)
					{
							Encoder_A0_Restore();
						  C236 =0;	
							S_Cap_Pulse_Pro[0].used=0;
					}
			}
		  else if(C_number == 239)
			{								
				 if(S_Cap_Pulse_Pro[1].used==1)
				 {
						Encoder_A1_Restore();
					  C239 =0;	
						S_Cap_Pulse_Pro[1].used=0;
				 }
			}
		  else if(C_number == 251)
			{
				 if(S_Cap_Pulse_Pro[0].used==1)
				 {
						 Encoder_AB0_Restore();
						 S_Cap_Pulse_Pro[0].used=0;
				 }
			}
			else if(C_number == 253)
			{
					if(S_Cap_Pulse_Pro[1].used==1)
					{
						 Encoder_AB1_Restore();
						 S_Cap_Pulse_Pro[1].used=0;
					}
			}
//			else
#endif /* #if ENCODE_FUNC ==1	*/
//			{
				p_data =plc_16BitBuf + 0x0500 + process_addr; //指向值地址
				*p_data =0; //清零地址 高位
				p_data +=1; //因为是32位
				*p_data =0; //清零地址 低位
				PLC_BIT_OFF( 0x00E0 + process_addr); //指向溢出线圈并清溢出线圈 
//			}
		}
		else
		{
				p_data=plc_16BitBuf + 0x0500+process_addr; //指向值地址
				*p_data=0; //清零地址
				PLC_BIT_OFF(0x00E0+process_addr); //指向溢出线圈并清溢出线圈               
		}
	}
	OUT(0X3700+process_addr);
}

static void RST_T_C(void)                            //所有T_C位RST指令函数
{  
	switch(*PLC_Addr/0x100)
	{
		case 0x86: RESET_T(*PLC_Addr),PLC_Addr++;break;//复位T
		case 0x8E: RESET_C(*PLC_Addr),PLC_Addr++;break;//复位C
	}
}

 
static void MOV_TO_K_H(u8 i,u32 data,u8 addr)//进行 MOV ?? K?X&Y&S&M 地址计算
{  
	u8 LL_BIT;                                 //需要向左移动多少位用寄存器
	u16 JOB_ADDR;
	int64_t MOV_DATA_64BIT,MOV_DATA_64BIT_BACKUP,MOV_DATA_BACKUP1;  //移动32位数据
	mov_d_addr |=addr<<8; 
	mov_d_addr +=Transfer;                      //成批传递和多点时 Transfer会有数据平时都是0
	LL_BIT =mov_d_addr%0x20;						        //需要移动多少位
	JOB_ADDR =(mov_d_addr/0x20)*4;              //所在的起始地址  
	
	switch(i)							 
	{ //移动位数,需要的数据做好处理,将数据移动需要的位数,后面要将数据取反用
		case 0x82: // 传送K1即4位
		{
			MOV_DATA_64BIT_BACKUP =data&0X0000000F;
		  MOV_DATA_64BIT_BACKUP <<=LL_BIT;
		  MOV_DATA_BACKUP1 =~(0X0000000F<<LL_BIT);
   		break;
		}
		case 0x84: // 传送K2即8位
		{
			MOV_DATA_64BIT_BACKUP =data&0X000000FF;
		  MOV_DATA_64BIT_BACKUP <<=LL_BIT;
		  MOV_DATA_BACKUP1 =~(0X000000FF<<LL_BIT); 
		  break;
		}
		case 0x86: // 传送K3即12位
		{
			MOV_DATA_64BIT_BACKUP=data&0X00000FFF;
		  MOV_DATA_64BIT_BACKUP<<=LL_BIT;
		  MOV_DATA_BACKUP1=~(0X00000FFF<<LL_BIT); 
		  break;
		}
		case 0x88: // 传送K4即16位
		{
			MOV_DATA_64BIT_BACKUP =data&0X0000FFFF;
			MOV_DATA_64BIT_BACKUP <<=LL_BIT;
			MOV_DATA_BACKUP1 =~(0X0000FFFF<<LL_BIT); 
			break;
		}
		case 0x8A: //传送K5即20位
		{
			MOV_DATA_64BIT_BACKUP=data&0X000FFFFF;
		  MOV_DATA_64BIT_BACKUP<<=LL_BIT;
		  MOV_DATA_BACKUP1=~(0X000FFFFF<<LL_BIT); 
		  break;
		}
		case 0x8C: //传送K6即24位
		{
			MOV_DATA_64BIT_BACKUP=data&0X00FFFFFF;
		  MOV_DATA_64BIT_BACKUP<<=LL_BIT;
		  MOV_DATA_BACKUP1=~(0X00FFFFFF<<LL_BIT); 
		  break;
		}
		case 0x8E: //传送K7即28位
		{
			MOV_DATA_64BIT_BACKUP=data&0X0FFFFFFF;
		  MOV_DATA_64BIT_BACKUP<<=LL_BIT;
		  MOV_DATA_BACKUP1=~(0X0FFFFFFF<<LL_BIT); 
		  break;
		}
		case 0x90: //传送K8即32位
		{
			MOV_DATA_64BIT_BACKUP=data&0XFFFFFFFF;
		  MOV_DATA_64BIT_BACKUP<<=LL_BIT;
		  MOV_DATA_BACKUP1=~(0XFFFFFFFF<<LL_BIT); 
		  break;
		}
		default: 
    {			
			PLC_Addr+=3; 
		  break;  //遇到不支持的命令
		}
	}                                          
	MOV_DATA_64BIT=PLC_RAM64(RAM_ADDR+JOB_ADDR);
	MOV_DATA_64BIT&=MOV_DATA_BACKUP1;             //将需要传送的位置清空 
	MOV_DATA_64BIT|=MOV_DATA_64BIT_BACKUP;        //传入需要到的位置   
	PLC_RAM64(RAM_ADDR+JOB_ADDR)=MOV_DATA_64BIT;  //将数据传到目标位置
}
 

static signed int MOV_K(u8 Addr)	              //进行K?X&Y&S&M计算
{ 
	static u16 LL_BIT,JOB_ADDR;                   //需要向左移动多少位用寄存器
	static uint64_t MOV_DATA_64BIT;               //移动64位数据
	mov_d_addr|=(Addr<<8);                    
	mov_d_addr+=Transfer;                         //成批传递和多点时 Transfer会有数据平时都是0
	LL_BIT=mov_d_addr%0x20;						            //需要移动多少位
	JOB_ADDR=(mov_d_addr/0x20)*4;                 //所在的起始地址                 
	MOV_DATA_64BIT=PLC_RAM64(RAM_ADDR+JOB_ADDR),
	MOV_DATA_64BIT>>=LL_BIT; 									 
	return  (signed int)MOV_DATA_64BIT;
}

//计算出高位地址
u16 D_C_T_addr(u8 l_value)
{ 
	static u16 temp; 
	switch(*PLC_Addr/0x100)
	{
		case 0x80: //大于等于D1000
		{
			temp=l_value+((*PLC_Addr%0x100)*0x100),temp=0x0700+temp/2,PLC_Addr++;      
		  break;
		}
		case 0x82: //算出T的地址，将地址变为值
		{
			temp=l_value+((*PLC_Addr%0x100)*0x100),temp=0x0800+temp/2,PLC_Addr++;      
		  break;
		}
		case 0x84: //算出C的地址，将地址变为值
		{
			temp=l_value+((*PLC_Addr%0x100)*0x100),temp=0x0500+temp/2,PLC_Addr++;      
		  break;
		}
		case 0x86: //算出D的地址，将地址变为值
		{
			temp=l_value+((*PLC_Addr%0x100)*0x100),temp=0x1000+temp/2,PLC_Addr++;      
		  break;
		}
		case 0x88: //大于等于D1000
		{
			temp=l_value+((*PLC_Addr%0x100)*0x100),temp=0x1000+temp/2+1000,PLC_Addr++; 
		  break;
		}
	}
	return temp;
}

//=======================================================================================================
// 函数名称:  static u16 addr_value(void)
// 功能描述： 计算PLC地址或k的实数
// 输　入:  void      
// 输　出:  地址或数据     
// 全局变量:  

//-------------------------------------------------------------------------------------------------------
// 修改人:
// 日　期:
// 备  注: 优化程序数量测试中部分指令测试  优化后返回地址 k是数据
//-------------------------------------------------------------------------------------------------------
//=======================================================================================================
static u16 addr_value(void)                                        
{  
	static u8 temp;static u16 temp1;
	switch(*PLC_Addr/0x100)
	{
		case 0x84: 
		{
				temp=*PLC_Addr;
				PLC_Addr++;
				temp1=*PLC_Addr<<8|temp;
				PLC_Addr++;
				Flag_bit=0;
				break;//进行如 K4M0 之类的传送
		}			
		case 0x86: 
		{
				temp=*PLC_Addr;
				PLC_Addr++;
				temp1=D_C_T_addr(temp);                       
				break;//算出D、C、T的地址   
    }			
	}
	return temp1;
}



//=======================================================================================================
// 函数名称:  static u32 addr_value_prog(void)   
// 功能描述： 计算PLC地址或k的实数
// 输　入:  void      
// 输　出:  地址或数据     
// 全局变量:  

//-------------------------------------------------------------------------------------------------------
// 修改人:
// 日　期:
// 备  注: 
//=======================================================================================================
static u32 addr_value_prog(void)                                      
{  
	static u32 temp; 
	u16 Type_F,temp2,Data1,Data2;
	Data1=*PLC_Addr;PLC_Addr++;Data2=*PLC_Addr;
	temp2=Type_F=0;
	Type_F   = (Data1 & 0xff00);
	Type_F  |= (Data2 >> 8);
	
	temp2  = (Data2 << 8);
	temp2 |=mov_d_addr=(u8)Data1;
	
	if(Type_F == 0x8680)      temp=RAM_D8000_ADDR+temp2,                  PLC_Addr++;//算出D的地址 D8000
	else if(Type_F == 0x8682) temp=RAM_T_ADDR+temp2,                      PLC_Addr++;//算出T的地址 
	else if(Type_F == 0x8684) temp=RAM_C_ADDR+temp2,                      PLC_Addr++;//算出C的地址
	else if(Type_F == 0x8686) temp=RAM_D_ADDR+temp2,                      PLC_Addr++;//算出D的地址 
	else if(Type_F == 0x8688) temp=RAM_D1000_ADDR+temp2,                  PLC_Addr++;//大于等于D1000
	else if(Type_F == 0x8482) temp=MOV_K(*PLC_Addr)&0X0000000F,Flag_bit=0,PLC_Addr++;//进行如 K4M0 之类的传送    
	else if(Type_F == 0x8484) temp=MOV_K(*PLC_Addr)&0X000000FF,Flag_bit=0,PLC_Addr++;//进行如 K4M0 之类的传送    
	else if(Type_F == 0x8486) temp=MOV_K(*PLC_Addr)&0X00000FFF,Flag_bit=0,PLC_Addr++;//进行如 K4M0 之类的传送    
	else if(Type_F == 0x8488) temp=MOV_K(*PLC_Addr)&0X0000FFFF,Flag_bit=0,PLC_Addr++;//进行如 K4M0 之类的传送    
	else if(Type_F == 0x848A) temp=MOV_K(*PLC_Addr)&0X000FFFFF,Flag_bit=0,PLC_Addr++;//进行如 K4M0 之类的传送    
	else if(Type_F == 0x848C) temp=MOV_K(*PLC_Addr)&0X00FFFFFF,Flag_bit=0,PLC_Addr++;//进行如 K4M0 之类的传送    
	else if(Type_F == 0x848E) temp=MOV_K(*PLC_Addr)&0X0FFFFFFF,Flag_bit=0,PLC_Addr++;//进行如 K4M0 之类的传送    
	else if(Type_F == 0x8490) temp=MOV_K(*PLC_Addr),           Flag_bit=0,PLC_Addr++;//进行如 K4M0 之类的传送  
	return temp;
}

unsigned short V0_V3(u16 temp1)
{
	u8 temp=PLC_v_z_addr(temp1);
	if(temp==0)       return D8029;
	else if(temp==1)  return D8183;
	else if(temp==2)  return D8185;
	else if(temp==3)  return D8187;
	else	return 0;
}

unsigned short V4_V7(u16 temp1)
{
	u8 temp=PLC_v_z_addr(temp1);
	if(temp==0)       return D8189;
  else if(temp==1)  return D8191;
  else if(temp==2)  return D8193;
  else if(temp==3)  return D8195;
  else	return 0;
}

unsigned short Z0_Z3(u16 temp1)
{
	u8 temp=PLC_v_z_addr(temp1);
	if(temp==0)       return D8028;
  else if(temp==1)  return D8182;
  else if(temp==2)  return D8184;
  else if(temp==3)  return D8186;
  else	return 0;
}

unsigned short Z4_Z7(u16 temp1)
{
	u8 temp=PLC_v_z_addr(temp1);
	if(temp==0)       return D8188;
  else if(temp==1)  return D8190;
  else if(temp==2)  return D8192;
  else if(temp==3)  return D8194;
  else	return 0;
}

unsigned int DZ0_Z3(u16 temp1)
{
	u8 temp=PLC_v_z_addr(temp1);
	if(temp==0)       return D8028+D8029*0X10000;
  else if(temp==1)  return D8182+D8183*0X10000;
  else if(temp==2)  return D8184+D8184*0X10000;
  else if(temp==3)  return D8186+D8185*0X10000;
  else	return 0;
}

unsigned short DZ4_Z7(u16 temp1)
{
	u8 temp=PLC_v_z_addr(temp1);
	if(temp==0)       return D8188+D8189*0X10000;
  else if(temp==1)  return D8190+D8191*0X10000;
  else if(temp==2)  return D8192+D8193*0X10000;
  else if(temp==3)  return D8194+D8193*0X10000;
  else	return 0;
}
//=======================================================================================================
// 函数名称:  static void target(void)
// 功能描述： 加减法“与”“或”“异或”共用赋值函数	如DMOV、DADD、DSUB等指令的结果传递出去
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void D_target(void)                                      
{  
	 u16 Type_F,temp2,Data1,Data2;
	 Data1=*PLC_Addr;PLC_Addr++;Data2=*PLC_Addr;
	 temp2=Type_F=0;
	 Type_F   = (Data1 & 0xff00);
	 Type_F  |= (Data2 >> 8);
/************************************/	
	 temp2  = (Data2 << 8);
	 temp2 |=mov_d_addr=(u8)Data1;
/************************************/	
	switch(Type_F)
	{
	  case 0x8480: MOV_TO_K_H(Type_F,trade,*PLC_Addr),PLC_Addr++,Transfer_bit=1;break; //进行如 K4M0 之类的传送  
		case 0x8482: MOV_TO_K_H(Type_F,trade,*PLC_Addr),PLC_Addr++,Transfer_bit=1;break; //进行如 K4M0 之类的传送    
		case 0x8484: MOV_TO_K_H(Type_F,trade,*PLC_Addr),PLC_Addr++,Transfer_bit=1;break; //进行如 K4M0 之类的传送    
		case 0x8486: MOV_TO_K_H(Type_F,trade,*PLC_Addr),PLC_Addr++,Transfer_bit=1;break; //进行如 K4M0 之类的传送    
		case 0x8488: MOV_TO_K_H(Type_F,trade,*PLC_Addr),PLC_Addr++,Transfer_bit=1;break; //进行如 K4M0 之类的传送    
		case 0x848A: MOV_TO_K_H(Type_F,trade,*PLC_Addr),PLC_Addr++,Transfer_bit=1;break; //进行如 K4M0 之类的传送    
		case 0x848C: MOV_TO_K_H(Type_F,trade,*PLC_Addr),PLC_Addr++,Transfer_bit=1;break; //进行如 K4M0 之类的传送    
		case 0x848E: MOV_TO_K_H(Type_F,trade,*PLC_Addr),PLC_Addr++,Transfer_bit=1;break; //进行如 K4M0 之类的传送    
		case 0x8490: MOV_TO_K_H(Type_F,trade,*PLC_Addr),PLC_Addr++,Transfer_bit=1;break; //进行如 K4M0 之类的传送     
		
		case 0x8680: PLC_RAM32(RAM_D8000_ADDR+temp2+Transfer)=trade,PLC_Addr++;break; //算出D的地址 D8000
		case 0x8682: PLC_RAM32(RAM_T_ADDR+temp2+Transfer)=trade,PLC_Addr++;break; //算出T的地址 
		case 0x8684: PLC_RAM32(RAM_C_ADDR+temp2+Transfer)=trade,PLC_Addr++;break; //算出C的地址
		case 0x8686: PLC_RAM32(RAM_D_ADDR+temp2+Transfer)=trade,PLC_Addr++;break; //算出D的地址 
		case 0x8688: PLC_RAM32(RAM_D1000_ADDR+temp2+Transfer)=trade,PLC_Addr++;break; //大于等于D1000
		/************************************************K1M0寄存器"Z"*******************************************************************/
		case 0xA482: mov_d_addr=+DZ0_Z3(temp2),MOV_TO_K_H(Type_F,trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K1M0的V0-V3	
		case 0xA483: mov_d_addr=+DZ4_Z7(temp2),MOV_TO_K_H(Type_F,trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K1M0的V4-V7	
		/************************************************K2M0寄存器"Z"*******************************************************************/
		case 0xA484: mov_d_addr=+DZ0_Z3(temp2),MOV_TO_K_H(Type_F,trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K2M0的V0-V3	
		case 0xA485: mov_d_addr=+DZ4_Z7(temp2),MOV_TO_K_H(Type_F,trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K2M0的V4-V7		
		/************************************************K3M0寄存器"Z"*******************************************************************/
		case 0xA486: mov_d_addr=+DZ0_Z3(temp2),MOV_TO_K_H(Type_F,trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K3M0的V0-V3	
		case 0xA487: mov_d_addr=+DZ4_Z7(temp2),MOV_TO_K_H(Type_F,trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K3M0的V4-V7		
		/************************************************K4M0寄存器"Z"*******************************************************************/
		case 0xA488: mov_d_addr=+DZ0_Z3(temp2),MOV_TO_K_H(Type_F,trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K4M0的V0-V3
		case 0xA489: mov_d_addr=+DZ4_Z7(temp2),MOV_TO_K_H(Type_F,trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K4M0的V4-V7		
		/************************************************K5M0寄存器"Z"*******************************************************************/
		case 0xA48A: mov_d_addr=+DZ0_Z3(temp2),MOV_TO_K_H(Type_F,trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K5M0的V0-V3	
		case 0xA48B: mov_d_addr=+DZ4_Z7(temp2),MOV_TO_K_H(Type_F,trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K5M0的V4-V7		
		/************************************************K6M0寄存器"Z"*******************************************************************/
		case 0xA48C: mov_d_addr=+DZ0_Z3(temp2),MOV_TO_K_H(Type_F,trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K6M0的V0-V3	
		case 0xA48D: mov_d_addr=+DZ4_Z7(temp2),MOV_TO_K_H(Type_F,trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K6M0的V4-V7		
		/************************************************K7M0寄存器"Z"*******************************************************************/
		case 0xA48E: mov_d_addr=+DZ0_Z3(temp2),MOV_TO_K_H(Type_F,trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K7M0的V0-V3	
		case 0xA48F: mov_d_addr=+DZ4_Z7(temp2),MOV_TO_K_H(Type_F,trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K7M0的V4-V7		
		/************************************************K8M0寄存器"Z"*******************************************************************/
		case 0xA490: mov_d_addr=+DZ0_Z3(temp2),MOV_TO_K_H(Type_F,trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//k8M0的V0-V4	
		case 0xA491: mov_d_addr=+DZ4_Z7(temp2),MOV_TO_K_H(Type_F,trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K8M0的V4-V7			
		/************************************************T寄存器"Z"*******************************************************************/
		case 0xA682: PLC_RAM32(RAM_T_ADDR+PLC_D_C_T_addr(temp2)+DZ0_Z3(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出T的地址，的Z0-Z3
		case 0xA683: PLC_RAM32(RAM_T_ADDR+PLC_D_C_T_addr(temp2)+DZ4_Z7(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出T的地址，的Z4-Z7
		/************************************************C寄存器"Z"*******************************************************************/
		case 0xA684: 
			PLC_RAM32(RAM_C_ADDR+PLC_D_C_T_addr(temp2)+DZ0_Z3(temp2)*2)=(u16)trade;
		  PLC_Addr++;
		  break;//算出C的地址，的Z0-Z3
		case 0xA685: PLC_RAM32(RAM_C_ADDR+PLC_D_C_T_addr(temp2)+DZ4_Z7(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出C的地址，的Z4-Z7
		/************************************************D寄存器"Z"*******************************************************************/
		case 0xA686: PLC_RAM32(RAM_D_ADDR+PLC_D_C_T_addr(temp2)+DZ0_Z3(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出D的地址，的Z0-Z3
		case 0xA687: PLC_RAM32(RAM_D_ADDR+PLC_D_C_T_addr(temp2)+DZ4_Z7(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出D的地址，的Z4-Z7
		case 0xA688: PLC_RAM32(RAM_D1000_ADDR+PLC_D_C_T_addr(temp2)+DZ0_Z3(temp2)*2)=(u16)trade,PLC_Addr++;break;//大于等于D1000的Z0-Z3
		case 0xA689: PLC_RAM32(RAM_D1000_ADDR+PLC_D_C_T_addr(temp2)+DZ4_Z7(temp2)*2)=(u16)trade,PLC_Addr++;break;//大于等于D1000的Z4-Z7
 }
	 PLC_Addr+=2;
}

//=======================================================================================================
// 函数名称:  static void target(void)
// 功能描述： 加减法“与”“或”“异或”共用赋值函数	如MOV、ADD、SUB等指令的结果传递出去
// 输　入:  void      
// 输　出:  void
// 全局变量:  

//=======================================================================================================
static void target(void)                                      
{  
	 u16 Type_F,temp2,Data1,Data2;
	 Data1 =*PLC_Addr;
	 PLC_Addr++;
	 Data2 =*PLC_Addr;
	
	 temp2=Type_F=0;
	 Type_F  = (Data1 & 0xff00);
	 Type_F  |= (Data2 >> 8);
/************************************/	
	 temp2  = (Data2 << 8);
	 temp2 |=mov_d_addr=(u8)Data1;
/************************************/	
	switch(Type_F)
	{
		/* 进行如 K4M0 之类的传送 */
		case  0x8482:
		case  0x8484:
		case  0x8486:
		case  0x8488:
		{
			MOV_TO_K_H(Type_F,(u16)trade,*PLC_Addr);
		  PLC_Addr++;
		  Transfer_bit=1;
		  break;
    }			
//    case  0x8482: MOV_TO_K_H(Type_F,(u16)trade,*PLC_Addr),PLC_Addr++,Transfer_bit=1;break; //进行如 K4M0 之类的传送    		
//		case  0x8484: MOV_TO_K_H(Type_F,(u16)trade,*PLC_Addr),PLC_Addr++,Transfer_bit=1;break; //进行如 K4M0 之类的传送    
//		case  0x8486: MOV_TO_K_H(Type_F,(u16)trade,*PLC_Addr),PLC_Addr++,Transfer_bit=1;break; //进行如 K4M0 之类的传送    
//		case  0x8488: MOV_TO_K_H(Type_F,(u16)trade,*PLC_Addr),PLC_Addr++,Transfer_bit=1;break; //进行如 K4M0 之类的传送    
		
		case  0x8680: PLC_RAM16(RAM_D8000_ADDR+temp2+Transfer)=(u16)trade,PLC_Addr++;break; //算出D的地址 D8000
		case  0x8682: PLC_RAM16(RAM_T_ADDR+temp2+Transfer)=(u16)trade,PLC_Addr++;break; //算出T的地址 
		case  0x8684: PLC_RAM16(RAM_C_ADDR+temp2+Transfer)=(u16)trade,PLC_Addr++;break; //算出C的地址
		case  0x8686: //算出D的地址 
		{
			PLC_RAM16(RAM_D_ADDR+temp2+Transfer)=(u16)trade;
			PLC_Addr++;
			break; 
		}
		case  0x8688: PLC_RAM16(RAM_D1000_ADDR+temp2+Transfer)=(u16)trade,PLC_Addr++;break; //大于等于D1000
		/************************************************K1M0寄存器"V"*******************************************************************/
		case  0x9482: mov_d_addr=+V0_V3(temp2),MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K1M0的V0-V3	
		case  0x9483: mov_d_addr=+V4_V7(temp2),MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K1M0的V4-V7	
		/************************************************K2M0寄存器"V"*******************************************************************/
		case  0x9484: mov_d_addr=+V0_V3(temp2),MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K2M0的V0-V3	
		case  0x9485: mov_d_addr=+V4_V7(temp2),MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K2M0的V4-V7		
		/************************************************K3M0寄存器"V"*******************************************************************/
		case  0x9486: mov_d_addr=+V0_V3(temp2),MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K3M0的V0-V3	
		case  0x9487: mov_d_addr=+V4_V7(temp2),MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K3M0的V4-V7		
		/************************************************K4M0寄存器"V"*******************************************************************/
		case  0x9488: mov_d_addr=+V0_V3(temp2),MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K4M0的V0-V3	
		case  0x9489: mov_d_addr=+V4_V7(temp2),MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K4M0的V4-V7						
		/************************************************T寄存器"V"*******************************************************************/
		case  0x9682: PLC_RAM16(RAM_T_ADDR+PLC_D_C_T_addr(temp2)+V0_V3(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出T的地址，的V0-V3
		case  0x9683: PLC_RAM16(RAM_T_ADDR+PLC_D_C_T_addr(temp2)+V4_V7(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出T的地址，的V4-V7
		/************************************************C寄存器"V"*******************************************************************/
		case  0x9684: PLC_RAM16(RAM_C_ADDR+PLC_D_C_T_addr(temp2)+V0_V3(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出C的地址，的V0-V3
		case  0x9685: PLC_RAM16(RAM_C_ADDR+PLC_D_C_T_addr(temp2)+V4_V7(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出C的地址，的V4-V7
		/************************************************D寄存器"V"*******************************************************************/
		case  0x9686: PLC_RAM16(RAM_D_ADDR+PLC_D_C_T_addr(temp2)+V0_V3(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出D的地址，的V0-V3
		case  0x9687: PLC_RAM16(RAM_D_ADDR+PLC_D_C_T_addr(temp2)+V4_V7(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出D的地址，的V4-V7
		case  0x9688: PLC_RAM16(RAM_D1000_ADDR+PLC_D_C_T_addr(temp2)+V0_V3(temp2)*2)=(u16)trade,PLC_Addr++;break;//大于等于D1000的V0-V3
		case  0x9689: PLC_RAM16(RAM_D1000_ADDR+PLC_D_C_T_addr(temp2)+V4_V7(temp2)*2)=(u16)trade,PLC_Addr++;break;//大于等于D1000的V4-V7
		
		/************************************************K1M0寄存器"Z"*******************************************************************/
		case  0xA482: mov_d_addr=+Z0_Z3(temp2),MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K1M0的V0-V3	
		case  0xA483: mov_d_addr=+Z4_Z7(temp2),MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K1M0的V4-V7	
		/************************************************K2M0寄存器"Z"*******************************************************************/
		case  0xA484: mov_d_addr=+Z0_Z3(temp2),MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K2M0的V0-V3	
		case  0xA485: mov_d_addr=+Z4_Z7(temp2),MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K2M0的V4-V7		
		/************************************************K3M0寄存器"Z"*******************************************************************/
		case  0xA486: mov_d_addr=+Z0_Z3(temp2),MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K3M0的V0-V3	
		case  0xA487: mov_d_addr=+Z4_Z7(temp2),MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K3M0的V4-V7		
		/************************************************K4M0寄存器"Z"*******************************************************************/
		case  0xA488: mov_d_addr=+Z0_Z3(temp2),MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K4M0的V0-V3
		case  0xA489: mov_d_addr=+Z4_Z7(temp2),MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K4M0的V4-V7			
		/************************************************T寄存器"Z"*******************************************************************/
		case  0xA682: PLC_RAM16(RAM_T_ADDR+PLC_D_C_T_addr(temp2)+Z0_Z3(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出C的地址，的Z0-Z3
		case  0xA683: PLC_RAM16(RAM_T_ADDR+PLC_D_C_T_addr(temp2)+Z4_Z7(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出C的地址，的Z4-Z7
		/************************************************C寄存器"Z"*******************************************************************/
		case  0xA684: PLC_RAM16(RAM_C_ADDR+PLC_D_C_T_addr(temp2)+Z0_Z3(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出C的地址，的Z0-Z3
		case  0xA685: PLC_RAM16(RAM_C_ADDR+PLC_D_C_T_addr(temp2)+Z4_Z7(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出C的地址，的Z4-Z7
		/************************************************D寄存器"Z"*******************************************************************/
		case  0xA686: PLC_RAM16(RAM_D_ADDR+PLC_D_C_T_addr(temp2)+Z0_Z3(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出D的地址，的Z0-Z3
		case  0xA687: PLC_RAM16(RAM_D_ADDR+PLC_D_C_T_addr(temp2)+Z4_Z7(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出D的地址，的Z4-Z7
		case  0xA688: PLC_RAM16(RAM_D1000_ADDR+PLC_D_C_T_addr(temp2)+Z0_Z3(temp2)*2)=(u16)trade,PLC_Addr++;break;//大于等于D1000的Z0-Z3
		case  0xA689: PLC_RAM16(RAM_D1000_ADDR+PLC_D_C_T_addr(temp2)+Z4_Z7(temp2)*2)=(u16)trade,PLC_Addr++;break;//大于等于D1000的Z4-Z7
    }
}

static s16 Special_cos_value(u16 num)                                      
{  
  static s16 temp; 
	 u16 Type_F,temp2,Data1,Data2;
	
	 Data1=*PLC_Addr;
	 PLC_Addr++;
	 Data2=*PLC_Addr;
	
	 temp2=Type_F=0;
	 Type_F = (Data1 & 0xff00)|(Data2 >> 8);
/************************************/	
	 temp2  = (Data2 << 8);
	 temp2 |=mov_d_addr=(u8)Data1;
/************************************/	
	
	// Type_F = 0x8480时做如下处理
	if((Type_F == 0x8480) || (Type_F == 0x9480) || (Type_F == 0xA480))
		Type_F |= (num <<1);	
	
	switch(Type_F)
	{
		case  0x8080: //算出K值
		{
			temp=temp2;                                    
			PLC_Addr++;
			break;  
		}
		case  0x8280: //算出H值
		{
			temp=temp2,                                      
			PLC_Addr++;
			break; 
		}
		case  0x8482: temp=MOV_K(*PLC_Addr)&0X0000000F,Transfer_bit1=1,PLC_Addr++;break;  //进行如 K4M0 之类的传送    
		case  0x8484: temp=MOV_K(*PLC_Addr)&0X000000FF,Transfer_bit1=1,PLC_Addr++;break;  //进行如 K4M0 之类的传送    
		case  0x8486: temp=MOV_K(*PLC_Addr)&0X00000FFF,Transfer_bit1=1,PLC_Addr++;break;  //进行如 K4M0 之类的传送    
		case  0x8488: temp=MOV_K(*PLC_Addr)&0X0000FFFF,Transfer_bit1=1,PLC_Addr++;break;  //进行如 K4M0 之类的传送    	
		case  0x8680: //算出D的地址 D8000
		{
			temp=PLC_RAM16(RAM_D8000_ADDR + temp2 + Transfer);
			PLC_Addr++;
			break;         
		}
		case  0x8682: //算出T的地址
		{
			temp=PLC_RAM16(RAM_T_ADDR + temp2 + Transfer);
			PLC_Addr++;
			break;         
		}			
		case  0x8684: //算出C的地址
		{
			temp=PLC_RAM16(RAM_C_ADDR + temp2 + Transfer);
			PLC_Addr++;
			break;         
		}
		case  0x8686: //算出D的地址 
		{
			temp=PLC_RAM16(RAM_D_ADDR + temp2 + Transfer);
			PLC_Addr++; 
			break;         
		}
		case  0x8688: //大于等于D1000
		{
			temp=PLC_RAM16(RAM_D1000_ADDR + temp2 + Transfer);
			PLC_Addr++;
			break;         
		}
		/************************************************K寄存器"V"*******************************************************************/
		case  0x9080: temp=temp2+D8029,PLC_Addr++;break;//算出K的地址，的V0
		case  0x9081: temp=temp2+D8183,PLC_Addr++;break;//算出K的地址，的V1
		case  0x9082: temp=temp2+D8185,PLC_Addr++;break;//算出K的地址，的V2
		case  0x9083: temp=temp2+D8187,PLC_Addr++;break;//算出K的地址，的V3
		case  0x9084: temp=temp2+D8189,PLC_Addr++;break;//算出K的地址，的V4
		case  0x9085: temp=temp2+D8191,PLC_Addr++;break;//算出K的地址，的V5
		case  0x9086: temp=temp2+D8193,PLC_Addr++;break;//算出K的地址，的V6
		case  0x9087: temp=temp2+D8195,PLC_Addr++;break;//算出K的地址，的V7
		case  0x9280: temp=temp2+D8029,PLC_Addr++;break;//算出H的地址，的V0
		case  0x9281: temp=temp2+D8183,PLC_Addr++;break;//算出H的地址，的V1
		case  0x9282: temp=temp2+D8185,PLC_Addr++;break;//算出H的地址，的V2
		case  0x9283: temp=temp2+D8187,PLC_Addr++;break;//算出H的地址，的V3
		case  0x9284: temp=temp2+D8189,PLC_Addr++;break;//算出H的地址，的V4
		case  0x9285: temp=temp2+D8191,PLC_Addr++;break;//算出H的地址，的V5
		case  0x9286: temp=temp2+D8193,PLC_Addr++;break;//算出H的地址，的V6
		case  0x9287: temp=temp2+D8195,PLC_Addr++;break;//算出H的地址，的V7 	
		
		/************************************************K1M0寄存器"V"*******************************************************************/
		case  0x9482: mov_d_addr=+V0_V3(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000000F,PLC_Addr++;break;//K1M0的V0-V3	
		case  0x9483: mov_d_addr=+V4_V7(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000000F,PLC_Addr++;break;//K1M0的V4-V7	
		/************************************************K2M0寄存器"V"*******************************************************************/
		case  0x9484: mov_d_addr=+V0_V3(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X000000FF,PLC_Addr++;break;//K2M0的V0-V3	
		case  0x9485: mov_d_addr=+V4_V7(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X000000FF,PLC_Addr++;break;//K2M0的V4-V7		
		/************************************************K3M0寄存器"V"*******************************************************************/
		case  0x9486: mov_d_addr=+V0_V3(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X00000FFF,PLC_Addr++;break;//K3M0的V0-V3	
		case  0x9487: mov_d_addr=+V4_V7(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X00000FFF,PLC_Addr++;break;//K3M0的V4-V7		
		/************************************************K4M0寄存器"V"*******************************************************************/
		case  0x9488: mov_d_addr=+V0_V3(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000FFFF,PLC_Addr++;break;//K4M0的V0-V3	
		case  0x9489: mov_d_addr=+V4_V7(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000FFFF,PLC_Addr++;break;//K4M0的V4-V7	


		/************************************************T寄存器"V"*******************************************************************/
		case  0x9682: temp=PLC_RAM16(RAM_T_ADDR+PLC_D_C_T_addr(temp2)+V0_V3(temp2)*2),PLC_Addr++;break;//算出T的地址，的V0-V3
		case  0x9683: temp=PLC_RAM16(RAM_T_ADDR+PLC_D_C_T_addr(temp2)+V4_V7(temp2)*2),PLC_Addr++;break;//算出T的地址，的V4-V7
		/************************************************C寄存器"V"*******************************************************************/
		case  0x9684: temp=PLC_RAM16(RAM_C_ADDR+PLC_D_C_T_addr(temp2)+V0_V3(temp2)*2),PLC_Addr++;break;//算出C的地址，的V0-V3
		case  0x9685: temp=PLC_RAM16(RAM_C_ADDR+PLC_D_C_T_addr(temp2)+V4_V7(temp2)*2),PLC_Addr++;break;//算出C的地址，的V4-V7
		/************************************************D寄存器"V"*******************************************************************/
		case  0x9686: temp=PLC_RAM16(RAM_D_ADDR+PLC_D_C_T_addr(temp2)+V0_V3(temp2)*2),PLC_Addr++;break;//算出D的地址，的V0-V3
		case  0x9687: temp=PLC_RAM16(RAM_D_ADDR+PLC_D_C_T_addr(temp2)+V4_V7(temp2)*2),PLC_Addr++;break;//算出D的地址，的V4-V7
		case  0x9688: temp=PLC_RAM16(RAM_D1000_ADDR+PLC_D_C_T_addr(temp2)+V0_V3(temp2)*2),PLC_Addr++;break;//大于等于D1000的V0-V3
		case  0x9689: temp=PLC_RAM16(RAM_D1000_ADDR+PLC_D_C_T_addr(temp2)+V4_V7(temp2)*2),PLC_Addr++;break;//大于等于D1000的V4-V7
	
		case  0xA080: temp=temp2+D8028,PLC_Addr++;break;//算出K的地址，的Z0
		case  0xA081: temp=temp2+D8182,PLC_Addr++;break;//算出K的地址，的Z1
		case  0xA082: temp=temp2+D8184,PLC_Addr++;break;//算出K的地址，的Z2
		case  0xA083: temp=temp2+D8186,PLC_Addr++;break;//算出K的地址，的Z3
		case  0xA084: temp=temp2+D8188,PLC_Addr++;break;//算出K的地址，的Z4
		case  0xA085: temp=temp2+D8190,PLC_Addr++;break;//算出K的地址，的Z5
		case  0xA086: temp=temp2+D8192,PLC_Addr++;break;//算出K的地址，的Z6
		case  0xA087: temp=temp2+D8194,PLC_Addr++;break;//算出K的地址，的Z7 	
		case  0xA280: temp=temp2+D8028,PLC_Addr++;break;//算出H的地址，的Z0
		case  0xA281: temp=temp2+D8182,PLC_Addr++;break;//算出H的地址，的Z1
		case  0xA282: temp=temp2+D8184,PLC_Addr++;break;//算出H的地址，的Z2
		case  0xA283: temp=temp2+D8186,PLC_Addr++;break;//算出H的地址，的Z3
		case  0xA284: temp=temp2+D8188,PLC_Addr++;break;//算出H的地址，的Z4
		case  0xA285: temp=temp2+D8190,PLC_Addr++;break;//算出H的地址，的Z5
		case  0xA286: temp=temp2+D8192,PLC_Addr++;break;//算出H的地址，的Z6
		case  0xA287: temp=temp2+D8194,PLC_Addr++;break;//算出H的地址，的Z7 	
		/************************************************K1M0寄存器"Z"*******************************************************************/
		case  0xA482: mov_d_addr=+Z0_Z3(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000000F,PLC_Addr++;break;//K1M0的V0-V3	
		case  0xA483: mov_d_addr=+Z4_Z7(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000000F,PLC_Addr++;break;//K1M0的V4-V7	
		/************************************************K2M0寄存器"Z"*******************************************************************/
		case  0xA484: mov_d_addr=+Z0_Z3(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X000000FF,PLC_Addr++;break;//K2M0的V0-V3	
		case  0xA485: mov_d_addr=+Z4_Z7(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X000000FF,PLC_Addr++;break;//K2M0的V4-V7		
		/************************************************K3M0寄存器"Z"*******************************************************************/
		case  0xA486: mov_d_addr=+Z0_Z3(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X00000FFF,PLC_Addr++;break;//K3M0的V0-V3	
		case  0xA487: mov_d_addr=+Z4_Z7(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X00000FFF,PLC_Addr++;break;//K3M0的V4-V7		
		/************************************************K4M0寄存器"Z"*******************************************************************/
		case  0xA488: mov_d_addr=+Z0_Z3(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000FFFF,PLC_Addr++;break;//K4M0的V0-V3
		case  0xA489: mov_d_addr=+Z4_Z7(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000FFFF,PLC_Addr++;break;//K4M0的V4-V7		
		case  0xA682: temp=PLC_RAM16(RAM_T_ADDR+PLC_D_C_T_addr(temp2)+Z0_Z3(temp2)*2),PLC_Addr++;break;//算出T的地址，的Z0-Z3
		case  0xA683: temp=PLC_RAM16(RAM_T_ADDR+PLC_D_C_T_addr(temp2)+Z4_Z7(temp2)*2),PLC_Addr++;break;//算出T的地址，的Z4-Z7
		/************************************************C寄存器"Z"*******************************************************************/
		case  0xA684: temp=PLC_RAM16(RAM_C_ADDR+PLC_D_C_T_addr(temp2)+Z0_Z3(temp2)*2),PLC_Addr++;break;//算出C的地址，的Z0-Z3
		case  0xA685: temp=PLC_RAM16(RAM_C_ADDR+PLC_D_C_T_addr(temp2)+Z4_Z7(temp2)*2),PLC_Addr++;break;//算出C的地址，的Z4-Z7
		/************************************************D寄存器"Z"*******************************************************************/
		case  0xA686: temp=PLC_RAM16(RAM_D_ADDR+PLC_D_C_T_addr(temp2)+Z0_Z3(temp2)*2),PLC_Addr++;break;//算出D的地址，的Z0-Z3
		case  0xA687: temp=PLC_RAM16(RAM_D_ADDR+PLC_D_C_T_addr(temp2)+Z4_Z7(temp2)*2),PLC_Addr++;break;//算出D的地址，的Z4-Z7
		case  0xA688: temp=PLC_RAM16(RAM_D1000_ADDR+PLC_D_C_T_addr(temp2)+Z0_Z3(temp2)*2),PLC_Addr++;break;//大于等于D1000的Z0-Z3
		case  0xA689: temp=PLC_RAM16(RAM_D1000_ADDR+PLC_D_C_T_addr(temp2)+Z4_Z7(temp2)*2),PLC_Addr++;break;//大于等于D1000的Z4-Z7
	}	
	return temp;
}

// 7新增
static void SpecialTarget(void)                                      
{  
	 u16 Type_F,temp2,Data1,Data2;
	 u16 tmp =0;
	
	 Data1 =*PLC_Addr;
	 PLC_Addr++;
	 Data2 =*PLC_Addr;
	
	 temp2=Type_F=0;
	 Type_F  = (Data1 & 0xff00);
	 Type_F  |= (Data2 >> 8);
/************************************/	
	 temp2  = (Data2 << 8);
	 temp2 |=mov_d_addr=(u8)Data1;
/************************************/	
	// Type_F = 0x8480时做如下处理
	if((Type_F == 0x8480) || (Type_F == 0x9480) || (Type_F == 0xA480))
	{
			if(trade <= 0x000F) // 4
				tmp =0x0002;
			else if(trade <= 0x00FF) // 8
				tmp =0x0004;
			else if(trade <= 0x0FFF) // 12
				tmp =0x0006;
			else if(trade <= 0xFFFF) // 16
				tmp =0x0008;	
			else if(trade <= 0xFFFFF) // 20
				tmp =0x000A;	
			else if(trade <= 0xFFFFFF) // 24
				tmp =0x000C;	
			else if(trade <= 0xFFFFFFF) // 28
				tmp =0x000E;	
			else if(trade <= 0xFFFFFFFF) // 32
				tmp =0x0010;	
			Type_F |=tmp;	
  }
	
	switch(Type_F)
	{
		/* 进行如 K4M0 之类的传送 */
		case  0x8482: // 4
		case  0x8484: // 8
		case  0x8486: // 12
		case  0x8488: // 16
		case  0x848A: // 20
		case  0x848C: // 24
		case  0x848E: // 28
		case  0x8490: // 32
		{
			MOV_TO_K_H(Type_F,trade,*PLC_Addr);
		  PLC_Addr++;
		  Transfer_bit=1;
		  break;
    }	
		case  0x8680: PLC_RAM16(RAM_D8000_ADDR+temp2+Transfer)=(u16)trade,PLC_Addr++;break; //算出D的地址 D8000
		case  0x8682: PLC_RAM16(RAM_T_ADDR+temp2+Transfer)=(u16)trade,PLC_Addr++;break; //算出T的地址 
		case  0x8684: PLC_RAM16(RAM_C_ADDR+temp2+Transfer)=(u16)trade,PLC_Addr++;break; //算出C的地址
		case  0x8686: PLC_RAM16(RAM_D_ADDR+temp2+Transfer)=(u16)trade,PLC_Addr++;break; //算出D的地址 
		case  0x8688: PLC_RAM16(RAM_D1000_ADDR+temp2+Transfer)=(u16)trade,PLC_Addr++;break; //大于等于D1000		
		
		/************************************************KxM0寄存器"V"*******************************************************************/
		case  0x9482: // K1M0的V0-V3
		case  0x9484: // K2M0的V0-V3	
		case  0x9486: // K3M0的V0-V3	
		case  0x9488: // K4M0的V0-V3	
    {			
			mov_d_addr=+V0_V3(temp2);
		  MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100);
		  PLC_Addr++;
		  break;
		}		
		case  0x9483: // K1M0的V4-V7	
		case  0x9485: // K2M0的V4-V7	
		case  0x9487: // K3M0的V4-V7	
		case  0x9489: // K4M0的V4-V7		
		{
			mov_d_addr=+V4_V7(temp2);
		  MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100);
		  PLC_Addr++;
		  break;
		}
		
		/************************************************T寄存器"V"*******************************************************************/
		case  0x9682: PLC_RAM16(RAM_T_ADDR+PLC_D_C_T_addr(temp2)+V0_V3(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出T的地址，的V0-V3
		case  0x9683: PLC_RAM16(RAM_T_ADDR+PLC_D_C_T_addr(temp2)+V4_V7(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出T的地址，的V4-V7
		/************************************************C寄存器"V"*******************************************************************/
		case  0x9684: PLC_RAM16(RAM_C_ADDR+PLC_D_C_T_addr(temp2)+V0_V3(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出C的地址，的V0-V3
		case  0x9685: PLC_RAM16(RAM_C_ADDR+PLC_D_C_T_addr(temp2)+V4_V7(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出C的地址，的V4-V7
		/************************************************D寄存器"V"*******************************************************************/
		case  0x9686: PLC_RAM16(RAM_D_ADDR+PLC_D_C_T_addr(temp2)+V0_V3(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出D的地址，的V0-V3
		case  0x9687: PLC_RAM16(RAM_D_ADDR+PLC_D_C_T_addr(temp2)+V4_V7(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出D的地址，的V4-V7
		case  0x9688: PLC_RAM16(RAM_D1000_ADDR+PLC_D_C_T_addr(temp2)+V0_V3(temp2)*2)=(u16)trade,PLC_Addr++;break;//大于等于D1000的V0-V3
		case  0x9689: PLC_RAM16(RAM_D1000_ADDR+PLC_D_C_T_addr(temp2)+V4_V7(temp2)*2)=(u16)trade,PLC_Addr++;break;//大于等于D1000的V4-V7
		
		/************************************************KxM0寄存器"Z"*******************************************************************/
		case  0xA482: //K1M0的V0-V3 
		case  0xA484: //K2M0的V0-V3	
		case  0xA486: //K3M0的V0-V3	
		case  0xA488: //K4M0的V0-V3
    {			
			mov_d_addr=+Z0_Z3(temp2);
		  MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100);
		  PLC_Addr++;
		  break;
		}
		case  0xA483: //K1M0的V4-V7	
		case  0xA485: //K2M0的V4-V7		
		case  0xA487: //K3M0的V4-V7
		case  0xA489: //K4M0的V4-V7	
		{
			mov_d_addr=+Z4_Z7(temp2);
		  MOV_TO_K_H(Type_F,(u16)trade,PLC_D_C_T_addr(temp2)/0x100);
		  PLC_Addr++;
		  break;
		}
		/************************************************T寄存器"Z"*******************************************************************/
		case  0xA682: PLC_RAM16(RAM_T_ADDR+PLC_D_C_T_addr(temp2)+Z0_Z3(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出T的地址，的Z0-Z3
		case  0xA683: PLC_RAM16(RAM_T_ADDR+PLC_D_C_T_addr(temp2)+Z4_Z7(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出T的地址，的Z4-Z7
		/************************************************C寄存器"Z"*******************************************************************/
		case  0xA684: PLC_RAM16(RAM_C_ADDR+PLC_D_C_T_addr(temp2)+Z0_Z3(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出C的地址，的Z0-Z3
		case  0xA685: PLC_RAM16(RAM_C_ADDR+PLC_D_C_T_addr(temp2)+Z4_Z7(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出C的地址，的Z4-Z7
		/************************************************D寄存器"Z"*******************************************************************/
		case  0xA686: PLC_RAM16(RAM_D_ADDR+PLC_D_C_T_addr(temp2)+Z0_Z3(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出D的地址，的Z0-Z3
		case  0xA687: PLC_RAM16(RAM_D_ADDR+PLC_D_C_T_addr(temp2)+Z4_Z7(temp2)*2)=(u16)trade,PLC_Addr++;break;//算出D的地址，的Z4-Z7
		case  0xA688: PLC_RAM16(RAM_D1000_ADDR+PLC_D_C_T_addr(temp2)+Z0_Z3(temp2)*2)=(u16)trade,PLC_Addr++;break;//大于等于D1000的Z0-Z3
		case  0xA689: PLC_RAM16(RAM_D1000_ADDR+PLC_D_C_T_addr(temp2)+Z4_Z7(temp2)*2)=(u16)trade,PLC_Addr++;break;//大于等于D1000的Z4-Z7
  }
	
	
	
}


//=======================================================================================================
// 函数名称:  static u16  cos_value(void)	
// 功能描述： 
// 输　入:  void      
// 输　出:  输出16位数据
// 全局变量:  

//-------------------------------------------------------------------------------------------------------
// 修改人:
// 日　期:
// 备  注: 
//-------------------------------------------------------------------------------------------------------
//=======================================================================================================
static s16 cos_value()                                      
{  
  static s16 temp; 
	 u16 Type_F,temp2,Data1,Data2;
	
	 Data1=*PLC_Addr;
	 PLC_Addr++;
	 Data2=*PLC_Addr;
	
	 temp2=Type_F=0;
	 Type_F = (Data1 & 0xff00)|(Data2 >> 8);
/************************************/	
	 temp2  = (Data2 << 8);
	 temp2 |=mov_d_addr=(u8)Data1;
/************************************/	
	switch(Type_F)
	{
		case  0x8080: //算出K值
		{
			temp=temp2;                                    
			PLC_Addr++;
			break;  
		}
		case  0x8280: //算出H值
		{
			temp=temp2,                                      
			PLC_Addr++;
			break; 
		}
		case  0x8482: temp=MOV_K(*PLC_Addr)&0X0000000F,Transfer_bit1=1,PLC_Addr++;break;  //进行如 K4M0 之类的传送    
		case  0x8484: temp=MOV_K(*PLC_Addr)&0X000000FF,Transfer_bit1=1,PLC_Addr++;break;  //进行如 K4M0 之类的传送    
		case  0x8486: temp=MOV_K(*PLC_Addr)&0X00000FFF,Transfer_bit1=1,PLC_Addr++;break;  //进行如 K4M0 之类的传送    
		case  0x8488: temp=MOV_K(*PLC_Addr)&0X0000FFFF,Transfer_bit1=1,PLC_Addr++;break;  //进行如 K4M0 之类的传送    	
		case  0x8680: //算出D的地址 D8000
		{
			temp=PLC_RAM16(RAM_D8000_ADDR + temp2 + Transfer);
			PLC_Addr++;
			break;         
		}
		case  0x8682: //算出T的地址
		{
			temp=PLC_RAM16(RAM_T_ADDR + temp2 + Transfer);
			PLC_Addr++;
			break;         
		}			
		case  0x8684: //算出C的地址
		{
			temp=PLC_RAM16(RAM_C_ADDR + temp2 + Transfer);
			PLC_Addr++;
			break;         
		}
		case  0x8686: //算出D的地址 
		{
			temp=PLC_RAM16(RAM_D_ADDR + temp2 + Transfer);
			PLC_Addr++; 
			break;         
		}
		case  0x8688: //大于等于D1000
		{
			temp=PLC_RAM16(RAM_D1000_ADDR + temp2 + Transfer);
			PLC_Addr++;
			break;         
		}
		 	/************************************************K寄存器"V"*******************************************************************/
		case  0x9080: temp=temp2+D8029,PLC_Addr++;break;//算出K的地址，的V0
		case  0x9081: temp=temp2+D8183,PLC_Addr++;break;//算出K的地址，的V1
		case  0x9082: temp=temp2+D8185,PLC_Addr++;break;//算出K的地址，的V2
		case  0x9083: temp=temp2+D8187,PLC_Addr++;break;//算出K的地址，的V3
		case  0x9084: temp=temp2+D8189,PLC_Addr++;break;//算出K的地址，的V4
		case  0x9085: temp=temp2+D8191,PLC_Addr++;break;//算出K的地址，的V5
		case  0x9086: temp=temp2+D8193,PLC_Addr++;break;//算出K的地址，的V6
		case  0x9087: temp=temp2+D8195,PLC_Addr++;break;//算出K的地址，的V7
		case  0x9280: temp=temp2+D8029,PLC_Addr++;break;//算出H的地址，的V0
		case  0x9281: temp=temp2+D8183,PLC_Addr++;break;//算出H的地址，的V1
		case  0x9282: temp=temp2+D8185,PLC_Addr++;break;//算出H的地址，的V2
		case  0x9283: temp=temp2+D8187,PLC_Addr++;break;//算出H的地址，的V3
		case  0x9284: temp=temp2+D8189,PLC_Addr++;break;//算出H的地址，的V4
		case  0x9285: temp=temp2+D8191,PLC_Addr++;break;//算出H的地址，的V5
		case  0x9286: temp=temp2+D8193,PLC_Addr++;break;//算出H的地址，的V6
		case  0x9287: temp=temp2+D8195,PLC_Addr++;break;//算出H的地址，的V7 	
		/************************************************K1M0寄存器"V"*******************************************************************/
		case  0x9482: mov_d_addr=+V0_V3(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000000F,PLC_Addr++;break;//K1M0的V0-V3	
		case  0x9483: mov_d_addr=+V4_V7(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000000F,PLC_Addr++;break;//K1M0的V4-V7	
		/************************************************K2M0寄存器"V"*******************************************************************/
		case  0x9484: mov_d_addr=+V0_V3(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X000000FF,PLC_Addr++;break;//K2M0的V0-V3	
		case  0x9485: mov_d_addr=+V4_V7(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X000000FF,PLC_Addr++;break;//K2M0的V4-V7		
		/************************************************K3M0寄存器"V"*******************************************************************/
		case  0x9486: mov_d_addr=+V0_V3(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X00000FFF,PLC_Addr++;break;//K3M0的V0-V3	
		case  0x9487: mov_d_addr=+V4_V7(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X00000FFF,PLC_Addr++;break;//K3M0的V4-V7		
		/************************************************K4M0寄存器"V"*******************************************************************/
		case  0x9488: mov_d_addr=+V0_V3(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000FFFF,PLC_Addr++;break;//K4M0的V0-V3	
		case  0x9489: mov_d_addr=+V4_V7(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000FFFF,PLC_Addr++;break;//K4M0的V4-V7						
		/************************************************T寄存器"V"*******************************************************************/
		case  0x9682: temp=PLC_RAM16(RAM_T_ADDR+PLC_D_C_T_addr(temp2)+V0_V3(temp2)*2),PLC_Addr++;break;//算出T的地址，的V0-V3
		case  0x9683: temp=PLC_RAM16(RAM_T_ADDR+PLC_D_C_T_addr(temp2)+V4_V7(temp2)*2),PLC_Addr++;break;//算出T的地址，的V4-V7
		/************************************************C寄存器"V"*******************************************************************/
		case  0x9684: temp=PLC_RAM16(RAM_C_ADDR+PLC_D_C_T_addr(temp2)+V0_V3(temp2)*2),PLC_Addr++;break;//算出C的地址，的V0-V3
		case  0x9685: temp=PLC_RAM16(RAM_C_ADDR+PLC_D_C_T_addr(temp2)+V4_V7(temp2)*2),PLC_Addr++;break;//算出C的地址，的V4-V7
		/************************************************D寄存器"V"*******************************************************************/
		case  0x9686: temp=PLC_RAM16(RAM_D_ADDR+PLC_D_C_T_addr(temp2)+V0_V3(temp2)*2),PLC_Addr++;break;//算出D的地址，的V0-V3
		case  0x9687: temp=PLC_RAM16(RAM_D_ADDR+PLC_D_C_T_addr(temp2)+V4_V7(temp2)*2),PLC_Addr++;break;//算出D的地址，的V4-V7
		case  0x9688: temp=PLC_RAM16(RAM_D1000_ADDR+PLC_D_C_T_addr(temp2)+V0_V3(temp2)*2),PLC_Addr++;break;//大于等于D1000的V0-V3
		case  0x9689: temp=PLC_RAM16(RAM_D1000_ADDR+PLC_D_C_T_addr(temp2)+V4_V7(temp2)*2),PLC_Addr++;break;//大于等于D1000的V4-V7
	
		case  0xA080: temp=temp2+D8028,PLC_Addr++;break;//算出K的地址，的Z0
		case  0xA081: temp=temp2+D8182,PLC_Addr++;break;//算出K的地址，的Z1
		case  0xA082: temp=temp2+D8184,PLC_Addr++;break;//算出K的地址，的Z2
		case  0xA083: temp=temp2+D8186,PLC_Addr++;break;//算出K的地址，的Z3
		case  0xA084: temp=temp2+D8188,PLC_Addr++;break;//算出K的地址，的Z4
		case  0xA085: temp=temp2+D8190,PLC_Addr++;break;//算出K的地址，的Z5
		case  0xA086: temp=temp2+D8192,PLC_Addr++;break;//算出K的地址，的Z6
		case  0xA087: temp=temp2+D8194,PLC_Addr++;break;//算出K的地址，的Z7 	
		case  0xA280: temp=temp2+D8028,PLC_Addr++;break;//算出H的地址，的Z0
		case  0xA281: temp=temp2+D8182,PLC_Addr++;break;//算出H的地址，的Z1
		case  0xA282: temp=temp2+D8184,PLC_Addr++;break;//算出H的地址，的Z2
		case  0xA283: temp=temp2+D8186,PLC_Addr++;break;//算出H的地址，的Z3
		case  0xA284: temp=temp2+D8188,PLC_Addr++;break;//算出H的地址，的Z4
		case  0xA285: temp=temp2+D8190,PLC_Addr++;break;//算出H的地址，的Z5
		case  0xA286: temp=temp2+D8192,PLC_Addr++;break;//算出H的地址，的Z6
		case  0xA287: temp=temp2+D8194,PLC_Addr++;break;//算出H的地址，的Z7 	
		/************************************************K1M0寄存器"Z"*******************************************************************/
		case  0xA482: mov_d_addr=+Z0_Z3(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000000F,PLC_Addr++;break;//K1M0的V0-V3	
		case  0xA483: mov_d_addr=+Z4_Z7(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000000F,PLC_Addr++;break;//K1M0的V4-V7	
		/************************************************K2M0寄存器"Z"*******************************************************************/
		case  0xA484: mov_d_addr=+Z0_Z3(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X000000FF,PLC_Addr++;break;//K2M0的V0-V3	
		case  0xA485: mov_d_addr=+Z4_Z7(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X000000FF,PLC_Addr++;break;//K2M0的V4-V7		
		/************************************************K3M0寄存器"Z"*******************************************************************/
		case  0xA486: mov_d_addr=+Z0_Z3(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X00000FFF,PLC_Addr++;break;//K3M0的V0-V3	
		case  0xA487: mov_d_addr=+Z4_Z7(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X00000FFF,PLC_Addr++;break;//K3M0的V4-V7		
		/************************************************K4M0寄存器"Z"*******************************************************************/
		case  0xA488: mov_d_addr=+Z0_Z3(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000FFFF,PLC_Addr++;break;//K4M0的V0-V3
		case  0xA489: mov_d_addr=+Z4_Z7(temp2),temp=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000FFFF,PLC_Addr++;break;//K4M0的V4-V7		
		case  0xA682: temp=PLC_RAM16(RAM_T_ADDR+PLC_D_C_T_addr(temp2)+Z0_Z3(temp2)*2),PLC_Addr++;break;//算出T的地址，的Z0-Z3
		case  0xA683: temp=PLC_RAM16(RAM_T_ADDR+PLC_D_C_T_addr(temp2)+Z4_Z7(temp2)*2),PLC_Addr++;break;//算出T的地址，的Z4-Z7
		/************************************************C寄存器"Z"*******************************************************************/
		case  0xA684: temp=PLC_RAM16(RAM_C_ADDR+PLC_D_C_T_addr(temp2)+Z0_Z3(temp2)*2),PLC_Addr++;break;//算出C的地址，的Z0-Z3
		case  0xA685: temp=PLC_RAM16(RAM_C_ADDR+PLC_D_C_T_addr(temp2)+Z4_Z7(temp2)*2),PLC_Addr++;break;//算出C的地址，的Z4-Z7
		/************************************************D寄存器"Z"*******************************************************************/
		case  0xA686: temp=PLC_RAM16(RAM_D_ADDR+PLC_D_C_T_addr(temp2)+Z0_Z3(temp2)*2),PLC_Addr++;break;//算出D的地址，的Z0-Z3
		case  0xA687: temp=PLC_RAM16(RAM_D_ADDR+PLC_D_C_T_addr(temp2)+Z4_Z7(temp2)*2),PLC_Addr++;break;//算出D的地址，的Z4-Z7
		case  0xA688: temp=PLC_RAM16(RAM_D1000_ADDR+PLC_D_C_T_addr(temp2)+Z0_Z3(temp2)*2),PLC_Addr++;break;//大于等于D1000的Z0-Z3
		case  0xA689: temp=PLC_RAM16(RAM_D1000_ADDR+PLC_D_C_T_addr(temp2)+Z4_Z7(temp2)*2),PLC_Addr++;break;//大于等于D1000的Z4-Z7
	}	
	return temp;
}

//=======================================================================================================
// 函数名称:  static u32 cos_u32_value(void) 
// 功能描述： 
// 输　入:  void      
// 输　出:  输出32位数据
// 全局变量:  
//-------------------------------------------------------------------------------------------------------
// 修改人:
// 日　期:
// 备  注: 
//-------------------------------------------------------------------------------------------------------
//=======================================================================================================
#define  D_data  u32data.data 
static s32 cos_u32_value(void)                                      
{   
	 u16 Type_F,temp2,Data1,Data2;
	 unsigned short temp;
	 Data1=*PLC_Addr;
	 PLC_Addr++;
	 Data2=*PLC_Addr;
	 temp2=Type_F=0;
	 Type_F   = (Data1 & 0xff00);
	 Type_F  |= (Data2 >> 8);
/************************************/	
	 temp2  = (Data2 << 8);
	 temp2 |=mov_d_addr=(u8)Data1;
/************************************/
	switch(Type_F)
	{		
		case  0x8080: u32data.data1[0]=temp2,PLC_Addr++,u32data.data1[1]=cos_value(),PLC_Addr-=2;break;//算出K值
		case  0x8280: u32data.data1[0]=temp2,PLC_Addr++,u32data.data1[1]=cos_value(),PLC_Addr-=2;break;//算出H值
		case  0x8482: D_data=MOV_K(*PLC_Addr)&0X0000000F,Transfer_bit1=1,PLC_Addr++;break;//进行如 K4M0 之类的传送    
		case  0x8484: D_data=MOV_K(*PLC_Addr)&0X000000FF,Transfer_bit1=1,PLC_Addr++;break;//进行如 K4M0 之类的传送    
		case  0x8486: D_data=MOV_K(*PLC_Addr)&0X00000FFF,Transfer_bit1=1,PLC_Addr++;break;//进行如 K4M0 之类的传送    
		case  0x8488: D_data=MOV_K(*PLC_Addr)&0X0000FFFF,Transfer_bit1=1,PLC_Addr++;break;//进行如 K4M0 之类的传送    
		case  0x848A: D_data=MOV_K(*PLC_Addr)&0X000FFFFF,Transfer_bit1=1,PLC_Addr++;break;//进行如 K4M0 之类的传送    
		case  0x848C: D_data=MOV_K(*PLC_Addr)&0X00FFFFFF,Transfer_bit1=1,PLC_Addr++;break;//进行如 K4M0 之类的传送    
		case  0x848E: D_data=MOV_K(*PLC_Addr)&0X0FFFFFFF,Transfer_bit1=1,PLC_Addr++;break;//进行如 K4M0 之类的传送    
		case  0x8490: D_data=MOV_K(*PLC_Addr),           Transfer_bit1=1,PLC_Addr++;break;//进行如 K4M0 之类的传送   
			
		case  0x8680: D_data=PLC_RAM32(RAM_D8000_ADDR+temp2),PLC_Addr++;break;             //算出D的地址 D8000
		case  0x8682: D_data=PLC_RAM32(RAM_T_ADDR+temp2),PLC_Addr++;break;                 //算出T的地址 
		case  0x8684: D_data=PLC_RAM32(RAM_C_ADDR+temp2),PLC_Addr++;break;                 //算出C的地址
		case  0x8686: D_data=PLC_RAM32(RAM_D_ADDR+temp2),PLC_Addr++;break;                 //算出D的地址 
		case  0x8688: D_data=PLC_RAM32(RAM_D1000_ADDR+temp2),PLC_Addr++;break;                 //大于等于D1000
		case  0xA080: u32data.data1[0]=temp2+D8028,PLC_Addr++,u32data.data1[1]=cos_value()+D8029,PLC_Addr-=2;break;//算出K的地址，的Z0
		case  0xA081: u32data.data1[0]=temp2+D8182,PLC_Addr++,u32data.data1[1]=cos_value()+D8183,PLC_Addr-=2;break;//算出K的地址，的Z1
		case  0xA082: u32data.data1[0]=temp2+D8184,PLC_Addr++,u32data.data1[1]=cos_value()+D8185,PLC_Addr-=2;break;//算出K的地址，的Z2
		case  0xA083: u32data.data1[0]=temp2+D8186,PLC_Addr++,u32data.data1[1]=cos_value()+D8187,PLC_Addr-=2;break;//算出K的地址，的Z3
		case  0xA084: u32data.data1[0]=temp2+D8188,PLC_Addr++,u32data.data1[1]=cos_value()+D8189,PLC_Addr-=2;break;//算出K的地址，的Z4
		case  0xA085: u32data.data1[0]=temp2+D8190,PLC_Addr++,u32data.data1[1]=cos_value()+D8191,PLC_Addr-=2;break;//算出K的地址，的Z5
		case  0xA086: u32data.data1[0]=temp2+D8192,PLC_Addr++,u32data.data1[1]=cos_value()+D8193,PLC_Addr-=2;break;//算出K的地址，的Z6
		case  0xA087: u32data.data1[0]=temp2+D8194,PLC_Addr++,u32data.data1[1]=cos_value()+D8195,PLC_Addr-=2;break;//算出K的地址，的Z7 
		case  0xA280: u32data.data1[0]=temp2+D8028,PLC_Addr++,u32data.data1[1]=cos_value()+D8029,PLC_Addr-=2;break;//算出H的地址，的Z0
		case  0xA281: u32data.data1[0]=temp2+D8182,PLC_Addr++,u32data.data1[1]=cos_value()+D8183,PLC_Addr-=2;break;//算出H的地址，的Z1
		case  0xA282: u32data.data1[0]=temp2+D8184,PLC_Addr++,u32data.data1[1]=cos_value()+D8185,PLC_Addr-=2;break;//算出H的地址，的Z2
		case  0xA283: u32data.data1[0]=temp2+D8186,PLC_Addr++,u32data.data1[1]=cos_value()+D8187,PLC_Addr-=2;break;//算出H的地址，的Z3
		case  0xA284: u32data.data1[0]=temp2+D8188,PLC_Addr++,u32data.data1[1]=cos_value()+D8189,PLC_Addr-=2;break;//算出H的地址，的Z4
		case  0xA285: u32data.data1[0]=temp2+D8190,PLC_Addr++,u32data.data1[1]=cos_value()+D8191,PLC_Addr-=2;break;//算出H的地址，的Z5
		case  0xA286: u32data.data1[0]=temp2+D8192,PLC_Addr++,u32data.data1[1]=cos_value()+D8193,PLC_Addr-=2;break;//算出H的地址，的Z6
		case  0xA287: u32data.data1[0]=temp2+D8194,PLC_Addr++,u32data.data1[1]=cos_value()+D8195,PLC_Addr-=2;break;//算出H的地址，的Z7 				
		/************************************************K1M0寄存器"Z"*******************************************************************/
		case  0xA482: mov_d_addr=+DZ0_Z3(temp2),D_data=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000000F,PLC_Addr++;break;//K1M0的V0-V3	
		case  0xA483: mov_d_addr=+DZ4_Z7(temp2),D_data=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000000F,PLC_Addr++;break;//K1M0的V4-V7	
		/************************************************K2M0寄存器"Z"*******************************************************************/
		case  0xA484: mov_d_addr=+DZ0_Z3(temp2),D_data=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X000000FF,PLC_Addr++;break;//K2M0的V0-V3	
		case  0xA485: mov_d_addr=+DZ4_Z7(temp2),D_data=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X000000FF,PLC_Addr++;break;//K2M0的V4-V7		
		/************************************************K3M0寄存器"Z"*******************************************************************/
		case  0xA486: mov_d_addr=+DZ0_Z3(temp2),D_data=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X00000FFF,PLC_Addr++;break;//K3M0的V0-V3	
		case  0xA487: mov_d_addr=+DZ4_Z7(temp2),D_data=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X00000FFF,PLC_Addr++;break;//K3M0的V4-V7		
		/************************************************K4M0寄存器"Z"*******************************************************************/
		case  0xA488: mov_d_addr=+DZ0_Z3(temp2),D_data=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000FFFF,PLC_Addr++;break;//K4M0的V0-V3
		case  0xA489: mov_d_addr=+DZ4_Z7(temp2),D_data=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0000FFFF,PLC_Addr++;break;//K4M0的V4-V7		
		/************************************************K5M0寄存器"Z"*******************************************************************/
		case  0xA48A: mov_d_addr=+DZ0_Z3(temp2),D_data=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X000FFFFF,PLC_Addr++;break;//K1M0的V0-V3	
		case  0xA48B: mov_d_addr=+DZ4_Z7(temp2),D_data=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X000FFFFF,PLC_Addr++;break;//K1M0的V4-V7	
		/************************************************K6M0寄存器"Z"*******************************************************************/
		case  0xA48C: mov_d_addr=+DZ0_Z3(temp2),D_data=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X00FFFFFF,PLC_Addr++;break;//K2M0的V0-V3	
		case  0xA48D: mov_d_addr=+DZ4_Z7(temp2),D_data=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X00FFFFFF,PLC_Addr++;break;//K2M0的V4-V7		
		/************************************************K7M0寄存器"Z"*******************************************************************/
		case  0xA48E: mov_d_addr=+DZ0_Z3(temp2),D_data=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0FFFFFFF,PLC_Addr++;break;//K3M0的V0-V3	
		case  0xA48F: mov_d_addr=+DZ4_Z7(temp2),D_data=MOV_K(PLC_D_C_T_addr(temp2)/0x100)&0X0FFFFFFF,PLC_Addr++;break;//K3M0的V4-V7		
		/************************************************K8M0寄存器"Z"*******************************************************************/
		case  0xA490: mov_d_addr=+DZ0_Z3(temp2),D_data=MOV_K(PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K4M0的V0-V3
		case  0xA491: mov_d_addr=+DZ4_Z7(temp2),D_data=MOV_K(PLC_D_C_T_addr(temp2)/0x100),PLC_Addr++;break;//K4M0的V4-V7	
		/************************************************T寄存器"Z"*******************************************************************/
		case  0xA682: {temp=(PLC_D_C_T_addr(temp2)+DZ0_Z3(temp2)*2);if(temp>=510) PLC_PROG_ERROR(M8067,6706),D_data=0; else D_data=PLC_RAM32(RAM_T_ADDR+temp),PLC_Addr++;}break;//算出T的地址，的Z0-Z3
		case  0xA683: {temp=(PLC_D_C_T_addr(temp2)+DZ4_Z7(temp2)*2);if(temp>=510) PLC_PROG_ERROR(M8067,6706),D_data=0; else D_data=PLC_RAM32(RAM_T_ADDR+temp),PLC_Addr++;}break;//算出T的地址，的Z4-Z7
		/************************************************C寄存器"Z"*******************************************************************/
		case  0xA684: {temp=(PLC_D_C_T_addr(temp2)+DZ0_Z3(temp2)*2);if(temp>=510) PLC_PROG_ERROR(M8067,6706),D_data=0; else D_data=PLC_RAM32(RAM_C_ADDR+temp),PLC_Addr++;}break;//算出C的地址，的Z0-Z3
		case  0xA685: {temp=(PLC_D_C_T_addr(temp2)+DZ4_Z7(temp2)*2);if(temp>=510) PLC_PROG_ERROR(M8067,6706),D_data=0; else D_data=PLC_RAM32(RAM_C_ADDR+temp),PLC_Addr++;}break;//算出C的地址，的Z4-Z7
		/************************************************D寄存器"Z"*******************************************************************/
		case  0xA686: {temp=(PLC_D_C_T_addr(temp2)+DZ0_Z3(temp2)*2);if(temp>=15998) PLC_PROG_ERROR(M8067,6706),D_data=0; else D_data=PLC_RAM32(RAM_D_ADDR+temp);PLC_Addr++;}break;//算出D的地址，的Z0-Z3
		case  0xA687: {temp=(PLC_D_C_T_addr(temp2)+DZ4_Z7(temp2)*2);if(temp>=15998) PLC_PROG_ERROR(M8067,6706),D_data=0; else D_data=PLC_RAM32(RAM_D_ADDR+temp);PLC_Addr++;}break;//算出D的地址，的Z4-Z7
		case  0xA688: {temp=(PLC_D_C_T_addr(temp2)+DZ0_Z3(temp2)*2);if(temp>=13998) PLC_PROG_ERROR(M8067,6706),D_data=0; else D_data=PLC_RAM32(RAM_D1000_ADDR+temp),PLC_Addr++;}break;//大于等于D1000的Z0-Z3
		case  0xA689: {temp=(PLC_D_C_T_addr(temp2)+DZ4_Z7(temp2)*2);if(temp>=13998) PLC_PROG_ERROR(M8067,6706),D_data=0; else D_data=PLC_RAM32(RAM_D1000_ADDR+temp),PLC_Addr++;}break;//大于等于D1000的Z4-Z7			
	}
	  PLC_Addr+=2;
			return D_data;
}
//=======================================================================================================
// 函数名称: static float float_value(void)
// 功能描述： 
// 输　入:  void      
// 输　出:  输出float数据
// 全局变量:  

//-------------------------------------------------------------------------------------------------------
// 修改人:
// 日　期:
// 备  注: 
//-------------------------------------------------------------------------------------------------------
//=======================================================================================================
static float float_value(void)                                     
{  
	 u16 Type_F,temp1,temp2,Data1,Data2,Data3,Data4;
	 Data1=*PLC_Addr;
	 PLC_Addr++;
	 Data2=*PLC_Addr;
	 PLC_Addr++;
	 Data3=*PLC_Addr;
	 PLC_Addr++;
	 Data4=*PLC_Addr;
	
	 Type_F   = (Data1 & 0xff00);
	 Type_F  |= (Data2 >> 8);
/************************************/	
	 temp1  = (Data2 << 8);
	 temp1 |=(u8)Data1;
/************************************/
	 temp2  = (Data4 << 8);
	 temp2 |=(u8)Data3;
/************************************/	
	 if(Type_F == 0x8080)      u32data.data1[0]=temp1,u32data.data1[1]=temp2, FLOAT.DATA=(float)u32data.data, PLC_Addr++;//算出K值
	 else if(Type_F == 0x8280) u32data.data1[0]=temp1,u32data.data1[1]=temp2, FLOAT.DATA=(float)u32data.data, PLC_Addr++;//算出H值            
	 else if(Type_F == 0x8680) FLOAT.DATA=PLC_RAMfolta(RAM_D8000_ADDR+temp1),PLC_Addr++;             //算出D的地址 D8000
	 else if(Type_F == 0x8682) FLOAT.DATA=PLC_RAMfolta(RAM_T_ADDR+temp1),PLC_Addr++;                 //算出T的地址 
	 else if(Type_F == 0x8684) FLOAT.DATA=PLC_RAMfolta(RAM_C_ADDR+temp1),PLC_Addr++;                 //算出C的地址
	 else if(Type_F == 0x8686) FLOAT.DATA=PLC_RAMfolta(RAM_D_ADDR+temp1),PLC_Addr++;                 //算出D的地址 
   else if(Type_F == 0x8688) FLOAT.DATA=PLC_RAMfolta(RAM_D1000_ADDR+temp1),PLC_Addr++;             //大于等于D1000
	 return FLOAT.DATA;
}

static void RST_D(void)                                 
{  
	u8 temp,addr,l_value;
	if(PLC_ACC_BIT&0x01)
	{   
		l_value=*PLC_Addr;
		PLC_Addr++;
		addr=*PLC_Addr/0x100;
		
		if(addr==0x86)
			temp=l_value+((*PLC_Addr%0x100)*0x100),temp=0x0700+temp/2,plc_16BitBuf[temp]=0;
		else if(addr==0x88)
			temp=l_value+((*PLC_Addr%0x100)*0x100),temp=0x1000+temp/2,plc_16BitBuf[temp]=0;
		else if(addr==0x80)
			temp=l_value+((*PLC_Addr%0x100)*0x100),temp=0x1000+temp/2+1000,plc_16BitBuf[temp]=0;
		else 
	    PLC_PROG_ERROR(M8065,6501);
		
//		PLC_Addr++;
	}
	else PLC_Addr+=2;
}

//=======================================================================================================
// 函数名称:  static void target(void)
// 功能描述： 加减法“与”“或”“异或”共用赋值函数	如DEMOV、DEADD、DESUB等指令的结果传递出去
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void float_target(void)		
{ 
	u16 temp;
	temp=addr_value() ;            
	plc_16BitBuf[temp]=FLOAT.DATA1[0];
	plc_16BitBuf[temp+1]=FLOAT.DATA1[1];
	PLC_Addr+=2;
}

static void PID(void)
{
	s16 PVn;  // 测量值
	s16 SV;   // 设定目标Desired value
	s16 Ts;   // 取样时间
	s32 Su;
	s16 KP; // P 
	s16 Ti; // I
	s16 KD; // D
	s16 TD; // 微分增益
	u32 Addr,Addr1; // 地址记录
	u32 csp;        // PID内部计算地址
	if((PLC_ACC_BIT&0X01)==0X01)        //母线成立不
	{
		SV=PLC_RAM16(addr_value_prog());   // 设定值
		PVn=PLC_RAM16(addr_value_prog());  // 测量数据
		Addr=addr_value_prog();            // 读取参数起始地址
		Addr1= addr_value_prog();
		Ts=PLC_RAM16(Addr);                // 取样时间
		KP=PLC_RAM16(Addr+6);  // P
		Ti=PLC_RAM16(Addr+8);  // I
		KD=PLC_RAM16(Addr+10); // D
		TD=PLC_RAM16(Addr+12); // 微分增益
		csp=Addr+14;           // 内部计算起始地址
		 
		PLC_RAM16(csp+14)=KP*((PLC_RAM16(csp)-PLC_RAM16(csp+2))+((Ts/Ti)*PLC_RAM16(csp))+PLC_RAM16(csp+10)); 
		
		PLC_RAM16(csp) = PLC_RAM16(csp+4) - SV;         // 计算本次偏差值    
		
		PLC_RAM16(csp+4)= PLC_RAM16(Addr+4)*PLC_RAM16(csp+6)+(1-PLC_RAM16(Addr+4))*PVn; 
		
		PLC_RAM16(csp+10)=(TD/(Ts+KD*TD))*(-2*PLC_RAM16(csp+6)+PLC_RAM16(csp+4)+PLC_RAM16(csp+8))+((KD*TD)/(Ts+KD*TD))*PLC_RAM16(csp+12);
		
		
		Su=PLC_RAM16(Addr1)+PLC_RAM16(csp+14);
		if(Su>32766)       PLC_RAM16(Addr1)=32767;
		else if(Su<-32767) PLC_RAM16(Addr1)=-32768;
		else PLC_RAM16(Addr1)= Su;
		PLC_RAM16(csp+12)=PLC_RAM16(csp+10);
		PLC_RAM16(csp+8)=PLC_RAM16(csp+6);
		PLC_RAM16(csp+6)=PLC_RAM16(csp+4);
		PLC_RAM16(csp+2)=PLC_RAM16(csp);
	}
}

/*  Read */
static void MOV(void)	          //MOV
{
	if(PLC_ACC_BIT&0X01)         // 判断条件满足否
	{
		trade=cos_value();
		target();
  }		
	else
	{
		PLC_Addr+=4;		           // 条件不满足执行跳过程序，减小CPU开销
	}
}

static void DMOV(void)	        //单精度数据传递
{  	
   if(PLC_ACC_BIT&0X01)
   trade=cos_u32_value(),D_target(); 
   else	 
   PLC_Addr+=8;		              //条件不满足执行跳过程序，减小CPU开销
}

static void DEMOV(void)	        //单精度数据传递
{  	
   if(PLC_ACC_BIT&0X01)
   trade=float_value(),float_target(); 
   else	 
   PLC_Addr+=8;		              //条件不满足执行跳过程序，减小CPU开销
}

//======================================================================================================
// 函数名称: static void ZRST(void) 
// 功能描述： ZRST指令函数
// 输　入:  void      
// 输　出:  void     
// 全局变量:  
 
//=======================================================================================================
static void ZRST(void)  
{ 
	u16 temp,temp1;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp=addr_value();
		temp1=addr_value();
		if(Flag_bit==0x00) 
		{
			for(;temp<=temp1;temp++)
			PLC_BIT_OFF(temp);
		} 
		else
		{		 
			for(;temp<=temp1;temp++)
			plc_16BitBuf[temp]=0;
		}
	}
	else PLC_Addr+=4;
}

static void MTR(void)
{
	u16 X,Y,M_Y,K_H,temp=0;
	u8 i,t;	 
	if(PLC_ACC_BIT&0X01)
	{
		X=addr_value();
		Y=addr_value();
		M_Y=addr_value();
		K_H=cos_value();
		for(i=0;i<K_H;i++)
		{
			temp=i*7;
			PLC_BIT_ON(Y+i);
			for(t=0;t<=7;t++)
			(PLC_BIT_TEST(X+t)) ? PLC_BIT_ON(M_Y+temp+t) : PLC_BIT_OFF(M_Y+temp+t);
		}
	}
	else PLC_Addr+=8;
}


static void REFF(void)
{
	 if(PLC_ACC_BIT&0X01)
		X_DIY=cos_value();
	 else 
		 X_DIY=10,PLC_Addr+=2;
}
//======================================================================================================
// 函数名称:  static void DSQR(void)
// 功能描述： 32位开方计算 DSQR
// 输　入:  void      
// 输　出:  void     
// 全局变量:  
// 调用模块: 

//=======================================================================================================
static void DSQR(void)
{
   if((PLC_ACC_BIT&0X01)==0X01)
   {
  		trade=(u32)sqrt((double)cos_u32_value());
	    target();	
   } 
}

static void HSCS(void)		//高速计数置位
{ 
	s32 temp1,temp2;
	temp1=cos_u32_value();
	temp2=cos_u32_value();
	if((temp2==temp1)&&((PLC_ACC_BIT&0X01)==0X01)) 
	PLC_ACC_BIT|=1;
	else
	PLC_ACC_BIT&=~1;
}

//======================================================================================================
// 函数名称:  static void SQR(void)
// 功能描述： 16位右移位 RCR指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void SQR(void)
{
   if((PLC_ACC_BIT&0X01)==0X01)
   {
  		trade=(u16)sqrt((double)cos_value());
	    target();	
   } 
}
//======================================================================================================
// 函数名称:  static void DRCR(void)
// 功能描述： 32位右移位 RCR指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void DRCR(void)	                 
{  	
	u32 temp1,temp2;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_u32_value(); 
		temp2=cos_u32_value(); 
		trade=temp1>>temp2;
		PLC_Addr-=8;
		D_target();	
		PLC_Addr+=4;
	}
}
//======================================================================================================
// 函数名称:  static void RCR(void)
// 功能描述： 16位右移位 RCR指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void RCR(void)	                 
{  	
	u16 temp1,temp2;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_value(); 
		temp2=cos_value(); 
		trade=temp1>>temp2;
		PLC_Addr-=4;
		target();	
		PLC_Addr+=2;
	}
	else PLC_Addr+=4;                       //没有动作跳过4步程序
}
//======================================================================================================
// 函数名称:  static void DROL(void)
// 功能描述： 32位左移位 RCL指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void DRCL(void)	                 
{  	
	u32 temp1,temp2;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_u32_value(); 
		temp2=cos_u32_value();  
		trade=temp1<<temp2;
		PLC_Addr-=8;       
		D_target();	
		PLC_Addr+=4;
	}
}
//======================================================================================================
// 函数名称:  static void ROL(void)
// 功能描述： 16位左移位 RCL指令
// 输　入:  void      
// 输　出:  void     

//=======================================================================================================
static void RCL(void)	                 
{  	
	u16 temp1,temp2;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_value(); 
		temp2=cos_value(); 
		trade=temp1<<temp2;
		PLC_Addr-=4;
		target();	
		PLC_Addr+=2;
	}
	else PLC_Addr+=4;                      //没有动作跳过4步程序
}
//======================================================================================================
// 函数名称:  static void DROR(void)
// 功能描述： 32位循环右位 ROR指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  
 
//=======================================================================================================
static void DROR(void)	                 
{  	
	u32 temp1,temp2;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_u32_value(); 
		temp2=cos_u32_value(); 
		trade=ROTATE_RIGHT(temp1,32,temp2);
		PLC_Addr-=8;
		D_target();	
		PLC_Addr+=4;
	}
}
//======================================================================================================
// 函数名称:  static void ROR(void)
// 功能描述： 16位循环右位 ROR指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void ROR(void)	                 
{  	
	u16 temp1,temp2;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_value(); 
		temp2=cos_value(); 
		trade=ROTATE_RIGHT(temp1,16,temp2);
		PLC_Addr-=4;
		target();	
		PLC_Addr+=2;
	}
	else PLC_Addr+=4;                      //没有动作跳过4步程序
}

//======================================================================================================
// 函数名称:  static void DROL(void)
// 功能描述： 32位循环左移位 ROL指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  
 
//=======================================================================================================
static void DROL(void)	                 
{  	
	u32 temp1,temp2;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_u32_value(); 
		temp2=cos_u32_value();  
		trade=ROTATE_LEFT(temp1,32,temp2);
		PLC_Addr-=8;       
		D_target();	
		PLC_Addr+=4;
	}
}
//======================================================================================================
// 函数名称:  static void ROL(void)
// 功能描述： 16位循环左移位 ROL指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void ROL(void)	                 
{  	
	u16 temp1,temp2;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_value(); 
		temp2=cos_value(); 
		trade=ROTATE_LEFT(temp1,16,temp2);
		PLC_Addr-=4;
		target();	
		PLC_Addr+=2;
	}
	else PLC_Addr+=4;                      //没有动作跳过4步程序
}

//======================================================================================================
// 函数名称:  static void DSWAP(void)	
// 功能描述： 32位上下交换 DSWAP指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void DSWAP(void)	                 
{  	
	u32 temp;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp=cos_u32_value(); 
		trade=swap_u32(temp);
		PLC_Addr-=4;
		D_target();	
	}
}
//=======================================================================================================
// 函数名称:  static void DGBIN(void)	
// 功能描述： 16位上下交换 DGBIN指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void DGBIN(void)	                 
{  	
	signed int temp;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp=cos_value();		  
		trade=GtoB(temp);
		D_target();		 
	}
	else PLC_Addr+=8;                      //没有动作跳过8步程序
}

//=======================================================================================================
// 函数名称:  static void GBIN(void)	
// 功能描述： 16位上下交换 GBIN指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void GBIN(void)	                 
{  	
	signed short int temp;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp=cos_value();		  
		trade=(u16)GtoB((unsigned int)temp);
		target();		 
	}
	else PLC_Addr+=4;                      //没有动作跳过4步程序
}
//=======================================================================================================
// 函数名称:  static void DGRY(void)	
// 功能描述： 16位上下交换 DGRY指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void DGRY(void)	                 
{  	
	signed int temp;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp=cos_value();		  
		trade=BtoG(temp);
		D_target();		 
	}
	else PLC_Addr+=8;                      //没有动作跳过8步程序
}

//=======================================================================================================
// 函数名称:  static void GRY(void)	
// 功能描述： 16位上下交换 GRY指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void GRY(void)	                 
{  	
	signed short int temp;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp=cos_value();		  
		trade=(u16)BtoG((unsigned int)temp);
		target();		 
	}
	else PLC_Addr+=4;                      //没有动作跳过4步程序
}

//=======================================================================================================
// 函数名称:  static void SWAP(void)	
// 功能描述： 16位上下交换 SWAP指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void SWAP(void)	                 
{  	
	signed short int temp;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp=cos_value();	  
		trade=swap_u16(temp);
		PLC_Addr-=2;
		target();		 
	}
	else PLC_Addr+=4;                      //没有动作跳过4步程序
}

//=======================================================================================================
// 函数名称:  static void SFTR(void)	 
// 功能描述： SFTR指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  
 
//=======================================================================================================
//static void SFTR(void)	                 
//{  	
//signed short int addr1,addr2,temp1,temp2,temp5,temp6,i;
//	u8 temp3,temp4;
//   if(PLC_ACC_BIT&0X01)
//   {
//		  addr1=addr_value();
//		  Flag_bit=0xff;
//		 
//		  addr2=addr_value();
//		  Flag_bit=0xff;
//		 
//		  temp1=cos_value();
//		  temp5=cos_value();
//		  temp2=temp5+temp4;
//		  temp6=temp1-temp5;
//  		for(i=0;i<temp6;i++)
//		  { 
//				(PLC_BIT_TEST(addr2+i)) ? PLC_BIT_ON(addr2+i) : PLC_BIT_OFF(addr2+i);
//			}
//			for(;i<temp1;i++)
//		  { 
//				(PLC_BIT_TEST(addr1+i)) ? PLC_BIT_ON(addr2+i) : PLC_BIT_OFF(addr2+i);
//			}
//	 }
//	 else PLC_Addr+=8;                      //没有动作跳过4步程序
//}



//=======================================================================================================
// 函数名称:  static void XCH(void)	 
// 功能描述： 16位交换传送 XCH指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void XCH(void)	                 
{  	
	signed short int temp1,temp2;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_value();
		temp2=cos_value();
		PLC_Addr-=4;
		trade=temp2;
		D_target();
		trade=temp1;
		D_target();
	}
	else PLC_Addr+=4;                      //没有动作跳过4步程序
}
//=======================================================================================================
// 函数名称:  static void DFMOV(void)	 
// 功能描述： 32位交换传送 DXCH指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  
 
//=======================================================================================================
static void DXCH(void)	                 
{  	
	signed int temp1,temp2;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_u32_value();
		temp2=cos_u32_value();
		PLC_Addr-=8;
		trade=temp2;
		D_target();
		trade=temp1;
		D_target();
	}
}
//=======================================================================================================
// 函数名称:  static void DFMOV(void)	 
// 功能描述： 32位多点传送 DFMOV指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  
 
//=======================================================================================================
static void DFMOV(void)	               
{  	
	signed short int temp,i;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		trade=cos_u32_value();            //要传递的数据
		D_target();                       //把第一个传递出去
		temp=cos_u32_value();             //            <<<-------------|
		PLC_Addr-=4;                      //PLC_Addr-=4是为了调回到上面 |<<-----|
		for(i=1;i<temp;i++)               //                                    |
		{                                 //                                    |
			if(Transfer_bit==1)Transfer=i*32;//                                  |
			else Transfer=i*4;             //                                    |
			PLC_Addr-=4;D_target();        //PLC_Addr-=4是为了传递出去要的位数	  |
		}                                 //                                    |
		PLC_Addr+=2;Transfer=0;           //PLC_Addr+=2是为了跳过上面调回去的---|
	}
}

//=======================================================================================================
// 函数名称:  static void FMOV(void)	 
// 功能描述： 16位多点传送 FMOV指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void FMOV(void)	             
{  	
	signed short int temp,i;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		trade=cos_value();                //要传递的数据
		target();                         //把第一个传递出去
		temp=cos_value();                 //            <<<-------------|
		PLC_Addr-=2;                      //PLC_Addr-=2是为了调回到上面 |<<-----|
		for(i=1;i<temp;i++)               //                                    |
		{                                 //                                    |
			if(Transfer_bit==1)
				Transfer=i*16;//                                  |
			else 
				Transfer=i*2;             //                                    |
			PLC_Addr-=2;
			target();          //PLC_Addr-=2是为了传递出去要的位数	  |
		}                                 //                                    |
		PLC_Addr+=2;
		Transfer=0;           //PLC_Addr+=2是为了跳过上面调回去的---|
	}
	else 
	{
		PLC_Addr+=6;                    //没有动作跳过6步程序
	}
}

//=======================================================================================================
// 函数名称:  static void FMOV(void)	 
// 功能描述： 16位成批传送 BMOV指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void BMOV(void)	                 
{  	
	signed short int n,i;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
			trade=cos_value();                //要传递的数据
			target();                         //把第一个传递出去
			n=cos_value();
		
			PLC_Addr-=2;                      //<<<---------------------------------|
			for(i = 1;i < n;i++)                
			{                                   
				if(Transfer_bit1==1)
					Transfer=i*16;                  
				else 
					Transfer=i*2;                   
				
				PLC_Addr-=4;                              
				trade=cos_value();                //要传递的数据
				
				if(Transfer_bit==1)
					Transfer =i*16;
				else 
					Transfer =i*2;
				target();                         //把第传递出去
			}         
			PLC_Addr+=2;                        //PLC_Addr+=2是为了跳过上面调回去的-----|
			Transfer=0;             						
	}
	else 
	{
		PLC_Addr+=6;                          //按键没按下就跳过6步程序
	}
}

static void DCML(void)	                 //32位数据取反
{  	
	s32 temp1;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_u32_value();
		trade=~temp1;
		D_target();
	}
	else PLC_Addr+=8;                     //跳过8步程序
}

static void CML(void)	                    //数据取反
{  	
	signed short int temp1;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_value();
		trade=~temp1;
		target();
	}
	else PLC_Addr+=4;                   //跳过4步程序
}


u16 bcd[4]={0x1,0x10,0x100,0x1000};
static void SMOV(void)	          //16位比较传送指令
{ 
	u16 temp1,temp2,temp3,temp4,temp5,temp6;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_value();
		temp2=cos_value();
		temp3=cos_value();
		temp4=addr_value();
		temp5=cos_value();
		temp1%=bcd[temp2];
		for(temp6=0;temp6<temp3;temp6++)
		{ 
			temp2--;temp5--;
			plc_16BitBuf[temp4]|=(temp1/bcd[temp2])*bcd[temp5];
			if((temp2==1)&&(temp5==1))plc_16BitBuf[temp4]|=temp1%0x10*bcd[temp5], temp6=temp3+1;
			else temp1%=bcd[temp2];
		}
	}
	else PLC_Addr+=10;              //跳过10步程序
}

//======================================================================================================
// 函数名称: static void TZCP(void)
// 功能描述：TZCP指令函数
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void TZCP(void)
{
	u16 h,min,s,temp,temp1,temp3,temp4,h1,min1,s1;
	if((PLC_ACC_BIT&0X01)==0X01)
	{	
		temp3=addr_value();               //S1 区域比较下限值
		h=plc_16BitBuf[temp3];
		min=plc_16BitBuf[temp3+1];
		s=plc_16BitBuf[temp3+2];
		
		temp4=addr_value();               //S2 区域比较上限值
		h1=plc_16BitBuf[temp4];
		min1=plc_16BitBuf[temp4+1];
		s1=plc_16BitBuf[temp4+2];
		
		temp=addr_value();                //S3 时间比较值
		
		temp1=addr_value();Flag_bit=0XFF;
		PLC_BIT_OFF(temp1);PLC_BIT_OFF(temp1+1);PLC_BIT_OFF(temp1+2);  
		if((h>=plc_16BitBuf[temp])&&(min>=plc_16BitBuf[temp+1])&&(s>plc_16BitBuf[temp+2]))
		{PLC_BIT_ON(temp1);}
		else if(((h<=plc_16BitBuf[temp])&&(min<=plc_16BitBuf[temp+1])&&(s<=plc_16BitBuf[temp+1]))&&((h1>=plc_16BitBuf[temp])&&(min1>=plc_16BitBuf[temp+1])&&(s1>=plc_16BitBuf[temp+2])))
		{PLC_BIT_ON(temp1+1);}
		else if((h1<=plc_16BitBuf[temp])&&(min1<=plc_16BitBuf[temp+1])&&(s1<plc_16BitBuf[temp+2]))
		{PLC_BIT_ON(temp1+2);}
	}
}

static void EZCP(void)	          //16位比较传送指令
{ 
	float temp1,temp2,temp3;u32 temp4;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=float_value();
		temp2=float_value();
		temp3=float_value();
		temp4=addr_value();Flag_bit=0XFF;
		PLC_BIT_OFF(temp4);PLC_BIT_OFF(temp4+1);PLC_BIT_OFF(temp4+2);
		if(temp1>temp3)       PLC_BIT_ON(temp4+0); 
		else if((temp1<=temp3)&&(temp3<=temp2)) PLC_BIT_ON(temp4+1);
		else if(temp2<temp3)  PLC_BIT_ON(temp4+2); 
	}
	else PLC_Addr+=16;              //跳过16步程序
}

static void DZCP(void)	          //16位比较传送指令
{ 
	s32 temp1,temp2,temp3,temp4;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_u32_value();
		temp2=cos_u32_value();
		temp3=cos_u32_value();
		temp4=addr_value();Flag_bit=0XFF;
		PLC_BIT_OFF(temp4);PLC_BIT_OFF(temp4+1);PLC_BIT_OFF(temp4+2);
		if(temp1>temp3)       PLC_BIT_ON(temp4); 
		else if((temp1<=temp3)&&(temp3<=temp2)) PLC_BIT_ON(temp4+1);
		else if(temp2<temp3)  PLC_BIT_ON(temp4+2); 
	}
	else PLC_Addr+=16;              //跳过16步程序
}

static void ZCP(void)	            //16位比较传送指令
{ 
	signed short int temp1,temp2,temp3,temp4;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_value();
		temp2=cos_value();
		temp3=cos_value();
		temp4=addr_value();Flag_bit=0XFF;
		PLC_BIT_OFF(temp4);PLC_BIT_OFF(temp4+1);PLC_BIT_OFF(temp4+2);
		if(temp1>temp3)       PLC_BIT_ON(temp4); 
		else if((temp1<=temp3)&&(temp3<=temp2)) PLC_BIT_ON(temp4+1);
		else if(temp2<temp3)  PLC_BIT_ON(temp4+2); 
	}
	else 
	{
		PLC_Addr+=8;              //跳过8步程序
	}
}
//======================================================================================================
// 函数名称: static void TCMP(void)
// 功能描述：TCMP指令函数
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void TCMP(void)
{   
	u16 h,min,s,temp,temp1;
	if((PLC_ACC_BIT&0X01)==0X01)
	{	
		h=cos_value();min=cos_value();s=cos_value();
		temp=addr_value();
		temp1=addr_value();Flag_bit=0XFF;
		PLC_BIT_OFF(temp1);PLC_BIT_OFF(temp1+1);PLC_BIT_OFF(temp1+2);  
		if((h>=plc_16BitBuf[temp])&&(min>=plc_16BitBuf[temp+1])&&(s>plc_16BitBuf[temp+2]))
		{PLC_BIT_ON(temp1);}
		else if((h==plc_16BitBuf[temp])&&(min==plc_16BitBuf[temp+1])&&(s==plc_16BitBuf[temp+2]))
		{PLC_BIT_ON(temp1+1);}
		else if((h<=plc_16BitBuf[temp])&&(min<=plc_16BitBuf[temp+1])&&(s<plc_16BitBuf[temp+2]))
		{PLC_BIT_ON(temp1+2);}
	}
	else PLC_Addr+=10;
}

//======================================================================================================
// 函数名称: static void ECMP(void)
// 功能描述：ECMP指令函数
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void ECMP(void)	          //浮点比较传送指令
{ 
	signed short int temp3;
	static float temp1,temp2;
	if((PLC_ACC_BIT&0X01)==0X01)
	{	
		temp1=float_value();
		temp2=float_value();
		temp3=addr_value();Flag_bit=0XFF;
		PLC_BIT_OFF(temp3);PLC_BIT_OFF(temp3+1);PLC_BIT_OFF(temp3+2);
		if(temp1>temp2)       PLC_BIT_ON(temp3); 
		else if(temp1==temp2) PLC_BIT_ON(temp3+1);
		else if(temp1<temp2)  PLC_BIT_ON(temp3+2); 
	}
	else PLC_Addr+=12;              //跳过12步程序
}

//======================================================================================================
// 函数名称: static void DCMP(void)
// 功能描述：DCMP指令函数
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void DCMP(void)	          //32位比较传送指令
{ 
	signed short int temp3;
	static int temp1,temp2;
	if(PLC_ACC_BIT&0X01)
	{	
		temp1=cos_u32_value();
		temp2=cos_u32_value();
		temp3=addr_value();Flag_bit=0XFF;
		PLC_BIT_OFF(temp3);PLC_BIT_OFF(temp3+1);PLC_BIT_OFF(temp3+2);
		if(temp1>temp2)       PLC_BIT_ON(temp3+0); 
		else if(temp1==temp2) PLC_BIT_ON(temp3+1);
		else if(temp1<temp2)  PLC_BIT_ON(temp3+2); 
	}
	else PLC_Addr+=12;              //跳过12步程序
}

static void DCMPP(void)	  
{ 
	signed short int temp3;
	static int temp1,temp2;
	if(PLC_LDP_TEST())        //上升沿判断
	{ 
		temp1=cos_u32_value();
		temp2=cos_u32_value();
		temp3=addr_value();Flag_bit=0XFF;
		PLC_BIT_OFF(temp3);PLC_BIT_OFF(temp3+1);PLC_BIT_OFF(temp3+2);
		if(temp1>temp2)       PLC_BIT_ON(temp3+0); 
		else if(temp1==temp2) PLC_BIT_ON(temp3+1);
		else if(temp1<temp2)  PLC_BIT_ON(temp3+2);  
	}
	else
	PLC_Addr+=12;		         //条件不满足执行跳过程序，减小CPU开销
}
//======================================================================================================
// 函数名称: static void CMP(void)
// 功能描述：CMP指令函数
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void CMP(void)	          //16位比较传送指令
{ 
	signed short int temp1,temp2,temp3;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_value();
		temp2=cos_value();
		temp3=addr_value();Flag_bit=0XFF;
		PLC_BIT_OFF(temp3);PLC_BIT_OFF(temp3+1);PLC_BIT_OFF(temp3+2);
		if(temp1>temp2)       PLC_BIT_ON(temp3); 
		else if(temp1==temp2) PLC_BIT_ON(temp3+1);
		else if(temp1<temp2)  PLC_BIT_ON(temp3+2); 	 
	}
	else PLC_Addr+=6;              //跳过6步程序
}
//======================================================================================================
// 函数名称: static void CMP_P(void)
// 功能描述：CMPP指令函数
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void CMPP(void)	  
{ 
	signed short int temp1,temp2,temp3;
	if(PLC_LDP_TEST())                       //上升沿判断
	{ 
		temp1=cos_value();
		temp2=cos_value();
		temp3=addr_value();Flag_bit=0XFF;
		PLC_BIT_OFF(temp3);PLC_BIT_OFF(temp3+1);PLC_BIT_OFF(temp3+2);
		if(temp1>temp2)       PLC_BIT_ON(temp3); 
		else if(temp1==temp2) PLC_BIT_ON(temp3+1);
		else if(temp1<temp2)  PLC_BIT_ON(temp3+2); 	 
	}
	else
	PLC_Addr+=6;		                          //条件不满足执行跳过程序，减小CPU开销
}


static void DINC(void)	         //32位逻辑运算 加1指令
{ 
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		trade=(u32)cos_u32_value()+1;
		PLC_Addr-=4;
		D_target();
	}
	else PLC_Addr+=4;              //跳过4步程序
}

static void DINC_P(void)	  //CALLP
{ 
	if(PLC_PL_BIT_TEST(PLC_Addr-PLC_LAD_START_ADDR)==off)//上升沿判断
	{ 
		if(PLC_ACC_BIT&0X01)			                      //当前值判断
		{
			PLC_PL_BIT_ON(PLC_Addr-PLC_LAD_START_ADDR);
			trade=(u32)cos_u32_value()+1;
			PLC_Addr-=4;
			trade++;
			D_target();
		}
		else PLC_Addr+=4;
	}
	else
	{
		if(!((PLC_ACC_BIT&0x01)==0x01))						 //当前值判断
		PLC_PL_BIT_OFF(PLC_Addr-PLC_LAD_START_ADDR);//
		PLC_Addr+=4;		                           //条件不满足执行跳过程序，减小CPU开销
	} 
}

static void INC(void)	            //逻辑运算 加1指令
{ 
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		trade=cos_value();
		PLC_Addr-=2;
		trade++;
		target();
	}
	else PLC_Addr+=2;              //跳过2步程序
}

static void INCP(void)	  //INCP
{ 
	if(PLC_LDP_TEST())//上升沿判断
	{ 
		trade=cos_value();
		PLC_Addr-=2;
		trade++;
		target();
	}
	else
	{
		PLC_Addr+=2;		                            //条件不满足执行跳过程序，减小CPU开销
	} 
}

static void DDEC(void)                             //32位逻辑运算 减1指令
{
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		trade=cos_u32_value()-1;
		PLC_Addr-=4;
		D_target();
	}
	else PLC_Addr+=4;              //跳过4步程序
}

static void DEC(void)             //逻辑运算 减1指令
{
	if(PLC_ACC_BIT&0X01)
	{
		trade=cos_value()-1;
		PLC_Addr-=2;
		target();
	}
	else PLC_Addr+=2;              //跳过2步程序
}

static void DECP(void)	          //INCP
{ 
	if(PLC_LDP_TEST())              //上升沿判断
	{ 
		trade=cos_value()-1;
		PLC_Addr-=2;
		target();
	}
	else
	PLC_Addr+=2;		                           //条件不满足执行跳过程序，减小CPU开销
}

static void DNEG(void)                             //32位逻辑运算
{
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		trade=0-cos_u32_value();
		PLC_Addr-=4;
		D_target();		 
	}
	else PLC_Addr+=4;              //跳过4步程序
}

static void NEG(void)                             //逻辑运算
{
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		trade=0-cos_value();
		PLC_Addr-=2;
		target();
	}
	else PLC_Addr+=2;              //跳过2步程序
}

static void DWAND(void)	                          //逻辑运算“与”
{ 
	u32 temp1,temp2;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_u32_value();
		temp2=cos_u32_value();;
		trade=temp1&temp2;
		D_target();
	}
	else PLC_Addr+=12;              //跳过12步程序
}

static void WAND(void)	                          //逻辑运算“与”
{ 
	signed short int temp1,temp2;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_value();
		temp2=cos_value();
		trade=temp1&temp2;
		target();
	}
	else PLC_Addr+=6;              //跳过6步程序
}

static void DWOR(void)	                           //逻辑运算“或”
{  
	u32 temp1,temp2;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_u32_value();
		temp2=cos_u32_value();;
		trade=temp1|temp2;
		D_target();
	}
	else PLC_Addr+=12;              //跳过12步程序
}

static void WOR(void)	                           //逻辑运算“或”
{  
	signed short int temp1,temp2;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_value();
		temp2=cos_value();
		trade=temp1|temp2;
		target();
	}
	else PLC_Addr+=6;              //跳过6步程序
}

static void DWXOR(void)	                          //逻辑运算“异或”
{ 
	u32 temp1,temp2;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1=cos_u32_value();
		temp2=cos_u32_value();
		trade=temp1^temp2;
		D_target();
	}
	else PLC_Addr+=12;              //跳过12步程序
}

static void WXOR(void)	                          //逻辑运算“异或”
{ 
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		trade=cos_value()^cos_value();
		target();
	}
	else PLC_Addr+=6;              //跳过6步程序
}
//======================================================================================================
// 函数名称: static void TADD(void)
// 功能描述：TADD指令函数
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void TADD(void)
{  
	u16 temp,temp1,temp2;
	if(PLC_ACC_BIT&0X01)
	{ 
		temp1=addr_value();              
		temp2=addr_value();  
		temp=addr_value();
		plc_16BitBuf[temp]=plc_16BitBuf[temp1]+plc_16BitBuf[temp2];
		plc_16BitBuf[temp+1]=plc_16BitBuf[temp1+1]+plc_16BitBuf[temp2+1];
		plc_16BitBuf[temp+2]=plc_16BitBuf[temp1+2]+plc_16BitBuf[temp2+2];
	}
}
void MEAN(void)
{
	u16 temp,temp2;uint64_t data;u32 temp1;
	if(PLC_ACC_BIT&0X01)
	{ 
		temp1=addr_value_prog();             
		PLC_Addr+=2;        //些跳过输出  
		temp2=cos_value();
		if(Flag_bit==0xff)  //是不是K4M0之类的寄存器
		{
			for(temp=0;temp<temp2;temp++)
			{data+=PLC_RAM16(temp1+temp*2);}
			PLC_Addr-=4;        //跳回输出
		}
		else
		{  
			data=(u16)temp1;
			PLC_Addr-=4;   //回跳
			for(temp=1;temp<temp2;temp++)
			{ 
				PLC_Addr-=2;
				Transfer=temp*16;
				data+=addr_value_prog();
			}
			Flag_bit=0xff;
		}
		trade=data/temp2;
		target();
		PLC_Addr+=2;        
	}
	else PLC_Addr+=6;
}
//=======================================================================================================
// 函数名称:  static void ADD(void)	 
// 功能描述： 16位交换传送 ADD指令  加法
// 输　入:  void      
// 输　出:  void     
// 全局变量:  
 
//=======================================================================================================
static void ADD(void)	   
{ 
	if(PLC_ACC_BIT&0X01)
	{
		trade=cos_value()+cos_value();
		target();
	}
	else PLC_Addr+=6;              //跳过6步程序
}
//=======================================================================================================
// 函数名称:  static void ALT(void)	 
// 功能描述： 16位交换传送 ALT指令
// 输　入:  void      
// 输　出:  void     
// 全局变量:  
 
//=======================================================================================================
static void ALT(void)	                 
{  	
	signed int temp;
	if(PLC_ACC_BIT&0X01)
	{
		temp=addr_value(); 
		if(PLC_BIT_TEST(temp))
		PLC_BIT_OFF(temp);
		else
		PLC_BIT_ON(temp);
	}
	else PLC_Addr+=2;                      //没有动作跳过2步程序
}

// 参考
//static void ZCP(void)	            //16位比较传送指令
//{ 
//	signed short int temp1,temp2,temp3,temp4;
//	if((PLC_ACC_BIT&0X01)==0X01)
//	{
//		temp1=cos_value();
//		temp2=cos_value();
//		temp3=cos_value();
//		temp4=addr_value();Flag_bit=0XFF;
//		PLC_BIT_OFF(temp4);PLC_BIT_OFF(temp4+1);PLC_BIT_OFF(temp4+2);
//		if(temp1>temp3)       PLC_BIT_ON(temp4); 
//		else if((temp1<=temp3)&&(temp3<=temp2)) PLC_BIT_ON(temp4+1);
//		else if(temp2<temp3)  PLC_BIT_ON(temp4+2); 
//	}
//	else 
//	{
//		PLC_Addr+=8;              //跳过8步程序
//	}
//}
static void FROM(void)	                 
{  
		if(PLC_ACC_BIT&0X01)
	{	
			PLC_Addr+=8;                      //没有动作跳过8步程序
	}	
		else 
	{
		PLC_Addr+=8;                      //没有动作跳过8步程序
	}		
}
static void TO(void)	                 
{ 
		if(PLC_ACC_BIT&0X01)
	{	
				PLC_Addr+=8;                      //没有动作跳过8步程序
	}	
		else 
	{
		PLC_Addr+=8;                      //没有动作跳过8步程序
	}			
}
//=======================================================================================================
// 函数名称:  RS
// 功能描述： RS  S. m D. n ；S和m表示发送数据地址和点数； D和n表示接收数据地址和点数
// 输　入:  
// 输　出:    
// 全局变量:  
// 调用模块: 
// 备  注:  把RS改造成类似ADPRW指令的Modbus功能,实现Modbus主机模式

// ADPRW S. S1. S2. S3. S4./D. ； S. 子站地址，S1. 功能码，S2. S3. 发送数据地址和点数 ,S4/D 接收数据地址或者数据 

// 1、主从模式切换         D8120
// 2、子站号              D8121

// 3、指令执行结束        M8122  ->M8180

// 4、重试次数            D8126  ->D8132
// 5、发生重试            M8153  ->M8128

// 6、MODBUS通信发生出错  D8152 M8152  ->D8125	 M8125	

// 7、发生超时            M8129  ->M8129	
    			
//=======================================================================================================

static void RS(void)	                 
{  
	if(PLC_ACC_BIT&0X01)
	{
		uint8_t ch =0;
		
		if( HS_MOD1_MODE ==1) // 第1路modbus主机模式
			ch =0;			
		else if(HS_MOD2_MODE ==1) // 第2路modbus主机模式
			ch =1;
		
    // 超时处理，重试次数处理，
		if(g_tModH[ch].txStatus == 1) // 如果有发送操作，需要判断子站发送是否超时
		{
			 if( ((g_tModH[ch].fAck & 0x01) !=0x01) && ((g_tModH[ch].fAck & 0x02) !=0x02)) // 无应答并且应答无超时，则直接跳出
				 return;
			 
			 g_tModH[ch].txStatus =0; 			 
			
        if((g_tModH[ch].fAck & 0x0E)) // 应答出错（长度、CRC、超时）
			  {
					 g_tModH[ch].fAck &= ~0x0E;
					
					 g_tModH[ch].retryCnt ++; // 重试次数累加
					
				    /* 重试处理 */
					  D8132 =(D8132 > 20)? 20 : D8132; // 最大重试次数限制		
					
					  if(g_tModH[ch].retryCnt <= D8132)
					  {							  
								PLC_BIT_ON(M8128); // 标记发生重试,供梯形图上查看							
							  goto jumpLabel;							
						}
						else  // 重试次数超设定值，则放弃次操作
						{
							  g_tModH[ch].retryCnt =0; // 累计重试次数清零								
								PLC_BIT_ON(M8180);       // 标记执行结束								
								PLC_BIT_ON(M8125);       // 通信出错
							  D8125 = 8125;            // 通讯出错代号							
						    PLC_BIT_ON(M8129);       // 标记发生超时									
                PLC_BIT_OFF(M8128);      // 发生重试清零												
                return;							
						}				
				}	
		}
		
		// 第一次执行或者正常执行结束
    g_tModH[ch].retryCnt =0; // 累计重试次数清零	
		PLC_BIT_OFF(M8125);      // 通讯出错清零
		D8125 =0;                // 通讯出错代码清零			
		PLC_BIT_OFF(M8129);      // 发生超时清零				
		PLC_BIT_OFF(M8128);      // 发生重试清零		
		
		/* 传人记，20170212修改 */
		if( (g_tModH[ch].fAck & 0x01) ==0x01) // 正常执行结束	
    {			
		  g_tModH[ch].fAck &=~0x01;
			PLC_BIT_ON(M8180);                  // 标记执行结束
			return;
    }	
	  PLC_BIT_OFF(M8180);                   // 执行结束清零
		
		g_tModH[ch].slaveAddr = HS_MOD_SAVLE_ADDR;  // 子站号		
		g_tModH[ch].funCode=cos_value();            // 功能码  
		g_tModH[ch].addr=cos_value();               // 对象的地址
		g_tModH[ch].num=cos_value();                // 操作的数量
		//place=addr_value(); 
		g_tModH[ch].place=cos_value();              // 存放数据位置或者发送数据位置 
		
jumpLabel:	// 重试	
	  ModHostMode_SendDeal(ch,g_tModH[ch].slaveAddr,g_tModH[ch].funCode, 
													g_tModH[ch].addr, g_tModH[ch].num,g_tModH[ch].place);		
	
	}
	else 
	{
		PLC_Addr+=8;                      //没有动作跳过8步程序
	}
}


//======================================================================================================
// 函数名称: static void TRD(void)
// 功能描述：TRD指令函数
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void TRD(void)
{
	u16 temp;
	if(PLC_ACC_BIT&0X01)
	{
		temp=addr_value();
		// 传人记，20180524修改
		plc_16BitBuf[temp] =D8018; // 年
		plc_16BitBuf[temp+1] =D8017; // 月
		plc_16BitBuf[temp+2] =D8016; // 日
		plc_16BitBuf[temp+3] =D8015; // 时
		plc_16BitBuf[temp+4] =D8014; // 分
		plc_16BitBuf[temp+5] =D8013; // 秒
		plc_16BitBuf[temp+6] =D8019; // 星期
	}
	else PLC_Addr+=2;              //跳过2步程序
}

//======================================================================================================
// 函数名称: static void TWR(void)
// 功能描述：TWR指令函数
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void TWR(void)
{
	u16 temp;
	if(PLC_ACC_BIT&0X01)
	{
		temp=addr_value();
		
#if RTC_FUNC
		RTC_Set(plc_16BitBuf[temp],plc_16BitBuf[temp+1],plc_16BitBuf[temp+2],plc_16BitBuf[temp+3],plc_16BitBuf[temp+4],plc_16BitBuf[temp+5]);
#endif
		
	}
	else PLC_Addr+=2;              //跳过2步程序
}

//======================================================================================================
// 函数名称: static void TSUB(void)
// 功能描述：TSUB指令函数
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void TSUB(void)
{
	u16 temp,temp1,temp2;
	if(PLC_ACC_BIT&0X01)
	{
		temp1=addr_value();  
		temp2=addr_value();  
		temp=addr_value();
		plc_16BitBuf[temp]=plc_16BitBuf[temp1]-plc_16BitBuf[temp2];
		plc_16BitBuf[temp+1]=plc_16BitBuf[temp1+1]-plc_16BitBuf[temp2+1];
		plc_16BitBuf[temp+2]=plc_16BitBuf[temp1+2]-plc_16BitBuf[temp2+2];
	}
}

static void SUB(void)	   //减法
{ 
	if(PLC_ACC_BIT&0X01)
	{
		PLC_Err=PLC_Addr;
		trade=cos_value()-cos_value();
		target();
	} 
	else PLC_Addr+=6;              //跳过6步程序
}

static void DBCD(void)	            //二进制转换DBCD
{ 
	signed int can1,add1,add2,add3,add4,buffer1,buffer2,buffer3,buffer4;
	if(PLC_ACC_BIT&0X01)
	{
		PLC_Err=PLC_Addr;
		can1=cos_u32_value();
		add1=can1%10;
		add2=can1/10;
		add2=add2%10;
		add3=can1/100;
		add3=add3%10;
		add4=can1/1000;
		add4=add4%10;  
		
		buffer1=can1/10000;
		buffer1=buffer1%10;
		
		buffer2=can1/100000;
		buffer2=buffer2%10;
		
		buffer3=can1/1000000;
		buffer3=buffer3%10;
		
		buffer4=can1/10000000;
		buffer4=buffer4%10;
		
		trade=buffer4*16*256*65536+buffer3*256*65536+buffer2*16*65536+buffer1*65536+add4*16*256+add3*256+add2*16+add1;
		D_target();
	}
	else PLC_Addr+=8;              //跳过4步程序
}

static void BCD(void)	            //二进制转换BCD
{ 
	signed short Ia, Ic;
	if((PLC_ACC_BIT&0X01)==0X01)
	{ 
		PLC_Err=PLC_Addr;
		Ic = cos_value();
		Ia   = (Ic / 1000) << 12;
		Ic  %= 1000;
		Ia  |= (Ic / 100 ) << 8;
		Ic  %= 100;
		Ia  |= (Ic / 10 ) << 4;
		Ic  %= 10;
		Ia  |=  Ic;
		trade=Ia;
		target();
	}
	else PLC_Addr+=4;              //跳过4步程序
}

static void DBIN(void)	         //二进制转换DBIN
{ 
	signed int can1,add1,add2,add3,add4,buffer1,buffer2,buffer3,buffer4;
	if(PLC_ACC_BIT&0X01)
	{  
		PLC_Err=PLC_Addr;
		can1=cos_u32_value();
		add1=can1%16;
		add2=can1/16;
		add2=add2%16;
		add3=can1/256;
		add3=add3%16;
		add4=can1/(16*256);
		add4=add4%16;
		
		can1=can1/65536;
		buffer1=can1%16;
		buffer2=can1/16;
		buffer2=buffer2%16;
		buffer3=can1/256;
		buffer3=buffer3%16;
		buffer4=can1/(16*256);
		buffer4=buffer4%16;
		
		trade=buffer4*10000000+buffer3*1000000+buffer2*100000+buffer1*10000+add4*1000+add3*100+add2*10+add1;
		
		D_target();
	}
	else PLC_Addr+=8;              //跳过4步程序
}

static void BIN(void)	            //二进制转换BIN
{ 
	signed short Ia, Ic;
	if((PLC_ACC_BIT&0X01)==0X01)
	{	
		PLC_Err=PLC_Addr;
		Ic = cos_value();
		Ia   = ((Ic >> 12) & 0x0f) * 1000;
		Ia  += ((Ic >> 8 ) & 0x0f) * 100;
		Ia  += ((Ic >> 4 ) & 0x0f) * 10;
		Ia  +=   Ic        & 0x0f;
		trade=Ia;
		target();
	}
	else PLC_Addr+=4;              //跳过4步程序
}
 
static void MUL(void)	 //乘法
{ 
	signed int temp1,temp2;u32 temp3;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		PLC_Err=PLC_Addr;
		temp1=cos_value();
		temp2=cos_value();
		temp3=addr_value_prog(); 
		PLC_RAM32(temp3)=temp1*temp2;
	}
	else PLC_Addr+=6;              //跳过6步程序
}
 

static void DIV(void)	 //除法
{  
	signed short int temp1,temp2,temp3;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		PLC_Err=PLC_Addr;
		temp1=cos_value();
		temp2=cos_value();
		temp3=addr_value();
		plc_16BitBuf[temp3]=temp1/temp2;
		plc_16BitBuf[temp3+1]=temp1%temp2;
	}
	else PLC_Addr+=6;              //跳过6步程序
}

static void DADD(void)
{
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		PLC_Err=PLC_Addr;
		trade=cos_u32_value()+cos_u32_value();
		D_target();
	}
	else PLC_Addr+=12;              //跳过12步程序
}

static void DSUB(void)
{
	if((PLC_ACC_BIT&0X01)==0X01)
	{  
		PLC_Err=PLC_Addr;
		trade=cos_u32_value()-cos_u32_value();
		D_target();
	}
	else PLC_Addr+=12;              //跳过12步程序
}

/* 传人记，20170529修复 */
static void DMUL(void)    
{	 
	if(PLC_ACC_BIT&0X01)
	{
	  uint16_t index;		
		u64data.data=(int64_t)(cos_u32_value()*cos_u32_value()); // 些存放64位共同体
		index=addr_value();  // 存放的位置
		PLC_Addr+=2;               
		plc_16BitBuf[index] = u64data.data1[0]; // 输出
		plc_16BitBuf[index + 1] = u64data.data1[1]; 
		plc_16BitBuf[index + 2] = u64data.data1[2]; 
		plc_16BitBuf[index + 3] = u64data.data1[3];	
	}
	else
	{
		PLC_Addr+=12; // 跳过12步程序
	}
}


/* 传人记，20170529修复 */
static void DDIV(void)
{
	if(PLC_ACC_BIT&0X01)
	{
		int32_t divisor,dividend;
		uint16_t index;
		
		divisor = cos_u32_value();             
		dividend = cos_u32_value();		
		
		u32data.data =divisor / dividend; // 商
		u32data1.data =divisor % dividend; // 余数
		
		index =addr_value(); // 存放的位置
		PLC_Addr+=2;                         
		plc_16BitBuf[index] = u32data.data1[0]; // 输出
		plc_16BitBuf[index + 1]=u32data.data1[1]; 
		plc_16BitBuf[index + 2]=u32data1.data1[0]; 
		plc_16BitBuf[index + 3]=u32data1.data1[1];	
	}
	else
  {		
		PLC_Addr+=12;              //跳过12步程序
	}
}

static void DFLT(void)	                 //整数转浮点
{  	
	signed int temp1;
	if(PLC_ACC_BIT&0X01)
	{
		temp1=cos_u32_value();
		trade1=(float)temp1;					
		float_target();
	}
	else PLC_Addr+=8;              //跳过8步程序
}
//======================================================================================================
// 函数名称:  static void DESQR(void)
// 功能描述： 浮点二进制换整数
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void DINT(void)
{
	if(PLC_ACC_BIT&0X01)
	{
		trade=(u32)float_value();
		D_target();
	} 
	else PLC_Addr+=8;              //跳过8步程序
}
//======================================================================================================
// 函数名称:  static void INT(void)
// 功能描述： 浮点二进制换整数
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void INT(void)
{
	if(PLC_ACC_BIT&0X01)
	{
		trade=(u16)float_value();
		PLC_Addr-=2; 
		target();
	} 
	else PLC_Addr+=4;              //跳过4步程序
}
//======================================================================================================
// 函数名称:  static void FLT(void)
// 功能描述： 整数转浮点 FLT
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void FLT(void)	                
{  	
	if(PLC_ACC_BIT&0X01)
	{
		trade1=(float)cos_value();			
		float_target();PLC_Addr-=2; 
	}
	else PLC_Addr+=4;              //跳过4步程序
}

//======================================================================================================
// 函数名称:  static void DTAN(void)
// 功能描述： 浮点正切 DTAN
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void DTAN(void)
{
	if(PLC_ACC_BIT&0X01)
	{
		trade1=(float)tan((double)float_value());
		float_target();
	} 
	else PLC_Addr+=8;              //跳过8步程序
}
//======================================================================================================
// 函数名称:  static void DCOS(void)
// 功能描述： 浮点正余 DCOS
// 输　入:  void      
// 输　出:  void     
// 全局变量:  
 
//=======================================================================================================
static void DCOS(void)
{
	if(PLC_ACC_BIT&0X01)
	{
		trade1=(float)cos((double)float_value());
		float_target();
	} 
	else PLC_Addr+=8;              //跳过8步程序
}

//======================================================================================================
// 函数名称:  static void DSIN(void)
// 功能描述： 浮点正弦  DSIN
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void DSIN(void)
{
	if(PLC_ACC_BIT&0X01)
	{
		trade1=(float)sin((double)float_value());
		float_target();
	} 
	else PLC_Addr+=8;              //跳过8步程序
}
//======================================================================================================
// 函数名称:  static void DESQR(void)
// 功能描述： 浮点开方计算 DESQR
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void DESQR(void)
{
	if(PLC_ACC_BIT&0X01)
	{
		trade1=(float)sqrt((double)float_value());
		float_target();
	} 
	else PLC_Addr+=8;              //跳过8步程序
}

//======================================================================================================
// 函数名称:  static void DEADD(void)	
// 功能描述： 浮点加法运算 指令DEADD
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void DEADD(void)	 
{  
	if(PLC_ACC_BIT&0X01)
	{
		trade1=float_value()+ float_value(); 						
		float_target();
	}
	else PLC_Addr+=12;              //跳过12步程序
}


static void DESUB(void)	           //浮点减法运算 
{  
	float temp1,temp2;
	if(PLC_ACC_BIT&0X01)
	{
		temp1 = float_value();              
		temp2 = float_value(); 
		
		trade1=temp1-temp2;					
		float_target();
	}
	else PLC_Addr+=12;              //跳过12步程序
}



static void DEDIV(void)	           //浮点除法运算 
{  
	float temp1,temp2;
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		temp1 = float_value();              
		temp2 = float_value(); 
		
		trade1=temp1/temp2;					
		float_target();
	}
	else PLC_Addr+=12;              //跳过12步程序
}

static void DEMUL(void)	                //浮点乘法运算
{  
	if((PLC_ACC_BIT&0X01)==0X01)
	{
		trade1=float_value()*float_value();					
		float_target();
	}
	else PLC_Addr+=12;              //跳过12步程序
}

// 传人记，2018年07月09日新增
static void SPD(void)	              
{  
	u16 parm1 = ((*(PLC_Addr + 1)) <<8) | (u8)(*PLC_Addr) ; // 参数1(S1.):X1或者X4	 
	if((PLC_ACC_BIT&0X01)==0X01)
	{		
		  spd_t * pSpd =NULL;//全局处理结构体		
			if(parm1 == 0x401){// X1
					pSpd=&S_spd[0];
			}
			else if(parm1 == 0x404){//X4
					pSpd=&S_spd[1];			 
			}
			else{
					return;
			}
		
			if(pSpd->used == 0)
			{
				  PLC_Addr +=2;		
					pSpd->s2 =cos_value(); // 参数2(S.)：时间
					pSpd->d =PLC_Addr; // 参数3(D.)：保存脉冲密度数据的起始字软元件的地址
			}
			
			/* 定时器初始化 */
		  if(parm1 == 0x401){// X1
					if(S_spd[0].used==0)
					{
						Encoder_A0_Configuration();
						S_spd[0].used =1;
					}
			}
			else if(parm1 == 0x404){//X4
					if(S_spd[1].used==0)
					{
						Encoder_A1_Configuration();
						S_spd[1].used =1;
					}
			}
				
			if (pSpd->used != 0) 
			{		
				if(pSpd->used == 1) 
				{// 脉冲当前个数，剩余测量时间		
						PLC_Addr = pSpd->d;
						if(Transfer_bit1==1)
							Transfer =1 *16;                  
						else 
							Transfer =1 *2;
						
						pSpd->cn = pSpd->totalN - pSpd->lastN; //D.+1 脉冲当前个数
					  trade = pSpd->cn;
  					target(); // 处理转换后数值		
						
					  PLC_Addr -=2;		
						if(Transfer_bit1==1)
							Transfer =2 *16;                  
						else 
							Transfer =2 *2;

						trade = pSpd->s2 - pSpd->t; //D.+2 剩余测量时间
						target(); // 处理转换后数值		
				}
				else if (pSpd->used ==2)
				{// 已到设定时间
					  pSpd->used =1;
					  Transfer =0;
						PLC_Addr = pSpd->d;						
						trade = pSpd->n; //D. 测定值：脉冲数
						target(); // 处理转换后数值		
				}
			}
	}
	else 
	{
		  if(parm1 == 0x401){// X1
					if(S_spd[0].used != 0) 
					{
							Encoder_A0_Restore();
						  memset((u8*)&S_spd[0],0,sizeof(spd_t));
					}
			}
			else if(parm1 == 0x404){//X4
					if(S_spd[1].used !=0) 
					{
							Encoder_A1_Restore();
						  memset((u8*)&S_spd[1],0,sizeof(spd_t));
					}	
			}			
  		PLC_Addr+=6;
	}        
}
// 传人记，20170512新增，待调试
// 范围检查没做 
//void FNC_DRVI_Func(void) 
//{

//	u16 Temp1,Temp2;
//	s32 Temp3;
//	pulse_Pro *pPulse_Pro=NULL;//全局处理结构体
//	Temp1= PLC_ProgAddReturn(PLC_PC+8);
//  Temp2= PLC_ProgAddReturn(PLC_PC+12);
//  PLC_BIT_OFF(M8029);
//	if(Temp1==0x500)
//	 {
//			pPulse_Pro=&Gpulse_Pro[0];
//			pPulse_Pro->Gpio_pin=Temp1;
//		  pPulse_Pro->Gpio_pin2=Temp2;//专门为DIRV做的方向角
//	 }else if(Temp1==0x501){
//			pPulse_Pro=&Gpulse_Pro[1];
//			pPulse_Pro->Gpio_pin=Temp1;				
//      pPulse_Pro->Gpio_pin2=Temp2;		 
//	 }else{
//		 //error
//	 }
//	 //最大100k 
//  if(PLC_ACC_BIT&0x01)
//	{
//		  if(pPulse_Pro->used==0)
//			{
//				 if(PLC_FNC_P_BIT_TEST(PLC_PC-PLC_PROG_START_ADD2)==0)
//					{ 	
//					   	PLC_FNC_P_BIT_ON(PLC_PC-PLC_PROG_START_ADD2);	
//							//pPulse_Pro->mode=ePTR_MODE;
//							pPulse_Pro->Place=PLC_PC;
//							pPulse_Pro->PLSR_MaxFreq=PLC_ByteRead16(PLC_PC+4); //frequency 
//							if(pPulse_Pro->PLSR_MaxFreq>=10000)
//							{
//									M8149=1;
//							}else{
//									M8149=0;
//							}
//							M8029=0;
//							//pPulse_Pro->PWM_Width=PLC_ByteRead16(PLC_PC);	//unused  in plsy mode 
//							pPulse_Pro->CurPto=0;
//							pPulse_Pro->mode=ePTR_MODE;
//							Temp3=(s32)PLC_ByteRead16(PLC_PC);
//							if(Temp3>=0)
//							{
//                PLC_BIT_ON(ppp&0x0FFF); 
//                pPulse_Pro->TargetPto=Temp3;							
//              }else{
//                PLC_BIT_OFF(ppp&0x0FFF);
//                pPulse_Pro->TargetPto=-Temp3;							
//              }

//					//		pPulse_Pro->TargetPto=PLC_ByteRead32(PLC_PC);//pulse count 
//							pPulse_Pro->used=1;													
//						  pPulse_Pro->Ptr_Raise_time=D8148;//max Time is  5000ms   ms 为单位
//			    	  PTO_Plc_Plan_Pro(pPulse_Pro);		
//							/*硬件相关的初始化*/
//							//PLSY_Plc_Pro(pPulse_Pro,ENABLE);
//					}				
//			}else{	
//        /*plsy is Running Just Go! Do nothing！*/
//      }
//		
//	}
// else 
//	{
//		PLC_FNC_P_BIT_OFF(PLC_PC-PLC_PROG_START_ADD2);	
//		 if(pPulse_Pro->used==1)
//		 {
//       if(pPulse_Pro->Place==PLC_PC) //哪个开启 哪个关闭
//			 {
//           //正真的关闭才行
//			    	 pPulse_Pro->used=0;
//				   //Close plsy Output  Hard releated  
//				     PLSY_Plc_Pro(pPulse_Pro,DISABLE);
//				     memset((u8*)pPulse_Pro,0,sizeof(pulse_Pro));				 
//       }
//     }
//	//	PLC_PWM_STOP(PLC_ProgAddReturn(PLC_PC+8));
//	}
// PLC_PC+=16;
//}

//void FNC_D_DRVI_Func(void)
//{
//	u16 Temp1,Temp2;
//	s32 Temp3;
//	pulse_Pro *pPulse_Pro=NULL;//全局处理结构体
//	Temp1= PLC_ProgAddReturn(PLC_PC+16);
//  Temp2= PLC_ProgAddReturn(PLC_PC+20);
//  PLC_BIT_OFF(M8029);
//	if(Temp1==0x500)
//	 {
//			pPulse_Pro=&Gpulse_Pro[0];
//			pPulse_Pro->Gpio_pin=Temp1;
//		  pPulse_Pro->Gpio_pin2=Temp2;//专门为DIRV做的方向角
//	 }else if(Temp1==0x501){
//			pPulse_Pro=&Gpulse_Pro[1];
//			pPulse_Pro->Gpio_pin=Temp1;				
//      pPulse_Pro->Gpio_pin2=Temp2;		 
//	 }else{
//		 //error
//	 }
//	 //最大100k 
//  if(PLC_ACC_BIT&0x01)
//	{
//		  if(pPulse_Pro->used==0)
//			{
//				 if(PLC_FNC_P_BIT_TEST(PLC_PC-PLC_PROG_START_ADD2)==0)
//					{ 	
//					   	PLC_FNC_P_BIT_ON(PLC_PC-PLC_PROG_START_ADD2);	
//							//pPulse_Pro->mode=ePTR_MODE;
//							pPulse_Pro->Place=PLC_PC;
//							pPulse_Pro->PLSR_MaxFreq=PLC_ByteRead32(PLC_PC+8); //frequency 
//							if(pPulse_Pro->PLSR_MaxFreq>=10000)
//							{
//									M8149=1;
//							}else{
//									M8149=0;
//							}
//							M8029=0;
//							//pPulse_Pro->PWM_Width=PLC_ByteRead16(PLC_PC);	//unused  in plsy mode 
//							pPulse_Pro->CurPto=0;
//							pPulse_Pro->mode=ePTR_MODE;
//							Temp3=(s32)PLC_ByteRead32(PLC_PC);
//							if(Temp3>=0)
//							{
//                PLC_BIT_ON(ppp&0x0FFF); 
//                pPulse_Pro->TargetPto=Temp3;							
//              }else{
//                PLC_BIT_OFF(ppp&0x0FFF);
//                pPulse_Pro->TargetPto=-Temp3;							
//              }

//							
//							
//					//		pPulse_Pro->TargetPto=PLC_ByteRead32(PLC_PC);//pulse count 
//							pPulse_Pro->used=1;													
//						  pPulse_Pro->Ptr_Raise_time=D8148;//max Time is  5000ms   ms 为单位
//			    	  PTO_Plc_Plan_Pro(pPulse_Pro);		
//							/*硬件相关的初始化*/
//							//PLSY_Plc_Pro(pPulse_Pro,ENABLE);
//					}				
//			}else{	
//        /*plsy is Running Just Go! Do nothing！*/
//      }
//		
//	}
// else 
//	{
//		PLC_FNC_P_BIT_OFF(PLC_PC-PLC_PROG_START_ADD2);	
//		 if(pPulse_Pro->used==1)
//		 {
//       if(pPulse_Pro->Place==PLC_PC) //哪个开启 哪个关闭
//			 {
//           //正真的关闭才行
//			    	 pPulse_Pro->used=0;
//				   //Close plsy Output  Hard releated  
//				     PLSY_Plc_Pro(pPulse_Pro,DISABLE);
//				     memset((u8*)pPulse_Pro,0,sizeof(pulse_Pro));				 
//       }
//     }
//	//	PLC_PWM_STOP(PLC_ProgAddReturn(PLC_PC+8));
//	}
// PLC_PC+=24;
//}

//void FNC_P_DRVI_Func(void)
//{
////NULL  
//}

//void FNC_ZRN_Func(void)
//{
//	//注册一个结构体进行即可
//	//这个应该没什么难度 
//	//编程注意分离思想 这样问题好解决
//}

//void FNC_D_ZRN_Func(void)
//{
////NULL  
//}


/*PLSY 频率（参数1）  脉冲数量（参数2）  脉冲输出（参数3：Y0/Y1） */
void PLSY(void)
{  
#if PWM_OUTPUT_FUNC
	
		pulse_Pro *pPulse_Pro=NULL;//全局处理结构体		
	  u16 parm3 = ((*(PLC_Addr + 5)) <<8) | (u8)(*(PLC_Addr + 4)) ; // 参数3:Y0或者Y1	 
	
//		PLC_BIT_OFF(M8029);	  
		if(parm3 == 0x500){// Y0
				pPulse_Pro=&gPulse_Pro[0];
				pPulse_Pro->Gpio_pin= parm3;
		}
		else if(parm3 == 0x501){//Y1
				pPulse_Pro=&gPulse_Pro[1];
				pPulse_Pro->Gpio_pin= parm3;				 
		}
		else{
        return;
		}
		//最大100k 
		if(PLC_ACC_BIT&0x01)
		{
			  if(pPulse_Pro->used != 3) // 为3时计数完成
			  {					
					pPulse_Pro->PWM_Cycle = cos_value(); // 脉冲频率
					pPulse_Pro->TargetPto = cos_value(); // 指定发出脉冲数
					pPulse_Pro->Place = parm3; // 输出地址

          /* 503修复，频率发生变化立即更新 */					
					if( (pPulse_Pro->used==0) 
						  || (pPulse_Pro->Last_PWM_Cycle != pPulse_Pro->PWM_Cycle) )
					{			
              pPulse_Pro->used=1;						
						  pPulse_Pro->Last_PWM_Cycle = pPulse_Pro->PWM_Cycle; // 记录当前频率
						
//							if(PLC_BIT_TEST(pPulse_Pro->Place)==0)
							{ 	
								PLC_BIT_ON(pPulse_Pro->Place);	// Yn置1
								pPulse_Pro->mode=ePTY_MODE;

								if(pPulse_Pro->PWM_Cycle>=32767)
										PLC_BIT_ON(M8149); // 停止脉冲输出
								else
										PLC_BIT_OFF(M8149);
								PLC_BIT_OFF(M8029);

								pPulse_Pro->CurPto=0;
								pPulse_Pro->mode = ePTY_MODE;


								/*硬件相关的初始化*/
								PLSY_Plc_Pro(pPulse_Pro,ENABLE);
							}				
					}
					else
					{	
					/*plsy is Running Just Go! Do nothing！*/
					}
			  }
				else // 传人记，20171230修改
				{
				   PLC_Addr +=4; // 跳过4步程序
				}
		}
		else 
		{
			  PLC_Addr +=4; // 跳过4步程序
				if(pPulse_Pro->used)
				{
						if(pPulse_Pro->Place == parm3) //哪个开启 哪个关闭
						{
								PLC_BIT_OFF(parm3);	// 关闭Yn
								 pPulse_Pro->used=0;
								//Close plsy Output  Hard releated  
								PLSY_Plc_Pro(pPulse_Pro,DISABLE);		
								memset((u8*)pPulse_Pro,0,sizeof(pulse_Pro));		
						}
				}
		}
		PLC_Addr+=1; // +2 ???
#endif
}


// 传人记，20180503新增
void DPLSY(void)
{
#if PWM_OUTPUT_FUNC
	pulse_Pro *pPulse_Pro=NULL;//全局处理结构体
	
	// 需要修改PLC_Addr=1byte
  u16 parm3 = ((*(PLC_Addr + 9)) <<8) | (u8)(*(PLC_Addr + 8)) ; // 参数3:Y0或者Y1	 
	
	if(parm3==0x500){
			pPulse_Pro=&gPulse_Pro[0];
			pPulse_Pro->Gpio_pin=parm3;
	 }
	 else if(parm3==0x501){
			pPulse_Pro=&gPulse_Pro[1];
			pPulse_Pro->Gpio_pin=parm3;				 
	 }
	 else{
      return;
	 }
	 //最大100k 
  if(PLC_ACC_BIT&0x01)
	{
			  if(pPulse_Pro->used != 3) // 为3时计数完成
			  {					
					pPulse_Pro->PWM_Cycle = cos_u32_value(); // 脉冲频率
					pPulse_Pro->TargetPto = cos_u32_value(); // 指定发出脉冲数
					pPulse_Pro->Place = parm3; // 输出地址

          /* 503修复，频率发生变化立即更新 */					
					if( (pPulse_Pro->used==0) 
						  || (pPulse_Pro->Last_PWM_Cycle != pPulse_Pro->PWM_Cycle) )
					{			
              pPulse_Pro->used=1;						
						  pPulse_Pro->Last_PWM_Cycle = pPulse_Pro->PWM_Cycle; // 记录当前频率
						
							if(PLC_BIT_TEST(pPulse_Pro->Place)==0)
							{ 	
								PLC_BIT_ON(pPulse_Pro->Place);	// Yn置1
								pPulse_Pro->mode=ePTY_MODE;

								if(pPulse_Pro->PWM_Cycle >= 200000)
										PLC_BIT_ON(M8149); // 停止脉冲输出
								else
										PLC_BIT_OFF(M8149);
								PLC_BIT_OFF(M8029);

								pPulse_Pro->CurPto=0;
								pPulse_Pro->mode = ePTY_MODE;


								/*硬件相关的初始化*/
								PLSY_Plc_Pro(pPulse_Pro,ENABLE);
							}				
					}
					else
					{	
					/*plsy is Running Just Go! Do nothing！*/
					}
			  }
				else // 传人记，20171230修改
				{
				   PLC_Addr +=8; // 跳过4步程序
				}
	}
 else 
	{
			  PLC_Addr +=8; // 跳过4步程序
				if(pPulse_Pro->used)
				{
						if(pPulse_Pro->Place == parm3) //哪个开启 哪个关闭
						{
								PLC_BIT_OFF(parm3);	// 关闭Yn
								 pPulse_Pro->used=0;
								//Close plsy Output  Hard releated  
								PLSY_Plc_Pro(pPulse_Pro,DISABLE);		
								memset((u8*)pPulse_Pro,0,sizeof(pulse_Pro));		
						}
				}
	}							
	PLC_Addr+=2;
#endif
}

/* PWM 脉宽（参数1） 周期（参数2） 脉冲输出（参数3：Y0/Y1）*/
 static void PWM(void)
 {
#if PWM_OUTPUT_FUNC
	
		u16 PWM_Cycle =0,PWM_Width=0;	 
		pulse_Pro *pPulse_Pro=NULL;//全局处理结构体
	 
	  u16 parm3 = ((*(PLC_Addr + 5)) <<8) | (u8)(*(PLC_Addr + 4)) ; // 参数3:Y0或者Y1	 
	 
		if(parm3 == 0x500)
		{
				pPulse_Pro=&gPulse_Pro[0];
				pPulse_Pro->Gpio_pin=parm3;
		}
		else if(parm3 == 0x501)
	  {
				pPulse_Pro=&gPulse_Pro[1];
				pPulse_Pro->Gpio_pin=parm3;				 
		}
		else
	  {
        return;
		}
		//最大100k 
		if(PLC_ACC_BIT&0x01)
		{ 
				PWM_Width = cos_value(); // 指定脉宽(0~32767ms)
		    PWM_Cycle = cos_value(); // 周期(1~32767ms)
			
			  // 参数判断
			  if((32767 < PWM_Width) || (32767 < PWM_Cycle)||(PWM_Cycle < PWM_Width)) // 参数设置有问题
					return;
			
			 	pPulse_Pro->Place = parm3; // 输出地址 Y0或者Y1
			
				if(pPulse_Pro->used==0)
				{
						pPulse_Pro->mode = ePWM_MODE;					
					
						pPulse_Pro->PWM_Width = PWM_Width; // 脉宽
						pPulse_Pro->PWM_Cycle = PWM_Cycle; // 周期

						pPulse_Pro->used=1;
						/*硬件相关的初始化*/
						PWM_Plc_Pro(pPulse_Pro,ENABLE);				
				}
				else
			  {	
					  /* 运行过程中改变周期和脉宽*/
						if((PWM_Cycle!=pPulse_Pro->PWM_Cycle)||(PWM_Width!=pPulse_Pro->PWM_Width))
						{
							 pPulse_Pro->PWM_Cycle=PWM_Cycle;
							 pPulse_Pro->PWM_Width=PWM_Width;
							 PWM_Plc_Pro(pPulse_Pro,ENABLE);	
						}
						/*Pwm is Running Just Go! Do nothing！*/
				}
		}
		else 
		{
			  PLC_Addr +=4; // 跳过4步程序
				if(pPulse_Pro->used)
				{
						if(pPulse_Pro->Place == parm3) //哪个开启 哪个关闭
						{
								pPulse_Pro->used=0;
								//Close Pwm Output  Hard releated  
								PWM_Plc_Pro(pPulse_Pro,DISABLE);	
								memset((u8*)pPulse_Pro,0,sizeof(pulse_Pro));						 
						}
				}
		}
		PLC_Addr+=1;	 
#endif
 }

 

static void Damount(void)	 //32位等于比较指令
{  
	s32 temp1,temp2;
	temp1=cos_u32_value();
	temp2=cos_u32_value();
	
	if((PLC_STL_Status ==1)||(MC.MC_Flg ==1)) 
	{
			if(PLC_STL_Status == 1)  //为STL状态区
			{ 
				PLC_ACC_BIT<<=1;		
				if((temp1==temp2)&&(PLC_BIT_TEST(PLC_STL_Addr)))      
				   PLC_ACC_BIT|=1;
			}
			
#if DEBUG
#warning "月20日新增，MC和MCR指令";
#endif			
		  if (MC.MC_Flg == 1)
		  {
				if((temp1==temp2)&&(MC.PLC_MC_BIT & 0x01))					
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x01);
				else				 
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x00);
		  }	
  }
	else
	{ 
		PLC_ACC_BIT<<=1;
		if(temp1==temp2)     //当前值判断
		PLC_ACC_BIT|=1;
	}
}

static void amount(void)	 //16位等于比较指令
{ 
	signed short int temp1,temp2;
	temp1=cos_value();
	temp2=cos_value();
  if((PLC_STL_Status ==1)||(MC.MC_Flg ==1)) 
	{
			if(PLC_STL_Status == 1) // 为STL状态区
			{  
				PLC_ACC_BIT<<=1;		
				if((temp1==temp2)&&(PLC_BIT_TEST(PLC_STL_Addr)))     
						PLC_ACC_BIT|=1;				
			}
#if DEBUG
#warning "月20日新增，MC和MCR指令";
#endif
		  if (MC.MC_Flg == 1)
		  {
				if((temp1==temp2)&&(MC.PLC_MC_BIT & 0x01))
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x01);
				else				 
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x00);
		  }			
  }
	else
	{ 
		PLC_ACC_BIT<<=1;
		if(temp1==temp2) // 当前值判断
			PLC_ACC_BIT|=1;
	}
}

static void amount_OR()
{ 
	signed short int temp1,temp2;
	temp1=cos_value();
	temp2=cos_value();
	if((temp1==temp2)||(PLC_ACC_BIT&0X01))  //当前值判断
			PLC_ACC_BIT|=0X01;
	else
			PLC_ACC_BIT&=0xFE; 
}

static void Damount_OR()
{ 
	s32 temp1,temp2;
	temp1=cos_u32_value();
	temp2=cos_u32_value();
	if((temp1==temp2)||(PLC_ACC_BIT&0X01))  //当前值判断
	PLC_ACC_BIT|=0X01;
	else
	PLC_ACC_BIT&=0xFE; 
}

static void Damount_and(void)	 //32位AND等于比较
{ 
	s32 temp1,temp2;
	temp1=cos_u32_value();
	temp2=cos_u32_value();
	if((temp1==temp2)&&((PLC_ACC_BIT&0X01)==0X01)) 
	PLC_ACC_BIT|=1;
	else
	PLC_ACC_BIT&=~1;
}

static void amount_and(void)	 //16位AND等于比较
{ 
	signed short int temp1,temp2;
	temp1=cos_value();
	temp2=cos_value();
	if((temp1==temp2)&&((PLC_ACC_BIT&0X01)==0X01)) 
	PLC_ACC_BIT|=1;
	else
	PLC_ACC_BIT&=~1;
}

static void Dbig(void)		    //32位大于比较指令
{ 
	s32 temp1,temp2;
	temp1=cos_u32_value();
	temp2=cos_u32_value();
	
	if((PLC_STL_Status == 1)||(MC.MC_Flg ==1))
	{
			if(PLC_STL_Status == 1)                       //为STL状态区
			{  
				PLC_ACC_BIT<<=1;		
				if((temp1>temp2)&&(PLC_BIT_TEST(PLC_STL_Addr)))    
				PLC_ACC_BIT|=1;
			}
#if DEBUG
#warning "年03月20日新增，MC和MCR指令";
#endif
			if(MC.MC_Flg ==1)
			{			
				 if((temp1>temp2)&&(MC.PLC_MC_BIT & 0x01))      
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x01);
				 else				 
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x00);
			}			
			
  }
	else
	{ 
		PLC_ACC_BIT<<=1;
		if(temp1>temp2) 						               //当前值判断
		PLC_ACC_BIT|=1;
	}
}

static void big(void)	// 16位大于比较指令
{ 
	signed short int temp1,temp2;
	temp1=cos_value();
	temp2=cos_value();
	
	if((PLC_STL_Status == 1)||(MC.MC_Flg ==1))
	{
			if(PLC_STL_Status == 1) // 为STL状态区
			{  
				PLC_ACC_BIT<<=1;		
				if((temp1>temp2)&&(PLC_BIT_TEST(PLC_STL_Addr)))      
				PLC_ACC_BIT|=1;
			}
#if DEBUG
#warning "年03月20日新增，MC和MCR指令";
#endif			
			if(MC.MC_Flg ==1)
			{			
				 if((temp1>temp2)&&(MC.PLC_MC_BIT & 0x01))      
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x01);
				 else				 
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x00);
			}
  }
	else
	{ 
		PLC_ACC_BIT<<=1;
		if(temp1>temp2) //当前值判断	billnie 20180615
				PLC_ACC_BIT|=1;
	}
}

static void big_OR()
{ 
	signed short int temp1,temp2;
	temp1=cos_value();
	temp2=cos_value();
	if((temp1>temp2)||(PLC_ACC_BIT&0X01))    
	PLC_ACC_BIT|=0X01;
	else
	PLC_ACC_BIT&=0xFE; 
}

static void Dbig_OR()
{ 
	s32 temp1,temp2;
	temp1=cos_u32_value();
	temp2=cos_u32_value();
	if((temp1>=temp2)||(PLC_ACC_BIT&0X01))    
	PLC_ACC_BIT|=0X01;
	else
	PLC_ACC_BIT&=0xFE; 
}

static void Dbig_and(void)		//32位AND大于比较指令
{ 
	s32 temp1,temp2;
	temp1=cos_u32_value();
	temp2=cos_u32_value();
	if((temp1>temp2)&&((PLC_ACC_BIT&0X01)==0X01)) 
	PLC_ACC_BIT|=1;
	else
	PLC_ACC_BIT&=~1;
}

static void big_and(void)		//16位AND大于比较指令
{ 
	signed short int temp1,temp2;
	temp1=cos_value();
	temp2=cos_value();
	if((temp1>temp2)&&((PLC_ACC_BIT&0X01)==0X01)) 
	PLC_ACC_BIT|=1;
	else
	PLC_ACC_BIT&=~1;
}

static void Dless(void)	     //32位小于比较指令
{ 
	s32 temp1,temp2;
	temp1=cos_u32_value();
	temp2=cos_u32_value();
	if((PLC_STL_Status == 1) ||(MC.MC_Flg ==1))
  {
			if(PLC_STL_Status == 1) //为STL状态区
			{  
				PLC_ACC_BIT<<=1;		
				if((temp1<temp2)&&(PLC_BIT_TEST(PLC_STL_Addr)))      
						PLC_ACC_BIT|=1;
			}
#if DEBUG
#warning "年03月20日新增，MC和MCR指令";
#endif
			if(MC.MC_Flg ==1)
			{
        if((temp1<temp2)&&(MC.PLC_MC_BIT & 0x01))    					
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x01);
				 else				 
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x00);
			}	
  }	
	else
	{ 
		PLC_ACC_BIT<<=1;
		if(temp1<temp2) // 当前值判断
				PLC_ACC_BIT|=1;
	}
}

static void less(void)	     //小于比较
{ 
	signed short int temp1,temp2;
	temp1=cos_value();
	temp2=cos_value();
	if((PLC_STL_Status == 1) ||(MC.MC_Flg ==1))
  { 		
			if(PLC_STL_Status == 1) //为STL状态区
			{  
				PLC_ACC_BIT<<=1;		
				if((temp1<temp2)&&(PLC_BIT_TEST(PLC_STL_Addr)))    
					 PLC_ACC_BIT|=1;
			}	
#if DEBUG
#warning "年03月20日新增，MC和MCR指令";
#endif			
			if(MC.MC_Flg ==1)
			{
				if((temp1<temp2)&&(MC.PLC_MC_BIT & 0x01))    
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x01);
				 else				 
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x00);
			}		
	}
	else
	{ 
		PLC_ACC_BIT<<=1;
		if(temp1<temp2) //当前值判断
		   PLC_ACC_BIT|=1;
	}
}

static void less_OR()
{ 
	signed short int temp1,temp2;
	temp1=cos_value();
	temp2=cos_value();
	if((temp1<temp2)||(PLC_ACC_BIT&0X01))    
	PLC_ACC_BIT|=0X01;
	else
	PLC_ACC_BIT&=0xFE; 
}

static void Dless_OR()
{ 
	s32 temp1,temp2;
	temp1=cos_u32_value();
	temp2=cos_u32_value();
	if((temp1<temp2)||(PLC_ACC_BIT&0X01))    
	PLC_ACC_BIT|=0X01;
	else
	PLC_ACC_BIT&=0xFE; 
}

static void Dless_and(void)	   //32位AND小于比较
{ 
	s32 temp1,temp2;
	temp1=cos_u32_value();
	temp2=cos_u32_value();
	if((temp1<temp2)&&((PLC_ACC_BIT&0X01)==0X01)) 
	PLC_ACC_BIT|=1;
	else
	PLC_ACC_BIT&=~1;
}

static void less_and(void)	   //16位AND小于比较
{ 
	signed short int temp1,temp2;
	temp1=cos_value();
	temp2=cos_value();
	if((temp1<temp2)&&((PLC_ACC_BIT&0X01)==0X01)) 
	PLC_ACC_BIT|=1;
	else
	PLC_ACC_BIT&=~1;
}

static void Dless_amount(void)	     //32位小于等于比较指令
{ 
	s32 temp1,temp2;
	temp1=cos_u32_value();
	temp2=cos_u32_value();
	if((PLC_STL_Status == 1) ||(MC.MC_Flg ==1)) 
	{
			if(PLC_STL_Status == 1)            //为STL状态区
			{  
					PLC_ACC_BIT<<=1;		
					if((temp1<=temp2)&&(PLC_BIT_TEST(PLC_STL_Addr)))     
							PLC_ACC_BIT|=1;
			}	
#if DEBUG
#warning "年03月20日新增，MC和MCR指令";
#endif
			if(MC.MC_Flg ==1)
			{
					if((temp1<=temp2)&&(PLC_BIT_TEST(PLC_STL_Addr))) 
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x01);
				 else				 
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x00);			
			}
  }
	else
	{ 
		PLC_ACC_BIT<<=1;
		if(temp1<=temp2) 						    //当前值判断
				PLC_ACC_BIT|=1;
	}
}	

static void less_amount(void)	      //16位小于等于比较指令
{ 
	signed short int temp1,temp2;
	temp1=cos_value();
	temp2=cos_value();
	if((PLC_STL_Status == 1) ||(MC.MC_Flg ==1)) 
	{
			if(PLC_STL_Status == 1)           //为STL状态区
			{  
//					if(temp1<=temp2)    
				  if((temp1<=temp2)&&(PLC_BIT_TEST(PLC_STL_Addr)))     
							PLC_ACC_BIT &=0xFF;
					else	
							PLC_ACC_BIT &=0xfE;
			}
#if DEBUG
#warning "年03月20日新增，MC和MCR指令";
#endif
			if(MC.MC_Flg ==1)
			{
			    if((temp1<=temp2)&&(PLC_BIT_TEST(PLC_STL_Addr))) 
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x01);
				 else				 
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x00);			
			}
  }	
	else
	{ 
		PLC_ACC_BIT<<=1;
		if(temp1<=temp2) 						               //当前值判断
				PLC_ACC_BIT|=1;
	}
}	

static void less_amount_OR()
{ 
	signed short int temp1,temp2;
	temp1=cos_value();
	temp2=cos_value();
	if((temp1<=temp2)||(PLC_ACC_BIT&0X01))    
	PLC_ACC_BIT|=0X01;
	else
	PLC_ACC_BIT&=0xFE; 
}

static void Dless_amount_OR()
{ 
	s32 temp1,temp2;
	temp1=cos_u32_value();
	temp2=cos_u32_value();
	if((temp1<=temp2)||(PLC_ACC_BIT&0X01))    
	PLC_ACC_BIT|=0X01;
	else
	PLC_ACC_BIT&=0xFE; 
}

static void Dless_amount_and(void)	   //32位AND小于等于比较
{ 
	s32 temp1,temp2;
	temp1=cos_u32_value();
	temp2=cos_u32_value();
	if((temp1<=temp2)&&((PLC_ACC_BIT&0X01)==0X01)) 
	PLC_ACC_BIT|=1;
	else
	PLC_ACC_BIT&=~1;
}

static void less_amount_and(void)	//16位AND小于等于比较
{ 
	signed short int temp1,temp2;
	temp1=cos_value();
	temp2=cos_value();
	if((temp1<=temp2)&&(PLC_ACC_BIT&0X01)) 
	PLC_ACC_BIT|=0X01;
	else
	PLC_ACC_BIT&=0XFE;
}

static void Dbig_amount(void)	// 32位大于等于比较指令
{ 
	s32 temp1,temp2;
	temp1=cos_u32_value();
	temp2=cos_u32_value();
	if((PLC_STL_Status == 1) ||(MC.MC_Flg ==1))
	{
			if(PLC_STL_Status == 1) //为STL状态区
			{  
				PLC_ACC_BIT<<=1;		
				if((temp1>=temp2)&&(PLC_BIT_TEST(PLC_STL_Addr)))     
				PLC_ACC_BIT|=1;
			}
#if DEBUG
#warning "年03月20日新增，MC和MCR指令";
#endif			
			if(MC.MC_Flg ==1)
		  {
				 if((temp1 >= temp2)&&(MC.PLC_MC_BIT & 0x01))    
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x01);
				 else				 
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x00);			
			}	
  }
	else
	{ 
		PLC_ACC_BIT<<=1;
		if(temp1>=temp2) //当前值判断
		PLC_ACC_BIT|=1;
	}
}

static void big_amount(void) //16位大于等于比较指令
{ 
	signed short int temp1,temp2;
	temp1=cos_value();
	temp2=cos_value();
	if((PLC_STL_Status == 1) ||(MC.MC_Flg ==1))
	{
			if(PLC_STL_Status == 1) //为STL状态区
			{  
				PLC_ACC_BIT<<=1;		
				if((temp1>=temp2)&&(PLC_BIT_TEST(PLC_STL_Addr)))     
						PLC_ACC_BIT|=1;
			}
#if DEBUG
#warning "年03月20日新增，MC和MCR指令";
#endif			
			if(MC.MC_Flg ==1)
		  {
				 if((temp1 >= temp2)&&(MC.PLC_MC_BIT & 0x01))    
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x01);
				 else				 
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x00);			
			}	
  }
	else
	{ 
		PLC_ACC_BIT<<=1;
		if(temp1>=temp2) // 当前值判断
				PLC_ACC_BIT|=1;
	}
}

static void big_amount_OR()
{ 
	signed short int temp1,temp2;
	temp1=cos_value();
	temp2=cos_value();
	if((temp1>=temp2)||(PLC_ACC_BIT&0X01))    
	PLC_ACC_BIT|=0X01;
	else
	PLC_ACC_BIT&=0xFE; 
}

static void Dbig_amount_OR()
{ 
	s32 temp1,temp2;
	temp1=cos_u32_value();
	temp2=cos_u32_value();
	if((temp1>=temp2)||(PLC_ACC_BIT&0X01))    
	PLC_ACC_BIT|=0X01;
	else
	PLC_ACC_BIT&=0xFE; 
}

static void Dbig_amount_and(void)	   //32位AND大于等于比较
{ 
	s32 temp1,temp2;
	temp1=cos_u32_value();
	temp2=cos_u32_value();
	if((temp1>=temp2)&&((PLC_ACC_BIT&0X01)==0X01)) 
	PLC_ACC_BIT|=1;
	else
	PLC_ACC_BIT&=~1;
}

static void big_amount_and(void)	   //16位AND大于等于比较
{ 
	signed short int temp1,temp2;
	temp1=cos_value();
	temp2=cos_value();
	if((temp1>=temp2)&&(PLC_ACC_BIT&0X01)) 
	PLC_ACC_BIT|=1;
	else
	PLC_ACC_BIT&=~1;
}

static void Dno_amount(void) // 32位不等于比较指令
{ 
	s32 temp1,temp2;
	temp1=cos_u32_value();
	temp2=cos_u32_value();
	
	if((PLC_STL_Status == 1) || (MC.MC_Flg ==1)) 
	{
			if(PLC_STL_Status == 1)            //为STL状态区
			{  
				PLC_ACC_BIT<<=1;		
				if((temp1 != temp2)&&(PLC_BIT_TEST(PLC_STL_Addr)))     
						PLC_ACC_BIT|=1;
			}
#if DEBUG
#warning "年03月20日新增，MC和MCR指令";
#endif
			if(MC.MC_Flg ==1)
		  {
				 if((temp1 != temp2)&&(MC.PLC_MC_BIT & 0x01))    
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x01);
				 else				 
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x00);			
			}	
  }
	else
	{ 
		PLC_ACC_BIT<<=1;
		if(temp1!=temp2) 					 //当前值判断
		PLC_ACC_BIT|=1;
	}
}

static void no_amount(void)	 //16位不等于比较指令
{ 
	signed short int temp1,temp2;
	temp1=cos_value();
	temp2=cos_value();
	
	if((PLC_STL_Status == 1) || (MC.MC_Flg ==1)) 
	{
			if(PLC_STL_Status == 1)            //为STL状态区
			{  
					PLC_ACC_BIT<<=1;		
					if((temp1!=temp2)&&(PLC_BIT_TEST(PLC_STL_Addr)))     
							PLC_ACC_BIT|=1;
			}	
#if DEBUG
#warning "年03月20日新增，MC和MCR指令";
#endif			
			if(MC.MC_Flg ==1)
		  {
				 if((temp1 != temp2)&&(MC.PLC_MC_BIT & 0x01))    
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x01);
				 else				 
						PLC_ACC_BIT = (MC.PLC_MC_BIT & 0x00);			
			}		
  }	
	else
	{ 
		PLC_ACC_BIT<<=1;
		if(temp1!=temp2) 						               //当前值判断
				PLC_ACC_BIT|=1;
	}
}

static void no_amount_OR()
{ 
	signed short int temp1,temp2;
	temp1=cos_value();
	temp2=cos_value();
	if((temp1!=temp2)||(PLC_ACC_BIT&0X01))     
	PLC_ACC_BIT|=0X01;
	else
	PLC_ACC_BIT&=0xFE; 
}

static void Dno_amount_OR()
{ 
	s32 temp1,temp2;
	temp1=cos_u32_value();
	temp2=cos_u32_value();
	if((temp1!=temp2)||(PLC_ACC_BIT&0X01))     
	PLC_ACC_BIT|=0X01;
	else
	PLC_ACC_BIT&=0xFE; 
}

static void Dno_amount_and(void)	   //32位AND不等于比较指令
{ 
	s32 temp1,temp2;
	temp1=cos_u32_value();
	temp2=cos_u32_value();
	if((temp1!=temp2)&&((PLC_ACC_BIT&0X01)==0X01)) 
	PLC_ACC_BIT|=1;
	else
	PLC_ACC_BIT&=~1;
}

static void no_amount_and(void)	   //16位AND不等于比较指令
{ 
	signed short int temp1,temp2;
	temp1=cos_value();
	temp2=cos_value();
	if((temp1!=temp2)&&((PLC_ACC_BIT&0X01)==0X01)) 
	PLC_ACC_BIT|=1;
	else
	PLC_ACC_BIT&=~1;
}


static void LDP(void)	                        //LDP
{
	if(PLC_PL_BIT_TEST(PLC_Addr-PLC_LAD_START_ADDR))	
	{ 
		PLC_ACC_BIT<<=1;
		if(!(PLC_LD_BIT(0X2fff&*PLC_Addr)))							                    //当前值判断
		PLC_PL_BIT_OFF(PLC_Addr-PLC_LAD_START_ADDR);		     
	} 
	else  							                                                     //上升沿判断
	{ 
		if(PLC_STL_Status == 1)                                             //为STL状态区
		{  
			PLC_ACC_BIT<<=1;
			if((PLC_LD_BIT(0X2fff&*PLC_Addr))&&(PLC_BIT_TEST(PLC_STL_Addr)))  //当前值判断 				              
			PLC_ACC_BIT|=0x01,PLC_PL_BIT_ON(PLC_Addr-PLC_LAD_START_ADDR);      //
		}	
		else
		{ 
			PLC_ACC_BIT<<=1;
			if(PLC_LD_BIT(0X2fff&*PLC_Addr))							                   //当前值判断
			PLC_ACC_BIT|=0x01,PLC_PL_BIT_ON(PLC_Addr-PLC_LAD_START_ADDR);      //
		}
	} 
	PLC_Addr++;	    
}

static void LDF(void)	 //LDF
{ 
	if(PLC_PL_BIT_TEST(PLC_Addr-PLC_LAD_START_ADDR))	 //上升沿判断
	{  
		if(PLC_STL_Status == 1)                       //为STL状态区 
		{  
			PLC_ACC_BIT<<=1;
			if((!(PLC_LD_BIT(0X2fff&*PLC_Addr)))&&(PLC_BIT_TEST(PLC_STL_Addr)))//当前值判断 				              
			PLC_ACC_BIT|=0x01,PLC_PL_BIT_ON(PLC_Addr-PLC_LAD_START_ADDR);
		}	
		else
		{ 
			PLC_ACC_BIT<<=1;
			if(!(PLC_LD_BIT(0X2fff&*PLC_Addr)))                     //当前值判断
			PLC_ACC_BIT|=1,PLC_PL_BIT_OFF(PLC_Addr-PLC_LAD_START_ADDR);//
		}		         
	}
	else
	{  
		PLC_ACC_BIT<<=1,PLC_ACC_BIT&=0XFE;        //清除输出标准开关
		if(PLC_LD_BIT(0X2fff&*PLC_Addr))        //当前值判断
		PLC_PL_BIT_ON(PLC_Addr-PLC_LAD_START_ADDR);//
	}  
	PLC_Addr++;
}

static void ANDP(void)	 //ANDP
{ 
	u8  logic;
	if(PLC_PL_BIT_TEST(PLC_Addr-PLC_LAD_START_ADDR))       //查出当前步号对应的逻辑值
	{ 
		logic=0;
		if(!(PLC_LD_BIT(0X2fff&*PLC_Addr)))   					   //当前值判断
		PLC_PL_BIT_OFF(PLC_Addr-PLC_LAD_START_ADDR);         //
	}  
	else	 							                                   //上升沿判断
	{ 
		if(PLC_LD_BIT(0X2fff&*PLC_Addr))	                 //当前值判断
		logic=1,PLC_PL_BIT_ON(PLC_Addr-PLC_LAD_START_ADDR);  //上升沿成立
		else
		logic=0;		                                         //上升沿不成立
	}
	if((PLC_ACC_BIT&0x01)&&(logic==1))
	PLC_ACC_BIT|=0X01;
	else
	PLC_ACC_BIT&=0XFE;
	PLC_Addr++;	    
}

static void ANDF(void)	 //ANDF
{ 
	u8  logic;
	if(PLC_PL_BIT_TEST(PLC_Addr-PLC_LAD_START_ADDR))			  //上升沿判断
	{ 
		if(!(PLC_LD_BIT(0X2fff&*PLC_Addr)))                //当前值判断
		logic=1,PLC_PL_BIT_OFF(PLC_Addr-PLC_LAD_START_ADDR);  //
		else
		logic=0;		 //
	}
	else
	{
		logic=0;
		if(PLC_LD_BIT(0X2fff&*PLC_Addr))                   //当前值判断
		PLC_PL_BIT_ON(PLC_Addr-PLC_LAD_START_ADDR);		        //
	}  
	if((PLC_ACC_BIT&0x01)&&(logic==1))
	PLC_ACC_BIT|=0X01;
	else
	PLC_ACC_BIT&=0XFE;
	PLC_Addr++;
} 

static void ORP(void)	 //ORP
{ 
	u8  logic;
	if(PLC_PL_BIT_TEST(PLC_Addr-PLC_LAD_START_ADDR))							  
	{ 
		logic=0;                                           //
		if(!(PLC_LD_BIT(0X2fff&*PLC_Addr)))							   //当前值判断
		PLC_PL_BIT_OFF(PLC_Addr-PLC_LAD_START_ADDR);		       //
	} 
	else                                                   //上升沿判断
	{ 
		if(PLC_LD_BIT(0X2fff&*PLC_Addr))							     //当前值判断
		logic=1,PLC_PL_BIT_ON(PLC_Addr-PLC_LAD_START_ADDR); //
		else
		logic=0;		 //
	}	
	
	if(((PLC_ACC_BIT&0x01)==0x01)||(logic==1))
	PLC_ACC_BIT|=0x01;
	else
	PLC_ACC_BIT&=0XFE;
	PLC_Addr++;	    
}

static void ORF(void)	 //ORF
{ 
	u8  logic;
	if(PLC_PL_BIT_TEST(PLC_Addr-PLC_LAD_START_ADDR))							                            //上升沿判断
	{ 
		if(!(PLC_LD_BIT(0X2fff&*PLC_Addr)))						    //当前值判断
		logic=1,PLC_PL_BIT_OFF(PLC_Addr-PLC_LAD_START_ADDR);//
		else
		logic=0;		 //
	}
	else
	{  
		logic=0;
		if(PLC_LD_BIT(0X2fff&*PLC_Addr))							   //当前值判断
		PLC_PL_BIT_ON(PLC_Addr-PLC_LAD_START_ADDR);	//
	}  
	if(((PLC_ACC_BIT&0x01)==0x01)||(logic==1))
	PLC_ACC_BIT|=1;
	else
	PLC_ACC_BIT&=~1;
	PLC_Addr++;
}

static void CJ_EX(u8 value)  //执行跳指令
{ 
	PLC_Addr++;
	if((*PLC_Addr&0xff00)==0x8000)
	{PLC_Addr=PLC_P_Addr[value/2],PLC_Addr++;}//取低位
}

static void CJ(void)
{ 
	if(PLC_ACC_BIT&0X01)
	{
		if((*PLC_Addr&0xff00)==0x8800) CJ_EX(*PLC_Addr); 
	}
	else PLC_Addr+=2;
}
 
static void CJP(void)	  //CJP
{ 
	if(PLC_LDP_TEST())    //上升沿判断
	{if((*PLC_Addr&0xff00)==0x8800) CJ_EX(*PLC_Addr);}
	else
	PLC_Addr+=2;		      //条件不满足执行跳过程序，减小CPU开销
}
 
static void SRET(void)
{ 
	u8 temp;
	PLC_ACC_BIT=process[0];	    //返回上一个逻辑状态值
	PLC_Addr=p_save[0];	  	    //返回上一个子程序前的执行地址
	for(temp=62;temp>0;temp--)
	{
		process[temp]=process[temp+1];    //data mov down
		p_save[temp]=p_save[temp+1]; 
	}      
}


static void P_MOV(void)
{ 
	u8 temp;
	for(temp=62;temp>0;temp--)
	{
		process[temp+1]=process[temp];    //数据 MOV up
		p_save[temp+1]=p_save[temp]; 
	}
	process[0]=PLC_ACC_BIT;	               //保存上一个逻辑状态值
	p_save[0]=PLC_Addr;				             //保存上一个子程序前的执行地址
}

static void CALL_EX(u8 value)
{ 
	PLC_Addr++;
	if((*PLC_Addr&0xff00)==0x8000)
	{P_MOV(),PLC_Addr=PLC_P_Addr[value/2];}//	先压入状态寄存器，以及前一个P指针的地址，
}

#if DEBUG
#warning "年03月20日新增，MC和MCR指令";
#endif
void Func_MC()
{
//		MC.MC_Addr = 0x0FFF & *PLC_Addr;
		MC.PLC_MC_BIT <<= 1; // MC寄存器暂存位
		MC.MC_SFR += 1;
    MC.MC_Flg = 1; // MC运算标志
		if (PLC_ACC_BIT & 0x01)
				MC.PLC_MC_BIT |= 0x01; // 运算位		
		PLC_Addr += 2;
}

#if DEBUG
#warning "月20日新增，MC和MCR指令";
#endif
void Func_MCR()
{
//		MC.MC_Addr = 0x0FFF & *PLC_Addr;
		MC.PLC_MC_BIT >>= 1; // MC寄存器暂存位
		MC.MC_SFR -= 1;
		if (MC.MC_SFR <= 0)
		{
			 MC.MC_SFR =0;
			 MC.MC_Flg = 0;	// MC运算标志  MCR在指令中清除		
		}		
		PLC_Addr ++;
}


static void CALL(void)
{ 
	if(PLC_ACC_BIT&0X01)
	{ 
		if((*PLC_Addr&0xff00)==0x8800) 
		{CALL_EX(*PLC_Addr);}
	}
	else PLC_Addr+=2;
}

static void CALLP(void)	  //CALLP
{ 
	if(PLC_LDP_TEST())       //上升沿判断
	{if((*PLC_Addr&0xff00)==0x8800)CALL_EX(*PLC_Addr);}
	else
	PLC_Addr+=2;		        //条件不满足执行跳过程序，减小CPU开销 
}

// 传人记,20160929新增
static void FOR(void)
{ 
 if((PLC_ACC_BIT&0X01)==0X01)
  { 
		if(FOR_CMD.point<6) 
			FOR_CMD.point++;             //判断嵌套数量
	  else 
			FOR_CMD.point=0;
		FOR_CMD.count[FOR_CMD.point] =(cos_value()-1);   //存入目标循环次数记录下来
		FOR_CMD.Addr[FOR_CMD.point]=PLC_Addr;            //存入目标起始地址
		FOR_CMD.cycle[FOR_CMD.point]=0;                  //清楚当前循环次数
	}
}

// 0160929新增
static void FOR_NEXT(void)
{ 
    if(FOR_CMD.cycle[FOR_CMD.point]<FOR_CMD.count[FOR_CMD.point])  //当前次数小于目标次数则返回到FOR的后一个地址
    { 
	    PLC_Addr=FOR_CMD.Addr[FOR_CMD.point];   //返回上个起始地址
			FOR_CMD.cycle[FOR_CMD.point]++;         //PLC循环一次
    } 
}


void expand_SET(void)
{
  BIT_SET(0X2FFF&*PLC_Addr);PLC_Addr++;
}
	
void expand_RST(void)
{
  RST(0X2FFF&*PLC_Addr);PLC_Addr++;
}
	
void expand_OUT(void)
{
  OUT(0X2FFF&*PLC_Addr);PLC_Addr++;
}
	
void expand_LD(void)
{
  LD(0X2FFF&*PLC_Addr);PLC_Addr++;
}
	
void expand_LDI(void)
{
  LDI(0x2FFF&*PLC_Addr);PLC_Addr++;
}
	
void expand_AND(void)
{
  AND(0x2FFF&*PLC_Addr);PLC_Addr++;
}
	
void expand_ANI(void)
{
  ANI(0x2FFF&*PLC_Addr);PLC_Addr++;
}
	
void expand_OR(void)
{
  OR(0x2FFF&*PLC_Addr);PLC_Addr++;
}
	
void expand_ORI(void)
{
  ORI(0x2FFF&*PLC_Addr);PLC_Addr++;
}	


static void enable_T_K(void)
{ 
	static u16 *p_data;
	T_value =*PLC_Addr%0x100;                //赋低8位值
	PLC_Addr++;
	T_value +=(*PLC_Addr%0x100)*0x100;       //赋高8位值
	p_data =plc_16BitBuf+0x0900+T_number;    //指针指向T比较的值地址
	*p_data =T_value;                        //赋值给地址
	timer_enable(T_number);
	OUT(0X1600+(u8)T_number);
}

static void enable_T_D(void)
{
	plc_16BitBuf[0x0900+T_number]=plc_16BitBuf[0x1000+T_value];
	timer_enable(T_number);
	OUT(0X1600+(u8)T_number);
}

static void disable_T(void)
{
	timer_disble(T_number);
	OUT(0X1600+(u8)T_number);	 //disable T coil	
	OUT(0x0600+(u8)T_number);	 //reset T over coil
}


static void T_given_value_K(void)	
{
	if(PLC_ACC_BIT&0X01)
	{
		enable_T_K();
	}
	else
	{
		PLC_Addr++;
		disable_T(); 
	}
}
static void T_given_value_D(void)
{ 
	T_value=(*PLC_Addr%0x100)/2;
	PLC_Addr++;
	switch(*PLC_Addr/0x100) 
	{ 
		case 0x86: T_value+=(*PLC_Addr%0x100)*0x80;        break;
		case 0x88: T_value+=(*PLC_Addr%0x100)*0x80+1000;   break; 
	}
	if((PLC_ACC_BIT&0X01)==0X01)  //是否有效
		enable_T_D();
	else
		disable_T();
}

static void operation_T(void)
{ 
	T_number=*PLC_Addr;       //将操作定时器的号码送入
	PLC_Addr++;				        //下一个功能取是K赋值还是D赋值
	switch(*PLC_Addr/0x100) 
	{ 
		case 0x80: T_given_value_K();              break;  //进行K赋值操作
		case 0x86: T_given_value_D();              break;  //进行D赋值操作
	}	
}

//////////////////////////////////////////////////////////////////////////////////
// 0419整理
static void enable_C_K(void) // 用常数K进行赋值
{
	u16 temp_bit,*p_C_enable_coil;u32 C;
	C_value=*PLC_Addr%0x100; // 赋低8位值
	PLC_Addr++;
	C_value+=(*PLC_Addr%0x100)*0x100; // 赋高8位值
	if(C_number >= 200) // 判断是不是C200以上的 寄存器
	{  
			PLC_Addr++;		
			/* 得到C设定值 */
			C_value+=(*PLC_Addr%0x100)*0x10000; //赋低8位值
			PLC_Addr++;
			C_value+=(*PLC_Addr%0x100)*0x1000000;//赋高8位值					
			C = RAM_C200_ADDR + (C_number - 200)*4; // C200及以上
		
/* 23新增，编码器 */
#if ENCODE_FUNC ==1			
			if(C_number == 236)
			{
					if(S_Cap_Pulse_Pro[0].used==0)
					{
							Encoder_A0_Configuration();
						
							S_Cap_Pulse_Pro[0].mode=Signal_mode;
							//S_Cap_Pulse_Pro[0].Gpio_pin=;
							S_Cap_Pulse_Pro[0].used=1;
					}
					//	Get_A0_Count();
			}
		  else if(C_number == 239)
			{								
				 if(S_Cap_Pulse_Pro[1].used==0)
				 {
						Encoder_A1_Configuration();
					 	S_Cap_Pulse_Pro[1].mode=Signal_mode;
						//	S_Cap_Pulse_Pro[1].Gpio_pin=;
						S_Cap_Pulse_Pro[1].used=1;
				 }
					 // Get_A1_Count();
			}
		  else if(C_number == 251)
			{
					if(S_Cap_Pulse_Pro[0].used==0)
						 Encoder_AB0_Configration();
						// Get_AB0_Count();
			}
			else if(C_number == 253)
			{
				  if(S_Cap_Pulse_Pro[1].used==0)
						 Encoder_AB1_Configration();
						 // Get_AB1_Count();
			}
			else
#endif /* #if ENCODE_FUNC ==1	*/
			{
//					PLC_Addr++;		
//					/* 得到C设定值 */
//					C_value+=(*PLC_Addr%0x100)*0x10000; //赋低8位值
//					PLC_Addr++;
//					C_value+=(*PLC_Addr%0x100)*0x1000000;//赋高8位值					
//					C = RAM_C200_ADDR + (C_number - 200)*4; // C200及以上
					
					temp_bit=1<<(C_number%0x10);
					if(PLC_RAM32(C) < C_value) //把C当前值与目标值进行比较
					{
							p_C_enable_coil =plc_16BitBuf+0x0270+(C_number/0X10);
							if(!((*p_C_enable_coil&temp_bit)==temp_bit))
									PLC_RAM32(C)+=1;
					}					
//					if(PLC_RAM32(C) < C_value) // 比较溢出值
//							PLC_BIT_OFF(0x0E00+C_number);// 0x0E00是C溢出线圈的起始地址
//					else
//							PLC_BIT_ON(0x0E00+C_number);
			}
			if(PLC_RAM32(C) < C_value) // 比较溢出值
					PLC_BIT_OFF(0x0E00+C_number);// 0x0E00是C溢出线圈的起始地址
			else
					PLC_BIT_ON(0x0E00+C_number);
	}
	else
	{
		static u16 *p_data;
		p_data=plc_16BitBuf+0x0500+C_number; // C当前值 
		
		temp_bit=1<<(C_number%0x10);
		if(*p_data < C_value)             
		{
				p_C_enable_coil=plc_16BitBuf+0x0270+(C_number/0X10);//0x0270是C使能线圈字节地址
				if(!((*p_C_enable_coil&temp_bit)==temp_bit))
						*p_data+=1;
		}
		if(*p_data < C_value) //比较溢出值
				PLC_BIT_OFF(0x0E00+C_number); // 0x0E00是C溢出线圈的起始地址
		else
				PLC_BIT_ON(0x0E00+C_number);
	}
	OUT(0X2700+(u8)C_number); // 0X2700是C复位线圈的起始地址 
}

// 19修复，C200以上死机的问题
static void enable_C_D(void) // 用寄存器D进行赋值
{
	static u16 *p_data;
	u16 temp_bit,*p_C_enable_coil;u32 C;
	
	C_value=plc_16BitBuf[0x1000 + C_value]; // 得到C设定值
	
	if(C_number >= 200) //判断是不是C200以上的 寄存器
	{  		
			C_value +=plc_16BitBuf[0x1000+C_value+1]*0x10000;
			C = RAM_C200_ADDR + (C_number-200)*4;  
			temp_bit =1<<(C_number % 0x10);
		
/* 23新增，编码器 */
#if ENCODE_FUNC ==1			
			if(C_number == 236)
			{
					if(S_Cap_Pulse_Pro[0].used==0)
					{
							Encoder_A0_Configuration();
							S_Cap_Pulse_Pro[0].mode=Signal_mode;
							//S_Cap_Pulse_Pro[0].Gpio_pin=;
							S_Cap_Pulse_Pro[0].used=1;
					}
					//	Get_A0_Count();
			}
		  else if(C_number == 239)
			{								
				 if(S_Cap_Pulse_Pro[1].used==0)
				 {
						Encoder_A1_Configuration();
					 	S_Cap_Pulse_Pro[1].mode=Signal_mode;
						//	S_Cap_Pulse_Pro[1].Gpio_pin=;
						S_Cap_Pulse_Pro[1].used=1;
				 }
					 // Get_A1_Count();
			}
		  else if(C_number == 251)
			{
					if(S_Cap_Pulse_Pro[0].used==0)
						 Encoder_AB0_Configration();
						// Get_AB0_Count();
			}
			else if(C_number == 253)
			{
				  if(S_Cap_Pulse_Pro[1].used==0)
						 Encoder_AB1_Configration();
						 // Get_AB1_Count();
			}
			else
#endif /* #if ENCODE_FUNC ==1	*/
		  {
//					C_value +=plc_16BitBuf[0x1000+C_value+1]*0x10000;//					
//					C = RAM_C200_ADDR + (C_number-200)*4;  //					
//					temp_bit =1<<(C_number % 0x10);
							
					// 传人记，20170419修复，C200以上死机问题
					if(PLC_RAM32(C) < C_value) //把C当前值与目标值进行比较
					{
							p_C_enable_coil=plc_16BitBuf + 0x0270 + (C_number/0X10);
							if(!((*p_C_enable_coil & temp_bit) == temp_bit))
									PLC_RAM32(C)+=1;
					}

//					if(PLC_RAM32(C) < C_value) //把C当前值与目标值进行比较
//					{
//							PLC_BIT_OFF(0x0E00+C_number);	// 0x0E00是C溢出线圈的起始位地址	 
//					}
//					else
//					{
//							PLC_BIT_ON(0x0E00+C_number);
//					}		
//					PLC_Addr+=2;   
		  }
			 
			if(PLC_RAM32(C) < C_value) //把C当前值与目标值进行比较
					PLC_BIT_OFF(0x0E00 + C_number);	// 0x0E00是C溢出线圈的起始位地址	 
			else
					PLC_BIT_ON(0x0E00 + C_number);	
			PLC_Addr+=2;   			
	}
	else
	{
		p_data=plc_16BitBuf+0x0500+C_number; // 当前值
		temp_bit=1<<(C_number%0x10);
		if(*p_data<C_value) //把C当前值与目标值进行比较
		{
				p_C_enable_coil=plc_16BitBuf+0x0270+(C_number/0X10); //比较enable coil
				if(!((*p_C_enable_coil&temp_bit)==temp_bit))
					*p_data+=1;
		}
		if(*p_data<=C_value)  //比较溢出值
		   PLC_BIT_OFF(0x0E00+C_number);// 0x0E00是C溢出线圈的起始位地址		 
		else
		   PLC_BIT_ON(0x0E00+C_number);
	}
	OUT(0X2700 + (u8)C_number); // 0X2700是C复位线圈的起始位地址 
}
 
static void disable_C_K(void)
{	
	u32 C;static u16 *p_data;
	
	C_value=*PLC_Addr%0x100; //赋低8位值
	PLC_Addr++;
	C_value+=(*PLC_Addr%0x100)*0x100; //赋高8位值
	
	if(C_number>=200) //判断是不是C200以上的 寄存器
	{  
	
		/* 3新增，编码器 */
#if ENCODE_FUNC ==1			
			if(C_number == 236)
			{
					if(S_Cap_Pulse_Pro[0].used==1)
					{
							Encoder_A0_Restore();
						  C236 =0;	
							S_Cap_Pulse_Pro[0].used=0;
					}
			}
		  else if(C_number == 239)
			{								
					 if(S_Cap_Pulse_Pro[1].used==1)
					 {
							Encoder_A1_Restore();
						  C239 =0;	
							S_Cap_Pulse_Pro[1].used=0;
					 }
			}
		  else if(C_number == 251)
			{
					 if(S_Cap_Pulse_Pro[0].used==1)
					 {
							 Encoder_AB0_Restore();
							 S_Cap_Pulse_Pro[0].used=0;
					 }
			}
			else if(C_number == 253)
			{
					if(S_Cap_Pulse_Pro[1].used==1)
					{
						 Encoder_AB1_Restore();
						 S_Cap_Pulse_Pro[1].used=0;
					}
			}
//			else
#endif /* #if ENCODE_FUNC ==1	*/
//      {				
					PLC_Addr++;
					C_value +=(*PLC_Addr%0x100)*0x10000; //赋低8位值 // 传人记，20170426修复=改为+=
					PLC_Addr++;
					C_value +=(*PLC_Addr%0x100)*0x1000000;//赋高8位值
					
					C= RAM_C200_ADDR + (C_number - 200) *4;  
					
					if(PLC_RAM32(C) < C_value) //把C当前值与目标值进行比较
						PLC_BIT_OFF(0x0E00+C_number); // 0x0E00是C溢出线圈的起始位地址		 
					else
						PLC_BIT_ON(0x0E00+C_number);  
//	    }		
	}
	else
	{
		p_data=plc_16BitBuf+0x0500+C_number;
		if(*p_data < C_value) //比较溢出值
			PLC_BIT_OFF(0x0E00+C_number);		 
		else
			PLC_BIT_ON(0x0E00+C_number);       
	}
	OUT(0X2700+(u8)C_number); // 0X2700是C复位线圈的起始位地址 
}

static void disable_C_D(void)	//关闭计数器C
{ 
	u32 C;static u16 *p_data;  
	if(C_number >= 200) //判断是不是C200以上的 寄存器
	{ 
/* 23新增，编码器 */
#if ENCODE_FUNC ==1			
			if(C_number == 236)
			{
					if(S_Cap_Pulse_Pro[0].used==1)
					{
							Encoder_A0_Restore();
						  C236 =0;	
							S_Cap_Pulse_Pro[0].used=0;
					}
			}
		  else if(C_number == 239)
			{								
					 if(S_Cap_Pulse_Pro[1].used==1)
					 {
							Encoder_A1_Restore();
						  C239 =0;	
							S_Cap_Pulse_Pro[1].used=0;
					 }
			}
		  else if(C_number == 251)
			{
					 if(S_Cap_Pulse_Pro[0].used==1)
					 {
							 Encoder_AB0_Restore();
							 S_Cap_Pulse_Pro[0].used=0;
					 }
			}
			else if(C_number == 253)
			{
					if(S_Cap_Pulse_Pro[1].used==1)
					{
						 Encoder_AB1_Restore();
						 S_Cap_Pulse_Pro[1].used=0;
					}
			}
//			else	
#endif /* #if ENCODE_FUNC ==1	*/
//			{
					C_value=plc_16BitBuf[0x1000+C_value];
					C_value+=plc_16BitBuf[0x1000+C_value+1]*0x10000;
					
					C =RAM_C200_ADDR + (C_number - 200) *4; 
					
					if(PLC_RAM32(C) < C_value) //比较溢出值
						PLC_BIT_OFF(0x0E00+C_number);	// 0x0E00是C溢出线圈的起始位地址	 
					else
						PLC_BIT_ON(0x0E00+C_number);
					PLC_Addr+=2; 
//	     }		
	}
	else
	{
			C_value=plc_16BitBuf[0x1000+C_value];
			p_data=plc_16BitBuf+0x0500+C_number;
			if(*p_data<C_value)            //比较溢出值
				PLC_BIT_OFF(0x0E00+C_number);
			else
				PLC_BIT_ON(0x0E00+C_number);
	}	
	OUT(0X2700+(u8)C_number); // 0X2700是C复位线圈的起始位地址 
}

static void C_given_value_K(void)	//程序中以K来设定值
{
	if((PLC_ACC_BIT&0X01)==0X01)
		enable_C_K(); //开启计数器
	else
		disable_C_K(); 
}

static void C_given_value_D(void)	//程序中以D来设定值
{  
	C_value=(*PLC_Addr%0x100)/2;
	PLC_Addr++;
	switch(*PLC_Addr/0x100) 
	{ 
		case 0x86: C_value+=(*PLC_Addr%0x100)*0x80;        break;
		case 0x88: C_value+=(*PLC_Addr%0x100)*0x80+1000;   break; 
	}
	if(PLC_ACC_BIT&0X01) 
		enable_C_D();
	else
		disable_C_D();
}
 
static void operation_C()
{
	C_number=*PLC_Addr; //将操作计数器的号码送入
	PLC_Addr++;	//下一个功能取是K赋值还是D赋值
	switch(*PLC_Addr/0x100) 
	{
		case 0x80: C_given_value_K();break;  //进行K赋值操作
		case 0x86: C_given_value_D();break;  //进行D赋值操作
	}	
}

//=======================================================================================================
// 函数名称:  static void ASCI(void)	 
// 功能描述： HEX转ASCII
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void ASCI(void) // 7步          
{  	
	 if (PLC_ACC_BIT & 0x01)
	 {
//暂未实现
			 PLC_Addr += 6;
	 }
	 else{
			PLC_Addr += 6;
	 }
}


// 新增
//=======================================================================================================
// 函数名称:  static void HEX(void)	 
// 功能描述： ASCII转HEX
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void HEX(void)	                 
{  	
	 if (PLC_ACC_BIT & 0x01)
	 {
// 524暂未实现
	   PLC_Addr += 6;
	 }
	 else{
			PLC_Addr += 6;
	 }
}

//=======================================================================================================
// 函数名称:  DECO
// 功能描述： 译码
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void DECO()
{
		if (PLC_ACC_BIT & 0x01)
		{
				s16 sx;
				u16 nx;									
//			  sx = cos_value();  // 要译码的数据
//        dx = addr_value(); // 保存译码结果的位或者字软元件编号 
				PLC_Addr += 4;
        nx = cos_value(); // 保存译码结果的软元件的位点数（n=1~8，n=0时不处理）
			  PLC_Addr -= 6;
			  sx =Special_cos_value(nx); // 要译码的数据
			  /* n=1~8，n=0时不处理 */
			  if((1 <= nx) && (nx <=8)) 
				{
					trade = 0x0001 << (sx & DE[nx]);
					SpecialTarget(); // 保存译码结果的位或者字软元件编号 
					PLC_Addr += 2;
        }	
        else
				{					
					PLC_Addr += 4;
				}
		}
		else{
			 PLC_Addr += 6; 
		}
}

//=======================================================================================================
// 函数名称:  ENCO
// 功能描述： 编码
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void ENCO()
{
		if (PLC_ACC_BIT & 0x01)
		{
			s16 sx;
			u16 nx;
			u8 i;
			
			PLC_Addr += 4;
			nx = cos_value();     
			PLC_Addr -= 6;
			sx =Special_cos_value(nx); // 要译码的数据
			/* n=1~8，n=0时不处理 */
			if((1 <= nx) && (nx <=8)) 
			{
				for(i =0;i < 8;i++)
				{
					if(sx & (0x0001 << i))
					{
						trade = i & DE[nx];
						SpecialTarget(); // 编码的数据
						break;
					}
				}
				PLC_Addr += 2; 
		  }
			else
			{
				PLC_Addr += 4; 
			}
		}
		else{
			PLC_Addr += 6;
		}	
}

//=======================================================================================================
// 函数名称:  ENCO
// 功能描述： 七段码译码
// 输　入:  void      
// 输　出:  void     
// 全局变量:  

//=======================================================================================================
static void SEGD(void)
{
	if (PLC_ACC_BIT & 0x01)
	{
		u8 sx;		
		sx = (u8)(cos_value() & 0x000F); // 低4位 	
		trade = SEGD_LED[sx];
		target();
	}
	else
	{
		PLC_Addr += 4;
	}
}

//////////////////////////////////////////////////////////////////////////////////////

static void FNC_AppInstruct(void) 
 { 
	 switch(*PLC_Addr) 
	{
		case 0x0002: PLC_Addr++,expand_OUT();              break;  //M1535以上的指令
		case 0x0003: PLC_Addr++,expand_SET();              break;  //M1535以上的指令
		case 0x0004: PLC_Addr++,expand_RST();              break;  //M1535以上的指令
		
		case 0x0005: PLC_Addr++,expand_OUT();              break;  
		case 0x0006: PLC_Addr++,expand_SET();              break; 
		case 0x0007: PLC_Addr++,expand_RST();              break;  
		case 0x0008: PLC_Addr++,LPS();                     break; 
		case 0x0009: PLC_Addr++,LPF();                     break;  
		
#if DEBUG
#warning "年03月20日新增，MC和MCR指令";
#endif
		case 0x000A: PLC_Addr++,Func_MC();                 break;  
		case 0x000B: PLC_Addr++,Func_MCR();                break;  
		
		case 0x000C: PLC_Addr++,RST_T_C();                 break;  //执行RST C&T
		case 0x000D: PLC_Addr++,RST_D();                   break;  //执行D寄存器复位
		
		case 0x0010: PLC_Addr++,CJ();                      break;  //CJ  
		case 0x1010: PLC_Addr++,CJP();                     break;  //CJP  
		case 0x0012: PLC_Addr++,CALL();                    break;  //CALL
		case 0x1012: PLC_Addr++,CALLP();                   break;  //CALLP
		case 0x0014: PLC_Addr++,SRET();                    break;  //SRET			
		case 0x001C: PLC_Addr=PLC_Addr;                    break;  //FEND
		
		
		case 0X0020: PLC_Addr++,FOR();                     break;  //传人记 20160929新增，FOR循环
	  case 0X0022: PLC_Addr++,FOR_NEXT();                break;  //传人记 20160929新增，FOR_NEST 循环结束
		
		case 0X0024: PLC_Addr++,CMP();                     break;  //16位比较传送指令
		case 0X1024: PLC_Addr++,CMPP();                    break;  //16位上升沿比较传送指令
		case 0X0025: PLC_Addr++,DCMP();                    break;  //32位比较传送指令
		case 0X1025: PLC_Addr++,DCMPP();                   break;  //32位上升沿比较传送指令
		case 0X0026: PLC_Addr++,ZCP();                     break;  //16位区间值比较传送指令
		case 0X0027: PLC_Addr++,DZCP();                    break;  //32位区间值比较传送指令
		
		// 28读
		case 0x0028: PLC_Addr++,MOV();		                 break;  //执行16bit传送指令
		case 0X0029: PLC_Addr++,DMOV();                    break;  //DMOV 
		case 0X002A: PLC_Addr++,SMOV();                    break;  //SMOV 	
		case 0X002C: PLC_Addr++,CML();                     break;  //CML取反指令
		case 0X002D: PLC_Addr++,DCML();                    break;  //DCML取反指令
		case 0X002E: PLC_Addr++,BMOV();                    break;  //成批传送
		case 0X0030: PLC_Addr++,FMOV();                    break;  //多点传送
		case 0X0031: PLC_Addr++,DFMOV();                   break;  //32位多点传送	
		case 0X0032: PLC_Addr++,XCH();                     break;  //交换传送
		case 0X0033: PLC_Addr++,DXCH();                    break;  //32位交换传送
		case 0X0034: PLC_Addr++,BCD();                     break;  //二进制转换BCD
		case 0X0035: PLC_Addr++,DBCD();                    break;  //二进制转换DBCD
		case 0X0036: PLC_Addr++,BIN();                     break;  //二进制转换BIN
		case 0X0037: PLC_Addr++,DBIN();                    break;  //二进制转换DBIN
		
		case 0X0038: PLC_Addr++,ADD();					           break;  //加法指令
		case 0x0039: PLC_Addr++,DADD();                    break;  //DADD加法运算			
		case 0X003A: PLC_Addr++,SUB();					           break;  //减法指令
		case 0x003B: PLC_Addr++,DSUB();                    break;  //DSUB减法运算			
		case 0x003C: PLC_Addr++,MUL();                     break;  //MUL 乘法指令
		case 0x003D: PLC_Addr++,DMUL();                    break;  //DMUL乘法运算			
		case 0x003E: PLC_Addr++,DIV();                     break;  //DIV 乘法指令
		case 0x003F: PLC_Addr++,DDIV();                    break;  //DDIV除法运算			
		case 0x0040: PLC_Addr++,INC();                     break;  //16位逻辑运算加1指令
		case 0x1040: PLC_Addr++,INCP();                    break;  //16位上升沿逻辑运算加1指令
		case 0x0041: PLC_Addr++,DINC();                    break;  //32位逻辑运算加1指令
		case 0x1041: PLC_Addr++,DINC_P();                  break;  //32位上升沿逻辑运算加1指令
		case 0x0042: PLC_Addr++,DEC();                     break;  //16位逻辑运算减1指令
		case 0x1042: PLC_Addr++,DECP();                    break;  //16位上升沿逻辑运算减1指令
		case 0x0043: PLC_Addr++,DDEC();                    break;  //32位逻辑运算减1指令
		case 0x0044: PLC_Addr++,WAND();	                   break;  //逻辑运算与逻辑
		case 0x0045: PLC_Addr++,DWAND();	                 break;  //32位逻辑运算与逻辑
		case 0x0046: PLC_Addr++,WOR();                     break;  //逻辑运算或逻辑
		case 0x0047: PLC_Addr++,DWOR();                    break;  //32位逻辑运算或逻辑
		case 0x0048: PLC_Addr++,WXOR();                    break;  //逻辑运算异或逻辑
		case 0x0049: PLC_Addr++,DWXOR();                   break;  //32位逻辑运算异或逻辑
		case 0x004A: PLC_Addr++,NEG();                     break;  //逻辑运算取负数
		case 0x004B: PLC_Addr++,DNEG();                    break;  //32位逻辑运算取负数			
		
		/* 循环移位指令 */
		case 0x004C: PLC_Addr++,ROR();                     break;  //ROR
		case 0x004D: PLC_Addr++,DROR();                    break;  //DROR
		case 0x004E: PLC_Addr++,ROL();                     break;  //ROL
		case 0x004F: PLC_Addr++,DROL();                    break;  //DROL
		case 0x0050: PLC_Addr++,RCR();                     break;  //RCR
		case 0x0051: PLC_Addr++,DRCR();                    break;  //DRCR
		case 0x0052: PLC_Addr++,RCL();                     break;  //RCL
		case 0x0053: PLC_Addr++,DRCL();                    break;  //DRCL
		
		case 0x0060: PLC_Addr++,ZRST();                    break; 
		
		case 0x0062: PLC_Addr++,DECO();	                   break;  //DECO 译码，0423新增
 		case 0x0064: PLC_Addr++,ENCO();	                   break;  //ENCO 编码，423新增

		case 0x006A: PLC_Addr++,MEAN();                    break;	 //MEAN，求平均值指令		
		case 0x0070: PLC_Addr++,SQR();	                   break;  //SQR16位整数开方			
		case 0x0071: PLC_Addr++,DSQR();	                   break;  //SQR32位整数开方
		
		case 0x0072: PLC_Addr++,FLT();	                   break;  //16位整数转浮点
		case 0x0073: PLC_Addr++,DFLT();	                   break;  //32位整数转浮点	
		case 0x0076: PLC_Addr++,REFF();	                   break;  //REFF	
		case 0x0078: PLC_Addr++,MTR();	                   break;  //MTR
    case 0x007A: PLC_Addr++,HSCS();		                 break;  //高速计数置位  
		
		case 0x0080: PLC_Addr++,SPD();                     break;  // SPD
  	case 0x0084: PLC_Addr++,PWM();                     break;  // PWM输出
		case 0x0082: PLC_Addr++,PLSY();                    break;  // 高速脉冲输出
		case 0x0083: PLC_Addr++,DPLSY();                    break;  // 高速脉冲输出
		
		case 0x0094: PLC_Addr++,ALT();	                   break;  //ALT
		
	  case 0x00A2: PLC_Addr++,SEGD();	                   break;  //SEGD，

	  case 0x00AC: PLC_Addr++,FROM();	                   break;  //SEGD，
		case 0x00AE: PLC_Addr++,TO();	                     break;  //SEGD，
		// 传人记，20161128新增
		case 0x00B0: PLC_Addr++,RS();	                     break;  // RS，
		
	  case 0x00B4: PLC_Addr++,ASCI();	                   break;  //ASCI，
	  case 0x00B6: PLC_Addr++,HEX();	                   break;  //HEX，
		
		case 0x00C0: PLC_Addr++,PID();	                   break;  //PID
		case 0x00ED: PLC_Addr++,ECMP();	                   break;  //ECMP
		case 0x00EE: PLC_Addr++,EZCP();	                   break;  //EZCP
		
    case 0x00F1: PLC_Addr++,DEMOV();                   break;  //
// 		case 0x00FD: PLC_Addr++,DEBCD();	                 break;  //DEBCD

		case 0x0101: PLC_Addr++,DEADD();                   break;  //浮点加法运算
		case 0x0103: PLC_Addr++,DESUB();	                 break;  //浮点减法运算
		case 0x0107: PLC_Addr++,DEDIV();	                 break;  //浮点乘法运算
		case 0x0105: PLC_Addr++,DEMUL();                   break;  //浮点除法运算
		case 0x010F: PLC_Addr++,DESQR();                   break;  //DESQR浮点开方
		case 0x0112: PLC_Addr++,INT();                     break;  //INT
		case 0x0113: PLC_Addr++,DINT();                    break;  //DINT
		case 0x0115: PLC_Addr++,DSIN();	                   break;  //DSIN
		case 0x0117: PLC_Addr++,DCOS();	                   break;  //DCOS
		case 0x0119: PLC_Addr++,DTAN();	                   break;  //DTAN			

		
		case 0x0136: PLC_Addr++,SWAP();                    break;  //SWAP
		case 0x0137: PLC_Addr++,DSWAP();                   break;  //DSWAP
		
		case 0x0150: PLC_Addr++,TCMP();	                   break;  //TCMP
		case 0x0152: PLC_Addr++,TZCP();	                   break;  //TZCP
		case 0x0154: PLC_Addr++,TADD();	                   break;  //TADD
		case 0x0156: PLC_Addr++,TSUB();	                   break;  //TSUB		
		case 0x015C: PLC_Addr++,TRD();	                   break;  //TRD	
		case 0x015E: PLC_Addr++,TWR();	                   break;  //TWR	
		case 0x0164: PLC_Addr++,GRY();	                   break;  //GRY
		case 0x0165: PLC_Addr++,DGRY();	                   break;  //DGRY
		case 0x0166: PLC_Addr++,GBIN();	                   break;  //GBIN
		case 0x0167: PLC_Addr++,DGBIN();	                 break;  //DGBIN
		
	
		case 0x01C2: PLC_Addr++,expand_LD();               break;  //M1535以上的指令
		case 0x01C3: PLC_Addr++,expand_LDI();              break;  //
		case 0x01C4: PLC_Addr++,expand_AND();              break;  //
		case 0x01C5: PLC_Addr++,expand_ANI();              break;  //
		case 0x01C6: PLC_Addr++,expand_OR();               break;  //
		case 0x01C7: PLC_Addr++,expand_ORI();              break;  //
		
		case 0x01CA: PLC_Addr++,LDP();			               break;  //上升延处理程序
		case 0x01CB: PLC_Addr++,LDF();			               break;  //上升延处理程序
		case 0x01CC: PLC_Addr++,ANDP();			               break;  //上升延处理程序
		case 0x01CD: PLC_Addr++,ANDF();			               break;  //上升延处理程序
		case 0x01CE: PLC_Addr++,ORP();			               break;  //上升延处理程序
		case 0x01CF: PLC_Addr++,ORF();			               break;  //上升延处理程序
		
		
		case 0X01D0: PLC_Addr++,amount();                  break;  //LD 16位等于比较
		case 0X01D1: PLC_Addr++,Damount();                 break;  //LD 32位等于比较
		case 0X01D2: PLC_Addr++,big();                     break;  //LD 16位大于比较
		case 0X01D3: PLC_Addr++,Dbig();                    break;  //LD 32位大于比较
		case 0X01D4: PLC_Addr++,less();                    break;  //LD 16位小于比较
		case 0X01D5: PLC_Addr++,Dless();                   break;  //LD 32位小于比较
		case 0X01D8: PLC_Addr++,no_amount();	             break;  //LD 16位不等于比较指令
		case 0X01D9: PLC_Addr++,Dno_amount();	             break;  //LD 32位不等于比较指令
		case 0X01DA: PLC_Addr++,less_amount();             break;  //LD 16位小于等于比较
		case 0X01DB: PLC_Addr++,Dless_amount();            break;  //LD 32位小于等于比较
		case 0X01DC: PLC_Addr++,big_amount();              break;  //LD 16位大于等于比较
		case 0X01DD: PLC_Addr++,Dbig_amount();             break;  //LD 32位大于等于比较
		
		case 0X01E0: PLC_Addr++,amount_and();              break;  //LD AND 16位等于比较
		case 0X01E1: PLC_Addr++,Damount_and();             break;  //LD AND 32位等于比较
		case 0X01E2: PLC_Addr++,big_and();                 break;  //LD AND 16位大于比较
		case 0X01E3: PLC_Addr++,Dbig_and();                break;  //LD AND 32位大于比较
		case 0X01E4: PLC_Addr++,less_and();                break;  //LD AND 16位小于比较
		case 0X01E5: PLC_Addr++,Dless_and();               break;  //LD AND 32位小于比较
		case 0X01E8: PLC_Addr++,no_amount_and(); 	         break;  //LD 16位不等于比较指令
		case 0X01E9: PLC_Addr++,Dno_amount_and(); 	       break;  //LD 32位不等于比较指令
		case 0X01EA: PLC_Addr++,less_amount_and();         break;  //LD AND 16位小于等于比较
		case 0X01EB: PLC_Addr++,Dless_amount_and();        break;  //LD AND 32位小于等于比较
		case 0X01EC: PLC_Addr++,big_amount_and();          break;  //LD AND 16位大于等于比较
		case 0X01ED: PLC_Addr++,Dbig_amount_and();         break;  //LD AND 32位大于等于比较
		
		case 0X01F0: PLC_Addr++,amount_OR();               break;  //LD OR 16位等于比较
		case 0X01F1: PLC_Addr++,Damount_OR();              break;  //LD OR 32位等于比较
		case 0X01F2: PLC_Addr++,big_OR();                  break;  //LD OR 16位大于比较
		case 0X01F3: PLC_Addr++,Dbig_OR();                 break;  //LD OR 32位大于比较
		case 0X01F4: PLC_Addr++,less_OR();                 break;  //LD OR 16位小于比较
		case 0X01F5: PLC_Addr++,Dless_OR();                break;  //LD OR 32位小于比较
		case 0X01F8: PLC_Addr++,no_amount_OR(); 	         break;  //LD 16位不等于比较指令
		case 0X01F9: PLC_Addr++,Dno_amount_OR(); 	         break;  //LD 32位不等于比较指令
		case 0X01FA: PLC_Addr++,less_amount_OR();          break;  //LD OR 16位小于等于比较
		case 0X01FB: PLC_Addr++,Dless_amount_OR();         break;  //LD OR 32位小于等于比较
		case 0X01FC: PLC_Addr++,big_amount_OR();           break;  //LD OR 16位大于等于比较
		case 0X01FD: PLC_Addr++,Dbig_amount_OR();          break;  //LD OR 32位大于等于比较					
		
		case 0x000F: PLC_Addr=PLC_Addr;                    break;  //如果遇到END指令则使后面结束
		case 0XF7FF: PLC_Addr++,RET();                     break;  //RET
		

//		case ???: PLC_Addr++,WSFR();                 break;  //WSFR 字右移动
//		case ???: PLC_Addr++,WSFL();                 break;  //WSFL 字左移动
//		case ???: PLC_Addr++,SFWR();                 break;  //SFWR 移位写入
//		case ???: PLC_Addr++,SFRD();                 break;  //SFRD 移位读出

// 		case ???: PLC_Addr++,SUM();	                 break;  //SUM
// 		case ???: PLC_Addr++,BON();	                 break;  //BON

//	  case ???: PLC_Addr++,PRUN();	               break;  //PRUN
		
		default:PLC_PROG_ERROR(M8065,02); PLC_Addr++;      break;  //遇到不支持的命令
	}
}
 
void find_p(void)//查找 P 所在的地址
{  
	u16 temp;
	PLC_Addr=PLC_LAD_START_ADDR;
	for(temp=0;temp<15999;temp++)//总共16000步
	{ 
		if((*PLC_Addr&0xFF00)==0xB000)
		PLC_P_Addr[*PLC_Addr%0x100]=PLC_Addr;
		PLC_Addr++;
	}
}

void RST_Y(void)
{
	plc_16BitBuf[80]=0;
	plc_16BitBuf[81]=0;
}

u16 find_toend(void)//查找 P 所在的地址
{  
	u16 temp;
	PLC_Addr=PLC_LAD_START_ADDR-1;
	temp=0;	
	do{
	  PLC_Addr++; 
		temp++;
	}
	while((!(*PLC_Addr==0x000f))&&(temp<15998)); 	 
	return temp; 
}

static u8  puls,run_flag; 

// 新增，PLC在Stop状态下默认初始化
void PLC_StopDefaultInit(void)
{
		PLC_BIT_OFF(M8000);	                     // 没有运行强制M80000为OFF
		PLC_BIT_ON(M8001);	                     // 没有运行强制M80001为on
		D8012=0; 
		edit_prog=0;	                           // 编程时要用到
		puls=0; 		                             // 初始化脉冲用到8002 8003
//		STOP_LAMP_OFF;                           // 关闭运行灯,如果从运行状态切换到停止状态，需要清除Y输出
		gCommLedFlashFlg =0; // 灭
	  PLC_STL_CMD = 0;   		
		PLC_STL_Status = 0; 	                   // 上次程序中的步进	
	
		/* 脉冲输出 */
		/* 传人记，20170425新增 */
#if PWM_OUTPUT_FUNC
		PWM_Y0_Restore_Config();                  // 恢复IO口的设置
		PWM_Y1_Restore_Config();
		PulseOutputInit();                        // 初始化脉冲输出全局结构体 
#endif
		/* 脉冲输入 */
#if ENCODE_FUNC
		Cap_PulseInputInit();                     // 脉冲模式设置																		 
		X_Io_Restore();                           // 编码器IO硬件复位		
#endif

}

// 9优化
void PLC_ProInstructParse(void)
{ 
  if(PLC_RUN) // 拨动开关至RUN,运行程序
  {
		if(run_flag == 1)
		{
			 run_flag = 0;
			 PLC_RW_RAM_8BIT(0X01E0)=0x09;		 
		}
		
		if(PLC_RW_RAM_8BIT(0X01E0)==0x09)     // 是否需要运行程序
		{
				PLC_BIT_ON(M8000);	              // 运行强制M80000为ON
				PLC_BIT_OFF(M8001);	              // 运行强制M80001为off
//				RUN_LAMP_ON;                      // 亮起运行灯
			
			  if(gCommLedFlashFlg!=2)
					gCommLedFlashFlg =1; // 常亮
			
				if(edit_prog== 0)		            // 判断是否存在程序编辑，如果编辑一次程序后必需重新计算P所在的址
				{ 
						edit_prog=1;					
						find_p();
						if(find_toend()>15998)
						{  
							 PLC_RW_RAM_8BIT(0X01E0)=0x09;
goto all_end;  
						}
				}
				
				if(puls == 0x00)		               // 初始化脉冲用到8002 8003
				{
						PLC_BIT_ON(M8002);
						PLC_BIT_OFF(M8003);
				}
				
				PLC_Addr =PLC_LAD_START_ADDR;       // PLC到起始地址
				
				if(Write_Pro_flag == 0)            // 非梯形图写入
					 PLC_IO_Refresh();               // 刷新Y输出  
				
				do
			  {
					switch(*PLC_Addr/0x100)          // 取高8位的数据
					{ 
						case 0x06: operation_T(),PLC_Addr++;                    break;  //operation all timer
						case 0x0E: operation_C(),PLC_Addr++;                    break;  //
						
						/* 操作S位元件所有的函数 */
						case 0x20: LD(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x30: LDI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x40: AND(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x50: ANI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x60: OR(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x70: ORI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  // 
						/* 操作S位元件所有的函数 */
						case 0x21: LD(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x31: LDI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x41: AND(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x51: ANI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x61: OR(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x71: ORI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						/* 操作S位元件所有的函数 */
						case 0x22: LD(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x32: LDI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x42: AND(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x52: ANI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x62: OR(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x72: ORI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						/* 操作S位元件所有的函数 */
						case 0x23: LD(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x33: LDI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x43: AND(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x53: ANI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x63: OR(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x73: ORI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						/* 操作X位元件所有的函数 */
						case 0x24: LD(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x34: LDI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x44: AND(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x54: ANI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x64: OR(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x74: ORI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						/* 操作Y位元件所有的函数 */
						case 0x25: LD(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x35: LDI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x45: AND(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x55: ANI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x65: OR(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x75: ORI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0XC5: OUT(0X0FFF&*PLC_Addr),PLC_Addr++;			      break;  //
						case 0XD5: BIT_SET(0X0FFF&*PLC_Addr),PLC_Addr++;	      break;  //
						case 0XE5: RST(0X0FFF&*PLC_Addr),PLC_Addr++;			      break;  //
						/* 操作T位元件所有的函数 */
						case 0x26: LD(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x36: LDI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x46: AND(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x56: ANI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x66: OR(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x76: ORI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0XC6: OUT(0X0FFF&*PLC_Addr),PLC_Addr++;			      break;  //
						/* 操作T位元件所有的函数 */
						case 0x27: LD(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x37: LDI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x47: AND(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x57: ANI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x67: OR(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x77: ORI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0XC7: OUT(0X0FFF&*PLC_Addr),PLC_Addr++;			      break;  //
						/* 操作M0_255位元件所有的函数 */
						case 0x28: LD(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x38: LDI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x48: AND(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x58: ANI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x68: OR(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x78: ORI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0XC8: OUT(0X0FFF&*PLC_Addr),PLC_Addr++;			      break;  //
						case 0XD8: BIT_SET(0X0FFF&*PLC_Addr),PLC_Addr++;	      break;  //
						case 0XE8: RST(0X0FFF&*PLC_Addr),PLC_Addr++;			      break;  //	
						/* 操作M256_511位元件所有的函数 */
						case 0x29: LD(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x39: LDI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x49: AND(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x59: ANI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x69: OR(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x79: ORI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0XC9: OUT(0X0FFF&*PLC_Addr),PLC_Addr++;			      break;  //
						case 0XD9: BIT_SET(0X0FFF&*PLC_Addr),PLC_Addr++;	      break;  //
						case 0XE9: RST(0X0FFF&*PLC_Addr),PLC_Addr++;			      break;  //
						/* 操作M512_767位元件所有的函数 */
						case 0x2A: LD(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x3A: LDI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x4A: AND(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x5A: ANI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x6A: OR(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x7A: ORI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0XCA: OUT(0X0FFF&*PLC_Addr),PLC_Addr++;			      break;  //
						case 0XDA: BIT_SET(0X0FFF&*PLC_Addr),PLC_Addr++;	      break;  //
						case 0XEA: RST(0X0FFF&*PLC_Addr),PLC_Addr++;			      break;  //
						/* 操作M768_1023位元件所有的函数 */
						case 0x2B: LD(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x3B: LDI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x4B: AND(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x5B: ANI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x6B: OR(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x7B: ORI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0XCB: OUT(0X0FFF&*PLC_Addr),PLC_Addr++;			      break;  //
						case 0XDB: BIT_SET(0X0FFF&*PLC_Addr),PLC_Addr++;	      break;  //
						case 0XEB: RST(0X0FFF&*PLC_Addr),PLC_Addr++;			      break;  //
						/* 操作M1024_1279位元件所有的函数 */
						case 0x2C: LD(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x3C: LDI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x4C: AND(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x5C: ANI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x6C: OR(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x7C: ORI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0XCC: OUT(0X0FFF&*PLC_Addr),PLC_Addr++;			      break;  //
						case 0XDC: BIT_SET(0X0FFF&*PLC_Addr),PLC_Addr++;	      break;  //
						case 0XEC: RST(0X0FFF&*PLC_Addr),PLC_Addr++;			      break;  //
						/* 操作M1280_1535位元件所有的函数 */
						case 0x2D: LD(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x3D: LDI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x4D: AND(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x5D: ANI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x6D: OR(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x7D: ORI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0XCD: OUT(0X0FFF&*PLC_Addr),PLC_Addr++;			      break;  //
						case 0XDD: BIT_SET(0X0FFF&*PLC_Addr),PLC_Addr++;	      break;  //
						case 0XED: RST(0X0FFF&*PLC_Addr),PLC_Addr++;			      break;  //
						/* 操作C0-C255位元件所有的函数 */
						case 0x2E: LD(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x3E: LDI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x4E: AND(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x5E: ANI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x6E: OR(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x7E: ORI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						/*m8000-m8255*/
						case 0x2F: LD(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x3F: LDI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0x4F: AND(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x5F: ANI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;
						case 0x6F: OR(0X0FFF&*PLC_Addr),PLC_Addr++;             break;  //
						case 0x7F: ORI(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0XCF: OUT(0X0FFF&*PLC_Addr),PLC_Addr++;			      break;  //
						case 0XDF: BIT_SET(0X0FFF&*PLC_Addr),PLC_Addr++;	      break;  //
						case 0XEF: RST(0X0FFF&*PLC_Addr),PLC_Addr++;			      break;  //
						/**********************STL步进模式***************************/
						case 0xF0: STL(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //S
						case 0xF1: STL(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0xF2: STL(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						case 0xF3: STL(0X0FFF&*PLC_Addr),PLC_Addr++;            break;  //
						
						////////////////////////////////////////////////////////
						//基本逻辑指令二，如ANB、ORB、MPP、MRD、MPS、INV 等
						case 0XFF: 
						{
							other_function(*PLC_Addr);
							PLC_Addr++;	      
							break;   //MPP,MPS
						}
						case 0xB0: //指针P标识
						{
							PLC_Addr++;                                  
							break;  
						}
						case 0x00: //遇到0X001C为FEND,0X000F为END指令   
						{
							if(((*PLC_Addr%0x100)==0x1C)||((*PLC_Addr%0x100)==0x0F))
goto all_end;
						}
						
						//////////////////////////////////////////////////////////////
						// 9注释，应用指令				
						//////////////////////////////////////////////////////////////					
						default: //遇到不支持的命令 ,此处需要执行命令为16bit的指令
						{
							FNC_AppInstruct();                           
							break; 
						}
					}
				}while(1);
				
all_end: 
					D8010=D8011=D8012=plc_scan_time;       // 保持扫描时间
					plc_scan_time=0;                       // 清除扫描时间	
					puls=0x01;		 
					PLC_BIT_OFF(M8002),PLC_BIT_ON(M8003);  // 初始化脉冲用到8002 8003
				  
#if DEBUG
#warning "年03月20日新增，MC和MCR指令";
#endif
					MC.PLC_MC_BIT = 0;    
					MC.MC_SFR = 0;				  
						 
		 } 
		else  // 软停
		{ 
	      PLC_StopDefaultInit();
			
				if(Write_Pro_flag == 0)
				{
					 RST_Y(); 
					 PLC_IO_Refresh();                     // 刷新Y输出  
					 RST_T_D_C_M_data();
				}  
				
		}
	}
	else	// 拨动开关至STOP	 
	{    
      PLC_StopDefaultInit();
		
			Write_Pro_flag = 0;
			RST_Y();   												 // 如果从运行状态切换到停止状态，需要清除Y输出 
			PLC_IO_Refresh();
		
			if(run_flag == 0)
			{
					run_flag = 1;
					plc_scan_time=0;                      
					RST_T_D_C_M_data();
			}			
	}
	plc_16BitBuf[0X701]=0X000;		          // 设置版本号
} 

