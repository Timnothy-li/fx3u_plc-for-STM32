/********************************************************/
// CPU需要：STM32F103--RAM内存不小于64K	Flash内存不小于128K
// 本代码已在STM32F103RCT6 RDT6 VCT6 VET6测试通过
// 编辑日期：2017年5月10日
// editor by 传人记

// Y0  PC9 TIM8_CH4
// Y1  PA11 TIM1_CH4
/********************************************************/

#include "pwm_output.h"
#include <string.h>

#if PWM_OUTPUT_FUNC

pulse_Pro gPulse_Pro[2];//全局处理结构体

//全局结构初始化
void PulseOutputInit(void)
{
   memset(gPulse_Pro,0,2*sizeof(pulse_Pro));
}

// 传人记，20170510新增,Y0 PC9 TIM8_CH4
void TIM8_NVIC_Configuration(void)
{
    NVIC_InitTypeDef NVIC_InitStructure; 
    
  //  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);  													
    NVIC_InitStructure.NVIC_IRQChannel = TIM8_UP_IRQn;	  
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 15;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;	
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

// 传人记，20170510新增,Y1 PA11 TIM1_CH4 => PA8 TIM1_CH1 
void TIM1_NVIC_Configuration(void)
{
    NVIC_InitTypeDef NVIC_InitStructure; 
    
  //  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);  													
    NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_IRQn;	  
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 15;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;	
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

// 传人记，20170510新增,Y0 PC9 TIM8_CH4
void PWM_Y0_Set_Config(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

	/* TIM8 clock enable */
	//PCLK1经过2倍频后作为TIM8的时钟源等于72MHz
  // RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE); 

  /* GPIOC clock enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE); 

  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_9 ;	 //TIM8_CH4
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; // 复用推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOC, &GPIO_InitStructure);
	
}

// 传人记，20170510新增,Y1 PA11 TIM1_CH4 => PA8 TIM1_CH1 
 void PWM_Y1_Set_Config(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

	//PCLK1经过2倍频后作为TIM1的时钟源等于72MHz
  // RCC_APB2PeriphClockCmd(RCC_APB1Periph_TIM1, ENABLE); 

  /* GPIOA clock enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE); 

  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_11 ;	// PA11 TIM1_CH4 => PA8 TIM1_CH1 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; // 复用推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
	
}

// 传人记，20170510新增,Y0 PC9 TIM8_CH4
void PWM_Y0_Restore_Config(void)
{
	  GPIO_InitTypeDef GPIO_InitStructure;
	  TIM_OC4PreloadConfig(TIM8, TIM_OCPreload_Disable); // CH4 PWM失能
	  
	  RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOC,ENABLE);
  	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;	
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;       
  	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_Init(GPIOC, &GPIO_InitStructure);
//	  GPIO_SetBits(GPIOC,GPIO_Pin_9);	// turn off all led
	  GPIO_ResetBits(GPIOC,GPIO_Pin_9);
}

// 传人记，20170510新增,Y1 PA11 TIM1_CH4 => PA8 TIM1_CH1 
void PWM_Y1_Restore_Config(void)
{
	  GPIO_InitTypeDef GPIO_InitStructure;
//	  TIM_OC4PreloadConfig(TIM1, TIM_OCPreload_Disable); // CH4 PWM失能
	  TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Disable); // Y1 PA11 TIM1_CH4 => PA8 TIM1_CH1 
	
	  RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA,ENABLE);
  	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;	// PA11 TIM1_CH4 => PA8 TIM1_CH1 
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;       
  	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_Init(GPIOA, &GPIO_InitStructure);
//	  GPIO_SetBits(GPIOA,GPIO_Pin_11);	 // turn off all led
	  GPIO_ResetBits(GPIOA,GPIO_Pin_11); 	// PA11 TIM1_CH4 => PA8 TIM1_CH1 
}


/*   传人记，20171229修改,Y0 PC9 TIM8_CH4
 * 函数名：接口值需要进行修改一下即可
 * 描述  ：配置TIM3输出的PWM信值号的模式，如周期、极性、占空比
 */
void Y0_Tim_Mode_Config(u16 Prescaler,u16 Peroid,u16 CCRx_Val ,Pluse_Mode PtoMode)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	TIM_BDTRInitTypeDef TIM_BDTRInitStructure; // 传人记，20180102修改 
	
	TIM_DeInit(TIM8);
  //PCLK1经过2倍频后作为TIM8的时钟源等于72MHz
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE); 
  /* Time base configuration */		 
  TIM_TimeBaseStructure.TIM_Period        =    Peroid;              // ARR，当定时器从0计数到999，即为1000次，为一个定时周期
  TIM_TimeBaseStructure.TIM_Prescaler     =    Prescaler;	          // PSC，设置预分频：不预分频，即为72MHz
  TIM_TimeBaseStructure.TIM_ClockDivision =    TIM_CKD_DIV1 ;	      // 设置时钟分频系数：不分频
  TIM_TimeBaseStructure.TIM_CounterMode   =    TIM_CounterMode_Up;  // 向上计数模式
	TIM_TimeBaseStructure.TIM_RepetitionCounter =0; // 传人记，20180102修改	
  TIM_TimeBaseInit(TIM8, &TIM_TimeBaseStructure);

  /* Channel 4 Configuration in PWM mode */
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;	             // 配置为PWM模式1
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;      // 当定时器计数值小于CCR1_Val时为高电平    
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;	 // TIM 输出使能
  TIM_OCInitStructure.TIM_Pulse = CCRx_Val;	                     // CCR，设置占空比。反转模式时无效  
	
	
	// 下面几个参数是高级定时器才会用到，通用定时器不用配置
	TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_Low;      // 输出相同,TIM_OCNPolarity_High时反相,传人记，20180102修改 
	TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable; // TIM_OutputNState_Disable 传人记，20180102修改	
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;      // 传人记，20180102修改 
	TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCIdleState_Reset;   // 传人记，20180102修改 
	
  TIM_OC4Init(TIM8, &TIM_OCInitStructure);	                     // 使能通道4	 TIM_OC4Init
  TIM_OC4PreloadConfig(TIM8, TIM_OCPreload_Enable);	             // 启用CCR1寄存器的影子寄存器（直到产生更新事件才更改设置）
													                                       // 使能TIMx在CCR1上的预装载寄存
	/* 传人记，20180102修改 */  
	// 死区和刹车功能配置，高级定时器才有的，通过定时器不用配置
	TIM_BDTRInitStructure.TIM_OSSRState = TIM_OSSRState_Disable;     
	TIM_BDTRInitStructure.TIM_OSSIState = TIM_OSSIState_Disable;     
	TIM_BDTRInitStructure.TIM_LOCKLevel = TIM_LOCKLevel_OFF;     
	TIM_BDTRInitStructure.TIM_DeadTime = 0x90;  // 
	TIM_BDTRInitStructure.TIM_Break = TIM_Break_Disable;      
	TIM_BDTRInitStructure.TIM_BreakPolarity = TIM_BreakPolarity_High;      
	TIM_BDTRInitStructure.TIM_AutomaticOutput = TIM_AutomaticOutput_Enable;     
	TIM_BDTRConfig(TIM8, &TIM_BDTRInitStructure); 																															 
																																 
  //TIM_ITConfig(TIM8,TIM_IT_Update,ENABLE);
	if((PtoMode == ePTY_MODE)||(PtoMode == ePTR_MODE))               // PTO模式需要使用中断,如果单纯的PWM模式不需要
	{
		TIM_ITConfig(TIM8,TIM_IT_Update,ENABLE);
    TIM8_NVIC_Configuration();
  }
	
	TIM_ARRPreloadConfig(TIM8, ENABLE);		                         // 使能TIM8重载寄存器ARR
  /* TIM8 enable counter */
  TIM_Cmd(TIM8, ENABLE);                                         // 使能定时器8
	TIM_CtrlPWMOutputs(TIM8, ENABLE);  
}

/*   传人记，20170510新增,Y1 PA11 TIM1_CH4  => PA8 TIM1_CH1 
 *   建立一个数组来进行设置  分频和计数个数

 * 函数名：接口值需要进行修改一下即可
 * 描述  ：配置TIM3输出的PWM信值号的模式，如周期、极性、占空比
 * 输入  ：无
 * 输出  ：无
 * 调用  ：内部调用
 */
void Y1_Tim_Mode_Config(u16 Prescaler,u16 Peroid,u16 CCRx_Val,Pluse_Mode PtoMode)
{
		TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
		TIM_OCInitTypeDef        TIM_OCInitStructure;
	  TIM_BDTRInitTypeDef TIM_BDTRInitStructure; // 传人记，20180102修改 

		TIM_DeInit(TIM1);
	  //PCLK1经过2倍频后作为TIM1的时钟源等于72MHz
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1,ENABLE);

		TIM_TimeBaseStructure.TIM_Period        =    Peroid;             // ARR，当定时器从0计数到999，即为1000次，为一个定时周期
		TIM_TimeBaseStructure.TIM_Prescaler     =    Prescaler;	         // PSC，设置预分频：不预分频，即为72MHz
		TIM_TimeBaseStructure.TIM_ClockDivision =    TIM_CKD_DIV1 ;	     // 设置时钟分频系数：不分频
		TIM_TimeBaseStructure.TIM_CounterMode   =    TIM_CounterMode_Up; // 向上计数模式
	 
   	TIM_TimeBaseStructure.TIM_RepetitionCounter =0; // 传人记，20180102修改	
		TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);	

	  // 启动ARR的影子寄存器（直到产生更新事件才更改设置）
	  TIM_ARRPreloadConfig(TIM1, ENABLE);		                         	 // 使能TIM1重载寄存器ARR
	
	  // CRJ,20171220修改，Y1 PA11 TIM1_CH4  => PA8 TIM1_CH1 
		/* pwm输出配置 */ 
		TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;    
		TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; 
		TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;  
		TIM_OCInitStructure.TIM_Pulse = CCRx_Val; // CCR，设置占空比。反转模式时无效  
	
    // 下面几个参数是高级定时器才会用到，通用定时器不用配置
		TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_Low;     // 输出相同,TIM_OCNPolarity_High时反相,传人记，20180102修改 
		TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable; // TIM_OutputNState_Disable 传人记，20180102修改	
    TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;      // 传人记，20180102修改 
    TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCIdleState_Reset;   // 传人记，20180102修改 
	
// CRJ,20171220备注：Channel 4	
		TIM_OC4Init(TIM1,&TIM_OCInitStructure);                        // 使能通道4	 TIM_OC4Init
		TIM_OC4PreloadConfig(TIM1, TIM_OCPreload_Enable);	             // 启用CCR1寄存器的影子寄存器（直到产生更新事件才更改设置）
// CRJ,20171220备注：Channel 1
		//TIM_OC1Init(TIM1,&TIM_OCInitStructure);                          // 使能通道1	 TIM_OC1Init
		//TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);	               // 启用CCR1寄存器的影子寄存器（直到产生更新事件才更改设置）

    /* 传人记，20180102修改 */  
    // 死区和刹车功能配置，高级定时器才有的，通过定时器不用配置
		TIM_BDTRInitStructure.TIM_OSSRState = TIM_OSSRState_Disable;     
		TIM_BDTRInitStructure.TIM_OSSIState = TIM_OSSIState_Disable;     
		TIM_BDTRInitStructure.TIM_LOCKLevel = TIM_LOCKLevel_OFF;     
		TIM_BDTRInitStructure.TIM_DeadTime = 0x90;  // 
		TIM_BDTRInitStructure.TIM_Break = TIM_Break_Disable;      
		TIM_BDTRInitStructure.TIM_BreakPolarity = TIM_BreakPolarity_High;      
		TIM_BDTRInitStructure.TIM_AutomaticOutput = TIM_AutomaticOutput_Enable;     
		TIM_BDTRConfig(TIM1, &TIM_BDTRInitStructure); 
		 

		/* TIM1 Main Output Enable */
//    TIM_CtrlPWMOutputs(TIM1,ENABLE);	
		if((PtoMode==ePTY_MODE)||(PtoMode==ePTR_MODE))                   // PTO模式需要使用中断，如果单纯的PWM模式不需要
		{
				TIM_ITConfig(TIM1,TIM_IT_Update,ENABLE);
				TIM1_NVIC_Configuration();
		}
//		TIM_ARRPreloadConfig(TIM1, ENABLE);		                         	 // 使能TIM1重载寄存器ARR
		/* TIM1 enable counter */
		TIM_Cmd(TIM1, ENABLE);                                             // 使能定时器1
		TIM_CtrlPWMOutputs(TIM1, ENABLE);                                  // TIM1_OC通道输出PWM(一定要加)
}

// 传人记，20170510新增,Y0 PC9 TIM8_CH4
void Y0_Tim_Mode_Config_DIASBLE(void)
{
	  TIM_ITConfig(TIM8,TIM_IT_Update,DISABLE);
   	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, DISABLE); 
    TIM_Cmd(TIM8, DISABLE); // 使能定时器8
	  TIM_CtrlPWMOutputs(TIM8, DISABLE); //使能定时器8	
}

// 传人记，20170510新增,Y1 PA11 TIM1_CH4  => PA8 TIM1_CH1 
void Y1_Tim_Mode_Config_DIASBLE(void)
{
	  TIM_ITConfig(TIM1,TIM_IT_Update,DISABLE);
   	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, DISABLE); 
    TIM_Cmd(TIM1, DISABLE); 
    TIM_CtrlPWMOutputs(TIM1, DISABLE); //使能定时器1	
}

// 传人记，20170510新增,定时器8中断服务函数
/*
typedef struct _pulse_Pro{
	 u32 used;
   u16 Gpio_pin; //输出IO口
   u16 Gpio_pin2; //FOR DIRV
   Pluse_Mode mode; //PWM 还是  PTO 
   u32 CurPto;     //当前已发脉冲个数  //  只对  PTO 有效
	 u32 TargetPto;  //目标脉冲个数      //  只对  PWM 有效
	 u32 Place;      //启动位置  这个是关键
	 u32 PWM_Width;  //only for pwm 脉冲宽度
	 u32 PWM_Cycle;  //only for pwm  脉冲最大频率
	 u32 Ptr_Raise_time; //上升时间
	 u32 Interploter;  //  根据 脉冲频率算出来的 插补周期 
	 u32 Stage;       //处于哪个阶段  是  加速段  匀速段  减速段   三角加减速 没有 匀速阶段
	 u32 StageCurPto; //每个阶段的当前脉冲数据
	 u32 StageTartag; //一个阶段目标脉冲数目
	 u32 StageCurFre; //当前阶段的脉冲频??
	 u32 IncrementPl;//acc/dec 每次增加的脉冲个数
	 u32 IncrementTimes;//acc/dec	 次数
				
   u32 PLSRMODE;//  是梯形加减速   还是三角加减速
	 u32 PLSR_MaxFreq;// PLSR脉冲输出最大频率
}pulse_Pro;			*/

void TIM8_UP_IRQHandler(void)
{
	static u8 i=0,j=0,k=0;
	pulse_Pro *pPulse_Pro;//全局处理结构体
	pPulse_Pro=&gPulse_Pro[0];

	if ( TIM_GetITStatus(TIM8 , TIM_IT_Update) != RESET ) 
	{	
		 if((pPulse_Pro->mode == ePTY_MODE) && (pPulse_Pro->used ==1))
		 {
				 pPulse_Pro->CurPto++;
				 if(pPulse_Pro->Gpio_pin2!=0)
				 {
           PLC_BIT_ON(M8147);
         }
				 D8140 = pPulse_Pro->CurPto&0xffff;
				 D8141= pPulse_Pro->CurPto>>16;
         if((pPulse_Pro->CurPto >= pPulse_Pro->TargetPto) && (pPulse_Pro->TargetPto !=0)) // 计数到设定值,传人记，20180419修复脉冲数为0时一直输出
				 {
						pPulse_Pro->used=3; // 标记计数完成
						D8140= pPulse_Pro->CurPto&0xffff;
						D8141= pPulse_Pro->CurPto>>16;
					 
						if(pPulse_Pro->Gpio_pin2!=0)
						{
							 PLC_BIT_OFF(M8147);
							 PLC_BIT_ON(M8145);
						}	
						PLC_BIT_ON(M8029);
						pPulse_Pro->Gpio_pin2=0; // FOR DIRV						
						Y0_Tim_Mode_Config_DIASBLE();					
						PWM_Y0_Restore_Config();//恢复 IO的设置  
						
					  PLC_BIT_OFF(pPulse_Pro->Place); // 关Y输出
         }
      }
		  else if((pPulse_Pro->mode==ePTR_MODE)&& (pPulse_Pro->used ==1)) // PWM 还是  PTO 
			{ // PLSR暂未处理
					pPulse_Pro->CurPto++; // 当前已发脉冲个数,只对PTO有效
					pPulse_Pro->StageCurPto++; // 目标脉冲个数 ,只对PWM 有效

					if((pPulse_Pro->CurPto >= pPulse_Pro->TargetPto) && (pPulse_Pro->TargetPto !=0)) // 计数到设定值,传人记，20180419修复脉冲数为0时一直输出
					{
							pPulse_Pro->used =0; // 标记计数完成
							D8140= pPulse_Pro->CurPto&0xffff;
							D8141= pPulse_Pro->CurPto>>16;
							if(pPulse_Pro->Gpio_pin2!=0)
							{
								 PLC_BIT_OFF(M8147);
								 PLC_BIT_ON(M8145);
							}	
							PLC_BIT_ON(M8029);
							pPulse_Pro->Gpio_pin2=0;
							memset(pPulse_Pro,0,sizeof(pulse_Pro));//全部清结构体数据
							Y0_Tim_Mode_Config_DIASBLE();
							PWM_Y0_Restore_Config();//恢复 IO的设置  							 
							PLC_BIT_OFF(pPulse_Pro->Place); // 关Y输出 							 
							i=j=k=0;
							goto ll0;							 
					}
				 
					if(pPulse_Pro->PLSRMODE==eTRIOD_MODE)//梯形速度处理
					{ //每次状态切换好  都要清楚TARGET值  保证每个状态只会切换一次
							if((i==0)&&(pPulse_Pro->CurPto>=pPulse_Pro->AccPulseCount)&&( pPulse_Pro->Stage!=ePLAIN_STAGE))//做状态的切换
							{
									pPulse_Pro->Stage=ePLAIN_STAGE; 
									pPulse_Pro->StageTartag=0; // 强制进入 350 行程序
									i=1;
							}
							else if((j==0)&&(pPulse_Pro->Stage!=eDEC_STAGE)&&(pPulse_Pro->CurPto>=(pPulse_Pro->AccPulseCount+pPulse_Pro->PlainPulseCount)))
							{
								 pPulse_Pro->Stage=eDEC_STAGE;
								 pPulse_Pro->StageTartag=0;
								 j=1;
							}
					}
					else if(pPulse_Pro->PLSRMODE==eTRI_MODE)
					{
							if((k==0)&&(pPulse_Pro->Stage!=eDEC_STAGE)&&(pPulse_Pro->CurPto>=pPulse_Pro->AccPulseCount))//做状态的切换
							{
								 pPulse_Pro->Stage=eDEC_STAGE;
								 pPulse_Pro->StageTartag=0;
								 k=1;
							}
					}			 
				 
					if(pPulse_Pro->StageCurPto>=pPulse_Pro->StageTartag)//还在阶段里面呢 
					{
             //if(pPulse_Pro->PLSRMODE==eTRIOD_MODE)//梯形速度处理
             {
                if(pPulse_Pro->Stage==eACC_STAGE)//再这边做状态切换
								{
									pPulse_Pro->StageCurFre+=pPulse_Pro->IncrementPl;//阶段一  加速段  阶段的脉冲个数
										if(pPulse_Pro->StageCurFre>=pPulse_Pro->PLSR_MaxFreq)
										{
											pPulse_Pro->StageCurFre=pPulse_Pro->PLSR_MaxFreq;
											if(pPulse_Pro->PLSRMODE==eTRIOD_MODE)
											{
												 pPulse_Pro->Stage=ePLAIN_STAGE;											
											}
											else if(pPulse_Pro->PLSRMODE==eTRI_MODE)
											{
													pPulse_Pro->Stage=eDEC_STAGE;
											}
										}
										pPulse_Pro->StageTartag=(u32)(pPulse_Pro->StageCurFre*(pPulse_Pro->Interploter/1000.0));
										pPulse_Pro->StageTartag=(pPulse_Pro->StageTartag==0)?1:pPulse_Pro->StageTartag;
										PLSR_Plc_Pro(pPulse_Pro,DISABLE);//还不知道能不能启动起来呢 
										PLSR_Plc_Pro(pPulse_Pro,ENABLE);
                  
                }
								else if(pPulse_Pro->Stage==ePLAIN_STAGE)
								{
//ll0: 
                 pPulse_Pro->StageCurFre=pPulse_Pro->PlainCurFreq;
								 pPulse_Pro->StageTartag=pPulse_Pro->PlainPulseCount;//阶段的脉冲个数
								 PLSR_Plc_Pro(pPulse_Pro,DISABLE);
					       PLSR_Plc_Pro(pPulse_Pro,ENABLE);		
									
                }
								else if(pPulse_Pro->Stage==eDEC_STAGE)
								{
									if(pPulse_Pro->StageCurFre>pPulse_Pro->IncrementPl)//如果比较小的话 那么就按那个小的来吧 发完自动结束就可以
									{
								  	pPulse_Pro->StageCurFre-=pPulse_Pro->IncrementPl;//阶段一  加速段  阶段的脉冲个数
                  }
									
									pPulse_Pro->StageTartag=(u32)(pPulse_Pro->StageCurFre*(pPulse_Pro->Interploter/1000.0));
									pPulse_Pro->StageTartag=(pPulse_Pro->StageTartag==0)?1:pPulse_Pro->StageTartag;
									PLSR_Plc_Pro(pPulse_Pro,DISABLE);
					        PLSR_Plc_Pro(pPulse_Pro,ENABLE);
                }
             }	
						 pPulse_Pro->StageCurPto=0;						 
          }		
				 if(pPulse_Pro->Gpio_pin2!=0)
				 {
           PLC_BIT_ON(M8147);
         }
			}
	   	TIM_ClearITPendingBit(TIM8 ,TIM_IT_COM|TIM_IT_Break|TIM_IT_Trigger|TIM_IT_CC4|TIM_IT_CC3|TIM_IT_CC2|TIM_IT_CC1|TIM_FLAG_Update);    
ll0:
			return;
	}		 	
}

// 传人记，20170510新增,定时器1中断服务函数
void TIM1_UP_IRQHandler(void)
{
		static u8 i=0,j=0,k=0;
		pulse_Pro *pPulse_Pro;//全局处理结构体
		pPulse_Pro=&gPulse_Pro[1];

		if ( TIM_GetITStatus(TIM1 , TIM_IT_Update) != RESET ) 
		{	
			if((pPulse_Pro->mode==ePTY_MODE)&& (pPulse_Pro->used ==1))
			{
				 pPulse_Pro->CurPto++;
				 if(pPulse_Pro->Gpio_pin2!=0)
				 {
					 PLC_BIT_ON(M8148);
				 }
				 D8142= pPulse_Pro->CurPto&0xffff;
				 D8143= pPulse_Pro->CurPto>>16;
				 if((pPulse_Pro->CurPto >= pPulse_Pro->TargetPto) && (pPulse_Pro->TargetPto !=0)) // 计数到设定值,传人记，20180419修复脉冲数为0时一直输出
				 {
						pPulse_Pro->used =3; // 标记计数完成
						D8142= pPulse_Pro->CurPto&0xffff;
						D8143= pPulse_Pro->CurPto>>16;
					 
						if(pPulse_Pro->Gpio_pin2!=0)
						{
							 PLC_BIT_OFF(M8148);
							 PLC_BIT_ON(M8146);
						}					 
						PLC_BIT_ON(M8029);
						pPulse_Pro->Gpio_pin2=0;
						Y1_Tim_Mode_Config_DIASBLE();
						PWM_Y1_Restore_Config();//恢复 IO的设置    
						
					  PLC_BIT_OFF(pPulse_Pro->Place); // 关Y输出
         }
      }
		  else if((pPulse_Pro->mode==ePTR_MODE)&& (pPulse_Pro->used ==1)) // PWM 还是  PTO 
			{ // PLSR暂未处理
					pPulse_Pro->CurPto++; // 当前已发脉冲个数,只对PTO有效
					pPulse_Pro->StageCurPto++; // 目标脉冲个数 ,只对PWM 有效

					if((pPulse_Pro->CurPto>=pPulse_Pro->TargetPto) && (pPulse_Pro->TargetPto !=0)) // 计数到设定值,传人记，20180419修复脉冲数为0时一直输出
					{
							pPulse_Pro->used=0;
							D8140= pPulse_Pro->CurPto&0xffff;
							D8141= pPulse_Pro->CurPto>>16;
							if(pPulse_Pro->Gpio_pin2!=0)
							{
								 PLC_BIT_OFF(M8148);
								 PLC_BIT_ON(M8146);
						  }	
							PLC_BIT_ON(M8029);
							pPulse_Pro->Gpio_pin2=0;
							memset(pPulse_Pro,0,sizeof(pulse_Pro));//全部清结构体数据
							Y0_Tim_Mode_Config_DIASBLE();
							PWM_Y1_Restore_Config();//恢复 IO的设置 
							 
							PLC_BIT_OFF(pPulse_Pro->Place); // 关Y输出 
							
							i=j=k=0;
							goto ll0;							 
					}
				 
					if(pPulse_Pro->PLSRMODE==eTRIOD_MODE)//梯形速度处理
					{ //每次状态切换好  都要清楚TARGET值  保证每个状态只会切换一次
							if((i==0)&&(pPulse_Pro->CurPto>=pPulse_Pro->AccPulseCount)&&( pPulse_Pro->Stage!=ePLAIN_STAGE))//做状态的切换
							{
									pPulse_Pro->Stage=ePLAIN_STAGE; 
									pPulse_Pro->StageTartag=0; // 强制进入 350 行程序
									i=1;
							}
							else if((j==0)&&(pPulse_Pro->Stage!=eDEC_STAGE)&&(pPulse_Pro->CurPto>=(pPulse_Pro->AccPulseCount+pPulse_Pro->PlainPulseCount)))
							{
								 pPulse_Pro->Stage=eDEC_STAGE;
								 pPulse_Pro->StageTartag=0;
								 j=1;
							}
					}
					else if(pPulse_Pro->PLSRMODE==eTRI_MODE)
					{
							if((k==0)&&(pPulse_Pro->Stage!=eDEC_STAGE)&&(pPulse_Pro->CurPto>=pPulse_Pro->AccPulseCount))//做状态的切换
							{
								 pPulse_Pro->Stage=eDEC_STAGE;
								 pPulse_Pro->StageTartag=0;
								 k=1;
							}
					}	
					if(pPulse_Pro->StageCurPto>=pPulse_Pro->StageTartag)//还在阶段里面呢 
					{
             //if(pPulse_Pro->PLSRMODE==eTRIOD_MODE)//梯形速度处理
             {
                if(pPulse_Pro->Stage==eACC_STAGE)//再这边做状态切换
								{
									pPulse_Pro->StageCurFre+=pPulse_Pro->IncrementPl;//阶段一  加速段  阶段的脉冲个数
										if(pPulse_Pro->StageCurFre>=pPulse_Pro->PLSR_MaxFreq)
										{
											pPulse_Pro->StageCurFre=pPulse_Pro->PLSR_MaxFreq;
											if(pPulse_Pro->PLSRMODE==eTRIOD_MODE)
											{
												 pPulse_Pro->Stage=ePLAIN_STAGE;											
											}
											else if(pPulse_Pro->PLSRMODE==eTRI_MODE)
											{
													pPulse_Pro->Stage=eDEC_STAGE;
											}
										}
										pPulse_Pro->StageTartag=(u32)(pPulse_Pro->StageCurFre*(pPulse_Pro->Interploter/1000.0));
										pPulse_Pro->StageTartag=(pPulse_Pro->StageTartag==0)?1:pPulse_Pro->StageTartag;
										PLSR_Plc_Pro(pPulse_Pro,DISABLE);//还不知道能不能启动起来呢 
										PLSR_Plc_Pro(pPulse_Pro,ENABLE);
                  
                }
								else if(pPulse_Pro->Stage==ePLAIN_STAGE)
								{
//ll0: 
                 pPulse_Pro->StageCurFre=pPulse_Pro->PlainCurFreq;
								 pPulse_Pro->StageTartag=pPulse_Pro->PlainPulseCount;//阶段的脉冲个数
								 PLSR_Plc_Pro(pPulse_Pro,DISABLE);
					       PLSR_Plc_Pro(pPulse_Pro,ENABLE);		
									
                }
								else if(pPulse_Pro->Stage==eDEC_STAGE)
								{
									if(pPulse_Pro->StageCurFre>pPulse_Pro->IncrementPl)//如果比较小的话 那么就按那个小的来吧 发完自动结束就可以
									{
								  	pPulse_Pro->StageCurFre-=pPulse_Pro->IncrementPl;//阶段一  加速段  阶段的脉冲个数
                  }
									
									pPulse_Pro->StageTartag=(u32)(pPulse_Pro->StageCurFre*(pPulse_Pro->Interploter/1000.0));
										pPulse_Pro->StageTartag=(pPulse_Pro->StageTartag==0)?1:pPulse_Pro->StageTartag;
									PLSR_Plc_Pro(pPulse_Pro,DISABLE);
					        PLSR_Plc_Pro(pPulse_Pro,ENABLE);
                }
             }	
						 pPulse_Pro->StageCurPto=0;						 
          }		
				 if(pPulse_Pro->Gpio_pin2!=0)
				 {
						PLC_BIT_ON(M8148);
				 }
			}	   
			TIM_ClearITPendingBit(TIM1 ,TIM_IT_COM|TIM_IT_Break|TIM_IT_Trigger|TIM_IT_CC4|TIM_IT_CC3|TIM_IT_CC2|TIM_IT_CC1|TIM_FLAG_Update);    
			// time++;
ll0:
			return;
		}	
}

/*************************对外接口函数*************************/

/*  传人记，20171229修改
 *  Y0和Y1的PWM
 */
void PWM_Plc_Pro(pulse_Pro *pwm,FunctionalState NewState)
{
	if(NewState==ENABLE)//ENABLE
	{
		u16 Prescale, Peroid, CCRx_Val;
		
		Prescale =  35999;			
		Peroid =  (uint32_t)pwm->PWM_Cycle * 2 -1;
		CCRx_Val =  ((uint32_t)pwm->PWM_Cycle * 2) - ((uint32_t)pwm->PWM_Width * 2);
		
		if(pwm == &gPulse_Pro[0])
		{
			PWM_Y0_Set_Config();		
		  Y0_Tim_Mode_Config(Prescale,Peroid,CCRx_Val,pwm->mode);//SetUp Tim to PWM
		}
		else if(pwm==&gPulse_Pro[1])
		{
			PWM_Y1_Set_Config();
		  Y1_Tim_Mode_Config(Prescale,Peroid,CCRx_Val,pwm->mode);
		}
		else
	  {
		 //error
		}
	}
	else // 失能
	{//DISABLE
			if(pwm == &gPulse_Pro[0])
			{
					Y0_Tim_Mode_Config_DIASBLE();
					PWM_Y0_Restore_Config();
				 //X0_Tim_Mode_Config(StPwm->PWM_Cycle,StPwm->PWM_Width,StPwm->mode);				
			}
			else if(pwm == &gPulse_Pro[1])
			{
					Y1_Tim_Mode_Config_DIASBLE();
					PWM_Y1_Restore_Config();//恢复 IO的设置
				 //X0_Tim_Mode_Config(StPwm->PWM_Cycle,StPwm->PWM_Width,StPwm->mode);
			}
			else
		  {
			 //error
			}
  } 
}

//这边进行运动规划把  
void PTO_Plc_Plan_Pro(pulse_Pro *PtoPty)//PLSR_MaxFreq
{
		/*
		*  应该根据 用户给的时间来进行 判断设置 防止有大的阶跃这样对电机不好  
		*/
		float LInterploter=0;
		u32 Incrementtime=0;
//	if((PtoPty->PLSR_MaxFreq>150000)&&(PtoPty->PLSR_MaxFreq<=200000))
//	{
//    PtoPty->Interploter=5;
//		
//  }else if((PtoPty->PLSR_MaxFreq>100000)&&(PtoPty->PLSR_MaxFreq<=150000))
//  {
//    PtoPty->Interploter=10;
//		
//  }else if((PtoPty->PLSR_MaxFreq>50000)&&(PtoPty->PLSR_MaxFreq<=100000))
//  {
//    PtoPty->Interploter=10;
//		
//  }else if((PtoPty->PLSR_MaxFreq>10000)&&(PtoPty->PLSR_MaxFreq<=50000))
//  {
//    PtoPty->Interploter=30;
//		PtoPty->PLSR_MaxFreq/PtoPty->Ptr_Raise_time;
//  }else if((PtoPty->PLSR_MaxFreq>1000)&&(PtoPty->PLSR_MaxFreq<=10000))
//  {
//    PtoPty->Interploter=20;
//		
//  }else if((PtoPty->PLSR_MaxFreq>110)&&(PtoPty->PLSR_MaxFreq<=1000))
//  {
//    PtoPty->Interploter=100;
//  }

//	u32 yy=(u32)(PtoPty->PLSR_MaxFreq/(float)PtoPty->Ptr_Raise_time);
//	  if((yy<=(PtoPty->PLSR_MaxFreq/10))&&(yy>=10))
//		{
//         PtoPty->Interploter=1;
//    }else{
//			
//    }

//	 PtoPty->Interploter=(u32)(PtoPty->Ptr_Raise_time/10.0);
//	 PtoPty->Interploter=(PtoPty->Interploter==0)?1:PtoPty->Interploter;

		if((500000>PtoPty->PLSR_MaxFreq)&&(PtoPty->PLSR_MaxFreq>100000))
		{
				LInterploter=200;		
		}
		else if((100000>PtoPty->PLSR_MaxFreq)&&(PtoPty->PLSR_MaxFreq>10000))
		{
				LInterploter=100;		
		}
		else if((10000>PtoPty->PLSR_MaxFreq)&&(PtoPty->PLSR_MaxFreq>5000))
		{
				LInterploter=50;		
		}
		else if((5000>PtoPty->PLSR_MaxFreq)&&(PtoPty->PLSR_MaxFreq>1000))
		{
				LInterploter=20;		
		}	
		else if((1000>PtoPty->PLSR_MaxFreq)&&(PtoPty->PLSR_MaxFreq>100))
		{
				LInterploter=10;
		}

		Incrementtime=(u32)(PtoPty->PLSR_MaxFreq/LInterploter);
		PtoPty->Interploter=(u32)((float)PtoPty->Ptr_Raise_time/Incrementtime);//time>0
		if(PtoPty->Interploter==0)
		{
				PtoPty->Interploter=(PtoPty->Interploter==0)?1:PtoPty->Interploter;
				Incrementtime=(u32)(PtoPty->PLSR_MaxFreq/(float)PtoPty->Ptr_Raise_time);
		}
		//if()
	/*
	 *运动规划
	 */
//	 if(PtoPty->Ptr_Raise_time<=20)
//	 {
//      PtoPty->PLSR_MaxFreq/100=time;
//      PtoPty->Ptr_Raise_time/time=10;
//   }
	 
	{
		// u32 IncrementTimes= (u32)((float)PtoPty->Ptr_Raise_time/(float)PtoPty->Interploter);
		// float LInterploter=	PtoPty->PLSR_MaxFreq/(float)IncrementTimes;//更具公式算的 
		//u32 fmaxFre=0;	
		//加减速两次数据			
		u32 NeedPulseCount=(u32)((LInterploter*(1+Incrementtime)*Incrementtime)*PtoPty->Interploter/1000.0);//(PtoPty->PWM_Cycle/(float)PtoPty->Interploter)*(1+IncrementTimes)*IncrementTimes*((float)PtoPty->Interploter/1000.0);
		if(NeedPulseCount <= PtoPty->TargetPto) // 满足梯形速度的规划
		{ 
			 //在中断中处理看看看能不能正好发出 不然在中间段补脉冲
				PtoPty->IncrementPl=(u32)LInterploter;
				PtoPty->IncrementTimes=Incrementtime;
				PtoPty->Stage=eACC_STAGE;//
				PtoPty->PLSRMODE= eTRIOD_MODE;
				PtoPty->AccPulseCount=NeedPulseCount/2;
				PtoPty->DecPulseCount=NeedPulseCount-PtoPty->AccPulseCount;//NeedPulseCount/2;
				PtoPty->PlainCurFreq=PtoPty->PLSR_MaxFreq;     //匀速段的频率  和 脉冲个数计算
				PtoPty->PlainPulseCount=PtoPty->TargetPto-NeedPulseCount;			
		}
		else // 不满足梯形速度要求 进行三角形输出把 
		{
				//进行三角形的计算把 
				PtoPty->PLSR_MaxFreq=(u32)(1000*PtoPty->TargetPto/((float)(1+Incrementtime))/(float)PtoPty->Interploter);//(1000*PtoPty->TargetPto/(float)((IncrementTimes+1)*IncrementTimes));
				PtoPty->IncrementPl=(u32)PtoPty->PLSR_MaxFreq/Incrementtime;
				//在中断中处理看看看能不能正好发出 不然在中间段补脉冲
				PtoPty->AccPulseCount=PtoPty->TargetPto/2;
				PtoPty->DecPulseCount=PtoPty->TargetPto-PtoPty->AccPulseCount;//PtoPty->TargetPto/2;//这样做为了不会丢掉脉冲个数
				// eTRIOD_MODE, eTRI_MODE				   
				PtoPty->PLSRMODE=eTRI_MODE;
		}
	}
	//规划完成现在做发脉冲
	PtoPty->StageCurFre=PtoPty->IncrementPl;//阶段一  加速段
	PtoPty->StageTartag=(u32)(PtoPty->StageCurFre*(PtoPty->Interploter/1000.0));
	PtoPty->StageTartag=(PtoPty->StageTartag==0)?1:PtoPty->StageTartag;
	//开始输出
  PLSR_Plc_Pro(PtoPty,ENABLE);	
//  if(PtoPty->PWM_Cycle)
//	{	
//  }
//	else
//	{	
//  }
}

// 传人记，20170510新增
void PTO_Plc_Plan_CallBack(pulse_Pro *PtoPty)
{
}	
	
// 传人记，20170510新增,PLSY
void PLSY_Plc_Pro(pulse_Pro *pulse,FunctionalState NewState)
{
	if(NewState==ENABLE)//ENABLE
	{
		u16 Prescale;
		u16 Peroid;
		u16 CCRx_Val;		
		
		pulse->PWM_Width =50; //占空比固定50%

		if(pulse->PWM_Cycle <10){
			Prescale = 1199; 
			Peroid = 60000/pulse->PWM_Cycle-1;
		}
		else if(pulse->PWM_Cycle <100){
			Prescale = 119;
			Peroid = 600000/pulse->PWM_Cycle-1;
		}
		else if(pulse->PWM_Cycle <10000){ 			
			Prescale = 11;
			Peroid = 6000000/pulse->PWM_Cycle-1; 
		}
		else{  
			Prescale = 0;	
			Peroid = 72000000/pulse->PWM_Cycle-1; 
		}	
		/////////////////////////////////
		if (Peroid & 0x01)  
			CCRx_Val = ((Peroid + 1) * pulse->PWM_Width) / 100;
		else                 
			CCRx_Val = (Peroid * pulse->PWM_Width) / 100;
		
		if(pulse==&gPulse_Pro[0])
		{
			PWM_Y0_Set_Config();
		  Y0_Tim_Mode_Config(Prescale,Peroid,CCRx_Val,pulse->mode);
		}
		else if(pulse==&gPulse_Pro[1])
		{
			PWM_Y1_Set_Config();
		  Y1_Tim_Mode_Config(Prescale,Peroid,CCRx_Val,pulse->mode);
		}
		else
		{
		 //error
		}
	}
	else
	{//DISABLE
			if(pulse==&gPulse_Pro[0])
			{
				Y0_Tim_Mode_Config_DIASBLE();
				PWM_Y0_Restore_Config();
			 //X0_Tim_Mode_Config(StPwm->PWM_Cycle,StPwm->PWM_Width,StPwm->mode);				
			}
			else if(pulse==&gPulse_Pro[1])
			{
				Y1_Tim_Mode_Config_DIASBLE();
				PWM_Y1_Restore_Config();//恢复 IO的设置
			 //X0_Tim_Mode_Config(StPwm->PWM_Cycle,StPwm->PWM_Width,StPwm->mode);
			}
			else
			{
			 //error
			}
  }
}

// 传人记，20170510新增,PLSR
void PLSR_Plc_Pro(pulse_Pro *pulse,FunctionalState NewState)
{
	if(NewState==ENABLE)//ENABLE
	{
		u16 Prescale;
		u16 Peroid;
		u16 CCRx_Val;		
		pulse->PWM_Width =50; //占空比固定50%
		if(pulse->StageCurFre <10){
			Prescale = 1199; 
			Peroid = 60000/pulse->StageCurFre-1;
		}
		else if(pulse->StageCurFre <100){
			Prescale = 119;
			Peroid = 600000/pulse->StageCurFre-1;
		}
		else if(pulse->StageCurFre <10000){ 			
			Prescale = 11;
			Peroid = 6000000/pulse->StageCurFre-1; 
		}
		else{  
			Prescale = 0;	
			Peroid = 72000000/pulse->StageCurFre-1; 
		}	
		/////////////////////////////////
		if (Peroid & 0x01)  // 奇数
			CCRx_Val = ((Peroid + 1) * pulse->PWM_Width) / 100;
		else                 // 偶数
			CCRx_Val = (Peroid * pulse->PWM_Width) / 100;
		
		if(pulse==&gPulse_Pro[0])
		{
			PWM_Y0_Set_Config();
		  Y0_Tim_Mode_Config(Prescale,Peroid,CCRx_Val,pulse->mode);		
		}
		else if(pulse==&gPulse_Pro[1])
		{
			PWM_Y1_Set_Config();
		  Y1_Tim_Mode_Config(Prescale,Peroid,CCRx_Val,pulse->mode);
		}
		else
	  {
		 //error
		}
	}
	else
  {//DISABLE
			if(pulse==&gPulse_Pro[0])
			{
				Y0_Tim_Mode_Config_DIASBLE();
				PWM_Y0_Restore_Config();
			 //X0_Tim_Mode_Config(StPwm->PWM_Cycle,StPwm->PWM_Width,StPwm->mode);
				
			}
			else if(pulse==&gPulse_Pro[1])
			{
				Y1_Tim_Mode_Config_DIASBLE();
				PWM_Y1_Restore_Config();//恢复 IO的设置
			 //X0_Tim_Mode_Config(StPwm->PWM_Cycle,StPwm->PWM_Width,StPwm->mode);
			}
			else
			{
			 //error
			}
  }
}

#endif

/*******************************************************************************************************
                                 传人记，2017年05月10日编写，end  file!!!
********************************************************************************************************/
