/********************************************************/
// CPU需要：STM32F103--RAM内存不小于64K	Flash内存不小于256K
// 本代码已在STM32F103RDT6、VET6测试通过

#include "bsp.h"			/* 底层硬件驱动 */
#include "main.h"
#include "PLC_Dialogue.h"

#include "modbus_host.h"
#include "can_network.h" 
#include "encode.h"
#include "pwm_output.h"

u8 run_flag;


void _IWDG_Init(void)
{
  uint32_t cnt = 0;
  RCC_LSICmd(ENABLE);
  while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
  {
    if (cnt < 1000000)
      cnt ++;
    else
      return;
  }
  IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
  IWDG_SetPrescaler(IWDG_Prescaler_32);
  IWDG_SetReload(3750); // 3s

  IWDG_ReloadCounter();
  IWDG_Enable();
}

// test
uint8_t powTest =0;

int main(void)
{ 
//	rstFlg = RCC->CSR;
  PLC_IO_config();		                       // PLC输入输出初始化 
#if RTC_FUNC
  RTC_Init();                                // RTC时钟配置	
#endif
	
	data_init();		                          //调用D8000~D8126初始化
	PLC_ReadKeepData();                      //PLC断电数据恢
	
  USART1_Configuration();                    //串口初始化 	
  bsp_Init();							                   //485初始化	
	HS_MOD_LOCAL_ADDR =1;                      //本机通信地址为1
	HS_MOD_SAVLE_ADDR =1;                      //从站通信为1	
	
	gCommLedFlashFlg =1;
	

#if CAN_FUNC
	can_Init();				                         //初始化STM32 CAN硬件	
#endif 	
#if ADC_FUNC
  ADC_init();	                               //ADC初始化	
#endif
	
#if DAC_FUNC
	DAC_out_init();
#endif

#if PWM_OUTPUT_FUNC
	/* 脉冲输出 */
  PWM_Y0_Restore_Config();                   //恢复IO口的设置
  PWM_Y1_Restore_Config();
	PulseOutputInit();                         //初始化脉冲输出全局结构体 	
#endif

#if ENCODE_FUNC
	/* 脉冲输入 */
	Cap_PulseInputInit();  // 脉冲模式设置																		 
	X_Io_Restore();        // 编码器IO硬件复位	
#endif
	
  NVIC_Confinguration(ENABLE);               //开启中断程序全部等级划
	_IWDG_Init();
	
  while(1)                                      
  {	
#if RTC_FUNC
     RTC_Get();		                          //时间扫描程序		
#endif
		
	   PLC_ProInstructParse();    	          //PLC指令解析	
        		
	  if(Send_out)
			TX_Process();	                        //发送串口数据 

#if CAN_FUNC
	   CAN_Poll(); 
#endif
	  
#if ADC_FUNC
		 ADC_Sample();
#endif
	
#if POWER_DOWN_WAY ==0    
		/* 低电压检测或者手动，保存断电保持数据 */
		if((!PVD) || (D8250 == 8250) || (powTest ==1))	   
		{	
			if(5 <(powerDownCnt ++))              //过滤
			{
				powTest =0;
				powerDownCnt =0;
				D8250 =528;
				plc_16BitBuf[80] = 0;               //关闭Y0~Y17输出
//				plc_16BitBuf[81] = 0;             //关闭Y20~Y37输出
				PLC_SaveKeepData(); 
			}
		}
		else
		{
		   powerDownCnt =0;
		}
#endif /* #if POWER_DOWN_WAY ==0 */    
				
    IWDG_ReloadCounter(); 
  }
}  
