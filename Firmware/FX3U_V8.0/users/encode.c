/********************************************************/
// CPU需要：STM32F103--RAM内存不小于64K	Flash内存不小于128K
// 本代码已在STM32F103RCT6 RDT6 VCT6 VET6测试通过
// 编辑日期：20170510
// editor by 传人记
/********************************************************/
#include "stm32f10x_it.h" 
#include "encode.h"
#include  <stdio.h>
#include <string.h>

#if ENCODE_FUNC
/*  
特殊功能必需 进行硬件初始化和 软件处理结构体初始化
*/
 Cap_Pulse_Pro  S_Cap_Pulse_Pro[2];
 spd_t  S_spd[2];
 
 // 脉冲计数
 volatile uint32_t a0Cnt=0, a1Cnt =0;
 volatile uint32_t a0Times=0, a1Times =0;
 
 //AB相
 volatile s16 ab0LastCount = 0 ,ab1LastCount = 0; 
 static s32 C251_4TIMES = 0 , C253_4TIMES = 0; //传人记，20180515新增
 
 void Cap_PulseInputInit(void )
 {
   memset(S_Cap_Pulse_Pro,0,sizeof(Cap_Pulse_Pro));
	 memset((u8*)S_spd,0,sizeof(S_spd)); // SPD指令
 }
 
 /* X1脉冲计数,复原 */
 void Encoder_A0_Restore(void)
 {
		GPIO_InitTypeDef GPIO_InitStructure;
		 
		TIM_DeInit(TIM3); 
		TIM_Cmd(TIM3, DISABLE);	 
		/*开启按键端口的时钟*/
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
	 
		/* B相：X1 PC6 TIM3_CH1
			 A相：X0 PC7 TIM3_CH2 */	
	 	GPIO_PinRemapConfig(GPIO_FullRemap_TIM3, ENABLE);
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6; 
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz; 
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 
		GPIO_Init(GPIOC, &GPIO_InitStructure);	
	 
	  a0Cnt=0;
	  a0Times=0; 
 }
 
 /*  X1脉冲计数，C236 */
void Encoder_A0_Configuration(void)
{
		GPIO_InitTypeDef GPIO_InitStructure;
		TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
		NVIC_InitTypeDef NVIC_InitStructure;
		RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOC, ENABLE);
	  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3 , ENABLE);
	
		/* X1 PC6 TIM3_CH1*/	
	 	GPIO_PinRemapConfig(GPIO_FullRemap_TIM3, ENABLE);
		GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(GPIOC, &GPIO_InitStructure);

	  /* 配置中断使用组合  抢占式3位(0-7)，响应式1位(0-1) */
	  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_3);
		NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn; 
	  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; 
	  NVIC_Init(&NVIC_InitStructure);
	
		//TIM3_CH1 PC6
		TIM_DeInit(TIM3);   
		TIM_TimeBaseStructure.TIM_Period = 0XFFFF; 
		TIM_TimeBaseStructure.TIM_Prescaler = 0; 
		TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; 
		TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; //TIM_CounterMode_CenterAligned1; 
		TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure); 
		
//		TIM_ITRxExternalClockConfig(TIM3,TIM_TS_ETRF); //设置为外部触发
//		TIM_ETRClockMode2Config(TIM3, TIM_ExtTRGPSC_OFF, TIM_ExtTRGPolarity_Inverted, 0);
//	  TIM_TIxExternalClockConfig(TIM3,TIM_TIxExternalCLK1Source_TI2,TIM_ICPolarity_Falling,0); // X0 PC7
	  TIM_TIxExternalClockConfig(TIM3,TIM_TIxExternalCLK1Source_TI1,TIM_ICPolarity_Falling,0); // X1 PC6
		
		// Clear all pending interrupts
		TIM_ClearFlag(TIM3, TIM_FLAG_Update);
		TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
		
		TIM_SetCounter(TIM3, 0);                              
		//TIM_ARRPreloadConfig(TIM3, ENABLE); 
	//	TIM_SelectInputTrigger(TIM3, TIM_TS_TI2FP2);
	//	TIM_SelectSlaveMode(TIM3, TIM_SlaveMode_External1);
		TIM_Cmd(TIM3, ENABLE); 

}

/*  X1脉冲计数，C236 */
void Get_A0_Count(void)
{
    C236 = a0Cnt + TIM_GetCounter(TIM3);
}

void TIM3_IRQHandler(void)
{ 	
	if(TIM3->SR&0x0001)//溢出中断
	{
		a0Cnt =(++a0Times) * 0xFFFF;		
		TIM_SetCounter(TIM3,0);
	}	
	TIM3->SR&=~(1<<0);//清除中断标志位 	
}

/* X4脉冲计数,复原 */
 void Encoder_A1_Restore(void)
 {
		GPIO_InitTypeDef GPIO_InitStructure;

		TIM_DeInit(TIM2); 
		TIM_Cmd(TIM2, DISABLE);	 

		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);

		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15; 
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz; 
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 

		GPIO_Init(GPIOD, &GPIO_InitStructure); 
		a1Cnt=0;
		a1Times=0; 
 }
 
/*  X4脉冲计数，C239 */
void Encoder_A1_Configuration(void)
{
		GPIO_InitTypeDef GPIO_InitStructure;
		TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
		NVIC_InitTypeDef NVIC_InitStructure;
		RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOD, ENABLE);
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);
  	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2 , ENABLE);
	
		/* X4 TIM2_CH1_ETR PA0 */	
		GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_15;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(GPIOD, &GPIO_InitStructure);
	
		/* 配置中断使用组合  抢占式3位(0-7)，响应式1位(0-1) */
	  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_3);
		NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn; 
	  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; 
	  NVIC_Init(&NVIC_InitStructure);

		//TIM2_CH1_ETR PA0
		TIM_DeInit(TIM2);                
		TIM_TimeBaseStructure.TIM_Period = 0XFFFF;
		TIM_TimeBaseStructure.TIM_Prescaler = 0;
		TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; 
		TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;//TIM_CounterMode_CenterAligned1;
		TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

		TIM_SetCounter(TIM2, 0);
		//TIM_ARRPreloadConfig(TIM2, ENABLE);
//		TIM_ITRxExternalClockConfig(TIM2,TIM_TS_ETRF); //设置为外部触发
//		TIM_ETRClockMode2Config(TIM2, TIM_ExtTRGPSC_OFF, TIM_ExtTRGPolarity_Inverted, 0);
//		TIM_TIxExternalClockConfig(TIM2,TIM_TIxExternalCLK1Source_TI2,TIM_ICPolarity_Falling,0); // X03 PA1
		TIM_TIxExternalClockConfig(TIM2,TIM_TIxExternalCLK1Source_TI1,TIM_ICPolarity_Falling,0); // X04 PA0
		// Clear all pending interrupts
		TIM_ClearFlag(TIM2, TIM_FLAG_Update);
		TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
		
		TIM_SetCounter(TIM2, 0);                              
		// TIM_ARRPreloadConfig(TIM2, ENABLE); 
		// TIM_SelectInputTrigger(TIM2, TIM_TS_TI2FP2);
		// TIM_SelectSlaveMode(TIM2, TIM_SlaveMode_External1);		
		TIM_Cmd(TIM2, ENABLE); 
}

/*  X4脉冲计数，C239 */
void Get_A1_Count(void)
{
     C239 = a1Cnt + TIM_GetCounter(TIM2); 
}

void TIM2_IRQHandler(void)
{ 	
	if(TIM2->SR&0x0001)//溢出中断
	{
		a1Cnt =(++a1Times) * 0xFFFF;		
		TIM_SetCounter(TIM2,0);
	}	
	TIM2->SR&=~(1<<0);//清除中断标志位 	
}

////////////////////////////////////////////////////////////
/* 用编码器IO,配置为普通输入IO
*  X0 --- PC7
*  X1 --- PC6
*  X3 --- PA1
*  X4 --- PA0
*/
void  X_Io_Restore(void)
{  
	GPIO_InitTypeDef GPIO_InitStructure;
	
  // 传人记，2018年07月10日修复
	TIM_Cmd(TIM3, DISABLE);	 
  TIM_Cmd(TIM2, DISABLE);	 
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_14| GPIO_Pin_15; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz; 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 	
	GPIO_Init(GPIOD, &GPIO_InitStructure);
	

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 ; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz; 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 	
	GPIO_Init(GPIOC, &GPIO_InitStructure);
}

/* 第一路编码器AB，复原*/
 void Encoder_AB0_Restore(void)
 {	 
		GPIO_InitTypeDef GPIO_InitStructure;
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
		TIM_Cmd(TIM3, DISABLE); 
	 
		/* B相：X1 PC6 TIM3_CH1
			 A相：X0 PC7 TIM3_CH2 */	
	 	GPIO_PinRemapConfig(GPIO_FullRemap_TIM3, ENABLE);
	 
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 ; 
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz; 
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 
		GPIO_Init(GPIOC, &GPIO_InitStructure);
	 
	  ab0LastCount =0;	
    C251 =0;	 
	 	C251_4TIMES = 0;	//francis 20170624 添加
 }
 
/* 第一路编码器AB，配置*/
void Encoder_AB0_Configration(void)  // 4倍频
{
		GPIO_InitTypeDef GPIO_InitStructure;
		TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
		TIM_ICInitTypeDef TIM_ICInitStructure;
//	  NVIC_InitTypeDef NVIC_InitStructure;
		RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOC, ENABLE);
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3 , ENABLE);

		/* B相：X1 PC6 TIM3_CH1
			 A相：X0 PC7 TIM3_CH2 */	
	 	GPIO_PinRemapConfig(GPIO_FullRemap_TIM3, ENABLE);
	
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 ;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(GPIOC,&GPIO_InitStructure);
	
//		/* 配置中断使用组合  抢占式3位(0-7)，响应式1位(0-1) */
//	  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_3);
//	
//		NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn; 
//	  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
//	  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
//	  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; 
//	  NVIC_Init(&NVIC_InitStructure);

		TIM_DeInit(TIM3);
		TIM_TimeBaseStructure.TIM_Prescaler = 0x0;  // No prescaling
		TIM_TimeBaseStructure.TIM_Period = (360*4)-1;  //设定计数器重装值   TIMx_ARR = (360*4)-1
		TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
		TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  
		TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
		
	  TIM_EncoderInterfaceConfig(TIM3, TIM_EncoderMode_TI12, //使用编码器模式3，上升下降都计数 即可实现4倍频计数
		                           TIM_ICPolarity_BothEdge ,
															 TIM_ICPolarity_BothEdge);
		TIM_ICStructInit(&TIM_ICInitStructure);
		TIM_ICInitStructure.TIM_ICFilter = 6;//ICx_FILTER;
		TIM_ICInit(TIM3, &TIM_ICInitStructure);

		// Clear all pending interrupts
//		TIM_ClearFlag(TIM3, TIM_FLAG_Update);
//		TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
		
		//Reset counter
		TIM_SetCounter(TIM3,0);
		TIM_Cmd(TIM3, ENABLE); 

		S_Cap_Pulse_Pro[0].mode=AB_mode;
		//S_Cap_Pulse_Pro[0].Gpio_pin=;
		S_Cap_Pulse_Pro[0].used=1;
}


#define MAX_COUNT  720

/* 第一路编码器 */
s32 ENC_Get_AB0_Electrical_Angle(void)
{
  s16  curCount = TIM3->CNT;
  s32 dAngle = curCount - ab0LastCount;
	
  if(dAngle >= MAX_COUNT)
  {
			dAngle -= (MAX_COUNT <<1); 
  }
	else if(dAngle < -MAX_COUNT)
	{
			dAngle += (MAX_COUNT <<1); 	
  }
	
	//TIM_SetCounter(TIM3,0);
  ab0LastCount = curCount;
  return (s32)dAngle;  
}


void Get_AB0_Count(void)//每个毫秒都进行计算
{
	//C251 +=ENC_Get_AB0_Electrical_Angle();
	C251_4TIMES += ENC_Get_AB0_Electrical_Angle();  // 传人记，20180515新增4倍频与1倍频判断
	if(PLC_BIT_TEST(M8198))
		C251 = C251_4TIMES;
	else
		C251 = C251_4TIMES >> 2;
}

//第一路编码器
//void TIM3_IRQHandler(void)
//{ 	
//	if(TIM3->SR&0x0001)//溢出中断
//	{
////		TIM_SetCounter(TIM3,0);
//		Get_AB0_Count();
//	}	
//	TIM3->SR&=~(1<<0);//清除中断标志位 	
//}


/* 第二路编码器AB，复原*/
 void Encoder_AB1_Restore(void)
 {	 
		GPIO_InitTypeDef GPIO_InitStructure;
		TIM_Cmd(TIM2, DISABLE); 

		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
//		RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);

		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_14; 
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz; 
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 
		
		GPIO_Init(GPIOD, &GPIO_InitStructure);	
	 
	 	ab1LastCount =0;	
	  C253 =0;
		C253_4TIMES = 0;	 
 }
 
/* 第二路编码器AB，配置*/ 
void Encoder_AB1_Configration(void)
{
		GPIO_InitTypeDef GPIO_InitStructure;
		TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
		TIM_ICInitTypeDef TIM_ICInitStructure;
		RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA, ENABLE);
//		RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2 , ENABLE);
	
		//B相：X4 PA0 TIM2_CH1；  A相: X3 PA1 TIM2_CH2
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_14;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(GPIOD,&GPIO_InitStructure);

//		/* 配置中断使用组合  抢占式3位(0-7)，响应式1位(0-1) */
//	  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_3);//	
//		NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn; 
//	  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
//	  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
//	  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; 
//	  NVIC_Init(&NVIC_InitStructure);	
				
	  TIM_DeInit(TIM2);
		TIM_TimeBaseStructure.TIM_Prescaler = 0x0;  // No prescaling
		TIM_TimeBaseStructure.TIM_Period =(360*4)-1;  //设定计数器重装值   TIMx_ARR = (360*4)-1
		TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
		TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  
		TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
	 
	  TIM_EncoderInterfaceConfig(TIM2, TIM_EncoderMode_TI12, //使用编码器模式3，上升下降都计数 即可实现4倍频计数
		                           TIM_ICPolarity_BothEdge ,
															 TIM_ICPolarity_BothEdge);
															 
		TIM_ICStructInit(&TIM_ICInitStructure);
		TIM_ICInitStructure.TIM_ICFilter = 6;//ICx_FILTER;
		TIM_ICInit(TIM2, &TIM_ICInitStructure);
	 
	 // Clear all pending interrupts
//		TIM_ClearFlag(TIM2, TIM_FLAG_Update);
		//TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
		
		//Reset counter
		TIM_SetCounter(TIM2,0);	 
		TIM_Cmd(TIM2, ENABLE); 
		
		S_Cap_Pulse_Pro[1].mode=AB_mode;
		//S_Cap_Pulse_Pro[1].Gpio_pin=;
		S_Cap_Pulse_Pro[1].used=1;
}


/* 第二路编码器 */
s32 ENC_Get_AB1_Electrical_Angle(void)
{
  s16  curCount = TIM2->CNT;
  s32 dAngle = curCount - ab1LastCount;
  if(dAngle >= MAX_COUNT)
	{
			dAngle -= MAX_COUNT; 
  }
	else if(dAngle < -MAX_COUNT)
	{
			dAngle += MAX_COUNT;
  }
	//TIM_SetCounter(TIM2,0);
  ab1LastCount = curCount;
  return (s32)dAngle;  
}

/* 第二路编码器，获取AB相计数 */
void Get_AB1_Count(void) //每个毫秒都进行计算
{
		//C253 +=ENC_Get_AB1_Electrical_Angle();
	C253_4TIMES += ENC_Get_AB1_Electrical_Angle(); // 传人记，20180515新增4倍频与1倍频判断
	if(PLC_BIT_TEST(M8198))
		C253 = C253_4TIMES;
	else
		C253 = C253_4TIMES >> 2;
}


//第二路编码器
//void TIM2_IRQHandler(void)
//{ 	
//	if(TIM2->SR&0x0001)//溢出中断
//	{
////		TIM_SetCounter(TIM2,0);
//		Get_AB0_Count();
//	}	
//	TIM2->SR&=~(1<<0);//清除中断标志位 	
//}


	/*
	*  X0 --- PC7
	*  X1 --- PC6
	*  X3 --- PA1
	*  X4 --- PA0
	*/
void Encode_Pro_Callback(void)
{
   /* 第一路 */
   if(S_Cap_Pulse_Pro[0].used ==1) 
	 {
		   if(S_Cap_Pulse_Pro[0].mode==Signal_mode) // X1脉冲计数
				  Get_A0_Count();
			 else if (S_Cap_Pulse_Pro[0].mode==AB_mode) // 编码器AB,A：X1  B:X0
					Get_AB0_Count(); 
	 }
   /* 第二路 */
	 if(S_Cap_Pulse_Pro[1].used ==1)
	 {
		   if(S_Cap_Pulse_Pro[1].mode==Signal_mode) // X4脉冲计数
				 Get_A1_Count();
			 else if (S_Cap_Pulse_Pro[1].mode==AB_mode) // 编码器AB,A：X4  B:X3
				 Get_AB1_Count();				 
	 }
}

/* SPD指令：设定时间内脉冲个数统计 */
void SpdTime(void)
{
	/* X1 */
	if (S_spd[0].used !=0)
	{
		S_spd[0].t ++;
		S_spd[0].totalN =a0Cnt + TIM_GetCounter(TIM3);
		if(S_spd[0].s2 <= S_spd[0].t) //到达设定时间
		{ 
			S_spd[0].used =2;
			
			S_spd[0].n = S_spd[0].totalN - S_spd[0].lastN;
			S_spd[0].lastN  =S_spd[0].totalN; 
			S_spd[0].t = 0;
		}
	}
	
  /* X4 */
	if(S_spd[1].used !=0)
	{
		S_spd[1].t ++;
		S_spd[1].totalN  =a1Cnt + TIM_GetCounter(TIM2); 
		if(S_spd[1].s2 < S_spd[1].t) //到达设定时间
		{
			S_spd[1].used =2;
			S_spd[1].n = S_spd[1].totalN - S_spd[1].lastN;
			S_spd[1].lastN  =S_spd[1].totalN; 
			S_spd[1].t = 0;
		}
	}
}
#endif

/*******************************************************************************************************
                                 传人记，2017年4月17日编写，end  file!!!
********************************************************************************************************/



