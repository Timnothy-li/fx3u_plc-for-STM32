
#ifndef __ENCODE_H
#define	__ENCODE_H

#include "stm32f10x.h"
#include "stm32f10x_tim.h"
#include "stm32f10x_gpio.h"

#include "plc_conf.h"
#include "PLC_Dialogue.h"

#if ENCODE_FUNC

typedef enum _Cap_Pulse_Mode
{
	Signal_mode =0,
	AB_mode,
}Cap_Pulse_Mode;

// 传人记，2018年07月09日新增SPD指令
typedef struct
{
	u8   used;       // 状态标志
  u32  t;          // 计时
  u32  s2;         // 设定的时间（ms）
  u32  n;          // 到设定时间的测定值（脉冲数）
	u32  cn;         // 设定时间内的当前值（脉冲数）
	u32  totalN;     // 脉冲累计总数
	u32  lastN;      // 记录上一次（脉冲数）
  const u16*  d;   // 保存脉冲密度数据的起始字软元件编号（地址）
}spd_t;
extern spd_t S_spd[2];


typedef struct _Cap_Pulse_Pro{
	 u32 used;
   //u16 Gpio_pin;   //输出IO口
   Cap_Pulse_Mode mode; //PWM 还是  PTO 
}Cap_Pulse_Pro;

extern Cap_Pulse_Pro  S_Cap_Pulse_Pro[2];
void Cap_PulseInputInit(void );
void  X_Io_Restore(void);
void Encoder_A0_Configuration(void);
void Get_A0_Count(void);

void Encoder_A1_Configuration(void);
void Get_A1_Count(void);

void Encoder_AB0_Configration(void);
void Get_AB0_Count(void);

void Encoder_AB1_Configration(void);
void Get_AB1_Count(void);

void Encode_Pro_Callback(void);

void Encoder_A0_Restore(void);
void Encoder_A1_Restore(void);
void Encoder_AB0_Restore(void);
void Encoder_AB1_Restore(void);


s32 ENC_Get_Electrical_Angle(void);
extern void SpdTime(void);

#endif
#endif


