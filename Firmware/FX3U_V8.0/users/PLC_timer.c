/********************************************************/
// CPU需要：STM32F103--RAM内存不小于64K	Flash内存不小于128K
// 本代码已在STM32F103RDT6、VET6测试通过


//4) 定时器功能（与三菱兼容）：
//4.1） 常规定时器 T0～T255 共256点
//      T0～T199为100ms定时器，共200点
//      T200～T245为10ms定时器，共46点
//4.2） 积算定时器 T246～T255 共10点
//      T246～T249为1ms积算定时， 共4点
//      T250～T255为100ms积算定时器，共6点

/********************************************************/
#include "PLC_timer.h"
#include "stm32f10x_it.h"
#include "stm32f10x_tim.h"
#include <stdio.h> 
#include "PLC_Dialogue.h"
#include "plc_conf.h"
//#include "encode.h"

/*******************************************************************************
* 函数名  : Delay
* 描述    : 延时函数(ms)
* 输入    : d:延时系数，单位为毫秒
* 输出    : 无
* 返回    : 无 
* 说明    : 延时是利用Timer2定时器产生的1毫秒的计数来实现的
*******************************************************************************/
void Delay(u16 time)
{    
   u16 i=0;  
   while(time--)
   {
	   i=12000; 
		 while(i--); 
	 }
}

static volatile u16 *pSetValue,*pCurrentValue;	
void timer_enable(u16 timer_number)
{    
    pSetValue=plc_16BitBuf + 0x0900 + timer_number; //计数器设定值地址
	  pCurrentValue=plc_16BitBuf+0x0800+timer_number; // 计数器当前值地址
	
		if(*pCurrentValue < *pSetValue)
		{
			if(PLC_BIT_TEST(0x600+timer_number)) // 判断触点是否闭合
			{
					PLC_BIT_ON(0x3800+timer_number); //复位线圈
			}
		  else 
			{
					PLC_BIT_OFF(0x600+timer_number); //值小于设定值时OFF
					if(PLC_BIT_TEST(0x3800 + timer_number)) 
					{
//						 if(timer_number < 246) // 非积算定时器，待测试
								*pCurrentValue=0;
						
						 PLC_BIT_OFF(0x3800 + timer_number);
					}
			}
		}
		else
		{
				PLC_BIT_ON(0x600+timer_number);  //值到达设定值时ON
		}
}

static volatile u16 *pT_DisSetValue,*pT_DisCurrentValue;	
void timer_disble(u16 timer_number)
{    
	pT_DisSetValue =plc_16BitBuf+0x0900+timer_number;	// 计数器设定地址
	pT_DisCurrentValue =plc_16BitBuf+0x0800+timer_number;	// 计数器当前值地址
	//
	PLC_BIT_OFF(0x600+timer_number);	//溢出线圈
	PLC_BIT_OFF(0x1600+timer_number); //使能线圈
	PLC_BIT_OFF(0x3800+timer_number); //复位线圈
	
	// 传人记，T246~T249 4点1ms积算定时器；T250~T255 6点100ms积算定时器；
	if(timer_number < 246) // 非积算定时器
	{
		*pT_DisSetValue=0;
		*pT_DisCurrentValue=0;
	}
}

/* 常规100ms，共200点 */
static volatile u16 *pT_100msSetValue,*pT_100msCurrentValue;
void T_100MS(void)
{
	u16 timer_count;
  for(timer_count =0;timer_count<200;timer_count++)
	{ 
		  pT_100msSetValue = plc_16BitBuf + 0x0900 + timer_count;
	    pT_100msCurrentValue =plc_16BitBuf + 0x0800 + timer_count;
	    if(PLC_BIT_TEST(0x1600 + timer_count)) //线圈状态
		  {
			  if( (*pT_100msCurrentValue) < (*pT_100msSetValue) ) //值状态
				{
					 u16 ret = PLC_BIT_TEST(0x600 + timer_count);
				  if(!ret) 
						*pT_100msCurrentValue +=1; // 计时
				}
		  }
	 }
}

/* 常规10ms，共46点 */
static volatile u16 *pT_10msSetValue,*pT_10msCurrentValue;
void T_10MS(void)
{  
   u16 timer_count;
   for(timer_count =200;timer_count<246;timer_count++)
   { 
		  pT_10msSetValue=plc_16BitBuf + 0x0900 + timer_count;
	    pT_10msCurrentValue=plc_16BitBuf + 0x0800 + timer_count;
	    if(PLC_BIT_TEST(0x1600 + timer_count)) //线圈状态
		  {
          if(*pT_10msCurrentValue<*pT_10msSetValue) //值状态
				  {
				     if(PLC_BIT_TEST(0x600 + timer_count)) ;
				     else 
							 *pT_10msCurrentValue +=1;
				  }
		  }
   }
}


/* 积算1ms，共4点 */
static volatile u16 *pT_1msSetValue,*pT_1msCurrentValue;
void T_1MS(void)
{ 
	 u16 timer_count;  
     for(timer_count =246;timer_count<250;timer_count++)
	   { 
		    pT_1msSetValue =plc_16BitBuf + 0x0900 + timer_count;
	      pT_1msCurrentValue =plc_16BitBuf + 0x0800 + timer_count;
	      if(PLC_BIT_TEST(0x1600 + timer_count))//线圈状态
		    {  
           if(*pT_1msCurrentValue<*pT_1msSetValue) //值状态
				   {
				     if(PLC_BIT_TEST(0x600 + timer_count)) ;
				     else 
							 *pT_1msCurrentValue +=1;
				  }
		    }		
	  }	
	  
}

/* 积算100ms，共6点 */
static volatile u16 *pHT_100msSetValue,*pHT_100msCurrentValue;
void T_H100MS(void)
{ 
	  u16 timer_count;
    for(timer_count =250;timer_count <256;timer_count++)
	  { 
		   pHT_100msSetValue =plc_16BitBuf+0x0900+timer_count;
	     pHT_100msCurrentValue =plc_16BitBuf+0x0800+timer_count;
	     if(PLC_BIT_TEST(0x1600 +timer_count))  // 线圈状态
		   {
           if(*pHT_100msCurrentValue < *pHT_100msSetValue) //值状态
				   {
				     if(PLC_BIT_TEST(0x600 + timer_count)) ;
				     else 
							 *pHT_100msCurrentValue +=1;
				   }
		   }
	   }
}

//static volatile u8 all_clock;
//static volatile u16 minute;
//void TIM2_IRQHandler(void)//1ms产生一次中断信号
//{ 
//  TIM_ClearFlag(TIM2, TIM_FLAG_Update); 
////	TIM2->SR=0; 	
//	
//	plc_scan_time +=10;	
//	all_clock++;	
//	X_filter();                     //检查X输入状态值	
//	
//  Encode_Pro_Callback();
//	
//	if(all_clock>99) 	              //m8011 10MS m8012 100MS  m8013 1SEC m8014 1minute   
//	{
//		all_clock=0;
//		PLC_BIT_ON(M8012);
//		T_1MS();
//	}	             
//	if((all_clock%10)==7)           //10ms定时器设计每计五次刷新一次
//	{
//		T_10MS();
//		PLC_BIT_OFF(M8011);
//	}
//	if((all_clock%10)==2)
//	{
//		PLC_BIT_ON(M8011);
//	}
//	if(all_clock==50)	              //两种100MS定时器分开刷新
//	{
//		T_100MS();
//		PLC_BIT_OFF(M8012);	
//	}

//	if(all_clock==90)	              //每100ms秒钟分钟定时器
//	{
//		T_H100MS();
//		minute++;
//	}
//		
//	if((all_clock%0x10)==0x02)	    //更新一次DAC数据
//	{
//		DAC_data();
//		filter();                     //ADC十个毫秒传送更新一次
//	}

//	if(minute%10==5)	              //刷新秒钟8013
//			PLC_BIT_OFF(M8013);

//	if(minute%10==0)
//			PLC_BIT_ON(M8013);

//	if(minute==300)			            //刷新分钟8014
//			PLC_BIT_OFF(M8014);

//	if(minute==0)
//			PLC_BIT_ON(M8014);

//	if(minute>599)
//			minute=0;		  
//	
//}

//void PLC_Timer(void)
//{
//	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;       
//	/* TIM2 clock enable */	
//	/* ---------------------------------------------------------------
//	TIM2配置:输出比较时间模式:
//	TIM2CLK = 36 MHz,预定标器= 7200,TIM5计数器时钟= 7.2 MHz
//	--------------------------------------------------------------- */
//	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
//	/* Time base configuration */
//	//这个就是自动装载的计数值，由于计数是从0开始的，计数72次后为71
//	TIM_TimeBaseStructure.TIM_Period = 36000;
//	// 这个就是预分频系数，当由于为0时表示不分频所以要减1
//	TIM_TimeBaseStructure.TIM_Prescaler = 1;
//		
//	// 使用的采样频率之间的分频比例
//	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
//	//向上计数
//	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
//	//初始化定时器2
//	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
//	/* Clear TIM2 update pending flag[清除TIM5溢出中断标志] */
//	TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
//	/* TIM IT enable */ //打开溢出中断
//	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
//	/* TIM2 enable counter */
//	TIM_Cmd(TIM2, ENABLE);  //计数器使能，开始工作
//}




