/************************************************************
库名称:    PLC_Dialogue
功能描述： PLC宏处理

************************************************************/
#ifndef __PLC_Dialogue_H__
#define __PLC_Dialogue_H__
#include "stm32f10x.h"

#define _countof(_Array) (sizeof(_Array) / sizeof(_Array[0]))
	
extern const unsigned char PLC_BIT_OR[];
extern const unsigned char PLC_BIT_AND[];

#define PROGRAM_BUF_SIZE     610  // 编程口协议通讯收发缓存大小

/*24新增 */
//#define PLC_16BIT_BUF_SIZE    12100      
#define PLC_16BIT_BUF_SIZE    16600           // ?*1024KB	
extern  u16 plc_16BitBuf[PLC_16BIT_BUF_SIZE]; 


extern u8  step_address[2000];         //为PLS OR PLF use

// 片内Flash使用情况
#define PLC_RAM_ADDR         0x8006000	      // 起始地址必需为0X800000开始加0X1000整倍数,因为后面写PLC程序时需要用到块擦除
#define PLC_RAM_START        0x8006002          //
#define PLC_LAD_START_ADDR   ((u16*)(0x800605C))// PLC梯形图程序开始地址


/* 0601新增 */
#if defined (STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined (STM32F10X_CL) || defined (STM32F10X_XL)
  #define FLASH_PAGE_SIZE    ((uint16_t)0x800)
#else
  #define FLASH_PAGE_SIZE    ((uint16_t)0x400)
#endif

#define STM32_FLASH_BASE     0x08000000  // STM32 FLASH的起始地址


#define KEEP_DATA_PAGE       90          // 保持数据保存
#define WR_FLASH_CNT_ADDR    (STM32_FLASH_BASE + (FLASH_PAGE_SIZE * KEEP_DATA_PAGE))
#define WR_FLASH_CNT_SIZE    4          // 写Flash次数预留4字节
#define KEEP_DATA_ADDR       (WR_FLASH_CNT_ADDR + WR_FLASH_CNT_SIZE)

/* 80318新增，备份软元件数据 */
#define KEEP_DATA_BACKUP_PAGE       92  // 保持数据保存
#define WR_FLASH_CNT_BACKUP_ADDR    (STM32_FLASH_BASE + (FLASH_PAGE_SIZE * KEEP_DATA_BACKUP_PAGE))
#define WR_FLASH_CNT_BACKUP_SIZE    4   // 写Flash次数预留4字节
#define KEEP_DATA_BACKUP_ADDR       (WR_FLASH_CNT_BACKUP_ADDR + WR_FLASH_CNT_BACKUP_SIZE)

// PLC RAM运行数据，C、D8000、T、D、D1000字软元件，总共使用23KB内存，到0x20006E80（对应27KB）
#define RAM_ADDR             0x20001000    // PLC_RAM起始地址

// C、D8000、T、D、D1000字软元件在RAM中分配，总共使用23KB内存
#define RAM_C_ADDR           0x20001A00    // PLC C起始地址,0xA00~0x8BE
#define RAM_C200_ADDR        0x20001C00    // PLC C起始地址,0xC00~0xC6E

//#define RAM_D8000_ADDR       0x20001E00    // PLC D8000起始地址,0xE00~0xFFE,
#define RAM_D8000_ADDR       0x20002800    // PLC D8000起始地址,0xE00~0xFFE,

#define RAM_T_ADDR           0x20002000    // PLC T起始地址,0x1000~0x1200 (当前值)
#define RAM_T_SET_ADDR       0x20002200    // PLC T起始地址,0x1200~0x13FE (设定值)

#define RAM_D_ADDR           0x20003000    // PLC D起始地址,0x2000~0x27CE
#define RAM_D1000_ADDR       0x200037D0    // PLC D1000起始地址,0x27D0~0x56B0


#define ROTATE_LEFT(x, s, n)        ((x) << (n)) | ((x) >> ((s) - (n)))      //循环左移  x为数据 s为数据位数 n为移动位数
#define ROTATE_RIGHT(x, s, n)       ((x) >> (n)) | ((x) << ((s) - (n)))      //循环右移  x为数据 s为数据位数 n为移动位数

#define swap_u16(x)        ((x) >> (8)) | ((x) << (8)) //上下交换传送
#define swap_u32(x)        ((x) >> (16))|((x) << (16)) //上下交换传送

#define	PLC_D_C_T_addr(x)  ((x) % (0x4000))         //V,Z的时候用

#define	PLC_v_z_addr(x)    ((x) / (0x4000))         //V,Z的时候用

#define PLC_RAM8(x)        (*(u8*)(u32)(x))		      //字节方式	R/W RAM	R ROM

#define PLC_RAM16(x)       (*(s16*)(u32)(x))				//半字方式	R/W RAM	R ROM

#define PLC_RAM32(x)       (*(s32*)(u32)(x))				//字方式	R/W RAM	R ROM

#define PLC_RAM64(x)       (*(int64_t*)(u32)(x))	  //字方式	R/W RAM	R ROM

#define PLC_RAMfolta(x)    (*(float*)(u32)(x))		  //字方式	R/W RAM	R ROM

#define trade1             FLOAT.DATA               //浮点输出的时候用

// 125修改，支持2路Modbus主从模式
#define PLC_RW_RAM_8BIT(x)     PLC_RAM8(RAM_ADDR + x)

// 71222新增
#define PLC_SXYTMC(x)	        (PLC_RAM8(RAM_ADDR + (x)/8))   // 读位字节地址对应的数值
#define PLC_SXYTMC_Kn(x)	    (PLC_RAM16(RAM_ADDR + (x)/8))	 
#define PLC_SXYTMC_Kn32(x)	  (PLC_RAM32(RAM_ADDR + (x)/8))	 


// 【/8】可理解为右移3位，即取高5位值；【%8】取低3位值
#define PLC_BIT_TEST(x)	    	( PLC_RAM8(RAM_ADDR+((x)/8)) & PLC_BIT_OR[(x)%8] ) // 0x20001000

#define PLC_BIT_ON(x)		  		(PLC_RAM8(RAM_ADDR+((x)/8))	|=PLC_BIT_OR[(x)%8])
#define PLC_BIT_OFF(x)				(PLC_RAM8(RAM_ADDR+((x)/8))	&=PLC_BIT_AND[(x)%8])

#define PLC_PL_BIT_ON(x)	 		(step_address[(x)/8] |=PLC_BIT_OR[(x)%8]) //写状态值为1	   上升延与下降沿使用
#define PLC_PL_BIT_OFF(x)	 		(step_address[(x)/8] &=PLC_BIT_AND[(x)%8])//写步状态为0上升延与下降沿使用共2K字节1600bit

#define on  1
#define off 0

typedef union
{
	struct
	{
		  u8 bit0		  :1;
  		u8 bit1		  :1;
  		u8 bit2		  :1;
  		u8 bit3		  :1;
  		u8 bit4		  :1;
  		u8 bit5		  :1;
  		u8 bit6		  :1;
  		u8 bit7		  :1;
		  u8 bit10		:1;
		  u8 bit11		:1;
		  u8 bit12		:1;
		  u8 bit13		:1;
		  u8 bit14		:1;
		  u8 bit15		:1;	
			u8 bit16		:1;
		  u8 bit17		:1;
	}bits;	            //可以按位域寻址
  u16 bytes;       	  //可以按字节寻址
}bit_byte;  		      //定义一个既能按位域寻址也可按字节寻址的新变量类型

///*
// *16位整型共同体
// */
//typedef union
//{
//  u8  plc_8gModbusBitBuf[PLC_16BIT_BUF_SIZE*2];
//	u16 plc_16BitBuf[PLC_16BIT_BUF_SIZE]; // 23KB
//} union_16BIT;

/*
 *浮点共同体
 */
typedef union                                
{
  float DATA;
	u16   DATA1[2];
	u32   bata;
} float_union;
/*
 *32位整型共同体
 */
typedef union
{
  s32 data;
	s16 data1[2];
} s32_union;
/*
 *64位整型共同体
 */
typedef union
{
  int64_t data;
	u16 data1[4];
} u64_union;

#endif

#ifndef __at
#define __at(_addr) __attribute__ ((at(_addr)))
#endif

/* 170601新增 */
extern FLASH_Status Flash_EraseProgramPage( uint8_t startPage, uint8_t pageNum);
extern void RST_T_D_C_M_data(void);
extern void RST_Y(void);


