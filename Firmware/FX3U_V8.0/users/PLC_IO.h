/****************************************************************************
PLC相关的特殊寄存器
专用辅助继电器                 描述
M8126                          全局标志
M8127                          通讯请求握手信号
M8128                          出错标志 
M8129                          通讯请求切换        
   
专用数据寄存器                描述
D8000 = 200;		   		        扫描时间
D8001 = 0X5EF6;	              型号版本 FX2N(C)
D8101 = 0X5EF6;	              型号版本 FX2N(C)
D8002 = 8;			   		        内存容量
D8102 = 8;			   		        内存容量
D8003 = 0x0010; 	   	        内存类型、寄存器类型
D8006                         CPU电池电压
D8010 = 10;			   	          扫描当前值
D8011 = 20;			   	          扫描最小时间(0.1MS)
D8012 = 140;			   	        扫描最长时间(0.1MS) 

D8120 = 0X4096                通讯格式
D8121                         从站号（最多16个）
D8127                         交换数据的首地址
D8128                         交换数据量
D8129                         网络通讯超时时间确认值
D8000                         看门狗         

通讯格式详解（D8120）
----------------------------------------------------------------------
位号	     |   含 义	   |          描述      
-----------+-------------+--------------------------------------------
b0	       |  数据长度	 |   0： 7位   1： 8位
-----------+-------------+--------------------------------------------
b2b1	     |  校验方式	 |   00：不用  01：奇校验  11：偶校验
-----------+-------------+--------------------------------------------
b3	       |   停止位	   |   0： 1位   1： 2位
-----------+-------------+--------------------------------------------
           |             |   0001：300      0111：4800
b7b6b5b4	 |   波特率	   |   0100：600      1000：9600
           |             |   0101：1200     1001：19200
           |             |   0110：2400
-----------+-------------+--------------------------------------------
b8		     |             |   0：不用   注：无协议通讯专用
-----------+-------------+--------------------------------------------
b9		     |             |   0：不用   同上
-----------+-------------+--------------------------------------------
b12b11b10	 |  通讯接口	 |   000：RS485（RS422）接口
           |             |   010：        RS232C接口
-----------+-------------+--------------------------------------------
b13	       |  求和检查	 |   0：不加求和码  1：自动加上求和码
-----------+-------------+-------------------------------------------
b14	       |   协议	     |   0：无协议通讯  1：专用通讯协议
-----------+-------------+--------------------------------------------
b15	       | 协议格式	   |   0：格式1  1：格式4				 
----------------------------------------------------------------------

举例：D8120 = 0X4096           通讯波特率是19200

*********************************************************************************/
#ifndef _PLC_IO_H 
#define _PLC_IO_H 

#include "stm32f10x.h"              //0X4221018C

#ifdef __cplusplus
extern "C"  {
#endif
#ifdef __cplusplus
}
#endif

/* 传人记，20170923新增 */
#define GPIO_ON(gpio,pin)     (gpio)->BSRR = (pin)
#define GPIO_OFF(gpio,pin)    (gpio)->BRR = (pin)
#define GPIO_TOGGLE(gpio,pin) (gpio)->ODR ^= (pin)

#define BIT_ADDR(addr, bitnum)         *((volatile unsigned long  *)((0x42000000)+(addr<<5)+(bitnum<<2)))                   //
//IO口地址映射                                                                
#define GPIOA_ODR_Addr    0x01080C  
#define GPIOB_ODR_Addr    0x010C0C 
#define GPIOC_ODR_Addr    0x01100C 
#define GPIOD_ODR_Addr    0x01140C 
#define GPIOE_ODR_Addr    0x01180C 
#define GPIOF_ODR_Addr    0x011C0C 
#define GPIOG_ODR_Addr    0x01200C 
//IO模式定义
#define GPIOA_IDR_Addr    0x010808 
#define GPIOB_IDR_Addr    0x010C08 
#define GPIOC_IDR_Addr    0x011008 
#define GPIOD_IDR_Addr    0x011408 
#define GPIOE_IDR_Addr    0x011808 
#define GPIOF_IDR_Addr    0x011C08 
#define GPIOG_IDR_Addr    0x012008  
//IO口操作,只对单一的IO口!

//确保n的值小于16!
#define PAout(n)   BIT_ADDR(GPIOA_ODR_Addr,n)  //输出 
#define PAin(n)    BIT_ADDR(GPIOA_IDR_Addr,n)  //输入 

#define PBout(n)   BIT_ADDR(GPIOB_ODR_Addr,n)  //输出 
#define PBin(n)    BIT_ADDR(GPIOB_IDR_Addr,n)  //输入 

#define PCout(n)   BIT_ADDR(GPIOC_ODR_Addr,n)  //输出 
#define PCin(n)    BIT_ADDR(GPIOC_IDR_Addr,n)  //输入 

#define PDout(n)   BIT_ADDR(GPIOD_ODR_Addr,n)  //输出 
#define PDin(n)    BIT_ADDR(GPIOD_IDR_Addr,n)  //输入 

#define PEout(n)   BIT_ADDR(GPIOE_ODR_Addr,n)  //输出 
#define PEin(n)    BIT_ADDR(GPIOE_IDR_Addr,n)  //输入 

#define PFout(n)   BIT_ADDR(GPIOG_ODR_Addr,n)  //输出 
#define PFin(n)    BIT_ADDR(GPIOG_IDR_Addr,n)  //输入 

#define PGout(n)   BIT_ADDR(GPIOG_ODR_Addr,n)  //输出 
#define PGin(n)    BIT_ADDR(GPIOG_IDR_Addr,n)  //输入 

#define PLC_RUN    1//!PAin(2)         // run/stop
#define PVD        PAin(8)

#define RUN_LAMP_ON       PEout(7)=0
#define STOP_LAMP_OFF     PEout(7)=1 

// CRJ,20170921新增
#define GPIO_RUN              GPIOE
#define PIN_RUN               GPIO_Pin_7

/* 错误指示灯 */
#define ERR_LAMP_ON        PEout(8)=0
#define ERR_LAMP_OFF       PEout(8)=1

/* 背光开关 */
#define LCD_POWER_ON   PEout(9)=0   
#define LCD_POWER_OFF  PEout(9)=1

/*
X0：PC7、X1：PD15、X2:PD14、X3:PD13、X4:PD12、X5:PD11、X6：PD10、X7:PD9
X10:PD8、X11：PDB15、X12:PB14、X13:PB13、X14：PB12、X15:PB11、X16:PB10、X17:PE15
X20:PE14、X21:PE13、X22:PE12、X23:PE11、X24:PE10、X25:PE10、X26:PE10、X27:PE10
X30:PE10、X31:PE10、X32:PE10、X33:PE10
*/

//输入X定义
#define X00 PCin(6)	
#define X01 PDin(15)
#define X02 PDin(14)
#define X03 PDin(13)
#define X04 PDin(12)		 
#define X05 PDin(11)
#define X06 PDin(10)
#define X07 PDin(9)

#define X10 PDin(8)		
#define X11 PBin(15)
#define X12 PBin(14)
#define X13 PBin(13)
#define X14 PBin(12)	
#define X15 PBin(11)
#define X16 PBin(10)
#define X17 PEin(15)

#define X20 PEin(14) 		  
#define X21 PEin(13)	  
#define X22 PEin(12)

#define X23 PEin(11)
#define X24 PEin(10)	
#define X25 PEin(10)	 	
#define X26 PEin(10)	
#define X27 PEin(10)	

#define X30 PEin(10) 
#define X31 PEin(10)	 
#define X32 PEin(10) 
#define X33 PEin(10)	 
#define X34 PEin(10)	

#if ADC_FUNC ==0
#define X35  PCin(0)		 	 
#define X36  PCin(1)		 
#endif

#if DAC_FUNC ==0
#define X37  PAin(4)	
#endif


/* 30输出
Y0:PC9、Y1:PA11、Y2:PC8、Y3:PC7 、Y4:PA12 、Y5:PA15、Y6：PC12、Y7：PD0
Y10:PD1、Y11:PD2、Y12:PD3、Y13:PD4、Y14:PD5、Y15:PD6、Y16:PD7、Y17：PB3
Y20:PB4、Y21:PB5、Y22:PB6、Y23:PB7、Y24:PE0、Y25:PE1、Y26:PE2、Y27：PE3
Y30:PE4、Y31:PE5、Y32:PE6、Y33:PC13、Y34:PC13、Y35:PC13
*/
//输出Y定义
#define Y00 PCout(9)  
#define Y01 PAout(11)  
#define Y02 PCout(8)
#define Y03 PCout(7)
#define Y04 PAout(12)   
#define Y05 PAout(15)
#define Y06 PCout(12) 
#define Y07 PDout(0)

#define Y10 PDout(1) 
#define Y11 PDout(2)
#define Y12 PDout(3)
#define Y13 PDout(4)
#define Y14 PDout(5)
#define Y15 PDout(6)
#define Y16 PDout(7)
#define Y17 PBout(3)

#define Y20 PBout(4)
#define Y21 PBout(5)
#define Y22 PBout(6)
#define Y23 PBout(7)
#define Y24 PEout(0)
#define Y25 PEout(1)
#define Y26 PEout(2)
#define Y27 PEout(3)

#define Y30 PEout(4)    
#define Y31 PEout(5)
#define Y32 PEout(6)
#define Y33 PCout(13)
#define Y34 PCout(13)    
#define Y35 PCout(13)
#define Y36 PCout(13)
#define Y37 PCout(13)


/* 传人记，20170601新增 */
extern uint8_t powerDownFlg;  // 断电检测标志
extern volatile uint8_t powerDownCnt;  // 掉电保存计时

#endif

